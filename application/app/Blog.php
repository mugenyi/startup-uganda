<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $with = ['postedBy'];
    protected $fillable = ['title','slug','image','content','summary','meta_tags','author_name','author_email','posted_by','status','view_count'];

    public function postedBy(){
        return $this->belongsTo(User::class,'posted_by');
    }
}
