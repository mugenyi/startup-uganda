<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Musonza\Chat\Traits\Messageable;

class Corporate extends Model
{
    //
    use Messageable;
    protected $with = ['user'];

    protected $fillable = ['name','tagline','about','address','phone_number','email','facebook','twitter','linkedIn','youtube','website','logo','image','banner','user_id','status','industries','sectors'];


    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function jobs()
    {
        return $this->hasMany(Job::class,'company')->where('company_type','corporate');
    }
}
