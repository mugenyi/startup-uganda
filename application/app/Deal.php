<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deal extends Model
{
    protected $fillable =['name','category','brief','roles','skills','application_guidelines','attachment','deadline','status','posted_by'];

    public function user()
    {
        return $this->belongsTo('App\User','posted_by');
    }
}
