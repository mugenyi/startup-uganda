<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    protected $fillable = ['event_name','start_date','end_date','apply_before','organiser','location','target_audience','summary','description','added_by','status','image','cover','link'];
}
