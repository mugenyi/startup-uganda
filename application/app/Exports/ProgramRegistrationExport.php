<?php

namespace App\Exports;

use App\ProgramRegistration;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProgramRegistrationExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($program)
    {
        $this->program = $program;
    }
    public function collection()
    {
        return ProgramRegistration::select('name','email','phone','sector','company','company_registration','team_members',
            'operation_years','investment_size','affliation')->with('programDetail')->where('program',$this->program)->get();
    }
    public function headings(): array
    {
        return ['NAME','EMAIL','PHONE','SECTOR','COMPANY','COMPANY_REGISTRATION','TEAM_MEMBERS',
            'OPERATION_YEARS','INVESTMENT_SIZE','AFFLIATION'];
    }
}
