<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    //
    protected $with = ['user'];
    protected $fillable = ['user_id','url','thought','post_type','post_id','status','image','title'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
