<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Musonza\Chat\Traits\Messageable;

class Government extends Model
{
    use Messageable;
    //
    protected $with = ['user'];

    protected $fillable = ['name','tagline','department','about','address','phone_number','email','facebook','twitter','linkedIn','youtube','logo','website','image','cover','user_id','status','sectors'];

    public function jobs()
    {
        return $this->hasMany(Job::class,'company')->where('company_type','government');
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
