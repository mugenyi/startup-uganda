<?php

namespace App\Http\Controllers;

use App\Corporate;
use App\Deal;
use App\Eso;
use App\Event;
use App\Government;
use App\Investor;
use App\Job;
use App\Mail\ClaimEmail;
use App\Mentor;
use App\Post;
use App\Program;
use App\Startup;
use App\Team;
use App\Traits\HelperTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    //
    public function dashboard()
    {
        $summary = [];
        $summary['startups'] = Startup::count();
        $summary['investors'] = Investor::count();
        $summary['governments'] = Government::count();
        $summary['mentors'] = Mentor::count();
        $summary['esos'] = Eso::count();
        $summary['programs'] = Program::count();
        $summary['events'] = Event::count();
        $summary['posts'] = Post::count();
        $summary['deals'] = Deal::count();
        $summary['users'] = User::where(['role'=>'user'])->count();

        return view('admin.dashboard',['data'=>$summary]);
    }

    /***************************************
     * CLAIMS
     * ************************************/
    public function addAdminEntity($category)
    {
        switch($category){
            case 'startup':
                return view('admin.new-startup');
            break;
            case 'investor':
                return view('admin.new-investor');
            break;
            case 'eso':
                return view('admin.new-eso');
            break;
            case 'mentor':
                return view('admin.new-mentor');
            break;
            case 'government':
                return view('admin.new-government');
            break;
            case 'corporation':
                return view('admin.new-corporation');
            break;
        }

    }

    public function sendClaimEmail(Request $request)
    {
        switch($request['category']){
            case 'startup':
                $sub = Startup::find($request['id']);
            break;
            case 'investor':
                $sub = Investor::find($request['id']);
            break;
            case 'mentor':
                $sub = Mentor::find($request['id']);
            break;
            case 'eso':
                $sub = Eso::find($request['id']);
            break;
            case 'government':
                $sub = Government::find($request['id']);
            break;
            case 'corporation':
                $sub = Corporate::find($request['id']);
            break;
        }

        $cate = $request['category'].'_'.$request['id'];
        $endPoint =  HelperTrait::cyper($cate,'encrypt');

        //Send loan email to user
        Mail::to($request['email'])
        ->send(new ClaimEmail($sub,$endPoint));
        flash('Email has been successfully sent to '.$request['email'])->success();
        return redirect()->back();

    }

    public function getMentors()
    {
        $mentors = Mentor::orderBy('id','desc')->paginate(9);
        return view('admin.mentors',['mentors'=>$mentors]);

    }

    public function viewMentor($id)
    {
        $mentor = Mentor::find($id);
        return view('admin.view-mentor',['mentor'=>$mentor]);
    }

    public function updateMentorStatus($id,$status)
    {
        $mentor = Mentor::find($id)->update(['status'=>$status]);
        return redirect()->back();
    }




    /**************** STARTUPS ******************/

    public function getStartups()
    {
        $startups = Startup::orderBy('id','desc')->paginate(9);
        return view('admin.startups',['startups'=>$startups]);
    }

    public function viewStartup($id)
    {
        $startup = Startup::find($id);
        $teams = Team::where(['category'=>'startup','category_id'=>$startup->id])->get();
        return view('admin.view-startup',['startup'=>$startup,'teams'=>$teams]);
    }

    public function updateStartupStatus($id,$status)
    {
        $startup = Startup::find($id)->update(['status'=>$status]);
        return redirect()->back();
    }


    public function featureStartUo($startupId,$feature){
        $status = $feature=='true'? true:false;
        $startup = Startup::find($startupId)->update(['is_featured'=>$status]);

        $in = $feature=='true'? 'featured':'un-featured';
        flash('Startup has been successfully '.$in )->success();
        return redirect()->back();
    }

    /************ INVESTOR ***********/
    public function getInvestors()
    {
        $investors = Investor::orderBy('id','desc')->paginate(9);
        return view('admin.investors',['investors'=>$investors]);
    }

    public function viewInvestor($id)
    {
        $investor = Investor::find($id);
        $teams = Team::where(['category'=>'investor','category_id'=>$investor->id])->get();
        return view('admin.view-investor',['investor'=>$investor,'teams'=>$teams]);
    }

    public function updateInvestorStatus($id,$status)
    {
        $investor = Investor::find($id)->update(['status'=>$status]);
        return redirect()->back();
    }

    /************ ESOs ***********/
    public function getEsos()
    {
        $esos = Eso::orderBy('id','desc')->paginate(9);
        return view('admin.esos',['esos'=>$esos]);
    }

    public function viewEso($id)
    {
        $eso = Eso::find($id);
        return view('admin.view-eso',['eso'=>$eso]);
    }

    public function updateEsoStatus($id,$status)
    {
        $investor = Eso::find($id)->update(['status'=>$status]);
        return redirect()->back();
    }


    /************ CORPORATIONS ***********/
    public function getCorporations()
    {
        $corporations = Corporate::orderBy('id','desc')->paginate(9);
        return view('admin.corporates',['corporates'=>$corporations]);
    }

    public function viewCorporation($id)
    {
        $corporation = Corporate::find($id);
        return view('admin.view-corporate',['corporate'=>$corporation]);
    }

    public function updateCorporationStatus($id,$status)
    {
        $investor = Corporate::find($id)->update(['status'=>$status]);
        return redirect()->back();
    }

    /************ Government ***********/
    public function getGovernments()
    {
        $governments = Government::orderBy('id','desc')->paginate(9);
        return view('admin.governments',['governments'=>$governments]);
    }

    public function viewGovernment($id)
    {
        $government = Government::find($id);

        $teams = Team::where(['category'=>'government','category_id'=>$government->id])->get();
        return view('admin.view-government',['government'=>$government,'teams'=>$teams]);
    }

    public function updateGovernmentStatus($id,$status)
    {
        $government = Government::find($id)->update(['status'=>$status]);
        return redirect()->back();
    }


    /********* Proggrams ********/
    public function getPrograms()
    {
        $programs = Program::orderBy('id','desc')->paginate();
        foreach($programs as $program){
            $program['starts'] = date("d/m/Y", strtotime($program->start_date));
            $program['ends'] = date("d/m/Y", strtotime($program->end_date));
        }
        return view('admin.programs',['programs'=>$programs]);
    }

    public function viewProgram($id)
    {
        $program = Program::find($id);
        return view('admin.view-program',['program'=>$program]);
    }

    public function updateProgramStatus($id,$status)
    {
        $program = Program::find($id)->update(['status'=>$status]);
        return redirect()->back();
    }

    /********* Events ********/
    public function getEvents()
    {
        $events = Event::orderBy('id','desc')->paginate();
        foreach($events as $event){
            $event['starts'] = date("d/m/Y", strtotime($event->start_date));
            $event['ends'] = date("d/m/Y", strtotime($event->end_date));
        }
        return view('admin.events',['events'=>$events]);
    }

    public function viewEvent($id)
    {
        $event = Event::find($id);
        return view('admin.view-event',['event'=>$event]);
    }


    /********* USERS ************/
    public function getUsers()
    {
        $users = User::where(['role'=>'user'])->paginate();
        return view('admin.users',['users'=>$users]);
    }

    public function userDetails($id)
    {
        $data = [];
        $user = User::find($id);
        $data['startups'] = Startup::where(['user_id'=>$id])->get();
        $data['investors'] = Investor::where(['user_id'=>$id])->get();
        $data['governments'] = Government::where(['user_id'=>$id])->get();

        return view('admin.user',['user'=>$user,'data'=>$data]);
    }


    /********* JOBS *********/
    public function getJobs(){
        $jobs = Job::orderBy('id','desc')->paginate();
        return view('admin.jobs',['jobs'=>$jobs]);
    }

    public function createJob(Request $request)
    {
        //validate
        $validatedData = Validator::make(request()->all(), [
            'job_title' => 'required|string',
            'company_type' => 'required',
            //'company' => 'required',
            'location' => 'required',
            'job_type' => 'required',
            'description' => 'required',
            'ends_at' => 'required',
        ]);
        if ($validatedData->fails()) {
            $error = "";
            foreach ($validatedData->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();
        }else{
            $data = $request->all();

            $company = explode('_',$data['company_type']);
            //dd($company);
            $data['company'] = $company[0];
            $data['company_type'] = $company[1];
            $data['status'] = "Pending";
            $data['description'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['description']);
            $data['added_by'] = Auth::user()->id;

            Job::create($data);
            flash('Job has been successfully registered. To publish, activate the job')->success();
        }
        return redirect()->back();
    }
}
