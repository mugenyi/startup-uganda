<?php

namespace App\Http\Controllers\Auth;

use App\Corporate;
use App\Eso;
use App\Government;
use App\Http\Controllers\Controller;
use App\Investor;
use App\Mentor;
use App\Providers\RouteServiceProvider;
use App\Startup;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->redirectTo = url()->previous();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validator->fails()) {
            foreach ($validator->messages()->getMessages() as $field_name => $messages)
            {
                return redirect()->back()->withErrors(['error'=>$messages[0]]);
            }
        }
        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'image' =>env('USER_PROFILE'),
            'role' => 'user',
        ]);
        if(isset($data['category_id'])){
            $category = $data['category'];
            $id = $data['category_id'];

            switch ($category) {
                case 'startup':
                  $bus = Startup::find($id)->update(['user_id'=>$user['id']]);
                  break;
                  case 'investor':
                    $sub = Investor::find($id)->update(['user_id'=>$user['id']]);
                break;
                case 'mentor':
                    $sub = Mentor::find($id)->update(['user_id'=>$user['id']]);
                break;
                case 'eso':
                    $sub = Eso::find($id)->update(['user_id'=>$user['id']]);
                break;
                case 'government':
                    $sub = Government::find($id)->update(['user_id'=>$user['id']]);
                break;
                case 'corporation':
                    $sub = Corporate::find($id)->update(['user_id'=>$user['id']]);
                break;

              }
              //return redirect()->route('user.dashboard');
        }
            return $user;


    }
}
