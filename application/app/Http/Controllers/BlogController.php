<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Str;
use Artesaos\SEOTools\Facades\SEOTools;
// import the Intervention Image Manager Class
use Image;
use Auth;

class BlogController extends Controller
{

    public function getBlogs(){
        $blogs = Blog::where(['status'=>'Published'])->orderBy('id','desc')->paginate(9);
        SEOTools::setTitle('Blogs');
        SEOTools::setDescription('Startup Uganda (SU) is an association of innovation and entrepreneurship support organizations (IESO) working towards strengthening the startup support ecosystem and sector.s');
        SEOTools::opengraph()->setUrl(route('blogs'));
        SEOTools::setCanonical(route('blogs'));
        SEOTools::opengraph()->addProperty('type', 'article');
        SEOTools::opengraph()->addProperty('image', 'https://cdn.lifehack.org/wp-content/uploads/2013/07/best-blogs-1024x768.jpeg');
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage('https://cdn.lifehack.org/wp-content/uploads/2013/07/best-blogs-1024x768.jpeg');
        return view('blogs',['blogs'=>$blogs]);
    }

    public function viewBlog($slug){
        $blog = Blog::where('slug',$slug)->first();
        if(is_null($blog)){
            flash("Invalid data, please try again")->warning();
            return redirect()->back();
        }else{
            SEOTools::setTitle($blog['title']);
            SEOTools::setDescription($blog['summary']);
            SEOTools::opengraph()->setUrl(route('blog.view',['slug'=>$blog['slug']]));
            SEOTools::setCanonical(route('blogs'));
            SEOTools::opengraph()->addProperty('type', 'article');
            SEOTools::opengraph()->addProperty('image', secure_asset(env('COVER').''.$blog['image']));
            SEOTools::twitter()->setSite('@StartupUganda_');
            SEOTools::jsonLd()->addImage(secure_asset(env('COVER').''.$blog['image']));
            return view('blog-view',['blog'=>$blog]);
        }
    }

    public function getAllBlogs(){
        $blogs = Blog::orderBy('id','desc')->paginate(9);
        return view('admin.blogs',['blogs'=>$blogs]);
    }


    public function create(){
        return view('admin.blog-create');
    }
    public function previewBlog($slug){
        $blog = Blog::where('slug',$slug)->first();
        if(is_null($blog)){
            flash("Invalid data, please try again")->warning();
            return redirect()->back();
        }else
        return view('admin.blog-preview',['blog'=>$blog]);
    }

    public function createBlog(Request $request){

        $validated = Validator::make(request()->all(), [
            'title'=> 'required|string|unique:blogs,title',
            'content'=> 'required',
            'summary'=> 'required',
            'author_name'=> 'required','author_email'=> 'required',
            'meta_tags'=> 'required',
            'feature_image'=> 'required|mimes:jpg,jpeg,png',
        ]);
        if ($validated->fails()) {
             //dd($validated->messages());
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .='Error!! '.$messages[0];
            }
            flash($error)->warning();
            return redirect()->back();
        }else{
            $data = $request->all();
            $data['slug'] = Str::slug($data['title']);
            $data['content'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['content']);
            $data['summary'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['summary']);
            $data['posted_by'] = Auth::user()->id;
            $data['status'] = 'Pending Review';
            $data['view_count'] = 0;
            //Upload Image
            $image       = $request->file('feature_image');
            $newName    = Str::random(12);
            $extension = $image->guessExtension();
            $filename = $newName.'.'.$extension;

            $file = $image->move(env('FULL'),$filename);
            $image_resize = Image::make($file);
            $image_resize->fit(350, 350);
            $image_resize->save(env('COVER').$filename);

            $data['image'] = $filename;


            Blog::create($data);
            flash('Blog has been successful registered. Please preview and publish if everything is ok')->success();
            return redirect()->back();
        }
       // 'title','slug','image','content','summary','meta_tags','author_name','author_email','posted_by','status','view_count'
    }

    public function editBlog($id)
    {
        $blog = Blog::find($id);
        return view('admin.blog-edit',['blog'=>$blog]);
    }
    public function updateBlog(Request $request){

        $validated = Validator::make(request()->all(), [
            'title'=> 'required|string',
            'content'=> 'required',
            'summary'=> 'required',
            'author_name'=> 'required','author_email'=> 'required',
            'meta_tags'=> 'required',
            'feature_image'=> 'required|mimes:jpg,jpeg,png',
            'id'=>'required|integer'
        ]);
        if ($validated->fails()) {
             //dd($validated->messages());
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .='Error!! '.$messages[0];
            }
            flash($error)->warning();
            return redirect()->back();
        }else{
            $data = $request->all();
            $data['slug'] = Str::slug($data['title']);
            $data['content'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['content']);
            $data['summary'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['summary']);
            $data['posted_by'] = Auth::user()->id;
            $data['status'] = 'Pending Review';
            $data['view_count'] = 0;
            //Upload Image
            $image       = $request->file('feature_image');
            $newName    = Str::random(12);
            $extension = $image->guessExtension();
            $filename = $newName.'.'.$extension;

            $file = $image->move(env('FULL'),$filename);
            $image_resize = Image::make($file);
            $image_resize->fit(350, 350);
            $image_resize->save(env('COVER').$filename);

            $data['image'] = $filename;


            Blog::find($data['id'])->update($data);
            flash('Blog has been successful updated. Please preview and publish if everything is ok')->success();
            return redirect()->back();
        }
       // 'title','slug','image','content','summary','meta_tags','author_name','author_email','posted_by','status','view_count'
    }

    public function actionBlog($id,$action){
        $blog = Blog::find($id);
        if (is_null($blog)) {
            flash('Error!! Invalid data provided')->warning();
            return redirect()->route('admin.blogs');
        }else{
            $state = $action=='deactivate'?"Deactivated":"Published";
            $blog->status = $state;
            $blog->save();

            flash('The blog has been successfully '.$state)->success();
            return redirect()->route('admin.blogs');
        }

    }
}
