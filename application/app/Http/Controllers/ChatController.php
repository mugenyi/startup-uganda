<?php

namespace App\Http\Controllers;

use App\Corporate;
use App\Eso;
use App\Government;
use App\Investor;
use App\Mentor;
use App\Startup;
use App\Traits\HelperTrait;
use App\User;
use Chat;
use Auth;

use Illuminate\Http\Request;

class ChatController extends Controller
{
    //

    public function initiateChat($sender,$receiver,$type)
    {
        $initiator = User::find($sender);
        if($type=='mentor')
            $receiver = Mentor::find($receiver);
        elseif($type=='startup')
            $receiver = Startup::find($receiver);
        elseif($type=='corporate')
            $receiver = Corporate::find($receiver);
        elseif($type=='eso')
            $receiver = Eso::find($receiver);
        elseif($type=='government')
            $receiver = Government::find($receiver);
        elseif($type=='investor')
            $receiver = Investor::find($receiver);
        else
            $receiver = User::find($receiver);

        $participants = [$initiator,$receiver];

        $getConversation = Chat::conversations()->between($initiator,$receiver);

        if(is_null($getConversation)){
            flash('Your chat with '.$receiver->name.' has been successfully initiated.')->success();
            $conversation = Chat::createConversation($participants)->makeDirect();
        }else{
            flash('You already have a chat with'.$receiver->name.'. Please go ahead and chat with them.')->warning();
        }
        return redirect()->route('user.chat');
    }

    public function getUserChats()
    {
        $participants = [];
        $user = User::find(Auth::user()->id);
        $conversations = Chat::conversations()->setPaginationParams(['sorting' => 'desc'])
        ->setParticipant($user)
        ->get();


        return view('user.chat',['conversations'=>$conversations]);

    }

    public function getCategoryChats($category,$id)
    {
        $user = HelperTrait:: getCategoryModel($category,$id);
        $conversations = Chat::conversations()->setPaginationParams(['sorting' => 'desc'])
        ->setParticipant($user)
        ->get();


        return view('user.chat',['conversations'=>$conversations,'category'=>$category,'id'=>$id,'model'=>$user]);
    }


    public function sendMessage(Request $request)
    {
        if(isset($request['category'])){
            $category = $request['category'];
            $id = $request['id'];
            $model = HelperTrait:: getCategoryModel($category,$id);
        }else{
            $model = User::find(Auth::user()->id);
        }

        $conversation = Chat::conversations()->getById($request['conversation_id']);
        $message = Chat::message($request['message'])
            ->from($model)
            ->to($conversation)
            ->send();

            return redirect()->back();
    }

}
