<?php

namespace App\Http\Controllers;

use App\Corporate;
use App\Traits\HelperTrait;
use Validator;
use Image;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOTools;

class CorporateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $corporates = Corporate::where(['user_id'=>Auth::user()->id])->orderBy('id','desc')->paginate(9);
        return view('user.corporates',['corporates'=>$corporates]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = Validator::make(request()->all(), [
            'name'=> 'required|string',
            'tagline'=> 'required|string',
            'about'=> 'required',
            'address'=> 'required',
            'phone_number'=> 'required',
            'email'=> 'required',

        ]);
        if ($validated->fails()) {
            dd($validated->messages());
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();

        }else{
            $data = $request->all();
            $data['about'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['about']);
            $data['image'] = env('BUSINESS_PROFILE');
            $data['cover'] = env('BUSINESS_BANNER');
            $data['logo'] = env('BUSINESS_LOGO');
            $data['user_id'] = Auth::user()->id;
            $data['status'] = 'pending';
            //dd($data);
            $data['about'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['about']);

            $corporate = Corporate::create($data);

            flash($corporate->name.' has been successsfully registered');
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Corporate  $corporate
     * @return \Illuminate\Http\Response
     */
    public function show($id,$slug)
    {
        $corporate = Corporate::findorfail($id);
        return view('user.view-corporate',['corporate'=>$corporate]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Corporate  $corporate
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $corporate = Corporate::find($id);
        return view('user.edit-corporate',['corporate'=>$corporate]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Corporate  $corporate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validated = Validator::make(request()->all(), [
            'name'=> 'required|string',
            'tagline'=> 'required|string',
            'about'=> 'required',
            'address'=> 'required',
            'phone_number'=> 'required',
            'email'=> 'required',

        ]);
        if ($validated->fails()) {
            //dd($validated->messages());
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();

        }else{

            $data = $request->all();
            $data['about'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['about']);
            $data['description'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['description']);
            $corp = Corporate::find($data['id'])->update($data);

            //flash($corp['name'].' has been successsfully updated');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Corporate  $corporate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Corporate $corporate)
    {
        //
    }

    public function bannerUpload(Request $request){
        if(isset($request['image'])) {
            $data = $request['image'];

            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $name = Str::random(12) . '.png';
            $full_image = env('FULL') . $name;
            file_put_contents($full_image, $data);

            $image_cover = Image::make($data);
            $image_cover->fit(300,230);
            $image_cover->save(env('COVER').$name);

            $banner = env('BANNER') . $name;
            file_put_contents($banner, $data);

            $startup = Corporate::find($request['id'])->update(['image' => $name, 'banner' => $name]);
            flash('Cover has been successfully updated');
            return redirect()->back();
        }else{
            flash('You didnot upload a file')->danger();
        }
        return redirect()->back();
    }
    public function uploadCover(Request $request)
    {
        $validatedData = Validator::make( $request->all(), [
            'upload_cover' => 'required|mimes:jpeg,png,jpg,gif,svg',
            'id' => 'required'
        ]);
        if ($validatedData->fails()) {
            foreach ($validatedData->messages()->getMessages() as $field_name => $messages)
            {
                flash('Error: '.$messages[0])->warning();
            }
        }else{
            if($request->hasFile('upload_cover')){
                $image       = $request->file('upload_cover');
                $newName    = Str::random(12);
                $extension = $image->guessExtension();
                $filename = $newName.'.'.$extension;
                // $width = Image::make('public/foo.jpg')->width();
                // $height = Image::make('public/foo.jpg')->height();

                $file = $image->move('public/uploads/full/',$filename);
                $width = Image::make('public/uploads/full/'.$filename)->width();
                $height = Image::make('public/uploads/full/'.$filename)->height();
                if($width < 860 || $height < 400){
                    flash('Error: Uploaded file it too small. Please consider 860px width and 400px height')->warning();
                }
                else{
                    $image_resize = Image::make($file);
                    $image_resize->fit(800,360);
                    $image_resize->save('public/uploads/banner/'.$filename);

                    $image_cover = Image::make($file);
                    $image_cover->fit(250,250);
                    $image_cover->save('public/uploads/cover/'.$filename);

                    $corporate = Corporate::find($request['id'])->update(['image'=>$filename,'banner'=>$filename]);
                    flash('Cover has been successfully updated');
                }
            }else{
                flash('You didnot upload a file')->danger();
            }
        }
        return redirect()->back();
    }

    public function uploadLogo(Request $request)
    {
        if($request->hasFile('upload_logo')){
            $image = $request->file('upload_logo');
            $newName    = Str::random(12);
            $extension = $image->guessExtension();
            $filename = $newName.'.'.$extension;

            $file = $image->move('public/uploads/full/',$filename);
            $image_resize = Image::make($file);
            $image_resize->fit(100, 100);
            $image_resize->save('public/uploads/logo/'.$filename);

            $corporate = Corporate::find($request['id'])->update(['logo'=>$filename]);

            flash(' Logo successfully updated')->success();

        }else{
            flash('You didnot upload a file')->danger();
        }

            return redirect()->back();

    }

    public function getCorporates()
    {
        $corporates = Corporate::where(['status'=>'Published'])->orderBy('id','desc')->paginate();

        SEOTools::setTitle('Corporations');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('corporations'));
        SEOTools::setCanonical(route('corporations'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));

        return view('corporates-list',['corporates'=>$corporates]);
    }

    public function viewCorporate($id,$slug)
    {
        $corporate = Corporate::with('jobs')->find($id);

        SEOTools::setTitle($corporate['name'].' - '.env('APP_NAME'));
        SEOTools::setDescription($corporate['tagline']);
        SEOTools::opengraph()->setUrl(route('view.corporation',['id'=>$corporate['id'],'slug'=>HelperTrait::slugify($corporate['name'])]));
        SEOTools::setCanonical(route('corporations'));
        SEOTools::opengraph()->addProperty('type', 'webPage');
        SEOTools::opengraph()->addProperty('image', secure_asset(env('BANNER').''.$corporate['cover']));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset(env('BANNER').''.$corporate['cover']));

        return view('corporate-view',['corporate'=>$corporate]);
    }

    public function editSectors(Request $request)
    {
        $corporate = Corporate::find($request['corporate_id']);

        $validated = Validator::make(request()->all(), [
            'corporate_id'=> 'required',
            'industries'=> 'required',
            'sectors'=> 'required'
        ]);
        if ($validated->fails()) {
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();
        }else{
            $data = $request->all();
            $industries="";
            foreach($data['industries'] as $a){
                $industries .=$a.', ';
            }
            $sectors="";
            foreach($data['sectors'] as $d){
                $sectors .=$d.', ';
            }

            $corporate->update(['sectors'=>$sectors,'industries'=>$industries]);
            flash('Corporation industries and sectors have been successfully updated')->success();

        }
        return redirect()->back();
    }


    public function searchCorporations(Request $request)
    {

        $where = [];
        $name = $request['name'];
        $district = $request['district'];
        $industry = $request['industry'];
        $sector = $request['sector'];

        // $where['status']= 'Published';

        if(!is_null($industry)){
            if(!is_null($sector)){
                $corporates = Corporate::where($where)->where('sectors', 'like', '%'.$sector.'%')
                    ->where('industries', 'like', '%'.$industry.'%')->where('name', 'like', '%'.$name.'%')->paginate();
            }else{
                $corporates = Corporate::where($where)->where('industries', 'like', '%'.$industry.'%')
                    ->where('name', 'like', '%'.$name.'%')->paginate();
            }
        }else{
            if(!is_null($sector)){
                $corporates = Corporate::where($where)->where('sectors', 'like', '%'.$sector.'%')
                    ->where('name', 'like', '%'.$name.'%')->paginate();
            }else{
                $corporates = Corporate::where($where)->where('sectors', 'like', '%'.$sector.'%')
                    ->where('name', 'like', '%'.$name.'%')->paginate();
            }
        }

        SEOTools::setTitle('Corporations - Startup Uganda');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('corporations'));
        SEOTools::setCanonical(route('corporations'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));

        return view('corporates-list',['corporates'=>$corporates]);
    }
}
