<?php

namespace App\Http\Controllers;

use App\Deal;
use App\Eso;
use App\Program;
use App\Traits\HelperTrait;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class DealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getDeals = Deal::with('user')->orderBy('id','desc')->paginate();
        return view('admin.deals',['deals'=>$getDeals]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $validatedData = Validator::make(request()->all(), [
            'name'=> 'required|string',
            'category' => 'required',
            'brief' => 'required',
            'roles' => 'required',
            'skills' => 'required',
            'application_guidelines' => 'required',
            'deadline' => 'required',
        ]);
        if ($validatedData->fails()) {
            $error = "";
            foreach ($validatedData->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();
            return redirect()->back();
        }else{
            $data = $request->all();

            $data['deadline']=date('Y-m-d H:i:s', strtotime($data['deadline']));
            $data['status'] = "Pending";
            $data['posted_by'] = Auth::user()->id;
            $data['brief'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['brief']);
            $data['roles'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['roles']);
            $data['skills'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['skills']);
            $data['application_guidelines'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['application_guidelines']);

            if(isset($request['attachment'])) {
                $file = $request->file('attachment');
                $destinationPath = env('FULL');
                $originalFile = $file->getClientOriginalName();
                $filename=strtotime(date('Y-m-d-H:isa')).$originalFile;
                $file->move($destinationPath, $filename);
                $data['attachment'] = $filename;

            }else{
                flash('You didnot upload a file')->danger();
            }
            Deal::create($data);
            flash('Offer has been successfully registered. Please add image and cover photo then publish, activate the program')->success();
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Deal  $deal
     * @return \Illuminate\Http\Response
     */
    public function show($deal)
    {
        $deal = Deal::find($deal);
        if (is_null($deal)) {
            flash('Unknown deal was searched for')->danger();
            return redirect()->back();
        }
        return view('admin.view-deal',['deal'=>$deal]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Deal  $deal
     * @return \Illuminate\Http\Response
     */
    public function edit(Deal $deal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Deal  $deal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Deal $deal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Deal  $deal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Deal $deal)
    {
        //
    }

    public function changeState($id,$state)
    {
        $deal = Deal::find($id)->update(['status'=>$state]);
        flash('Offer status has been changed to '.$state)->success();
        return redirect()->route('admin.deals');
    }


    /*************Front End**************/
    public function getStartUpOffers(){
        $getDeals = Deal::where('status','Active')
            ->whereDate('deadline','>',now())
            ->orderBy('id','desc')->paginate(10);
        return view('deals',['deals'=>$getDeals]);
    }

    public function viewStartUpOffer($id,$slug){
        $deal = Deal::find($id);
        SEOTools::setTitle($deal->name);
        SEOTools::setDescription(strlen($deal->brief) > 200 ? substr($deal->brief,0,200)."..." : $deal->brief);
        SEOTools::opengraph()->setUrl(route('deal.view',['id'=>$deal->id,'slug'=>HelperTrait::slugify($deal->name)]));
        SEOTools::setCanonical(route('deals'));
        SEOTools::opengraph()->addProperty('type', 'webpage');
        SEOTools::twitter()->setSite('@StartupUganda_');
        if ($deal->category == "Job"){
            SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/job.png'));
            SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/job.png'));
        }elseif($deal->category == "Bid"){
            SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/bid.png'));
            SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/bid.png'));
        }else{
            SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/other.png'));
            SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/other.png'));
        }

        //Other Deals
        $deals = Deal::where('id','!=',$id)->where('status','Active')
            ->whereDate('deadline','>',now())->orderBy('id','desc')->limit(4)->get();
        return view('view-deal',['deal'=>$deal,'deals'=>$deals]);
    }

}
