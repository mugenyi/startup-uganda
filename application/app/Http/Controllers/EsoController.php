<?php

namespace App\Http\Controllers;

use App\Eso;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Validator;
use Image;
use Auth;
use Illuminate\Support\Str;
use Artesaos\SEOTools\Facades\SEOTools;

class EsoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $esos = Eso::where(['user_id'=>Auth::user()->id])->orderBy('id','desc')->paginate(9);
        return view('user.esos',['esos'=>$esos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validated = Validator::make(request()->all(), [
            'name'=> 'required|string',
            'tagline'=> 'required|string',
            'about'=> 'required',
            'address'=> 'required',
            'phone_number'=> 'required',
            'email'=> 'required',

        ]);
        if ($validated->fails()) {
            dd($validated->messages());
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();

        }else{
            $data = $request->all();

            $data['image'] = env('BUSINESS_PROFILE');
            $data['banner'] = env('BUSINESS_BANNER');
            $data['logo'] = env('BUSINESS_LOGO');
            $data['user_id'] = Auth::user()->id;
            $data['status'] = 'pending';
            //dd($data);
            $data['about'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['about']);

            $eso = Eso::create($data);

            flash($eso->name.' has been successsfully registered');
            if(Auth::user()->role =='admin')
                return redirect()->route('admin.eso.view',['id'=>$eso['id']]);
            else{
                return redirect()->back();
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Eso  $eso
     * @return \Illuminate\Http\Response
     */
    public function show($id,$slug)
    {
        $eso = Eso::findorfail($id);
        return view('user.view-eso',['eso'=>$eso]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Eso  $eso
     * @return \Illuminate\Http\Response
     */
    public function edit(Eso $eso)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Eso  $eso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validated = Validator::make(request()->all(), [
            'name'=> 'required|string',
            'tagline'=> 'required|string',
            'about'=> 'required',
            'address'=> 'required',
            'phone_number'=> 'required',
            'email'=> 'required',

        ]);
        if ($validated->fails()) {
           // dd($validated->messages());
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();

        }else{

            $data = $request->all();
            $data['about'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['about']);

            $corp = Eso::find($data['id'])->update($data);

            //flash($corp['name'].' has been successsfully updated');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Eso  $eso
     * @return \Illuminate\Http\Response
     */
    public function destroy(Eso $eso)
    {
        //
    }

    public function bannerUpload(Request $request){
        if(isset($request['image'])) {
            $data = $request['image'];

            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $name = Str::random(12) . '.png';
            $full_image = env('FULL') . $name;
            file_put_contents($full_image, $data);

            $image_cover = Image::make($data);
            $image_cover->fit(300,230);
            $image_cover->save(env('COVER').$name);

            $banner = env('BANNER') . $name;
            file_put_contents($banner, $data);

            $startup = Eso::find($request['id'])->update(['banner' => $name, 'image' => $name]);
            flash('Cover has been successfully updated');
            return redirect()->back();
        }else{
            flash('You didnot upload a file')->danger();
        }
        return redirect()->back();
    }


    public function uploadCover(Request $request)
    {
        $validatedData = Validator::make( $request->all(), [
            'upload_cover' => 'required|mimes:jpeg,png,jpg,gif,svg',
            'id' => 'required'
        ]);
         if ($validatedData->fails()) {
            foreach ($validatedData->messages()->getMessages() as $field_name => $messages)
            {
                flash('Error: '.$messages[0])->warning();
            }
        }else{
            if($request->hasFile('upload_cover')){
                $image       = $request->file('upload_cover');
                $newName    = Str::random(12);
                $extension = $image->guessExtension();
                $filename = $newName.'.'.$extension;
                // $width = Image::make('public/foo.jpg')->width();
                // $height = Image::make('public/foo.jpg')->height();

                $file = $image->move('public/uploads/full/',$filename);
                $width = Image::make('public/uploads/full/'.$filename)->width();
                $height = Image::make('public/uploads/full/'.$filename)->height();
                if($width < 860 || $height < 400){
                    flash('Error: Uploaded file it too small. Please consider 860px width and 400px height')->warning();
                }
                else{
                    $image_resize = Image::make($file);
                    $image_resize->fit(800,360);
                    $image_resize->save('public/uploads/banner/'.$filename);

                    $image_cover = Image::make($file);
                    $image_cover->fit(250,250);
                    $image_cover->save('public/uploads/cover/'.$filename);

                    $eso = Eso::find($request['id'])->update(['image'=>$filename,'banner'=>$filename]);
                    flash('Cover has been successfully updated');
                }
            }else{
                flash('You didnot upload a file')->danger();
            }
        }
        return redirect()->back();
    }

    public function uploadLogo(Request $request)
    {
        if($request->hasFile('upload_logo')){
            $image       = $request->file('upload_logo');
            $newName    = Str::random(12);
            $extension = $image->guessExtension();
            $filename = $newName.'.'.$extension;

            $file = $image->move('public/uploads/full/',$filename);
            $image_resize = Image::make($file);
            $image_resize->fit(100, 100);
            $image_resize->save('public/uploads/logo/'.$filename);

            $eso = Eso::find($request['id'])->update(['logo'=>$filename]);

            flash(' Logo successfully updated')->success();

        }else{
            flash('You didnot upload a file')->danger();
        }
        return redirect()->back();
    }

    public function getEsos()
    {
        $esos = Eso::where(['status'=>'Published'])->orderBy('id','desc')->paginate();

        SEOTools::setTitle('ESOs');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('esos'));
        SEOTools::setCanonical(route('esos'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));

        return view('esos-list',['esos'=>$esos]);
    }

    public function viewEso($id,$slug)
    {
        $eso = Eso::find($id);

        SEOTools::setTitle($eso['name']);
        SEOTools::setDescription($eso['tagline']);
        SEOTools::opengraph()->setUrl(route('view.eso',['id'=>$eso['id'],'slug'=>HelperTrait::slugify($eso['name'])]));
        SEOTools::setCanonical(route('corporations'));
        SEOTools::opengraph()->addProperty('type', 'webPage');
        SEOTools::opengraph()->addProperty('image', secure_asset(env('BANNER').''.$eso['cover']));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset(env('BANNER').''.$eso['cover']));

        return view('eso-view',['eso'=>$eso]);
    }


    public function searchEsos(Request $request)
    {

        $where = [];
    $name = $request['name'];
    $district = $request['district'];
    $industry = $request['industry'];
    $sector = $request['sector'];

   // $where['status']= 'Published';

   $esos = Eso::where($where)->where('name', 'like', '%'.$name.'%')->paginate();


        SEOTools::setTitle('ESOs - Startup Uganda');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('esos'));
        SEOTools::setCanonical(route('esos'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));

        return view('esos-list',['esos'=>$esos]);
    }

}
