<?php

namespace App\Http\Controllers;

use App\Event;
use App\Traits\HelperTrait;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use Image;
use Illuminate\Support\Str;

class EventController extends Controller
{
    //

    public function getEvents()
    {
       $events = Event::where(['added_by'=>Auth::user()->id ])->orderBy('id','desc')->paginate(9);
       return view('user.events',['events'=>$events]);
    }

    public function viewEvent($id,$slug)
    {
        $event = Event::find($id);
        return view('user.view-event',['event'=>$event]);
    }

     //validate
    public function createEvent(Request $request)
    {
         $validatedData = Validator::make(request()->all(), [
            'event_name'=> 'required|string',
            'start_date' => 'required',
            'end_date' => 'required',
            'apply_before' => 'required',
            'organiser' => 'required',
            'location' => 'required',
            'target_audience' => 'required',
            'description' => 'required',
            'summary' => 'required'
        ]);
        if ($validatedData->fails()) {
            $error = "";
            foreach ($validatedData->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();
            return redirect()->back();
        }else{
            $data = $request->all();

            $data['start_date']=date('Y-m-d H:i:s', strtotime($data['start_date']));
            $data['end_date']=date('Y-m-d H:i:s', strtotime($data['end_date']));
            $data['apply_before']=date('Y-m-d H:i:s', strtotime($data['end_date']));
            $data['description'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['description']);
            $data['summary'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['summary']);
            $data['image'] = env('BUSINESS_PROFILE');
            $data['cover'] = env('BUSINESS_BANNER');
            $data['status'] = "Pending";
            $data['added_by'] = Auth::user()->id;

            Event::create($data);
            flash('Event has been successfully registered. Please add image and cover photo then publish, activate the event')->success();
            return redirect()->back();
        }

    }


    public function updateEvent(Request $request)
    {
         //validate
         $validatedData = Validator::make(request()->all(), [
            'event_name'=> 'required|string',
            'start_date' => 'required',
            'end_date' => 'required',
            'apply_before' => 'required',
            'organiser' => 'required',
            'location' => 'required',
            'target_audience' => 'required',
            'description' => 'required',
            'summary' => 'required',
            'event_id' => 'required',
        ]);
        if ($validatedData->fails()) {
            $error = "";
            foreach ($validatedData->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();
            return redirect()->back();
        }else{
            $data = $request->all();
            $data['summary'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['summary']);
            $data['description'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['description']);
            $event = Event::find($data['event_id']);

            $event->update($data);
            flash('Event has been successfully registered. Please add image and cover photo then publish, activate the event')->success();
            return redirect()->back();
        }

    }
    public function bannerUpload(Request $request){
        if(isset($request['image'])) {
            $data = $request['image'];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);

            $data = base64_decode($image_array_2[1]);
            $name = Str::random(12) . '.png';
            $full_image = env('FULL') . $name;
            file_put_contents($full_image, $data);

            $image_cover = Image::make($data);
            $image_cover->fit(300,230);
            $image_cover->save(env('COVER').$name);

            $banner = env('BANNER') . $name;
            file_put_contents($banner, $data);

            $event = Event::find($request['id'])->update(['image' => $name, 'cover' => $name]);
            flash('Cover has been successfully updated');
            return redirect()->back();
        }else{
            flash("You didn't upload a file")->danger();
        }
        return redirect()->back();
    }

    public function uploadCover(Request $request)
    {
        $validatedData = Validator::make( $request->all(), [
            'upload_cover' => 'required|mimes:jpeg,png,jpg,gif,svg',
            'id' => 'required'
        ]);
         if ($validatedData->fails()) {
            foreach ($validatedData->messages()->getMessages() as $field_name => $messages)
            {
                flash('Error: '.$messages[0])->warning();
            }
        }else{
            if($request->hasFile('upload_cover')){
                $image       = $request->file('upload_cover');
                $newName    = Str::random(12);
                $extension = $image->guessExtension();
                $filename = $newName.'.'.$extension;
                // $width = Image::make('public/foo.jpg')->width();
                // $height = Image::make('public/foo.jpg')->height();

                $file = $image->move('public/uploads/full/',$filename);

                $image_resize = Image::make($file);
                $image_resize->fit(800,360);
                $image_resize->save('public/uploads/banner/'.$filename);

                $image_cover = Image::make($file);
                $image_cover->fit(250,250);
                $image_cover->save('public/uploads/cover/'.$filename);


                $event = Event::find($request['id'])->update(['image'=>$filename,'cover'=>$filename]);
                flash('Cover has been successfully updated');

            }else{
                flash('You didnot upload a file')->danger();
            }
        }
        return redirect()->back();
    }

    public function updateEventStatus($id,$status)
    {
        $event = Event::find($id)->update(['status'=>$status]);
        flash('Event status has been updated to '.$status);
        return redirect()->back();
    }



    public function eventsListing()
    {
        $events = Event::orderBy('id','desc')->where(['status'=>'Active'])->paginate(12);
        SEOTools::setTitle('Events');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('events'));
        SEOTools::setCanonical(route('events'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));

        return view('events-list',['events'=>$events]);
    }

    public function eventDetails($id,$slug)
    {
        $event = Event::find($id);

        SEOTools::setTitle($event['event_name']);
        SEOTools::setDescription($event['summary']);
        SEOTools::opengraph()->setUrl(route('view.event',['id'=>$event['id'],'slug'=>HelperTrait::slugify($event['event_name'])]));
        SEOTools::setCanonical(route('events'));
        SEOTools::opengraph()->addProperty('type', 'webPage');
        SEOTools::opengraph()->addProperty('image', secure_asset(env('BANNER').$event['image']));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset(env('BANNER').$event['image']));

        return view('event-view',['event'=>$event]);
    }

    public function searchEvents(Request $request){
        $where = [];
        $name = $request['name'];
        $startDate = $request['startDate'];
        $endDate = is_null($request['endDate'])?date($request['endDate']):date('Y-m-d');
        $location = $request['location'];
        $organiser = $request['organiser'];

        $where['status'] = 'Active';

        if(isset($startDate) && !is_null($startDate)){
            $events = Event::orderBy('id','desc')->where($where)
                ->where('event_name', 'like', '%'.$name.'%')
                ->where('organiser', 'like', '%'.$organiser.'%')
                ->where('organiser', 'like', '%'.$location.'%')
                ->whereBetween('start_date',[$startDate,$endDate])->paginate(12);
        }else{
            $events = Event::orderBy('id','desc')->where($where)
                ->where('event_name', 'like', '%'.$name.'%')
                ->where('organiser', 'like', '%'.$organiser.'%')
                ->where('organiser', 'like', '%'.$location.'%')
                ->paginate(12);
        }

        SEOTools::setTitle('Events');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('events'));
        SEOTools::setCanonical(route('events'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));

        return view('events-list',['events'=>$events]);

    }


}
