<?php

namespace App\Http\Controllers;

use App\Feed;
use App\Traits\HelperTrait;
use Auth;
use OpenGraph;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class FeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feeds = Feed::orderBy('id','desc')->where(['status'=>'published'])->paginate(10);
        foreach($feeds as $feed){
            $feed['company'] = HelperTrait::getCompany($feed['post_type'],$feed['post_id']);
        }
        return view('feeds-view',['feeds'=>$feeds]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $validatedData = Validator::make(request()->all(), [
            'url' => 'required',
            'thought' =>'required',
            'company' => 'required'
        ]);
        if ($validatedData->fails()) {
            $error = "";
            foreach ($validatedData->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();
        }else{
            $data = $request->all();

            $company = explode('_',$data['company']);
            //dd($company);
            $data['post_id'] = $company[0];
            $data['post_type'] = $company[1];
            $data['status'] = "published";
            $data['user_id'] = Auth::user()->id;
           // dd($data);

            //Run Open Graph
            $fetch = OpenGraph::fetch($data['url']);
            if(count($fetch)>0){
                if(!isset($fetch['image'])){
                    $data['image'] = 'https://i.stack.imgur.com/y9DpT.jpg';
                }
                elseif(!empty($fetch['image']) || !is_null($fetch['image']) ){
                    $data['image'] = $fetch['image'];
                }

                if(!empty($fetch['title']) || !is_null($fetch['title']) ){
                    $data['title'] = $fetch['title'];
                }
            }




            Feed::create($data);
            flash('Your thought has been successfully registered. To publish, activate it for other users')->success();
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function show(Feed $feed)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function edit(Feed $feed)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feed $feed)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feed $feed)
    {
        //
    }


    public function getUserFeeds()
    {
        $feeds = Feed::orderBy('id','desc')->where(['user_id'=>Auth::user()->id])->paginate(15);
        foreach($feeds as $feed){
            $feed['company'] = HelperTrait::getCompany($feed['post_type'],$feed['post_id']);
        }
        return view('user.feeds',['feeds'=>$feeds]);
    }

    public function updateFeed($id,$status)
    {
        $feed = Feed::find($id)->update(['status'=>$status]);
        return redirect()->back();
    }
}
