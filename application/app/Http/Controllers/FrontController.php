<?php

namespace App\Http\Controllers;

use App\Event;
use App\Feed;
use App\Post;
use App\Program;
use App\Sector;
use App\Startup;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOTools;

class FrontController extends Controller
{
    //

    public function home()
    {
        //$startups = Startup::where(['status'=>'Published','is_featured'=>1])->inRandomOrder()->limit(3)->get();
        $programs = Program::where(['status'=>'Active'])->orderBy('id','desc')->limit(3)->get(); //
       // $feeds = Feed::where(['status'=>'Published'])->orderBy('id','desc')->limit(8)->get()->toArray();
        $posts = Post::where(['status'=>'Published'])->orderBy('id','desc')->limit(8)->get()->toArray();
        $events = Event::orderBy('id','desc')->where(['status'=>'Active'])->paginate(5);


        SEOTools::setTitle('Startup Uganda');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('startups'));
        SEOTools::setCanonical(route('startups'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));

        //'startups'=>$startups,
        return view('welcome',['feeds'=>$posts,'events'=>$events,'programs'=>$programs]);
    }

    public function aboutUs()
    {
        $boards = [
            ['name'=>'Richard Zulu','designation'=>'Chairperson','image'=>'zulu.jpg'],
            ['name'=>'Jean Kukunda','designation'=>'Vice Chairperson','image'=>'jean.jpg'],
            ['name'=>'CK Japheth','designation'=>'Secretary','image'=>'CK-Japheth.jpg'],
            ['name'=>'Dona Sava','designation'=>'Project Coordinator','image'=>'dona.jpg'],
            ['name'=>'Raymond Besiga','designation'=>'Board Member','image'=>'raymond.jpg'],
            ['name'=>'Alan Ananura','designation'=>'Board Member','image'=>'alan.jpg'],
            ['name'=>'Barbara Mutabazi','designation'=>'Board Member','image'=>'barbara.jpg'],
            ['name'=>'Keneth Twesigye','designation'=>'Board Member','image'=>'keneth.jpg'],
            ['name'=>'Stella Nakawuki Lukwago','designation'=>'Board Member','image'=>'stella.jpg'],
        ];

        SEOTools::setTitle('Startup Uganda');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('startups'));
        SEOTools::setCanonical(route('startups'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));

        return view('about',['boards'=>$boards]);
    }

    public function network(){
        $sectors = Sector::get();
        return view('network',['sectors'=>$sectors]);
    }
}
