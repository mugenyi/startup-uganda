<?php

namespace App\Http\Controllers;

use App\Government;
use App\Team;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Image;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Support\Str;

class GovernmentController extends Controller
{
    use HelperTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.new-government');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = Validator::make(request()->all(), [
            'name'=> 'required|string',
            'about'=> 'required',
            'user_id'=> 'required',
            'address'=> 'required',
            'phone_number'=> 'required',
            'email'=> 'required',

        ]);
        if ($validated->fails()) {
           // dd($validated->messages());
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();

        }else{
            $data = $request->all();

            $data['image'] = env('BUSINESS_PROFILE');
            $data['cover'] = env('BUSINESS_BANNER');
            $data['logo'] = env('BUSINESS_LOGO');
            $data['user_id'] = Auth::user()->id;
            $data['status'] = 'pending';

            $sectors="";
            foreach($data['sectors'] as $d){
                $sectors .=$d.', ';
            }
            $data['sectors'] = $sectors;
            $data['about'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['about']);

            $gov = Government::create($data);

            flash('Startup has been successfully registered. Please upload your logo, banner and pitch deck')->success();
            if(Auth::user()->role =='admin')
                return redirect()->route('admin.government.view',['id'=>$gov['id']]);
            else{
                return redirect()->route('user.view.government',['id'=>$gov['id'],'slug'=>$this->slugify($gov['name'],'government')]);
            }

        }
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Government  $government
     * @return \Illuminate\Http\Response
     */
    public function show($id,$slug)
    {
        $government = Government::find($id);
        $team = Team::where(['category'=>'government','category_id'=>$government->id,'memberType'=>'teamMember'])->get();
        $board = Team::where(['category'=>'government','category_id'=>$government->id,'memberType'=>'boardMember'])->get();
        return view('user.view-government',['government'=>$government,'teams'=>$team,'boards'=>$board]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Government  $government
     * @return \Illuminate\Http\Response
     */
    public function edit(Government $government)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Government  $government
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Government $government)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Government  $government
     * @return \Illuminate\Http\Response
     */
    public function destroy(Government $government)
    {
        //
    }

    public function bannerUpload(Request $request){
        if(isset($request['image'])) {
            $data = $request['image'];

            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $name = Str::random(12) . '.png';
            $full_image = env('FULL') . $name;
            file_put_contents($full_image, $data);

            $image_cover = Image::make($data);
            $image_cover->fit(300,230);
            $image_cover->save(env('COVER').$name);

            $banner = env('BANNER') . $name;
            file_put_contents($banner, $data);

            $government = Government::find($request['id'])->update(['image' => $name, 'cover' => $name]);
            flash('Cover has been successfully updated');
            return redirect()->back();
        }else{
            flash('You didnot upload a file')->danger();
        }
        return redirect()->back();
    }

    public function uploadCover(Request $request)
    {
        $validatedData = Validator::make( $request->all(), [
            'upload_cover' => 'required|mimes:jpeg,png,jpg,gif,svg',
            'id' => 'required'
        ]);
         if ($validatedData->fails()) {
            foreach ($validatedData->messages()->getMessages() as $field_name => $messages)
            {
                flash('Error: '.$messages[0])->warning();
            }
        }else{
            if($request->hasFile('upload_cover')){
                $image       = $request->file('upload_cover');
                $newName    = Str::random(12);
                $extension = $image->guessExtension();
                $filename = $newName.'.'.$extension;
                // $width = Image::make('public/foo.jpg')->width();
                // $height = Image::make('public/foo.jpg')->height();

                $file = $image->move('public/uploads/full/',$filename);

                $width = Image::make('public/uploads/full/'.$filename)->width();
                $height = Image::make('public/uploads/full/'.$filename)->height();
                if($width < 860 || $height < 400){
                   flash('Error: Uploaded file it too small. Please consider 860px width and 400px height')->warning();
                }
                else{
                    $image_resize = Image::make($file);
                    $image_resize->fit(800,360);
                    $image_resize->save('public/uploads/banner/'.$filename);

                    $image_cover = Image::make($file);
                    $image_cover->fit(250,250);
                    $image_cover->save('public/uploads/cover/'.$filename);


                    $program = Government::find($request['id'])->update(['image'=>$filename,'cover'=>$filename]);
                    flash('Cover has been successfully updated');
                }
            }else{
                flash('You didnot upload a file')->danger();
            }
        }
        return redirect()->back();
    }

    public function uploadLogo(Request $request)
    {
        if($request->hasFile('upload_logo')){
            $image       = $request->file('upload_logo');
            $newName    = Str::random(12);
            $extension = $image->guessExtension();
            $filename = $newName.'.'.$extension;

            $file = $image->move('public/uploads/full/',$filename);
            $image_resize = Image::make($file);
            $image_resize->fit(100, 100);
            $image_resize->save('public/uploads/logo/'.$filename);

            $gov = Government::find($request['id'])->update(['logo'=>$filename]);

            flash('Agenct Logo successfully updated')->success();

        }else{
            flash('You didnot upload a file')->danger();
        }
        return redirect()->back();
    }

    public function getAgencies()
    {
        $govs = Government::where(['status'=>'active'])->orderBy('id','desc')->paginate();
        SEOTools::setTitle('Government Agencies');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('governments'));
        SEOTools::setCanonical(route('governments'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));

        return view('government-list',['governments'=>$govs]);
    }

    public function viewGovernment($id,$slug)
    {
        $government = Government::with('jobs')->find($id);
        $team = Team::where(['category'=>'government','category_id'=>$government->id,'memberType'=>'teamMember'])->get();
        $board = Team::where(['category'=>'government','category_id'=>$government->id,'memberType'=>'boardMember'])->get();


        SEOTools::setTitle($government['name']);
        SEOTools::setDescription($government['tagline']);
        SEOTools::opengraph()->setUrl(route('view.government',['id'=>$government['id'],'slug'=>HelperTrait::slugify($government['name'])]));
        SEOTools::setCanonical(route('governments'));
        SEOTools::opengraph()->addProperty('type', 'webPage');
        SEOTools::opengraph()->addProperty('image', secure_asset(env('BANNER').''.$government['cover']));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset(env('BANNER').''.$government['cover']));

        return view('government-view',['government'=>$government,'teams'=>$team,'boards'=>$board]);

    }

    public function searchGovernments(Request $request)
    {

        $where = [];
    $name = $request['name'];
    $district = $request['district'];
    $industry = $request['industry'];
    $sector = $request['sector'];

   // $where['status']= 'Published';

//    if(!is_null($industry)){
//        if(!is_null($sector)){
//        $govs = Government::where($where)->where('sectors', 'like', '%'.$sector.'%')
//            ->where('industries', 'like', '%'.$industry.'%')->where('name', 'like', '%'.$name.'%')->paginate();
//        }else{
//            $govs = Government::where($where)->where('industries', 'like', '%'.$industry.'%')
//            ->where('name', 'like', '%'.$name.'%')->paginate();
//        }
//    }else{
        if(!is_null($sector)){
            $govs = Government::where($where)->where('sectors', 'like', '%'.$sector.'%')
            ->where('name', 'like', '%'.$name.'%')->paginate();
        }else{
            $govs = Government::where($where)->where('sectors', 'like', '%'.$sector.'%')
            ->where('name', 'like', '%'.$name.'%')->paginate();
        }
//    }

        SEOTools::setTitle('Government Agencies - Startup Uganda');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('governments'));
        SEOTools::setCanonical(route('governments'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));


        return view('government-list',['governments'=>$govs]);
    }

}
