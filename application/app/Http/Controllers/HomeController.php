<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(session()->has('intended-url')){
            $url = session('intended-url');
            session()->forget('intended-url');
            return Redirect::to($url);
        }else{
            if(Auth::user()->role =='user'){
                return redirect()->route('feeds');
            }elseif(Auth::user()->role =='admin'){
                return redirect()->route('admin.dashboard');
            }
        }
        return view('home');
    }




}
