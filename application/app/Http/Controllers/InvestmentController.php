<?php

namespace App\Http\Controllers;

use App\Investment;
use App\Mail\SendMail;
use App\Startup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

class InvestmentController extends Controller
{
    //

    public function closeStartupFundraising($id)
    {
        $startup = Startup::find($id)->update(['fundraising'=>false]);
        flash($startup['name'].' fundraising round has been closed')->success();
        return redirect()->back();
    }

    public function startupFundraising(Request $request)
    {
        $validated = Validator::make(request()->all(), [
            'id'=> 'required',
            'round'=> 'required',
            'raise_amount'=> 'required',
        ]);
        if ($validated->fails()) {
           // dd($validated->messages());
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages;
            }
            flash($error)->warning();
            return redirect()->back();
        }else{
            $data = $request->all();
            $startup = Startup::find($data['id']);
            $startup->update(['fundraising'=>true,'raise_amount'=>$data['raise_amount'],'round'=>$data['round']]);
            flash($startup['name'].' fundraising round has been closed')->success();
        }
        return redirect()->back();
    }

    public function recordStartupFunding(Request $request)
    {
        $validated = Validator::make(request()->all(), [
            'category'=> 'required',
            'category_id'=> 'required',
            'company_name'=> 'required',
            'company_type'=> 'required',
            'amount' =>'required'
        ]);
        if ($validated->fails()) {
           // dd($validated->messages());
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages;
            }
            flash($error)->warning();
            return redirect()->back();
        }else{
            $data = $request->all();

            $logo = $data['company_logo']==""?'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRs7NbpyRP68rnOkMGpdfNIB39PT9lIdBeJLA&usqp=CAU':$data['company_logo'];
            $invest = Investment::create(['category'=>$data['category'],'category_id'=>$data['category_id'],
            'company_name'=>$data['company_name'],'company_logo'=>$logo,'company_type'=>$data['company_type'],
            'amount'=>(int)$data['amount'],'round'=>$data['round']]);

            flash('Funds from have been successfully recorded')->success();
        }
        return redirect()->back();
    }

    public function recordStartupMessage(Request $request){
        $validated = Validator::make(request()->all(), [
            'startupId'=> 'required',
            'name'=> 'required',
            'email'=> 'required',
            'message'=> 'required',
        ]);
        if ($validated->fails()) {
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages;
            }
            flash($error)->warning();
            return redirect()->back();
        }else {
            $data = $request->all();
            //Send Email
            $startup = Startup::find($data['startupId']);
            //dd($startup);
            Mail::to($startup->user->email)->send(new SendMail($data));
            return back()->with('success','Your email has been successfully sent');
        }
    }

    public function deleteInvestment($id)
    {
        $in = Investment::find($id)->delete();
        flash('Funds from have been successfully deleted')->warning();
        return redirect()->back();
    }



}
