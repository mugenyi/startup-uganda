<?php

namespace App\Http\Controllers;

use App\Investor;
use App\District;
use App\Team;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Str;
use Artesaos\SEOTools\Facades\SEOTools;
// import the Intervention Image Manager Class
use Image;
use Auth;

class InvestorController extends Controller
{
    use HelperTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $districts = District::select('district')->get();
        return view('user.new-investor',['districts'=>$districts]);
    }
    public function createIndividual()
    {
        $districts = District::select('district')->get();
        return view('user.new-ind-investor',['districts'=>$districts]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = Validator::make(request()->all(), [
            'name'=> 'required|string',
            'legal_name'=> 'required',
            'founded_on'=> 'required',
            'user_id'=> 'required',
            'about'=> 'required',
            'address'=> 'required',
            'phone_number'=> 'required',
            'email'=> 'required',
            'investment_industries'=> 'required',
            'investment_sectors'=> 'required',

        ]);
        if ($validated->fails()) {
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();
        }else{
            $data = $request->all();
            $investment_industries="";
            foreach($data['investment_industries'] as $a){
                $investment_industries .=$a.', ';
            }
            $investment_sectors="";
            foreach($data['investment_sectors'] as $d){
                $investment_sectors .=$d.', ';
            }

            $data['investment_sectors'] = $investment_sectors;
            $data['investment_industries'] = $investment_industries;

            $data['status'] = 'Pending';
            $data['logo'] = env('BUSINESS_LOGO');
            $data['banner'] = env('BUSINESS_BANNER');
            $data['profile_photo'] = env('BUSINESS_PROFILE');
            $data['slug'] = $this->createSlug($data['name'],'investor');
            $data['about'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['about']);
            $data['experience'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['experience']);

            $investor = Investor::create($data);
            flash('Investor has been successfully registered. Please upload your logo, banner and pitch deck')->success();
            if(Auth::user()->role =='admin')
                return redirect()->route('admin.investor.view',['id'=>$investor['id']]);
            else{
                return redirect()->route('user.view.investor',['slug'=>$investor['slug']]);
            }
        }
    }

    public function storeIndividualInvestor(Request $request)
    {
        $validated = Validator::make(request()->all(), [
            'name'=> 'required|string',
            'user_id'=> 'required',
            'about'=> 'required',
            'address'=> 'required',
            'phone_number'=> 'required',
            'email'=> 'required',
            'investment_industries'=> 'required',
            'investment_sectors'=> 'required',

        ]);
        if ($validated->fails()) {
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();
        }else{
            $data = $request->all();
            $investment_industries="";
            foreach($data['investment_industries'] as $a){
                $investment_industries .=$a.', ';
            }
            $investment_sectors="";
            foreach($data['investment_sectors'] as $d){
                $investment_sectors .=$d.', ';
            }

            $data['investment_sectors'] = $investment_sectors;
            $data['investment_industries'] = $investment_industries;

            $data['status'] = 'Pending';
            $data['legal_name'] = $data['name'];
            $data['founded_on'] = date(now());
            $data['tagline'] = $data['name'].' in an individual investor on '.env('APP_NAME');
            $data['logo'] = env('BUSINESS_LOGO');
            $data['banner'] = env('BUSINESS_BANNER');
            $data['profile_photo'] = env('BUSINESS_PROFILE');
            $data['slug'] = $this->createSlug($data['name'],'investor');
            $data['about'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['about']);
            $data['experience'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['experience']);

            $investor = Investor::create($data);
            flash('Investor has been successfully registered. Please upload your logo, banner and pitch deck')->success();
            if(Auth::user()->role =='admin')
                return redirect()->route('admin.investor.view',['id'=>$investor['id']]);
            else{
                return redirect()->route('user.view.investor',['slug'=>$investor['slug']]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $investor = Investor::where(['slug'=>$slug])->first();

        $team = Team::where(['category'=>'investor','category_id'=>$investor->id,'memberType'=>'teamMember'])->get();
        $board = Team::where(['category'=>'investor','category_id'=>$investor->id,'memberType'=>'boardMember'])->get();
        return view('user.view-investor',['investor'=>$investor,'teams'=>$team,'boards'=>$board]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $investor = Investor::where(['slug'=>$slug])->first();
        return view('user.edit-investor',['investor'=>$investor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Investor $investor)
    {
        $validated = Validator::make(request()->all(), [
            'name'=> 'required|string',
            'legal_name'=> 'required',
            'founded_on'=> 'required',
            'user_id'=> 'required',
            'about'=> 'required',
            'address'=> 'required',
            'phone_number'=> 'required',
            'email'=> 'required',
            'investment_industries'=> 'required',
            'investment_sectors'=> 'required',
            'investor_id'=>'required'

        ]);
        if ($validated->fails()) {
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();
        }else{
            $data = $request->all();
            $investor = Investor::find($data['investor_id']);

            $investment_industries="";
            foreach($data['investment_industries'] as $a){
                $investment_industries .=$a.', ';
            }
            $investment_sectors="";
            foreach($data['investment_sectors'] as $d){
                $investment_sectors .=$d.', ';
            }

            $data['investment_sectors'] = $investment_sectors;
            $data['investment_industries'] = $investment_industries;

            $data['slug'] = $this->createSlug($data['name'],'investor');
            $data['about'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['about']);
            $data['experience'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['experience']);

            $investor->update($data);
            flash('Investor has been successfully updated. Please upload your logo, banner and pitch deck')->success();
            return redirect()->route('user.dashboard');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Investor $investor)
    {
        //
    }


    public function uploadLogo(Request $request)
    {
        if($request->hasFile('upload_logo')){
            $image       = $request->file('upload_logo');
            $newName    = Str::random(12);
            $extension = $image->guessExtension();
            $filename = $newName.'.'.$extension;

            $file = $image->move('public/uploads/full/',$filename);
            $image_resize = Image::make($file);
            $image_resize->fit(100, 100);
            $image_resize->save('public/uploads/logo/'.$filename);

            $startup = Investor::find($request['id'])->update(['logo'=>$filename]);
            flash('Logo successfully updated')->success();

        }else{
            flash('You didnot upload a file')->danger();
        }
            return redirect()->back();
    }

        public function bannerUpload(Request $request){
            if(isset($request['image'])) {
                $data = $request['image'];

                $image_array_1 = explode(";", $data);
                $image_array_2 = explode(",", $image_array_1[1]);
                $data = base64_decode($image_array_2[1]);
                $name = Str::random(12) . '.png';
                $full_image = env('FULL') . $name;
                file_put_contents($full_image, $data);

                $image_cover = Image::make($data);
                $image_cover->fit(300,230);
                $image_cover->save(env('COVER').$name);

                $banner = env('BANNER') . $name;
                file_put_contents($banner, $data);

                $startup = Investor::find($request['id'])->update(['banner' => $name, 'profile_photo' => $name]);
                flash('Cover has been successfully updated');
                return redirect()->back();
            }else{
                flash('You didnot upload a file')->danger();
            }
            return redirect()->back();
        }

        public function uploadCover(Request $request)
        {
            $validatedData = Validator::make( $request->all(), [
                'upload_cover' => 'required|mimes:jpeg,png,jpg,gif,svg',
                'id' => 'required'
            ]);
             if ($validatedData->fails()) {
                foreach ($validatedData->messages()->getMessages() as $field_name => $messages)
                {
                    flash('Error: '.$messages[0])->warning();
                }
            }else{
            if($request->hasFile('upload_cover')){
                $image       = $request->file('upload_cover');
                $newName    = Str::random(12);
                $extension = $image->guessExtension();
                $filename = $newName.'.'.$extension;
                // $width = Image::make('public/foo.jpg')->width();
                // $height = Image::make('public/foo.jpg')->height();

                $file = $image->move('public/uploads/full/',$filename);
                $width = Image::make('public/uploads/full/'.$filename)->width();
                $height = Image::make('public/uploads/full/'.$filename)->height();
                if($width < 860 || $height < 400){
                   flash('Error: Uploaded file it too small. Please consider 860px width and 400px height')->warning();
                }
                else{
                    $image_resize = Image::make($file);
                    $image_resize->fit(800,360);
                    $image_resize->save('public/uploads/banner/'.$filename);

                    $image_cover = Image::make($file);
                    $image_cover->fit(250,250);
                    $image_cover->save('public/uploads/cover/'.$filename);

                    $startup = Investor::find($request['id'])->update(['banner'=>$filename,'profile_photo'=>$filename]);
                    flash('Cover has been successfully updated');
                }
            }else{
                flash('You didnot upload a file')->danger();
            }
        }
            return redirect()->back();
        }

        public function getUserInvestors()
        {
            $investors = Investor::get();
            //return view('user.investor-list',['investors'=>$investors]);
        }


        public function investorListing()
        {
            $investors = Investor::where(['status'=>'Published'])->paginate();

            SEOTools::setTitle('Startup Uganda Investors');
            SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
            SEOTools::opengraph()->setUrl(route('investors'));
            SEOTools::setCanonical(route('investors'));
            SEOTools::opengraph()->addProperty('type', 'articles');
            SEOTools::twitter()->setSite('@StartupUganda_');
            SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));
            return view('investor-list',['investors'=>$investors]);
        }

        public function investorDetails($slug)
    {
        $investor = Investor::with('jobs')->where(['slug'=>$slug])->first();

        SEOTools::setTitle($investor['name']);
        SEOTools::setDescription($investor['tagline']);
        SEOTools::opengraph()->setUrl(route('investor.details',['slug'=>$investor['slug']]));
        SEOTools::setCanonical(route('investors'));
        SEOTools::opengraph()->addProperty('type', 'webPage');
        SEOTools::opengraph()->addProperty('image', secure_asset(env('BANNER').''.$investor['banner']));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset(env('BANNER').''.$investor['banner']));
        if($investor['investor_type']=='organisation'){
            $team = Team::where(['category'=>'investor','category_id'=>$investor->id,'memberType'=>'teamMember'])->get();
            $board = Team::where(['category'=>'investor','category_id'=>$investor->id,'memberType'=>'boardMember'])->get();
            return view('investor-view',['investor'=>$investor,'teams'=>$team,'boards'=>$board]);
        }else{
            return view('individual-view',['investor'=>$investor]);
        }

    }


    public function searchInvestors(Request $request)
    {
        $where = [];
        $name = $request['name'];
        $district = $request['district'];
        $industry = $request['industry'];
        $sector = $request['sector'];
        $investValue = $request['investValue'];
        $investor_type = $request['investor_type'];


        $where['status']= 'Published';
        if(!is_null($district))
            $where['district']=$district;

        if(!is_null($investor_type))
            $where['investor_type']=$investor_type;


        if(!is_null($name)){
            if(!is_null($sector)){
                if(!is_null($industry)){
                    $investors = Investor::where($where)->where('investment_sectors', 'like', '%'.$sector.'%')
                    ->where('investment_industries', 'like', '%'.$industry.'%')->where('name', 'like', '%'.$name.'%')->paginate();
                }else{
                    $investors = Investor::where($where)->where('investment_sectors', 'like', '%'.$sector.'%')
                    ->where('name', 'like', '%'.$name.'%')->paginate();
                }

            }else{
                $investors = Investor::where($where)->Where('name', 'like', '%'.$name.'%')->paginate();
            }

        }else{
            if(!is_null($sector)){
                if(!is_null($industry)){
                    $investors = Investor::where($where)->where('investment_sectors', 'like', '%'.$sector.'%')
                    ->where('investment_industries', 'like', '%'.$industry.'%')->paginate();
                }else{
                    $investors = Investor::where($where)->where('investment_sectors', 'like', '%'.$sector.'%')->paginate();
                }

            }else{
                $investors = Investor::where($where)->paginate();
            }

        }
        SEOTools::setTitle('Startup Uganda Investors');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('investors'));
        SEOTools::setCanonical(route('investors'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));

        return view('investor-list',['investors'=>$investors]);

    }
}
