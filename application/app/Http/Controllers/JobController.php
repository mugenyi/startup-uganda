<?php

namespace App\Http\Controllers;

use App\Investor;
use App\Job;
use App\Startup;
use App\Traits\HelperTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use Artesaos\SEOTools\Facades\SEOTools;

class JobController extends Controller
{
    //
    public function getJobs()
    {
        $user = Auth::user()->id;
        $jobs = Job::where(['added_by'=>$user])->orderBy('id','desc')->get();
        return view('user.jobs',['jobs'=>$jobs]);
    }

    /**
     * CREATE JOB
     */
    public function createJob(Request $request)
    {
        //validate
        $validatedData = Validator::make(request()->all(), [
            'job_title' => 'required|string',
            'company_type' => 'required',
            //'company' => 'required',
            'location' => 'required',
            'job_type' => 'required',
            'description' => 'required',
            'ends_at' => 'required',
        ]);
        if ($validatedData->fails()) {
            $error = "";
            foreach ($validatedData->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();
        }else{
            $data = $request->all();

            $company = explode('_',$data['company_type']);
            //dd($company);
            $data['company'] = $company[0];
            $data['company_type'] = $company[1];
            $data['status'] = "Pending";
            $data['description'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['description']);
            $data['added_by'] = Auth::user()->id;

            Job::create($data);
            flash('Job has been successfully registered. To publish, activate the job')->success();
        }
        return redirect()->back();
    }

    public function activateJob($jobId,$status)
    {
        Job::find($jobId)->update(['status'=>$status]);
        if($status == 'Active')
            flash('Job has been successfully published.')->success();
        else
            flash('Job has been successfully unpublished.')->warning();

        return redirect()->back();
    }

    public function jobStatus($jobId,$status)
    {
        $job = Job::find($jobId);
        if(!is_null($job)){
            $job->update(['status'=>$status]);
        }
        return redirect()->back();
    }


    public function jobsListing()
    {
        $jobs = Job::where(['status'=>'Active'])->whereDate('ends_at', '>', Carbon::now())
            ->orderBy('id','desc')->paginate();

        SEOTools::setTitle('Job Listings');
        SEOTools::setDescription('We are the greatest job hub in Uganda for fast career, employment and current vacancy opportunities from Employers to Job Seekers.');
        SEOTools::opengraph()->setUrl(route('jobs'));
        SEOTools::setCanonical(route('jobs'));
        SEOTools::opengraph()->addProperty('type', 'webpage');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));
        return view('jobs',['jobs'=>$jobs]);
    }

    public function viewJob($id,$slug)
    {

        $job = Job::find($id);
        if(!is_null($job)){
            $company = HelperTrait::getCompany($job['company_type'],$job['company']);

            SEOTools::setTitle($job['job_title']);
            SEOTools::setDescription(substr(strip_tags($job['description']), 0, 200).'...');
            SEOTools::opengraph()->setUrl(route('job.view',['id'=>$job['id'],'slug'=>$slug]));
            SEOTools::setCanonical(route('job.view',['id'=>$job['id'],'slug'=>$slug]));
            SEOTools::opengraph()->addProperty('type', 'webpage');
            SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
            SEOTools::twitter()->setSite('@StartupUganda_');
            SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));

            return view('job-view',['job'=>$job,'company'=>$company]);
        }else{
            flash('Unknown Job. Please try again');
            return redirect()->back();
        }

    }

    public function searchJobs(Request $request)
    {
        $where = [];
        $name = $request['name'];
        $location = $request['location'];
        $job_type = $request['job_type'];
        $company_type = $request['company_type'];


        $where['status']= 'Active';

        if(!is_null($job_type))
            $where['job_type']=$job_type;

        if(!is_null($company_type))
            $where['company_type']=$company_type;

        if(!is_null($name)){
            if(!is_null($location)){
                $jobs = Job::where($where)->orderBy('id','desc')
                    ->whereDate('ends_at', '>', Carbon::now())
                    ->where('job_title', 'like', '%'.$name.'%')
                    ->where('location', 'like', '%'.$location.'%')
                    ->paginate();
            }else{
                $jobs = Job::where($where)->orderBy('id','desc')
                    ->whereDate('ends_at', '>', Carbon::now())
                    ->where('job_title', 'like', '%'.$name.'%')
                    ->paginate();
            }
        }else{
            if(!is_null($location)){
                $jobs = Job::where($where)->orderBy('id','desc')
                    ->whereDate('ends_at', '>', Carbon::now())
                    ->where('location', 'like', '%'.$location.'%')
                    ->paginate();
            }else{
                $jobs = Job::where($where)
                    ->whereDate('ends_at', '>', Carbon::now())
                    ->orderBy('id','desc')
                    ->paginate();
            }
        }

        SEOTools::setTitle('Job Listings');
        SEOTools::setDescription('We are the greatest job hub in Uganda for fast career, employment and current vacancy opportunities from Employers to Job Seekers.');
        SEOTools::opengraph()->setUrl(route('jobs'));
        SEOTools::setCanonical(route('jobs'));
        SEOTools::opengraph()->addProperty('type', 'webpage');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));

        return view('jobs',['jobs'=>$jobs]);

    }

}
