<?php

namespace App\Http\Controllers;

use App\Mentor;
use App\Traits\HelperTrait;
use Artesaos\SEOTools\Facades\SEOTools;
use Validator;
use Image;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class MentorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mentors = Mentor::where(['user_id'=>Auth::user()->id])->orderBy('id','desc')->paginate(9);
        return view('user.mentors',['mentors'=>$mentors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = Validator::make(request()->all(), [
            'name'=> 'required|string',
            'about'=> 'required',
            'location'=> 'required',
            'phone_number'=> 'required',
            'email'=> 'required',

        ]);
        if ($validated->fails()) {
           // dd($validated->messages());
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();
            return redirect()->back();
        }else{
            $data = $request->all();

            if($request->hasFile('image')){
                $image       = $request->file('image');
                $newName    = Str::random(12);
                $extension = $image->guessExtension();
                $filename = $newName.'.'.$extension;

                $file = $image->move('public/uploads/full/',$filename);
                $image_resize = Image::make($file);
                $image_resize->fit(100, 100);
                $image_resize->save('public/uploads/logo/'.$filename);
                $data['photo'] = $filename;
            }else{
                $data['photo'] = env('BUSINESS_PROFILE');
            }

            $data['user_id'] = Auth::user()->id;
            $data['status'] = 'pending';
            $data['achievements'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['achievements']);
            $data['what_i_do'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['what_i_do']);
            $data['about'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['about']);
            $data['looking_for'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['looking_for']);
            $data['markets'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['markets']);


            $sectors="";
            foreach($data['sectors'] as $d){
                $sectors .=$d.', ';
            }
            $data['focus_areas'] = $sectors;


            $mentor = Mentor::create($data);

            flash('Mentor has been successfully registered.')->success();

            if(Auth::user()->role =='admin')
                return redirect()->route('admin.mentor.view',['id'=>$mentor['id']]);
            else{
                return redirect()->back();
            }
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function show($id,$slug)
    {
        $mentor = Mentor::find($id);
        return view('user.view-mentor',['mentor'=>$mentor]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function edit(Mentor $mentor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validated = Validator::make(request()->all(), [
            'name'=> 'required|string',
            'about'=> 'required',
            'location'=> 'required',
            'phone_number'=> 'required',
            'email'=> 'required',
            'mentor_id'=> 'required',

        ]);
        if ($validated->fails()) {
            // dd($validated->messages());
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();
            return redirect()->back();
        }else{
            $data = $request->all();

            if($request->hasFile('image')){
                $image       = $request->file('image');
                $newName    = Str::random(12);
                $extension = $image->guessExtension();
                $filename = $newName.'.'.$extension;

                $file = $image->move('public/uploads/full/',$filename);
                $image_resize = Image::make($file);
                $image_resize->fit(100, 100);
                $image_resize->save('public/uploads/logo/'.$filename);
                $data['photo'] = $filename;
            }else{
                //$data['photo'] = env('BUSINESS_PROFILE');
            }

            $data['status'] = 'pending';
            $data['achievements'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['achievements']);
            $data['what_i_do'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['what_i_do']);
            $data['about'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['about']);
            $data['looking_for'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['looking_for']);
            $data['markets'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['markets']);


            $sectors="";
            foreach($data['sectors'] as $d){
                $sectors .=$d.', ';
            }
            $data['focus_areas'] = $sectors;


            $mentor = Mentor::find($data['mentor_id'])->update($data);

            flash('Mentor has been successfully updated.')->success();

            if(Auth::user()->role =='admin')
                return redirect()->route('admin.mentor.view',['id'=>$mentor['id']]);
            else{
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mentor $mentor)
    {
        //
    }

    public function getAllMentors()
    {
        $mentors = Mentor::orderBy('id','desc')->where(['status'=>'active'])->paginate(9);

        SEOTools::setTitle('Mentors');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('corporations'));
        SEOTools::setCanonical(route('corporations'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));

        return view('mentors-list',['mentors'=>$mentors]);
    }

    public function viewMentor($id,$slug)
    {
        $mentor = Mentor::findorfail($id);

        SEOTools::setTitle('Mentor '.$mentor['name']);
        SEOTools::setDescription($mentor['about']);
        SEOTools::opengraph()->setUrl(route('view.mentor',['id'=>$mentor['id'],'slug'=>HelperTrait::slugify($mentor['name'])]));
        SEOTools::setCanonical(route('corporations'));
        SEOTools::opengraph()->addProperty('type', 'webPage');
        SEOTools::opengraph()->addProperty('image', secure_asset(env('LOGO').''.$mentor['photo']));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset(env('LOGO').''.$mentor['photo']));

        return view('mentor-view',['mentor'=>$mentor]);
    }


    public function searchMentors(Request $request)
    {

        $where = [];
        $name = $request['name'];
        $district = $request['district'];
        $industry = $request['industry'];
        $sector = $request['sector'];

        // $where['status']= 'Published';

//        if(!is_null($industry)){
//            if(!is_null($sector)){
//                $mentors = Mentor::where($where)->where('sectors', 'like', '%'.$sector.'%')
//                    ->where('industries', 'like', '%'.$industry.'%')->where('name', 'like', '%'.$name.'%')->paginate();
//            }else{
//                $mentors = Mentor::where($where)->where('industries', 'like', '%'.$industry.'%')
//                    ->where('name', 'like', '%'.$name.'%')->paginate();
//            }
//        }else{
            if(!is_null($sector)){
                $mentors = Mentor::where($where)->where('focus_areas', 'like', '%'.$sector.'%')
                    ->where('name', 'like', '%'.$name.'%')->paginate();
            }else{
                $mentors = Mentor::where($where)
                    ->where('name', 'like', '%'.$name.'%')->paginate();
            }
//        }

        SEOTools::setTitle('Mentors- Startup Uganda');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('mentors'));
        SEOTools::setCanonical(route('mentors'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));

        return view('mentors-list',['mentors'=>$mentors]);
    }
}
