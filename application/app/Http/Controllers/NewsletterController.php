<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Newsletter;

class NewsletterController extends Controller
{

    public function store(Request $request)
    {
        $validatedData = Validator::make( $request->all(), [
            'email' => 'required'
        ]);
        if ($validatedData->fails()) {
            foreach ($validatedData->messages()->getMessages() as $field_name => $messages)
            {

                return redirect()->back()->with('failure', 'Sorry! '.$messages[0]);
            }
        }else {
            if (!Newsletter::isSubscribed($request->email)) {
                Newsletter::subscribePending($request->email);
                return redirect()->back()->with('success', 'Thanks For Subscribing to our newsletter');
            }
            return redirect()->back()->with('failure', 'Sorry! You have already subscribed ');
        }
    }
}
