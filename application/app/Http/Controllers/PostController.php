<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Image;
use Auth;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Image as ImageImage;
use Artesaos\SEOTools\Facades\SEOTools;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make(request()->all(), [
            'post_title'=> 'required|string',
            'image' => 'required',
            'body' => 'required',
        ]);
        if ($validatedData->fails()) {
            $error = "";
            foreach ($validatedData->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();

        }else{
            $in_data = $request->all();
            if(isset($request['image'])) {
                $image       = $request->file('image');
                $newName    = Str::random(12);
                $extension = $image->guessExtension();
                $filename = $newName.'.'.$extension;

                $file = $image->move(env('FULL'),$filename);
                $image_resize = Image::make($file);
                $image_resize->fit(250, 250);
                $image_resize->save(env('BANNER').$filename);

                Post::create(['post_title'=>$in_data['post_title'],'image_name'=>$filename,
                    'body'=>$in_data['body'],'posted_by'=>Auth::user()->id,'status'=>'unpublished']);

                flash('Article has been registered. Please consider previewing it befor you publish')->success();

            }else{
                flash("You didn't upload a file")->danger();
            }
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('admin.article',['post'=>$post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }

    public function getArticles(){
        $posts = Post::orderBy('id','desc')->paginate(12);
        return view('admin.articles',['articles'=>$posts]);
    }

    public function publishArticle($id,$status){
        $post = Post::find($id);
        if ($status=='publish')
            $post->update(['status'=>'published']);
        else
            $post->update(['status'=>'unpublished']);

        return redirect()->back();
    }

    public function viewPost($slug){
        $in = explode('_', $slug);
        $post = Post::find(last($in));
        if(is_null($post)){
            return redirect()->back();
        }else{
        $posts = Post::orderBy('id','desc')->where(['status'=>'Published'])
            ->where('id','!=',last($in))->limit(4)->get();

        SEOTools::setTitle($post['post_title']);
        SEOTools::setDescription(substr($post['body'],0,120).'...');
        SEOTools::opengraph()->setUrl(route('post.view',['slug'=>Str::slug($post['post_title']).'_'.$post['id']]));
        SEOTools::setCanonical(route('startups'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('image', secure_asset(env('FULL').$post['image_name']));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset(env('FULL').$post['image_name']));

        return view('article',['post'=>$post,'posts'=>$posts]);
        }

    }
}
