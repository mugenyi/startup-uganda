<?php

namespace App\Http\Controllers;

use App\Mentor;
use App\Program;
use App\Traits\HelperTrait;
use Artesaos\SEOTools\Facades\SEOTools;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Image;
use Illuminate\Support\Str;
use Intervention\Image\Image as ImageImage;

class ProgramController extends Controller
{
    //
    public function getPrograms()
    {
        $programs = Program::where(['added_by'=>Auth::user()->id ])->orderBy('id','desc')->paginate(9);
        return view('user.programs',['programs'=>$programs]);
    }

    public function viewProgram($id,$slug)
    {
        $program = Program::find($id);
        return view('user.view-program',['program'=>$program]);
    }

    public function createProgram(Request $request)
    {
        //validate
        $validatedData = Validator::make(request()->all(), [
            'program_name'=> 'required|string',
            'start_date' => 'required',
            'end_date' => 'required',
            'apply_before' => 'required',
            'organiser' => 'required',
            'location' => 'required',
            'target_audience' => 'required',
            'description' => 'required',
            'summary' => 'required',
        ]);
        if ($validatedData->fails()) {
            $error = "";
            foreach ($validatedData->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();
            return redirect()->back();
        }else{
            $data = $request->all();

            $data['start_date']=date('Y-m-d H:i:s', strtotime($data['start_date']));
            $data['end_date']=date('Y-m-d H:i:s', strtotime($data['end_date']));
            $data['apply_before']=date('Y-m-d H:i:s', strtotime($data['end_date']));
            $data['image'] = env('BUSINESS_PROFILE');
            $data['cover'] = env('BUSINESS_BANNER');
            $data['status'] = "Pending";
            $data['added_by'] = Auth::user()->id;
            $data['description'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['description']);
            $data['summary'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['summary']);

            Program::create($data);
            flash('Program has been successfully registered. Please add image and cover photo then publish, activate the program')->success();
            return redirect()->back();
        }

    }
    public function updateProgram(Request $request)
    {
        //validate
        $validatedData = Validator::make(request()->all(), [
            'program_name'=> 'required|string',
            'start_date' => 'required',
            'end_date' => 'required',
            'apply_before' => 'required',
            'organiser' => 'required',
            'location' => 'required',
            'target_audience' => 'required',
            'description' => 'required',
            'summary' => 'required',
        ]);
        if ($validatedData->fails()) {
            $error = "";
            foreach ($validatedData->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();
            return redirect()->back();
        }else{
            $data = $request->all();
            $program = Program::find($data['programId']);
            $data['description'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['description']);
            $data['summary'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['summary']);

            $program->update($data);
            flash('Program has been successfully registered. Please add image and cover photo then publish, activate the program')->success();
            return redirect()->back();
        }

    }

    public function uploadLogo(Request $request)
    {
        if($request->hasFile('upload_logo')){
            $image       = $request->file('upload_logo');
            $newName    = Str::random(12);
            $extension = $image->guessExtension();
            $filename = $newName.'.'.$extension;

            $file = $image->move('public/uploads/full/',$filename);
            $image_resize = Image::make($file);
            $image_resize->fit(100, 100);
            $image_resize->save('public/uploads/logo/'.$filename);

            $startup = Startup::find($request['id'])->update(['logo'=>$filename]);
            flash('Logo successfully updated')->success();

        }else{
            flash('You didnot upload a file')->danger();
        }
        return redirect()->back();


    }


    public function bannerUpload(Request $request){
        if(isset($request['image'])) {
            $data = $request['image'];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);

            $data = base64_decode($image_array_2[1]);
            $name = Str::random(12) . '.png';
            $full_image = env('FULL') . $name;
            file_put_contents($full_image, $data);

            $image_cover = Image::make($data);
            $image_cover->fit(300,230);
            $image_cover->save(env('COVER').$name);


            $banner = env('BANNER') . $name;
            file_put_contents($banner, $data);

            $startup = Program::find($request['id'])->update(['image' => $name, 'cover' => $name]);
            flash('Cover has been successfully updated');
            return redirect()->back();
        }else{
            flash("You didn't upload a file")->danger();
        }
        return redirect()->back();
    }


    public function uploadCover(Request $request)
    {
        $validatedData = Validator::make( $request->all(), [
            'upload_cover' => 'required|mimes:jpeg,png,jpg,gif,svg',
            'id' => 'required'
        ]);
        if ($validatedData->fails()) {
            foreach ($validatedData->messages()->getMessages() as $field_name => $messages)
            {
                flash('Error: '.$messages[0])->warning();
            }
        }else{
            if($request->hasFile('upload_cover')){
                $image       = $request->file('upload_cover');
                $newName    = Str::random(12);
                $extension = $image->guessExtension();
                $filename = $newName.'.'.$extension;
                // $width = Image::make('public/foo.jpg')->width();
                // $height = Image::make('public/foo.jpg')->height();

                $file = $image->move('public/uploads/full/',$filename);

                $image_resize = Image::make($file);
                $image_resize->fit(800,360);
                $image_resize->save('public/uploads/banner/'.$filename);

                $image_cover = Image::make($file);
                $image_cover->fit(250,250);
                $image_cover->save('public/uploads/cover/'.$filename);


                $program = Program::find($request['id'])->update(['image'=>$filename,'cover'=>$filename]);
                flash('Cover has been successfully updated');

            }else{
                flash('You didn\'t upload a file')->danger();
            }
        }
        return redirect()->back();
    }



    public function programsListing()
    {
        $programs = Program::whereNotIn('added_by',['3'])->where(['status'=>'Active'])
            ->orderBy('id','desc')->paginate(12);

        SEOTools::setTitle('Programs- Startup Uganda');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('programs'));
        SEOTools::setCanonical(route('programs'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));
        return view('programs-list',['programs'=>$programs]);
    }

    public function programDetails($id,$slug)
    {
        //if(Auth::check()){

            $program = Program::find($id);

            SEOTools::setTitle($program['program_name']);
            SEOTools::setDescription($program['summary']);
            SEOTools::opengraph()->setUrl(route('view.program',['id'=>$program['id'],'slug'=>HelperTrait::slugify($program['program_name'])]));
            SEOTools::setCanonical(route('view.program',['id'=>$program['id'],'slug'=>HelperTrait::slugify($program['program_name'])]));
            SEOTools::opengraph()->addProperty('type', 'articles');
            SEOTools::opengraph()->addProperty('image', secure_asset('public/uploads/banner/'.$program['image']));
            SEOTools::twitter()->setSite('@StartupUganda_');
            SEOTools::jsonLd()->addImage(secure_asset('public/uploads/banner/'.$program['image']));

            return view('program-view',['program'=>$program]);
//        }else{
//            session()->put('intended-url',url()->current());
//            return redirect()->route('register')->withErrors(['message'=>'Please register to view our programs']);
//        }

    }


    public function searchPrograms(Request $request)
    {

        $where = [];
        $name = $request['name'];
        $district = $request['district'];
        $industry = $request['industry'];
        $sector = $request['sector'];

        // $where['status']= 'Published';

        if(!is_null($industry)){
            if(!is_null($sector)){
                $mentors = Program::where($where)->where('sectors', 'like', '%'.$sector.'%')
                    ->where('industries', 'like', '%'.$industry.'%')->where('name', 'like', '%'.$name.'%')->paginate();
            }else{
                $mentors = Program::where($where)->where('industries', 'like', '%'.$industry.'%')
                    ->where('name', 'like', '%'.$name.'%')->paginate();
            }
        }else{
            if(!is_null($sector)){
                $mentors = Program::where($where)->where('sectors', 'like', '%'.$sector.'%')
                    ->where('name', 'like', '%'.$name.'%')->paginate();
            }else{
                $mentors = Program::where($where)->where('sectors', 'like', '%'.$sector.'%')
                    ->where('name', 'like', '%'.$name.'%')->paginate();
            }
        }

        SEOTools::setTitle('Programs- Startup Uganda');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('programs'));
        SEOTools::setCanonical(route('programs'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));

        return view('programs-list',['programs'=>$programs]);
    }
}
