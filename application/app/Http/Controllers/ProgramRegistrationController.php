<?php

namespace App\Http\Controllers;

use App\Exports\ProgramRegistrationExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Illuminate\Support\Str;
use App\Program;
use App\ProgramRegistration;
use App\Mail\ProfileRegistry;
use App\Mail\ProgramRegistry;
use Illuminate\Support\Facades\Mail;

class ProgramRegistrationController extends Controller
{
    //
    public function getRegistrations(){
        $regs = ProgramRegistration::orderBy('id','desc')->paginate(15);
        return view('admin.registrations',['registrations'=>$regs]);
    }

    public function getProgramRegistrations($program){
        $regs = ProgramRegistration::where('program',$program)->orderBy('id','desc')->paginate(15);
        return view('admin.registrations',['registrations'=>$regs]);
    }

    public function exportRegistry(){
        $fileName = time().'tasks.csv';
        $result = ProgramRegistration::orderBy('id','desc')->get();
        //dd($result);

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=maurice.csv",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );
        $columns  = array('Program','Name', 'Email', 'Phone', 'Sector','Company','Company registration','Years of Operation',
            'Investment Ask','Affiliation','Pitchdeck','Team Members');

        $callback = function() use($result, $columns) {

            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($result as $res) {
                //dd($res);
                fputcsv($file, array(
                        $res->programDetail->program_name,$res->name,$res->email,$res->phone,$res->sector,
                        $res->company, $res->company_registration,$res->operation_years,$res->investment_size,
                        $res->affliation,$res->pitchdeck,$res->team_members)
                );
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }

public function exportProgramRegistry($program){
        $file = time().'-program-registrations.xlsx';
    return Excel::download(new ProgramRegistrationExport($program),$file);
//
//    $fileName = 'tasks.csv';
//    $results = ProgramRegistration::where('program',$program)->orderBy('id','desc')->get()->toArray();
//
//    $headers = array(
//        "Content-type"        => "text/csv",
//        "Content-Disposition" => "attachment; filename=$fileName",
//        "Pragma"              => "no-cache",
//        "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
//        "Expires"             => "0"
//    );
//
//    $columns = array('Program','Name', 'Email', 'Phone', 'Sector','Company','Company registration','Years of Operation',
//        'Investment Ask','Affiliation','Pitchdeck','Team Members');
//
//    $callback = function() use($results, $columns) {
//        $file = fopen('php://output', 'w');
//        fputcsv($file, $columns);
//
//        foreach ($results as $res) {
//            fputcsv($file, array($res->program_detail->program_name,$res->name,$res->email,$res->phone,$res->sector,
//                $res->company, $res->company_registration,$res->operation_years,$res->investment_size,
//                $res->affliation,$res->pitchdeck,$res->team_members));
//        }
//
//        fclose($file);
//    };
//
//    return response()->stream($callback, 200, $headers);

    }

    public function viewRegistration($id){
        $reg = ProgramRegistration::find($id);
        if(is_null($reg)){
            flash('Unknown registration ID')->warning();
            return redirect()->back();
        }
        return view('admin.registration',['reg'=>$reg]);
    }


    public function register(Request $request){
     $validated = Validator::make(request()->all(), [
                'name'=> 'required|string',
                'email'=> 'required',
                'phone'=> 'required',
                'sector'=> 'required',
                'company'=> 'required',
                'company_registration'=> 'required',
                'team_members'=> 'required',
                'operation_years'=> 'required',
                'investment_size'=> 'required',
                'affliation'=> 'required',
                'pitchdeck'=> 'required',
                'program'=> 'required',
            ]);
            if ($validated->fails()) {
                       // dd($validated->messages());
                        $error = "Please fill up all the required fields";
//                        foreach ($validated->messages()->getMessages() as $field_name => $messages){
//                            $error .= $messages;
//                        }
                        flash($error)->warning();
                        return redirect()->back();
                    }
                    else{
                        $data = $request->all();
                        $check = ProgramRegistration::where('email',$data['email'])->where('program',$data['program'])->first();

                        if(is_null($check)){
                            $filename = "Upload File";
                            if($request->hasFile('pitchdeck')){
                                $image= $request->file('pitchdeck');
                                $newName    = Str::random(12);
                                $extension = $image->guessExtension();
                                $filename = $newName.'.'.$extension;

                                $file = $image->move('public/uploads/full/',$filename);
                            }
                            $data['pitchdeck'] = $filename;
                            $registry = ProgramRegistration::create($data);
                            $program = Program::find($data['program']);

                            //Send loan email to user
                           // Mail::to('info@startupug.com')->send(new ProgramRegistry($registry,$program));
                            flash("You've been successfully registered for this event. Our team will be in touch for details")->success();
                            return redirect()->back();
                        }else{
                            $error = "You are already registered for this Program";
                            flash($error)->warning();
                            return redirect()->back();
                        }

                    }

    }

}
