<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Socialite;
use Auth;

class SocialController extends Controller
{
    public function redirect($provider)
    {
     return Socialite::driver($provider)->redirect();
    }

    public function Callback($provider){
        if ($provider =='twitter') {
            $userSocial =   Socialite::driver($provider)->user();
            $userSocial->email = 'twitterUser_'.rand(0000001,9999999).'_@startupug.com';
            $users = User::where(['provider'=>$provider,'provider_id'=>$userSocial->id])->first();
        }else {
            $userSocial =   Socialite::driver($provider)->stateless()->user();
            $users = User::where(['email'=>$userSocial->email])->first();
        }

        /****Check if User exists ****/
        if($users)
        {
            Auth::login($users);
            return redirect('/home');
        }
        else{
            $user = User::create([
                        'name'          => $userSocial->name,
                        'email'         => $userSocial->email,
                        //'image'         => $userSocial->avatar,
                        'provider_id'   => $userSocial->id,
                        'provider'      => $provider,
                        'image' =>env('USER_PROFILE'),
                        'role' => 'user',
                    ]);
                    Auth::login($user);
                    if (strpos($user->email, 'twitterUser_') !== false) {
                        flash('Please consider setting your correct email address by editing your profile')->warning();
                    }
                return redirect()->route('user.dashboard');
                }
        }
}
