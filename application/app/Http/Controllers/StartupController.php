<?php

namespace App\Http\Controllers;

use App\District;
use App\Job;
use App\Startup;
use App\Team;
use App\Traits\HelperTrait;
use App\User;
use Illuminate\Http\Request;
    use Validator;
    use Illuminate\Support\Str;
    use Artesaos\SEOTools\Facades\SEOTools;
    // import the Intervention Image Manager Class
    use Image;
    use Auth;

class StartupController extends Controller
{
    //
    use HelperTrait;

    public function createStartup(Request $request)
    {

        $validated = Validator::make(request()->all(), [
            'name'=> 'required|string',
            'tagline'=> 'required',
            'registration'=> 'required',
            'user_id'=> 'required','address'=> 'required',
            'phone_number'=> 'required','consumer_model'=> 'required','email'=> 'required',
            'stage'=> 'required',
            'about'=> 'required',
            'problem'=> 'required',
            'solution'=> 'required',
            'traction'=> 'required',
        ]);
        if ($validated->fails()) {
           // dd($validated->messages());
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages;
            }
            flash($error)->warning();
            return redirect()->back();
        }else{
            $data = $request->all();
            $consumer_model="";
            foreach($data['consumer_model'] as $a){
                $consumer_model .=$a.', ';
            }

            $data['about'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['about']);
            $data['problem'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['problem']);
            $data['solution'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['solution']);
            $data['traction'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['traction']);
            $data['consumer_model'] = $consumer_model;
            $data['registration_status'] = $data['registration']=="true"?true:false;
            $data['status'] = 'Pending';
            $data['logo'] = env('BUSINESS_LOGO');
            $data['profile_photo'] = env('BUSINESS_PROFILE');

            $data['banner'] = env('BUSINESS_BANNER');
            $data['slug'] = $this->createSlug($data['name'],'startup');
            if (isset($data['industries'])) {
                $industries = "";
                foreach ($data['industries'] as $a) {
                    $industries .= $a . ', ';
                }
                $data['industries'] = $industries;
            }if (isset($data['sectors'])) {
                $sectors="";
                foreach($data['sectors'] as $d){
                    $sectors .=$d.', ';
                }
                $data['sectors'] = $sectors;
            }


            $startup = Startup::create($data);

            flash('Startup has been successfully registered. Please upload your logo, banner and pitch deck')->success();
            if(Auth::user()->role =='admin'){
                return redirect()->route('admin.startup.view',['id'=>$startup['id']]);
            }
            else{
                return redirect()->route('user.view.startup',['slug'=>$startup['slug']]);
            }

        }

    }

    //'pitchDeck','logo','banner',

    public function startupListing()
    {
        $startups = Startup::where(['status'=>'Published'])->paginate();

        SEOTools::setTitle('Startup Uganda Ventures');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('startups'));
        SEOTools::setCanonical(route('startups'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::opengraph()->addProperty('image', secure_asset('public/theme/images/logo.png'));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));

        return view('startup-list',['startups'=>$startups]);
    }

    public function startupDetails($slug)
    {
        $startup = Startup::with('jobs')->where(['slug'=>$slug])->first();
        $team = Team::where(['category'=>'startup','category_id'=>$startup['id'],'memberType'=>'teamMember'])->get();
        $board = Team::where(['category'=>'startup','category_id'=>$startup['id'],'memberType'=>'boardMember'])->get();

        SEOTools::setTitle($startup['name']);
        SEOTools::setDescription($startup['tagline']);
        SEOTools::opengraph()->setUrl(route('startup.details',['slug'=>$startup['slug']]));
        SEOTools::setCanonical(route('startups'));
        SEOTools::opengraph()->addProperty('type', 'webPage');
        SEOTools::opengraph()->addProperty('image', secure_asset(env('BANNER').''.$startup['banner']));
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset(env('BANNER').''.$startup['banner']));

        return view('startup-view',['startup'=>$startup,'teams'=>$team,'boards'=>$board]);
    }



    public function editStartup($slug)
    {
        $startup = Startup::where(['slug'=>$slug])->first();
        if(!is_null($startup)){
            $districts = District::select('district')->get();
            return view('user.edit-startup',['startup'=>$startup,'districts'=>$districts]);
        }
        flash('Unknown Startup, please avoid tampering with the address bar')->warning();
        return redirect()->back();
    }



        public function getUserStartup($slug)
        {
            $startup = Startup::where(['slug'=>$slug])->first();
            $team = Team::where(['category'=>'startup','category_id'=>$startup->id])->get();
            return view('user.startup-view',['startup'=>$startup,'teams'=>$team]);
        }

        public function uploadLogo(Request $request)
        {
            if($request->hasFile('upload_logo')){
                $image       = $request->file('upload_logo');
                $newName    = Str::random(12);
                $extension = $image->guessExtension();
                $filename = $newName.'.'.$extension;

                $file = $image->move('public/uploads/full/',$filename);
                $image_resize = Image::make($file);
                $image_resize->fit(100, 100);
                $image_resize->save('public/uploads/logo/'.$filename);

                $startup = Startup::find($request['id'])->update(['logo'=>$filename]);
                flash('Logo successfully updated')->success();

            }else{
                flash('You didnot upload a file')->danger();
            }
            return redirect()->back();


        }

        public function bannerUpload(Request $request){
        //dd($request);
            if(isset($request['image'])) {
                $data = $request['image'];
                $image_array_1 = explode(";", $data);
                $image_array_2 = explode(",", $image_array_1[1]);

                $data = base64_decode($image_array_2[1]);
                $name = Str::random(12) . '.png';
                $full_image = env('FULL') . $name;
                file_put_contents($full_image, $data);

                $image_cover = Image::make($data);
                $image_cover->fit(300,230);
                $image_cover->save(env('COVER').$name);

                $banner = env('BANNER') . $name;
                file_put_contents($banner, $data);

                $startup = Startup::find($request['id'])->update(['banner' => $name, 'profile_photo' => $name]);
                flash('Cover has been successfully updated');
                return redirect()->back();
            }else{
                flash('You didnot upload a file')->danger();
            }
            return redirect()->back();
        }

        public function uploadCover(Request $request)
        {
            $validatedData = Validator::make( $request->all(), [
                'upload_cover' => 'required|mimes:jpeg,png,jpg,gif,svg',
                'id' => 'required'
            ]);
             if ($validatedData->fails()) {
                foreach ($validatedData->messages()->getMessages() as $field_name => $messages)
                {
                    flash('Error: '.$messages[0])->warning();
                }
            }else{
            if($request->hasFile('upload_cover')){
                $image       = $request->file('upload_cover');
                $newName    = Str::random(12);
                $extension = $image->guessExtension();
                $filename = $newName.'.'.$extension;

                $file = $image->move(env('FULL'),$filename);

                $width = Image::make(env('FULL').$filename)->width();
                 $height = Image::make(env('FULL').$filename)->height();
                 if($width < 860 || $height < 400){
                    flash('Error: Uploaded file it too small. Please consider 860px width and 400px height')->warning();
                 }
                 else{
                    $image_resize = Image::make($file);
                    $image_resize->fit(820,360);
                    $image_resize->save(env('BANNER').$filename);

                    $image_cover = Image::make($file);
                    $image_cover->fit(250,250);
                    $image_cover->save(env('COVER').$filename);

                    $startup = Startup::find($request['id'])->update(['banner'=>$filename,'profile_photo'=>$filename]);
                    flash('Cover has been successfully updated');
                 }


            }else{
                flash('You didnot upload a file')->danger();
            }
        }
            return redirect()->back();


        }

        public function uploadPitchDeck(Request $request)
        {
            //dd($request);
             $validatedData = Validator::make( $request->all(), [
                'pitch-deck' => 'required|mimes:pdf,doc,docx,ppt,pptx,jpg,jpeg,png',
                'id' => 'required'
            ]);
            if ($validatedData->fails()) {
                foreach ($validatedData->messages()->getMessages() as $field_name => $messages)
                {
                    flash('Error: '.$messages[0])->warning();
                }
            }else{

                if($request->hasFile('pitch-deck')){
                    $file      = $request->file('pitch-deck');
                    $newName    = Str::random(12);
                    $extension = $file->guessExtension();
                    $filename = $newName.'.'.$extension;

                    $file = $file->move('public/uploads/pitch/',$filename);

                    $startup = Startup::find($request['id'])->update(['pitchDeck'=>$filename]);
                    flash('Pitch-deck has been successfully updated');

                }else{
                    flash('You didnot upload a file');
                }
            }
            return redirect()->back();

        }

        public function UpdateStartup(Request $request)
        {
            $startup = Startup::find($request['startup_id']);

            $validated = Validator::make(request()->all(), [
                'startup_id'=> 'required',
                'name'=> 'required|string',
                'tagline'=> 'required',
                'registration'=> 'required',
                'user_id'=> 'required','address'=> 'required',
                'phone_number'=> 'required','consumer_model'=> 'required','email'=> 'required',
                'stage'=> 'required',
                'about'=> 'required',
                'problem'=> 'required','solution'=> 'required',
                'traction'=> 'required',
            ]);
            if ($validated->fails()) {
                $error = "";
                foreach ($validated->messages()->getMessages() as $field_name => $messages){
                    $error .=$messages[0];
                }
                flash($error)->warning();
            }else{
                $data = $request->all();
                $data['name'] = $data['name'];
                $data['about'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['about']);
                $data['problem'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['problem']);
                $data['solution'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['solution']);
                $data['traction'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $data['traction']);

                $consumer_model="";
                foreach($data['consumer_model'] as $a){
                    $consumer_model .=$a.', ';
                }
                $data['consumer_model'] = $consumer_model;
                $data['registration_status'] = $data['registration']=="true"?true:false;


                $startup->update($data);
                flash('Startup has been successfully registered. Please upload your logo, banner and pitch deck')->success();
            }
            return redirect()->route('user.view.startup',['slug'=>$startup['slug']]);
        }

        public function editSectors(Request $request)
        {
            $startup = Startup::find($request['startup_id']);

            $validated = Validator::make(request()->all(), [
                'startup_id'=> 'required',
                'industries'=> 'required',
                'sectors'=> 'required'
            ]);
            if ($validated->fails()) {
                $error = "";
                foreach ($validated->messages()->getMessages() as $field_name => $messages){
                    $error .=$messages[0];
                }
                flash($error)->warning();
            }else{
                $data = $request->all();
                $industries="";
                foreach($data['industries'] as $a){
                    $industries .=$a.', ';
                }
                $sectors="";
                foreach($data['sectors'] as $d){
                    $sectors .=$d.', ';
                }

                $startup->update(['sectors'=>$sectors,'industries'=>$industries]);
                flash('Startup have been successfully updated')->success();

            }
            return redirect()->back();
        }

        public function searchStartups(Request $request)
        {
            $where = [];
        $name = $request['name'];
        $district = $request['district'];
        $industry = $request['industry'];
        $sector = $request['sectors'];
        $fundraising = $request['fundraising'];
        $lookingToRaise = $request['lookingToRaise'];
        $revenue = $request['revenue'];


        $where['status']= 'Published';
        if(!is_null($revenue))
            $where['rev_generating']=$revenue=='Yes'? true:false;

        if(!is_null($fundraising))
            $where['rev_generating']=$fundraising=='Yes'? true:false;

        if(!is_null($district))
            $where['district']=$district;

        if(!is_null($district))
            $where['district']=$district;

            if(!is_null($name)){
                if(!is_null($sector)){
                    if(!is_null($industry)){
                        if(!is_null($lookingToRaise)){
                        $startups = Startup::where($where)->where('sectors', 'like', '%'.$sector.'%')
                            ->where('industries', 'like', '%'.$industry.'%')->where('name', 'like', '%'.$name.'%')
                            ->where('raise_amount','<=',$lookingToRaise)->paginate();
                        }else{
                            $startups = Startup::where($where)->where('sectors', 'like', '%'.$sector.'%')
                        ->where('industries', 'like', '%'.$industry.'%')->where('name', 'like', '%'.$name.'%')->paginate();
                        }
                    }else{
                        if(!is_null($lookingToRaise)){
                            $startups = Startup::where($where)->where('sectors', 'like', '%'.$sector.'%')
                            ->where('name', 'like', '%'.$name.'%')->where('raise_amount','<=',$lookingToRaise)->paginate();
                        }else{
                            $startups = Startup::where($where)->where('sectors', 'like', '%'.$sector.'%')
                            ->where('name', 'like', '%'.$name.'%')->paginate();
                        }
                    }

                }else{
                    if(!is_null($lookingToRaise)){
                        $startups = Startup::where($where)->Where('name', 'like', '%'.$name.'%')
                        ->where('raise_amount','<=',$lookingToRaise)->paginate();
                    }else{
                        $startups = Startup::where($where)->Where('name', 'like', '%'.$name.'%')->paginate();
                    }

                }

            }else{
                if(!is_null($sector)){
                    if(!is_null($industry)){
                        if(!is_null($lookingToRaise)){
                            $startups = Startup::where($where)->where('sectors', 'like', '%'.$sector.'%')
                            ->where('industries', 'like', '%'.$industry.'%')->where('raise_amount','<=',$lookingToRaise)->paginate();
                        }else{
                            $startups = Startup::where($where)->where('sectors', 'like', '%'.$sector.'%')
                            ->where('industries', 'like', '%'.$industry.'%')->paginate();
                        }
                    }else{
                        if(!is_null($lookingToRaise)){
                            $startups = Startup::where($where)->where('sectors', 'like', '%'.$sector.'%')
                            ->where('raise_amount','<=',$lookingToRaise)->paginate();
                        }
                        else{
                            $startups = Startup::where($where)->where('sectors', 'like', '%'.$sector.'%')->paginate();
                        }
                    }
                }else{
                    if(!is_null($lookingToRaise)){
                        $startups = Startup::where($where)->where('raise_amount','<=',$lookingToRaise)->paginate();
                    }else{
                        $startups = Startup::where($where)->paginate();
                    }
                }
            }

        SEOTools::setTitle('Startup Uganda Ventures');
        SEOTools::setDescription('Experience the startup ecosystem — invest in startups, research the fastest-growing companies, and find a job you love.');
        SEOTools::opengraph()->setUrl(route('startups'));
        SEOTools::setCanonical(route('startups'));
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::twitter()->setSite('@StartupUganda_');
        SEOTools::jsonLd()->addImage(secure_asset('public/theme/images/logo.png'));


            return view('startup-list',['startups'=>$startups]);
        }

}
