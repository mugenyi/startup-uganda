<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
// import the Intervention Image Manager Class
use Image;
use Validator;

class TeamController extends Controller
{
    //
    public function addTeamMember(Request $request)
    {

        $validated = Validator::make(request()->all(), [
            'name'=> 'required|string',
            'designation'=> 'required',
            'category'=> 'required',
            'category_id'=> 'required',
            'memberType'=> 'required',
        ]);
        if ($validated->fails()) {
            dd($validated->messages());
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages;
            }
            flash($error)->warning();
        }else{
            if($request->hasFile('photo')){
                $image       = $request->file('photo');
                $newName    = Str::random(12);
                $extension = $image->guessExtension();
                $filename = $newName.'.'.$extension;

                $file = $image->move('public/uploads/full/',$filename);
                $image_resize = Image::make($file);
                $image_resize->fit(120, 120);
                $image_resize->save('public/uploads/user/'.$filename);
                flash('Team member has been successfully added')->success();
            }else{
                $filename = 'avatar.png';
            }
            $team = Team::create(['member_name'=>$request['name'],'designation'=>$request['designation'],'photo'=>$filename,
            'category'=>$request['category'],'category_id'=>$request['category_id'],'memberType'=>$request['memberType']]);
        }

        return redirect()->back();
    }
}
