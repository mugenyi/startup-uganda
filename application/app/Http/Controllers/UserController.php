<?php

namespace App\Http\Controllers;

use App\Corporate;
use App\District;
use App\Eso;
use App\Government;
use App\Investor;
use App\Mentor;
use App\Startup;
use App\Traits\HelperTrait;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    public function dashboard()
    {
        $data = [];
        $data['startups'] = Startup::where(['user_id'=>Auth::user()->id])->get();
        $data['investors'] = Investor::where(['user_id'=>Auth::user()->id])->get();
        $data['governments'] = Government::where(['user_id'=>Auth::user()->id])->get();
        $data['mentors'] = Mentor::where(['user_id'=>Auth::user()->id])->get();

        return view('user.dashboard',['data'=>$data]);
    }

    public function editProfile()
    {
        $user = User::find(Auth::user()->id);
        $districts = District::select('district')->get();

        return view('user.edit-profile',['user'=>$user,'districts'=>$districts]);
    }

    public function editUserProfile(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $user = User::find($request->userId);
        $user->update($data);
        return redirect()->route('user.dashboard');

    }

    public function profilePhotoUpdate(Request $request){
        if($request->hasFile('photo')){
            $image       = $request->file('photo');
            $newName    = Str::random(12);
            $extension = $image->guessExtension();
            $filename = $newName.'.'.$extension;

            $file = $image->move('public/uploads/',$filename);
            $image_resize = Image::make($file);
            $image_resize->fit(200, 200);
            $image_resize->save('public/uploads/'.$filename);

            $user = User::find($request['user_id'])->update(['image'=>$filename]);
            flash('Logo successfully updated')->success();

        }else{
            flash('You didnot upload a file')->danger();
        }
        return redirect()->back();
    }

    public function PasswordUpdate(Request $request){
        $validated = Validator::make(request()->all(), [
            'user_id'=> 'required',
            'current'=> 'required',
            'new_password'=> 'required',
            'confirm_password'=> 'required',
        ]);
        if ($validated->fails()) {
            // dd($validated->messages());
            $error = "";
            foreach ($validated->messages()->getMessages() as $field_name => $messages){
                $error .=$messages[0];
            }
            flash($error)->warning();

        }else{
            $user = User::find($request['user_id']);
            if($request['new_password']==$request['confirm_password']){
                if(Hash::check($request['new_password'], $user->password)){
                    $user->update(['password'=>Hash::make($request['new_password'])]);
                }else{
                    flash("Entered password doesn't match your current password. Please try again")->warning();
                }
            }else{
                flash("New Passwords aren't a match. Please try again")->warning();

            }
        }

        return redirect()->back();
    }

    public function userProfile()
    {
        $user = User::find(Auth::user()->id);
        return view('user.my-profile',['user'=>$user]);
    }

    public function feed()
    {
        return view('user.feed');
    }

    public function addListing()
    {
        return view('user.add-entity');
    }
    public function addFiles($category,$slug)
    {
        if($category=='startup'){
            $entity = Startup::where(['slug'=>$slug])->first();
        }
        return view('user.add-files',['entity'=>$entity,'category'=>$category]);
    }

    public function registerStartup()
    {
        $districts = District::select('district')->get();
        return view('user.new-startup',['districts'=>$districts]);
    }




    /*****************************
     * CLAIM PROFILE
     * ***************************/
    public function getClaimProfile($endpoint)
    {
        $params =  explode("_", HelperTrait::cyper($endpoint,'dencrypt'));
        $type = $params[0];
        $id = $params[1];

        //check types
        switch ($type) {
            case 'startup':
              $bus = Startup::find($id);
              break;
            case 'investor':
                $bus = Investor::find($id);
                break;
            case 'mentor':
                $bus = Mentor::find($id);
            break;
                case 'eso':
                $bus = Eso::find($id);
            break;
            case 'government':
                $bus = Government::find($id);
            break;

            case 'corporation':
                $bus = Corporate::find($id);
            break;

            default:
              $bus = null;


          }
          $bus['category'] = $type;

          return view('auth.register',['business'=>$bus]);
    }


    public function logout(){
        Auth::logout();
        return redirect()->route('login');
    }


}

