<?php

namespace App\Http\Middleware;
use Auth;

use Closure;
use Illuminate\Support\Facades\Redirect;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

            if (Auth::check() && Auth::user()->role != 'admin') {
                if ($request->wantsJson()) {
                    return response()->json(['Message', 'You do not access to this module.'], 403);
                }
                abort(403, 'You do not access to this module.');
            }
            return $next($request);

    }
}
