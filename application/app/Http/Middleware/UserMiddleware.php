<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Support\Facades\Redirect;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

            if (Auth::check() && Auth::user()->role != 'user') {
                if ($request->wantsJson()) {
                    return response()->json(['Message', 'You do not access to this module.'], 403);
                }
                abort(403, 'You do not access to this module.');
            }
            return $next($request);

    }
}
