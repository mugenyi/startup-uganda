<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'user/startup/upload/banner','user/investor/upload/banner',
        'user/eso/upload/banner','user/government/upload/banner',
        'user/corporate/upload/banner','user/program/upload/banner',
        'user/event/upload/banner',
        'admin/startup/upload/banner','admin/eso/upload/banner','admin/event/upload/banner',
        'admin/program/upload/banner'
    ];
}
