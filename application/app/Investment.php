<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investment extends Model
{
    protected $fillable = ['category','category_id','company_name','company_logo','company_type','amount','round'];
}
