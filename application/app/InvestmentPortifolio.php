<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestmentPortifolio extends Model
{
    //
    protected $with = ['investor'];

    protected $fillable = ['investor_id','organisation_name','amount','round','other_investors'];
}
