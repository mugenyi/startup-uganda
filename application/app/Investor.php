<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Musonza\Chat\Traits\Messageable;

class Investor extends Model
{
    use Messageable;
    //protected $with = ['investorPortifolio'];
    protected $with = ['user'];

    protected $fillable = ['name','legal_name','founded_on','user_id','tagline','investor_type','about','about','experience','address',
    'phone_number','email','facebook','twitter','linkedIn','website','investment_industries','investment_sectors','logo','banner','profile_photo','status','slug'];

    public function jobs()
    {
        return $this->hasMany(Job::class,'company')->where('company_type','investor');
    }


    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
