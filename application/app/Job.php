<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    //
    protected $fillable = ['job_title','company_type','company','location','job_type','skills','description','ends_at','application_link','status','added_by'];


}
