<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClaimEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $sub;
    public $endpoint;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sub,$endpoint)
    {
        $this->sub = $sub;
        $this->endpoint = $endpoint;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.claim');
    }
}
