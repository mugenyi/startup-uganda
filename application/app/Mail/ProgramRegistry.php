<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProgramRegistry extends Mailable
{
    use Queueable, SerializesModels;
    public $registry;
    public $program;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data,$program)
    {
        $this->registry = $data;
        $this->program = $program;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.registry');
    }
}
