<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Musonza\Chat\Traits\Messageable;

class Mentor extends Model
{
    //
    use Messageable;

    protected $with = ['user'];

    protected $fillable = ['name','about','gender','email','phone_number','focus_areas','what_i_do','achievements','looking_for','location','markets','photo','user_id','status'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getParticipantDetailsAttribute()
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
        ];
    }
}
