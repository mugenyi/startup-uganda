<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $with = ['posted'];

    protected $fillable = ['post_title','image_name','body','posted_by','status'];

    public function posted(){
        return $this->belongsTo(User::class,'posted_by');
    }
}
