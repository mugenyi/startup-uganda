<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    //
    protected $with = ['user'];

    protected $fillable = ['program_name','start_date','end_date','apply_before','organiser','location','target_audience','summary','description','added_by','status','image','cover','link'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
