<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramRegistration extends Model
{
    //
    protected $with = ['programDetail'];

    protected $fillable =['name','email','phone','sector','company','company_registration','team_members',
        'operation_years','investment_size','affliation','pitchdeck','program'];

    public function programDetail()
        {
            return $this->belongsTo(Program::class,'program');
        }
}

