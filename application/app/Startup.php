<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Musonza\Chat\Traits\Messageable;

class Startup extends Model
{
    use Messageable;
    //
    protected $with = ['user'];

    protected $fillable = ['name','slug','tagline','founded_on','registration_status','user_id','address','district','phone_number','consumer_model','email','facebook',
    'twitter','linkedIn','youtube','website','stage','about','problem','solution','traction','pitchDeck','logo','profile_photo','banner','status','industries','sectors',
    'rev_generating','fundraising','raise_amount','round','is_featured'];


    public function jobs()
    {
        return $this->hasMany(Job::class,'company')->where('company_type','startup');
    }

    public function teams()
    {
        return $this->hasMany(Team::class)->where(['category'=>'startup','category_id'=>$this->id]);
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }



}
