<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    //
    protected $fillable = ['member_name','designation','photo','category','category_id','memberType'];

    public function startupTeam()
    {
        return $this->belongsTo(Startup::class,'category_id')->where('category','startup');
    }
}
