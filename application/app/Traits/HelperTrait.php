<?php

namespace App\Traits;

use App\Corporate;
use App\District;
use App\Eso;
use App\Event;
use App\Feed;
use App\Government;
use App\Industry;
use App\Investment;
use App\Investor;
use App\Mentor;
use App\Program;
use App\Sector;
use Illuminate\Support\Str;
use App\Startup;
use App\User;
use Auth;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;
use Chat;

trait HelperTrait
{
    public static function createSlug($name,$type)
    {
       $slug = Str::slug($name);
       if($type=='startup')
            $check = Startup::where(['slug'=>$slug])->count();
       elseif($type=='investor')
            $check = Investor::where(['slug'=>$slug])->count();

       if($check > 0 ){
        HelperTrait::createSlug($name.' '.rand(1,100),$type);
        }

       return $slug;
    }

    /**
     * Get Districts
     * @return mixed
     */
    public static function getDistricts()
    {
        $districts = District::select('district')->orderBy('district','asc')->get();
        return $districts;
    }

    /**
     * Get Industries
     * @return mixed
     */
    public static function getIndustries()
    {
        $industries = Industry::select('industry')->get();
        return $industries;
    }

    /**
     * Get Sectors
     * @return mixed
     */
    public static function getSectors()
    {
        $sectors = Sector::select('sector_name')->get();
        return $sectors;
    }

    public static function StringCompare($haystack,$needle)
    {
        if(strpos($haystack, $needle) !== false){
            return 'selected';
        } else{
            return '';
        }
    }

    public static function stringSector($sectors)
    {
        $sectorString = "";
        foreach($sectors as $sector){
            $sectorString .=$sector['sector_name'].' , ';
        }
        return $sectorString;
    }

    public static function getCategoryModel($category,$id)
    {
        switch(strtolower($category)){
            case 'startup':
                $from = Startup::find($id);
            break;
            case 'investor':
                $from = Investor::find($id);
            break;
            case 'mentor':
                $from = Mentor::find($id);
            break;
            case 'eso':
                $from = Eso::find($id);
            break;
            case 'government':
                $from = Government::find($id);
            break;
            case 'corporate':
                $from = Corporate::find($id);
            break;

            case 'user':
                $from = User::find($id);
            break;
        }
        return $from;
    }

    public static function getModelProfilePhoto($category,$id)
    {
        switch(strtolower($category)){
            case 'startup':
                $from = Startup::find($id);
                return 'logo/'.$from->logo;
            break;
            case 'investor':
                $from = Investor::find($id);
                return 'logo/'.$from->logo;
            break;
            case 'mentor':
                $from = Mentor::find($id);
                return 'logo/'.$from->photo;
            break;
            case 'eso':
                $from = Eso::find($id);
                return 'logo/'.$from->photo;
            break;
            case 'government':
                $from = Government::find($id);
                return 'logo/'.$from->logo;
            break;
            case 'corporation':
                case 'corporate':
                $from = Corporate::find($id);
                return 'logo/'.$from->logo;
            break;
            case 'user':
                $from = User::find($id);
                return $from->image;
            break;
        }

    }

    public static function getUserCompanies()
    {
        $list = [];
        $startups = Startup::select('id','name')->where(['user_id'=>Auth::user()->id])->get();
        foreach($startups as $startup){
            array_push($list,['name'=>$startup['name'].' (Startup)','param'=>$startup['id'].'_startup']);
        }

        $investors = Investor::select('id','name')->where(['user_id'=>Auth::user()->id])->get();
        foreach($investors as $investor){
            array_push($list,['name'=>$investor['name'].' (Investor)','param'=>$investor['id'].'_investor']);
        }

        $governments = Government::select('id','name')->where(['user_id'=>Auth::user()->id])->get();
        foreach($governments as $government){
            array_push($list,['name'=>$government['name'].' (Government)','param'=>$government['id'].'_government']);
        }
        $esos = Eso::select('id','name')->where(['user_id'=>Auth::user()->id])->get();
        foreach($esos as $eso){
            array_push($list,['name'=>$eso['name'].' (Eso)','param'=>$eso['id'].'_eso']);
        }
        return $list;
    }


    public static function getAllCompanies()
    {
        $list = [];
        $startups = Startup::select('id','name')->get();
        foreach($startups as $startup){
            array_push($list,['name'=>$startup['name'].' (Startup)','param'=>$startup['id'].'_startup']);
        }

        $investors = Investor::select('id','name')->get();
        foreach($investors as $investor){
            array_push($list,['name'=>$investor['name'].' (Investor)','param'=>$investor['id'].'_investor']);
        }

        $governments = Government::select('id','name')->get();
        foreach($governments as $government){
            array_push($list,['name'=>$government['name'].' (Government)','param'=>$government['id'].'_government']);
        }
        $esos = Eso::select('id','name')->get();
        foreach($esos as $eso){
            array_push($list,['name'=>$eso['name'].' (Eso)','param'=>$eso['id'].'_eso']);
        }
        return $list;
    }

    public static function getCompany($type,$id)
    {
        if($type=='startup'){
            $company = Startup::find($id);
            return $company;
        }
        elseif($type=='investor'){
            $company = Investor::find($id);
            return $company;
        }
        elseif($type=='government'){
            $company = Government::find($id);
            return $company;
        }
        elseif($type=='eso'){
            $company = Eso::find($id);
            return $company;
        }
        else
            return "UNKNOWN";
    }

    public static function slugify($name)
    {
        return Str::slug($name);
    }

    public static function cyper($string,$action)
    {

        $ciphering = "AES-128-CTR"; // Store the cipher method

        $iv_length = openssl_cipher_iv_length($ciphering); // Use OpenSSl Encryption method
        $options = 0;

        $encryption_iv = '1234567891011121'; // Non-NULL Initialization Vector for encryption
        $encryption_key = "9RrRKAEBENqkHP8K"; // Store the encryption key

        if($action =='encrypt'){
            return openssl_encrypt($string, $ciphering,$encryption_key, $options, $encryption_iv);
        }
        else{
            return openssl_decrypt ($string, $ciphering, $encryption_key, $options, $encryption_iv);
        }

        // $ENCRYPTION_KEY = '9RrRKAEBENqkHP8K';
        // $ENCRYPTION_ALGORITHM = 'AES-256-CBC';

        // if($action =='encrypt'){
        //     $EncryptionKey = base64_decode($ENCRYPTION_KEY);
        //     $InitializationVector  = openssl_random_pseudo_bytes(openssl_cipher_iv_length($ENCRYPTION_ALGORITHM));
        //     $EncryptedText = openssl_encrypt($string, $ENCRYPTION_ALGORITHM, $EncryptionKey, 0, $InitializationVector);
        //     return base64_encode($EncryptedText . '::' . $InitializationVector);
        // }
        // else{
        //     $EncryptionKey = base64_decode($ENCRYPTION_KEY);
        //     list($Encrypted_Data, $InitializationVector ) = array_pad(explode('::', base64_decode($string), 2), 2, null);
        //     return openssl_decrypt($Encrypted_Data, $ENCRYPTION_ALGORITHM, $EncryptionKey, 0, $InitializationVector);
        // }


    }


    /*********Show Chat Button *********/
    public static function showChatButton($initiator,$receiver)
    {
        if($initiator == $receiver)
            return false;
        else
            return true;

    }


    /*********** Feeds ***********/
    public static function getProgramFeeds($limit)
    {
       $programs = Program::inRandomOrder()->limit($limit)
           ->whereDate('apply_before', '>', Carbon::now())->get();
       return $programs;
    }
    public static function getEventsFeeds($limit)
    {
       $events = Event::inRandomOrder()->limit($limit)
           ->whereDate('apply_before', '>', Carbon::now())->get();
       return $events;
    }
    public static function getStartupsOfWeek($limit)
    {
       $startups = Startup::inRandomOrder()
           ->where(['status'=>'Published','is_featured'=>1])->limit($limit)->get();
       return $startups;
    }
    public static function getInvestorsOfWeek($limit)
    {
       $investors = Investor::inRandomOrder()
           ->where(['status'=>'Published'])->limit($limit)->get();
       return $investors;
    }
    // public static function getProgramFeeds($limit)
    // {
    //    $programs = Program::limit($limit)->get();
    //    return $programs;
    // }

    /********* Get Fundings ********/
    public static function getFundings($category,$id,$list)
    {
        if($list =='list')
        $invests = Investment::where(['category'=>$category,'category_id'=>$id])->get();
        if($list =='sum')
        $invests = Investment::where(['category'=>$category,'category_id'=>$id])->sum('amount');
        if($list =='count')
        $invests = Investment::where(['category'=>$category,'category_id'=>$id])->count();

        return $invests;
    }

    /********* Get Feeds ********/
    public static function getFeeds($category,$id,$limit)
    {
        $feeds = Feed::where(['post_type'=>$category,'post_id'=>$id])
            ->orderBy('id','desc')->limit($limit)->get();
            foreach($feeds as $feed){
                $feed['company'] = HelperTrait::getCompany($feed['post_type'],$feed['post_id']);
            }
        return $feeds;
    }

    public static function calculateFundingPercentage($collected,$needed)
    {
        if($needed == 0){
            return 0;
        }
        return round(($collected/$needed)*100,0);

    }

    public static function calculateDaysLeft($created,$apply)
    {
        $now = time(); // or your date as well
        $from = strtotime($created);
        $toApply = strtotime($apply);

        $total = $toApply - $now;
        $passed = $toApply - $from;
        return round(($passed - $total)/86400);
    }


    public static function createAvatar($name)
    {
        $response = Http::get('https://avatar.oxro.io/avatar.svg?name='.$name.'&background=f39c12&length=1');
        dd($response);
        return $response;
    }






    /*********************************************************
     * CHAT HELPERS
     */
    public static function getConversationWithTwoUsers($model,$conversationId)
    {
        # code...
        $explode = explode('_',$model);
        $participantModel =  HelperTrait::getCategoryModel($explode[0],$explode[1]);
        $conversation = Chat::conversations()->getById($conversationId);

        $messages = Chat::conversation($conversation)->setParticipant($participantModel)->getMessages();
        foreach($messages as $message){
            $message['posted'] = $participantModel->name;
        }
        Chat::conversation($conversation)->setParticipant($participantModel)->readAll();

        return $messages;
    }

    public static function countUnreadMessages($category,$id)
    {
        $user =  HelperTrait::getCategoryModel($category,$id);

        $unreadCount = Chat::messages()->setParticipant($user)->unreadCount();
        return $unreadCount;
    }

    public static function getParticipantDetails($sender)
    {
        # code...
    }

    public static function countUnreadConversationMessages($model,$conversationId)
    {
        $explode = explode('_',$model);
        $participantModel =  HelperTrait::getCategoryModel($explode[0],$explode[1]);
        $conversation = Chat::conversations()->getById($conversationId);
        $count = Chat::conversation($conversation)->setParticipant($participantModel)->unreadCount();
        return $count;
    }



    /***************************************************
    * STARTUP UGANDA HELPERS
    ****************************************************/
    public static function getStartupUgPrograms($limit){
        $programs = Program::where(['added_by'=>3])->limit($limit)->get();
        return $programs;
    }
}
