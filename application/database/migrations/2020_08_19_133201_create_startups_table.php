<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStartupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('startups', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('tagline');
            $table->integer('user_id');
            $table->boolean('registration_status');
            $table->date('founded_on')->nullable();
            $table->string('address');
            $table->string('district');
            $table->string('phone_number')->nullable();
            $table->string('email')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedIn')->nullable();
            $table->string('youtube')->nullable();
            $table->string('website')->nullable();
            $table->enum('stage',['Ideation','Validation','Early Traction','Scaling']);
            $table->string('consumer_model');
            $table->longText('about');
            $table->mediumText('problem');
            $table->mediumText('solution');
            $table->mediumText('traction')->nullable();
            $table->string('pitchDeck')->nullable();
            $table->string('logo')->nullable();
            $table->string('profile_photo')->nullable();
            $table->string('banner')->nullable();
            $table->enum('status',['Pending','Published','Unpublished'])->default('Pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('startups');
    }
}
