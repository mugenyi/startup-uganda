<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->string('job_title');
            $table->integer('positions')->default(1);
            $table->string('company_type');
            $table->integer('company');
            $table->string('location');
            $table->string('job_type');
            $table->string('skills');
            $table->longText('description');
            $table->dateTime('ends_at');
            $table->string('application_link');
            $table->enum('status',['Pending','Active','Expired']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
