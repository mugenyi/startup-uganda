<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('event_name');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->dateTime('apply_before');
            $table->string('organiser');
            $table->string('location');
            $table->string('target_audience');
            $table->text('summary');
            $table->longText('description');
            $table->integer('added_by');
            $table->enum('status',['Pending','Active','Closed','Ended']);
            $table->string('image')->nullable();
            $table->string('cover')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
