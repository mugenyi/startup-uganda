<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdToGovernment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('governments', function (Blueprint $table) {
            $table->integer('user_id')->after('banner');
            $table->enum('status',['active','pending','suspended']);
            $table->dropColumn('banner');
            //$table->dropColumn('tagline');
            $table->string('image')->after('status');
            $table->string('cover')->after('image');
            $table->mediumText('sectors')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('governments', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('status');
            $table->string('banner');
            $table->dropColumn('image');
            $table->dropColumn('cover');
        });
    }
}
