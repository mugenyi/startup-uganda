<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdToEsos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('esos', function (Blueprint $table) {
            $table->integer('user_id')->after('banner');
            $table->string('image')->after('logo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('esos', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('image');
        });
    }
}
