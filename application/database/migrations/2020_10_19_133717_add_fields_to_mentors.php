<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToMentors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mentors', function (Blueprint $table) {
            $table->string('name');
            $table->string('email');
            $table->string('phone_number');
            $table->text('about');
            $table->enum('gender',['Female','Male','Other']);
            $table->text('focus_areas');
            $table->text('what_i_do')->nullable();
            $table->text('achievements')->nullable();
            $table->text('looking_for')->nullable();
            $table->text('location')->nullable();
            $table->text('markets')->nullable();
            $table->text('photo')->nullable();
            $table->enum('status',['active','pending','suspended'])->default('active');
            $table->integer('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mentors', function (Blueprint $table) {
            //
        });
    }
}
