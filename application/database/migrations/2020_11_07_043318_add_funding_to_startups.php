<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFundingToStartups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('startups', function (Blueprint $table) {
            $table->boolean('rev_generating')->default(false);
            $table->boolean('fundraising')->default(false);
            $table->integer('raise_amount')->nullable();
            $table->string('round')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('startups', function (Blueprint $table) {
            $table->dropColumn('rev_generating');
            $table->dropColumn('fundraising');
            $table->dropColumn('raise_amount');
            $table->dropColumn('round');
        });
    }
}
