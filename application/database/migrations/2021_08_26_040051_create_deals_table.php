<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('category');
            $table->mediumText('brief');
            $table->mediumText('roles')->nullable();
            $table->mediumText('skills')->nullable();
            $table->mediumText('application_guidelines');
            $table->string('attachment')->nullable();
            $table->enum('status',['Pending','Active','Inactive'])->default('Pending');
            $table->date('deadline');
            $table->integer('posted_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deals');
    }
}
