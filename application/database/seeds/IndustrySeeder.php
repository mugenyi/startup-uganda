<?php

use App\Industry;
use Illuminate\Database\Seeder;

class IndustrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $industry = ['Advertising','Aeronautics Aerospace & Defence','Agriculture','AI','Airport Operations','Analytics','Animation','AR VR (Augmented + Virtual Reality)',
        'Architecture Interior Design','Art & Photography','Automotive','Biotechnology','Chemicals','Computer Vision','Construction','Dating Matrimonial','Design','Education',
        'Enterprise Software','Events','Fashion','Finance Technology','Food & Beverages','Green Technology','Healthcare & Lifesciences','House-Hold Services','Human Resources',
        'Internet of Things','IT Services','Logistics','Marketing','Media & Entertainment','Nanotechnology','Non- Renewable Energy','Other Specialty Retailers','Others',
        'Passenger Experience','Pets & Animals','Professional & Commercial Services','Real Estate','Renewable Energy','Retail','Robotics','Safety','Security Solutions','Social Impact',
        'Social Network','Sports','Technology Hardware','Telecommunication & Networking','Textiles & Apparel','Transportation & Storage','Travel & Tourism','Waste Management'];

        foreach($industry as $a){
            Industry::create(['industry'=>$a]);
        }
    }
}
