<?php

use App\Sector;
use Illuminate\Database\Seeder;

class SectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sectors = ['Accounting','AdTech','Advisory','Agri-Tech','Apparel','Apparel & Accessories','Application Development','Auto & Truck Manufacturers','Auto, Truck & Motorcycle Parts',
        'Biotechnology','Branding','Business Finance','Business Intelligence','Business Support Services','Business Support Supplies','Clean Tech','Cloud','Coaching',
        'Construction & Engineering','Dairy Farming','Data Science','Digital Marketing (SEO Automation)','Digital Media','Drones','E-learning','Education Technology',
        'Electronics','Embedded','Entertainment','ERP','Event Management','Experiential Travel','Food Processing','Food Processing','Freight & Logistics Services','Health & Wellness',
        'Healthcare IT','Healthcare Services','Healthcare Technology','Home Security Solutions','Housing','Industrial Design','Integrated Communication services','IT Consulting',
        'IT Management','Machine Learning','Manufacturing & Warehouse','Medical Devices Biomedical','Network Technology Solutions','New-age Construction Technologies','NGO','NLP',
        'Organic Agriculture','Others','Passenger Transportation Services','Payment Platforms','Personal Finance','Pharmaceutical','Product Development','Professional Information Services',
        'Project Management','Recruitment Jobs','Renewable Energy Solutions','Renewable Solar Energy','Restaurants','Retail Technology','Robotics Technology','Sales','Skill Development',
        'Smart Home','Social Commerce','Waste Management','Wearables','Web Development'];

        foreach($sectors as $sector){
            Sector::create(['sector_name'=>$sector]);
        }
    }
}
