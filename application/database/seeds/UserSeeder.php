<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['name'=>'Maurice Kamugisha','email'=>'root@gmail.com','image'=>env('USER_PROFILE'),'provider'=>NULL,'role'=>'admin',
            'provider_id'=>NULL,'password'=>bcrypt('secret'),'phone_number'=>NULL,'address'=>NULL,'city'=>NULL,
            'postal_code'=>NULL,'district'=>NULL,'facebook'=>NULL,'twitter'=>NULL,'about_me'=>NULL],
            ['name'=>'Raymond Mujuni','email'=>'raymond@gmail.com','image'=>env('USER_PROFILE'),'provider'=>NULL,'role'=>'admin',
            'provider_id'=>NULL,'password'=>bcrypt('secret'),'phone_number'=>NULL,'address'=>NULL,'city'=>NULL,
            'postal_code'=>NULL,'district'=>NULL,'facebook'=>NULL,'twitter'=>NULL,'about_me'=>NULL],
            ['name'=>'Startup Uganda','email'=>'info@startupug.com','image'=>env('USER_PROFILE'),'provider'=>NULL,'role'=>'user',
                'provider_id'=>NULL,'password'=>bcrypt('secret'),'phone_number'=>NULL,'address'=>NULL,'city'=>NULL,
                'postal_code'=>NULL,'district'=>NULL,'facebook'=>NULL,'twitter'=>NULL,'about_me'=>NULL],
        ];
        foreach($users as $user){
            User::create($user);
        }

    }
}
