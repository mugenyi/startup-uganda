@extends('build.master')
@section('content')

<div class="row bg-white mt-8 ">
    <div class="col-lg-6 pl-4 pr-4">
        <div class="container">
            <h2 class="text-orange" style="font-size: 80px">About Us</h2>
            <p class="fs-18 text-justify">Startup Uganda (SU) is an association of innovation and entrepreneurship support organizations (IESO) working towards strengthening the startup support
                ecosystem and sector.
            </p>
            <p class="fs-18 text-justify">Rooted in the fundamental belief that innovators and entrepreneurs are a driving source of economic, social, and environmental sustainable development,
                the association is committed to creating an enabling environment where startups can access the support they need to start and grow. As an association
                of organizations, SU’s approach stems from the conviction that we are stronger and more impactful together than we are alone.
                SU endeavours to deepen the trust, sharing and collaboration between members and the ecosystem at large.
            </p>
            <p class="fs-18 text-justify">SU was initiated in 2019 by eight IESO members with support from the United Nations Capital Development Fund (UNCDF). The association intends on expanding its reach and impact by including a greater number and variety of IESOs, and to work with government, private sector, donor communities, and other ecosystem supporters. Towards these ends, SU embarked on a strategy development process to set comprehensive priorities and a path for development and growth. This Strategic Plan (2020-2025) is an internal, guiding document for SU members, executive committee/ board of directors and secretariat.
                </p>
        </div>
    </div>
    <div class="col-md-6 " style="height:700px;background-image: url({{secure_asset('public/images/component.png')}});background-position:-360px;border-radius: 7px;">

    </div>


</div>
<div class="row ">
<div class="container bg-gray-100 pt-6 p-7">
    <div class="row">
        <div class="col-md-6 col-lg-6">
            <div class="d-flex mb-3">

                <div>
                    <h3 class="text-orange" style="font-size: 55px">Our Vision</h3>
                   <p class="mb-1 fs-18 text-justify pr-4">A modern and prosperous Uganda where innovators and entrepreneurs easily access the relevant support that they need to start, grow and scale their businesses.</p>
                </div>
             </div>



        </div>
        <div class="col-md-6 col-lg-6">
            <div class="d-flex mb-3">

                <div><h3 class="text-orange"  style="font-size: 55px">Our Mission</h3>
                   <p class="mb-1 fs-18 text-justify">SU is an association of innovation and entrepreneurship support organizations working together to improve collective capacities, influence and impact, and to create a more enabling environment for innovators and entrepreneurs — that, in turn, drives development across Uganda.</p>
                </div>
             </div>

        </div>


    </div>



</div>
</div>


    <div class="container mb-5 pt-6 ">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="d-flex mb-3">

                    <h2 class="pt-8 pl-4 text-orange" style="font-size: 45px">Our Objectives</h2>
                 </div>
            </div>
            <div class="col-md-6 col-lg-6 pl-4">
                <div class="d-flex mb-3">


                       <p class="mb-1 fs-18 text-left">
                           Startup Uganda will use the following six strategic goals over the next five years (2020-2025), as our guide towards the
                           vision and mission. The first four relate to our external impact and work with and through our members, the last two look
                           inwards, having been developed to ensure the establishment of a strong foundation for our new association to grow on.
                           In order to achieve these goals, corresponding Strategic Objectives have also been identified to provide a clear and substantive
                           framework for SU leadership, team and members in the coming years.
                           <a href="" class="text-orange">Learn More >></a>
                        </p>


                 </div>
                 <div class="mt-8">
                    <a href="{{secure_asset('public/images/board/SU-Strategic-Plan-2020-2025.pdf')}}" target="_blank">
                        <button class="btn border-wd-1 pl-8 pr-8 pt-4 pb-4 border-dark col-sm-12">DOWNLOAD FILE <i class="fa fa-download"></i></button>
                    </a>
                    <small class="text-muted col-sm-12">View our 5 year Strategy as Startup Uganda</small>
                 </div>


            </div>
        </div>
    </div>


<div class="row bg-gray-100 pt-6 pb-4 pl-4 pr-4">
    <div class="container mt-5">
        <h2 class="pt-4 text-orange">Team</h2>
        <div class="row">
            @foreach ($boards as $board)
            <div class="col-xl-4 col-md-12 mb-4">
                <div class="list-card1" >
                    <div class="row align-items-center">
                       <div class="col-md-12 col-sm-12">
                          <div class="media mt-0">
                             <img src="{{secure_asset('public/images/board/'.$board['image'])}}" style="width: 88px;height: 88px" alt="img" class="avatar avatar-xxl bradius">
                             <div class="media-body ml-3 pt-5">
                                <div class="d-md-flex align-items-center mt-1">
                                   <h5 class="mb-1">{{$board['name']}}</h5>
                                </div>
                                <span class="mb-0 fs-13 text-muted">{{$board['designation']}}</span>
                             </div>
                          </div>
                       </div>

                    </div>
                 </div>
            </div>
            @endforeach

        </div>
    </div>
</div>

<div class="row bg-white pt-6">
    <div class="container">
        <h2 class="fs-24 font-weight-bolder text-dark-gray" style="font-weight: bold"><span class="text-orange">STARTUP</span> UGANDA</h2>
        <h2 class="startupTitleHr fs-18 text-dark-gray font-weight-bold"><span>MEMBERS</span></h2>


        @include('includes.partners')
     </div>
</div>



@endsection
