@extends('build.admin')
@section('profile-summary')

    <div class="row">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title mb-0">Articles</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('admin.articles')}}">Articles</a></li>
                    </ol>
                </div>
                <div class="page-rightheader">
                    <a href="" data-target="#create_article" data-toggle="modal" class=""><i class="fa fa-plus"></i> Create Article</a>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('admin-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('admin.include.menu')
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="row">
                    @foreach ($articles as $article)
                        <div class="col-xl-4 col-lg-4 col-md-12">
                            @if (in_array($article->status,['unpublished','pending']))
                                <a href="{{route('admin.articles.publish',['id'=>$article->id,'status'=>'publish'])}}"  class="text-success">Publish</a>
                            @else
                                <a href="{{route('admin.articles.publish',['id'=>$article->id,'status'=>'unpublish'])}}" class="text-danger">Un-publish</a>
                            @endif
                            <a href="{{route('admin.articles.preview',['id'=>$article->id])}}" style="font-size: 15px" target="_blank">
                                <img src="{{secure_asset(env('BANNER').$article->image_name)}}" class="pt-3" style="width: 100%; height:200px;"  alt="">
                                <p class="pt-2">{{$article->post_title}}</p>
                            </a>

                        </div>
                    @endforeach
                </div>

                <div class="row">
                    {{$articles->links()}}
                </div>
            </div>

        </div>

    </div>


    <div class="modal" id="create_article">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Create Article</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
                </div>
                <form action="{{route('admin.articles.create')}}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Title <span class="text-red">*</span></label>
                                    <input type="text" required class="form-control" placeholder="Article title" name="post_title">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Image <span class="text-red">*</span></label>
                                    <input type="file" required class="form-control" id="date" name="image">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputPhone" class="form-label">Article body</label>
                                    <textarea class="form-control" id="description" name="body" rows="8"></textarea>
                                </div>
                            </div>



                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-indigo" type="submit">Submit</button>
                        <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection
@section('script')
    <!-- Initialize Quill editor -->
    <script>

        $(document).ready(function() {
            $('#description').summernote({height: 150});
        });
    </script>

@endsection
