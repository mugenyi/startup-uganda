@extends('build.admin')
@section('profile-summary')

    <div class="row">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title mb-0">Blogs</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('admin.blogs')}}">Blogs</a></li>
                    </ol>
                </div>
                <div class="page-rightheader">
                    <a href="{{route('admin.blogs')}}" ><i class="fa fa-plus"></i> View Blogs</a>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('admin-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('admin.include.menu')
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="row">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Create New Blog Post</h3>
                        </div>
                        <div class="card-body">
                            <form action="{{route('admin.blog.update')}}" method="POST" enctype="multipart/form-data">
                                <div class="row m-t-5">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$blog->id}}" />
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputName" class="form-label">Title</label>
                                            <input type="text" class="form-control" name="title" value="{{$blog->title}}" required placeholder="Blog Title">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPhone" class="form-label">Post Details</label>
                                            <textarea class="form-control" id="about-editor" required name="content" rows="12">
                                                {{$blog->content}}
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-label">Feature Image</div>
                                            <input type="file" class="form-control" required name="feature_image"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputName" class="form-label">Post tags</label>
                                            <input type="text" class="form-control" value="{{$blog->meta_tags}}" name="meta_tags" required placeholder="Blog Tags (Comma separated eg Startup, uganda)">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPhone" class="form-label">Post Summary</label>
                                            <textarea class="form-control" id="" required name="summary" maxlength="210" rows="3">
                                                {{ $blog->summary}}
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputName" class="form-label">Author Name</label>
                                            <input type="text" class="form-control"  value="{{$blog->author_name}}" name="author_name" required placeholder="Enter Author Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputName" class="form-label">Author Email</label>
                                            <input type="text" class="form-control" value="{{$blog->author_email}}" name="author_email" required placeholder="Enter Author Emaill">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary mt-4 mb-0">Post Blog</button>
                            </form>
                        </div>


                    </div>


                </div>

            </div>

        </div>

@endsection

    @section('script')
        <!-- Initialize Quill editor -->
            <script>
                $(document).ready(function() {
                    $('#about-editor').summernote({height: 300});

                });
            </script>
@endsection


