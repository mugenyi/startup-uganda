@extends('build.master')
@section('content')

    <div class="container mt-2 font-weight-bold">
        <div class="row mb-2">
            <a href="{{route('admin.blogs')}}" class="mr-3 text-orange "><i class="fa fa-list"></i> Back to Blogs</a>
            <a href="{{route('admin.blog.edit',['id'=>$blog->id,'slug'=>$blog->slug])}}" class="mr-3 text-black"><i class="fa fa-pencil"></i> Edit Blog</a>
            @if($blog->status == 'Published')
            <a href="{{route('admin.blog.action',['id'=>$blog->id,'action'=>'deactivate'])}}" class="mr-3 text-red"><i class="fa fa-eye-slash"></i> Deactivate Blog</a>
            @else
                <a href="{{route('admin.blog.action',['id'=>$blog->id,'action'=>'activate'])}}" class="mr-3 text-green"><i class="fa fa-check"></i> Activate Blog</a>
                @endif
        </div>
        <div class="row" style="background-image: url('{{secure_asset(env('FULL').$blog->image)}}');background-size: cover;background-position: center; height: 400px"  >

        </div>
        <div class="row mt-3">

            <h3 class="mb-2 text-orange">{{$blog->title}}</h3>

            <div class="mt-3 mb-4" style="width: 100%">
                {!! $blog->content !!}
            </div>


            @foreach(explode(',',$blog->meta_tags) as $tag)
                <span class="badge badge-dark mr-1 text-md">#{{$tag}}</span>
            @endforeach
        </div>
        <div class="row" style="width: 100%">
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="media mb-1">
                    <div class="media-body">
                        <div class="d-md-flex align-items-center mt-1">
                            <p class="mb-1 font-weight-semibold">{{$blog->author_name}} </p>
                        </div>
                        <span class="mb-0 fs-13 text-muted">Author Name</span>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="media mb-1">
                    <div class="media-body">
                        <div class="d-md-flex align-items-center mt-1">
                            <p class="mb-1 font-weight-semibold">{{$blog->author_email}}</p>
                        </div>
                        <span class="mb-0 fs-13 text-muted">Author Emaill</span>
                    </div>
                </div>

            </div>
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="media mb-1">
                    <div class="media-body">
                        <div class="d-md-flex align-items-center mt-1">
                            <p class="mb-1 font-weight-semibold">{{$blog->created_at}}</p>
                        </div>
                        <span class="mb-0 fs-13 text-muted">Created On</span>
                    </div>
                </div>

            </div>

        </div>

    </div>
@endsection
