@extends('build.admin')
@section('profile-summary')

    <div class="row">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title mb-0">Blogs</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('admin.blogs')}}">Blogs</a></li>
                    </ol>
                </div>
                <div class="page-rightheader">
                    <a href="{{route('admin.blog.create')}}" ><i class="fa fa-plus"></i> Create Blog</a>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('admin-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('admin.include.menu')
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="row">
                    @foreach ($blogs as $blog)
                        <div class="pr-2 col-md-6">
                            <div class="card-wrapper">
                                <div class="card-img">
                                    <img src="{{secure_asset(env('COVER').$blog->image)}}" alt="business template" title="" media-simple="true">
                                </div>
                                <div class="card-box p-2">
                                    <a href="{{route('admin.blog.preview',['slug'=>$blog->slug])}}">
                                        <h4 class="card-title pb-3 mbr-fonts-style display-7">{{$blog->title}}</h4>
                                    </a>
                                    <p class="mbr-text mbr-fonts-style display-7">
                                        Status: {{$blog->status}}<br/>
                                        View Count: {{$blog->view_count}}<br/>
                                        Created By: {{$blog->postedBy->name}}<br/>
                                    </p>
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>

                <div class="row">
                    {{$blogs->links()}}
                </div>
            </div>

        </div>

    </div>

@endsection
@section('script')


@endsection
