@extends('build.admin')
@section('admin-content')
<div class="container mt-5">
    <div class="page-header">
        <div class="page-leftheader">
           <h4 class="page-title mb-0">Hi! Welcome Back</h4>
           <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
              <li class="breadcrumb-item active" aria-current="page"><a href="#">Dashboard</a></li>
           </ol>
        </div>
        <div class="page-rightheader">
           <div class="btn btn-list">

            </div>
        </div>
     </div>

    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12">
            <div class="card">
              <div class="card-body">
                 <div class="row mb-3">
                     <div class="col-xl-3 col-6 mb-3 ">
                         <p class="text-uppercase text-orange mb-1">Registered Users</p>
                         <hr/>
                         <h3 class="mb-0 fs-20 number-font1">{{number_format($data['users'],0)}}</h3>
                         <p class="fs-12 text-muted"><a href="{{route('admin.users')}}">View Users</a></p>
                     </div>
                    <div class="col-xl-3 col-6 mb-3">
                        <p class="text-uppercase text-orange mb-1">Total Startups</p>
                        <hr/>
                       <h3 class="mb-0 fs-20 number-font1">{{number_format($data['startups'],0)}}</h3>
                    <p class="fs-12 text-muted"><a href="{{route('admin.startups')}}">View Registered startups</a></p>
                    </div>
                    <div class="col-xl-3 col-6 mb-3 ">
                       <p class="text-uppercase text-orange mb-1">Total Investors</p>
                        <hr/>
                       <h3 class="mb-0 fs-20 number-font1">{{number_format($data['investors'],0)}}</h3>
                       <p class="fs-12 text-muted"><a href="{{route('admin.investors')}}">View Registered Investors</a></p>
                    </div>
                    <div class="col-xl-3 col-6 mb-3">
                        <p class="text-uppercase text-orange mb-1">Total Government Agencies</p>
                        <hr/>
                        <h3 class="mb-0 fs-20 number-font1">{{number_format($data['governments'],0)}}</h3>
                       <p class="fs-12 text-muted"><a href="{{route('admin.governments')}}">View Government Agencies</a></p>
                    </div>
                    <div class="col-xl-3 col-6 mb-3 ">
                       <p class="text-uppercase text-orange mb-1">Total Mentors</p>
                        <hr/>
                       <h3 class="mb-0 fs-20 number-font1">{{number_format($data['mentors'],0)}}</h3>
                       <p class="fs-12 text-muted"><a href="{{route('admin.mentors')}}">View Registered Mentors</a></p>
                    </div>
                    <div class="col-xl-3 col-6 mb-3 ">
                       <p class="text-uppercase text-orange mb-1">Total ESOs</p>
                        <hr/>
                       <h3 class="mb-0 fs-20 number-font1">{{number_format($data['esos'],0)}}</h3>
                        <p class="fs-12 text-muted"><a href="{{route('admin.esos')}}">View Registered ESOs</a></p>
                    </div>
                    <div class="col-xl-3 col-6 mb-3 ">
                        <p class="text-uppercase text-orange mb-1">Total Programs</p>
                        <hr/>
                        <h3 class="mb-0 fs-20 number-font1">{{number_format($data['programs'],0)}}</h3>
                        <p class="fs-12 text-muted"><a href="{{route('admin.programs')}}">View Registered Programs</a></p>
                     </div>
                     <div class="col-xl-3 col-6 mb-3 ">
                         <p class="text-uppercase text-orange mb-1">Total Events</p>
                         <hr/>
                         <h3 class="mb-0 fs-20 number-font1">{{number_format($data['events'],0)}}</h3>
                         <p class="fs-12 text-muted"><a href="{{route('admin.events')}}">View Registered Events</a></p>
                      </div>
                     <div class="col-xl-3 col-6 mb-3 ">
                         <p class="text-uppercase text-orange mb-1">Articles</p>
                         <hr/>
                         <h3 class="mb-0 fs-20 number-font1">{{number_format($data['posts'],0)}}</h3>
                         <p class="fs-12 text-muted"><a href="{{route('admin.articles')}}">View Articles</a></p>
                      </div>
                     <div class="col-xl-3 col-6 mb-3 ">
                         <p class="text-uppercase text-orange mb-1">Startup Ug Deals</p>
                         <hr/>
                         <h3 class="mb-0 fs-20 number-font1">{{number_format($data['deals'],0)}}</h3>
                         <p class="fs-12 text-muted"><a href="{{route('admin.deals')}}">View Deals</a></p>
                      </div>


                 </div>

              </div>
           </div>
        </div>

     </div>
</div>


@endsection
