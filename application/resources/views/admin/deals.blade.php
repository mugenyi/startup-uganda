@extends('build.admin')
@section('profile-summary')

    <div class="row">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title mb-0">Registered Offers</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('admin.deals')}}">Offers</a></li>
                    </ol>
                </div>
                <div class="page-rightheader">
                    <a href="" data-target="#create_offer" data-toggle="modal"><i class="fa fa-plus"></i> Create Offer</a>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('admin-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('admin.include.menu')
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="row">
                    @foreach ($deals as $deal)
                        <div class="col-xl-4 col-lg-4 col-md-12">
                            <a href="{{route('admin.deal.view',['deal'=>$deal['id']])}}">
                                <div class="card box-widget widget-user">
                                    <div class="widget-user-image mt-5">
                                        @if ($deal->category == "Job")
                                            <img alt="{{$deal['name']}}" class="rounded-circle" src="{{secure_asset('public/theme/images/job.png')}}">
                                        @elseif($deal->category == "Bid")
                                            <img alt="{{$deal['name']}}" class="rounded-circle" src="{{secure_asset('public/theme/images/bid.png')}}">
                                        @else
                                            <img alt="{{$deal['name']}}" class="rounded-circle" src="{{secure_asset('public/theme/images/other.png')}}">
                                        @endif
                                    </div>
                                    <div class="card-body text-center">
                                        <div class="pro-user">
                                            <a href=""><h6 class="pro-user-username text-dark mb-1 font-weight-bold">{{$deal['name']}}</h6></a>
                                            <p class="pro-user-desc text-muted"> <i class="fa fa-clock-o"></i> {{$deal['deadline']}} | {{$deal['status']}}</p>
                                            @if($deal->status =='Active')
                                                <a href="{{route('admin.deal.action',['id'=>$deal->id,'state'=>'Inactive'])}}">Deactivate</a>
                                            @else
                                                <a href="{{route('admin.deal.action',['id'=>$deal->id,'state'=>'Active'])}}">Activate</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </a>

                        </div>

                    @endforeach
                </div>

                <div class="row">
                    {{$deals->links()}}
                </div>
            </div>

        </div>

    </div>

    <div class="modal" id="create_offer">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Create Event</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
                </div>
                <form action="{{route('admin.deal.create')}}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Offer Tittle <span class="text-red">*</span></label>
                                    <input type="text" required class="form-control" placeholder="Enter name of offer" name="name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Offer Category <span class="text-red">*</span></label>
                                    <select class="form-control" required="" name="category">
                                        <option value="">Select category</option>
                                        <option value="Job">Job</option>
                                        <option value="Bid">Bid</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Valid Until <span class="text-red">*</span></label>
                                    <input type="date"  min="{{date('Y-m-d')}}" required class="form-control" id="date" placeholder="Valid Untill" name="deadline">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputPhone" class="form-label">Brief about the offer</label>
                                    <textarea class="form-control" required id="brief" name="brief" rows="3" placeholder="Brief about the offer"></textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="roles" class="form-label">Roles for whoever wins the offer</label>
                                    <textarea class="form-control" id="roles" required name="roles" rows="2" placeholder="Roles for whoever wins the offer "></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="skills" class="form-label">Required Skills</label>
                                    <textarea class="form-control" id="skills" required name="skills" rows="2" placeholder="Required Skills"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="skills" class="form-label">Application Guidelines</label>
                                    <textarea class="form-control" id="guidelines" required name="application_guidelines" rows="2" placeholder="Application Guidelines"></textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputPhone" class="form-label">Offer Details File</label>
                                    <input type="file" required class="form-control" placeholder="Attachement" name="attachment">
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-indigo" type="submit">Register</button>
                        <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection
@section('script')
    <!-- Initialize Quill editor -->
    <script>

        $(document).ready(function() {
            $('#brief').summernote({height: 70});
            $('#roles').summernote({height: 70});
            $('#skills').summernote({height: 70});
            $('#guidelines').summernote({height: 70});
        });
    </script>

@endsection
