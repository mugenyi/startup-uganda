@extends('build.admin')
@section('profile-summary')

<div class="row">
    <div class="container">
        <div class="page-header">
            <div class="page-leftheader">
                <h4 class="page-title mb-0">Registered Government Agencies</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.governments')}}">Government Agencies</a></li>
                </ol>
            </div>
            <div class="page-rightheader">
                <a href="{{ route('admin.add.listing',['category'=>'government'])}}" class=""><i class="fa fa-plus"></i> Create Government Agency</a>

            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-content')
<div class="container">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            @include('admin.include.menu')
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="row">
                @foreach ($governments as $government)
                    <div class="col-xl-4 col-lg-4 col-md-12">
                    <a href="{{route('admin.government.view',['id'=>$government['id']])}}">
                            <div class="card box-widget widget-user">
                                <div class="widget-user-image mx-auto mt-5">
                                    <img alt="User Avatar" class="rounded-circle" src="{{secure_asset('public/uploads/logo/'.$government['logo'])}}">
                                </div>
                                <div class="card-body text-center">
                                    <div class="pro-user">
                                        <h4 class="pro-user-username text-dark mb-1 font-weight-bold">{{$government['name']}}</h4>
                                        <h6 class="pro-user-desc text-muted"> Status: {{$government['status']}}</h6>
                                    </div>
                                </div>
                            </div>
                        </a>

                    </div>
                @endforeach
            </div>

            <div class="row">
                {{$governments->links()}}
            </div>
        </div>

    </div>

</div>

 @endsection
