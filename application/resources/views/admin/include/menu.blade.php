<!--  dashboard-menu-->
<div class="card">
    <div class="card-header">
       <div class="card-title">NAVIGATION MENU</div>
    </div>
    <div class="card-body">
        <ul class="flex-column">
            <li class="nav-item1"><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
            <li class="nav-item1"><a href="{{route('admin.startups')}}">Startups</a></li>
            <li class="nav-item1"><a href="{{route('admin.mentors')}}">Mentors</a></li>
            <li class="nav-item1"><a href="{{route('admin.investors')}}">Investors</a></li>
            <li class="nav-item1"><a href="{{route('admin.esos')}}">ESOs</a></li>
            <li class="nav-item1"><a href="{{route('admin.corporates')}}">Corporations</a></li>
            <li class="nav-item1"><a href="{{route('admin.governments')}}">Government Agencies</a></li>
            <li class="nav-item1"><a href="{{route('admin.programs')}}">Programs</a></li>
            <li class="nav-item1"><a href="{{route('admin.programs.registrations')}}">Program Registrations</a></li>
            <li class="nav-item1"><a href="{{route('admin.events')}}">Events</a></li>
            <li class="nav-item1"><a href="{{route('admin.articles')}}">Articles</a></li>
            <li class="nav-item1"><a href="{{route('admin.jobs')}}">Job Adverts</a></li>
            <li class="nav-item1"><a href="{{route('admin.users')}}">Users</a></li>
            <li class="nav-item1"><a href="{{route('admin.deals')}}">Startup Ug Offers</a></li>
            <li class="nav-item1"><a href="{{route('admin.blogs')}}">Blogs</a></li>
            {{--  <li class="nav-item1"><a href="{{route('user.feed')}}">Your feeds</a></li>
             <li class="nav-item1"><a href="{{route('user.programs')}}">Your Programs</a></li>
             <li class="nav-item1"><a href="{{route('user.esos')}}">Your ESOs</a></li> --}}

        </ul>
    </div>
 </div>

<!-- dashboard-menu  end-->
