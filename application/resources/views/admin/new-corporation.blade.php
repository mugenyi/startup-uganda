@extends('build.admin')
@section('profile-summary')
    <div class="row">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                <h4 class="page-title mb-0">Register New Corporation</h4>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                <li class="breadcrumb-item"><a href="#">Corporation</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="#">Register</a></li>
            </ol>
            </div>
            <div class="page-rightheader">
                <div class="btn btn-list">

            </div>
        </div>
        </div>
        </div>
    </div>
@endsection
@section('admin-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('admin.include.menu')
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="card">
                    <div class="card-header">
                       <h3 class="card-title">Register New Corporation</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.corporates.create')}}" method="POST" enctype="multipart/form-data">
                            @csrf

                          <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">Corporation Name <span class="text-red">*</span></label>
                                        <input type="text" required class="form-control" placeholder="corporate Company Name" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Company Tagline <span class="text-red">*</span></label>
                                        <input type="text" required class="form-control" id="date" placeholder=" Company Tagline" name="tagline">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Address <span class="text-red">*</span></label>
                                        <input type="text" required class="form-control" placeholder="Company Address" name="address">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Email <span class="text-red">*</span></label>
                                        <input type="text" required class="form-control" placeholder="Company Email" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Facebook </label>
                                        <input type="text" class="form-control" id="facebook" placeholder="Facebook link" name="facebook">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Twitter </label>
                                        <input type="text" class="form-control" id="twitter" placeholder="Twitter link" name="twitter">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Phone Number <span class="text-red">*</span></label>
                                        <input type="text" required class="form-control" placeholder="Company Phone Number" name="phone_number">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Website </label>
                                        <input type="text" class="form-control" id="date" placeholder="Website" name="website">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">LinkedIn </label>
                                        <input type="text" class="form-control" id="linkedIn" placeholder="LinkedIn link" name="linkedIn">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Youtube </label>
                                        <input type="text" class="form-control" id="twitter" placeholder="Youtube link" name="youtube">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Describe this corporation</label>
                                        <textarea class="form-control" id="description" name="about" rows="4"></textarea>
                                    </div>
                                </div>



                            </div>
                          </div>
                            <div class="modal-footer">
                                <button class="btn btn-indigo" type="submit">Register corporation</button>
                                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                            </div>
                          </form>

                    </div>
                 </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
<!-- Initialize Quill editor -->
<script>
     $(document).ready(function() {
        $('#description').summernote({height: 100});
        $('#experience').summernote();
    });
  </script>
@endsection
