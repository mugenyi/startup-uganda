@extends('build.admin')
@section('profile-summary')
    <div class="row">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                <h4 class="page-title mb-0">Register New Mentor</h4>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                <li class="breadcrumb-item"><a href="#">Mentors</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="#">Register</a></li>
            </ol>
            </div>
            <div class="page-rightheader">
                <div class="btn btn-list">

            </div>
        </div>
        </div>
        </div>
    </div>
@endsection
@section('admin-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('admin.include.menu')
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="card">
                    <div class="card-header">
                       <h3 class="card-title">Register New Mentor</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.mentor.create')}}" method="POST" enctype="multipart/form-data">
                            @csrf

                          <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Mentor Name <span class="text-red">*</span></label>
                                        <input type="text" required class="form-control" placeholder="Mentor Name" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Gender<span class="text-red">*</span></label>
                                        <select class="form-control" name="gender">
                                            <option value="Female">Female</option>
                                            <option value="Male">Male</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Mentor Photo <span class="text-red">*</span></label>
                                        <div class="input-group file-browser mb-5">
                                            <input type="file" class="form-control border-right-0 browse-file" name="image" placeholder="choose" >

                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label class="form-label">Location <span class="text-red">*</span></label>
                                        <input type="text" required class="form-control" placeholder="Your location" name="location">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Email <span class="text-red">*</span></label>
                                        <input type="text" required class="form-control" placeholder="Email Address" name="email">
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label">Phone Number <span class="text-red">*</span></label>
                                        <input type="text" required class="form-control" placeholder="Phone Number" name="phone_number">
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">About Me</label>
                                        <textarea class="form-control" name="about" id="about" rows="5"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Investment sector</label>
                                        <select class="form-control select2 select2-hidden-accessible" data-placeholder="Choose Sector" name="sectors[]" multiple="" tabindex="-1" aria-hidden="true">
                                            {{ $sectors= App\Traits\HelperTrait::getSectors() }}
                                            @foreach ($sectors as $sector)
                                            <option value="{{$sector['sector_name']}}">
                                                {{$sector['sector_name']}}
                                            </option>
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">What i do</label>
                                        <textarea class="form-control" id="what_i_do" name="what_i_do" rows="4"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Looking for</label>
                                        <textarea class="form-control" id="looking_for" name="looking_for" rows="4"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">My Achievements</label>
                                        <textarea class="form-control" id="achievements" name="achievements" rows="4"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Interested in Markets</label>
                                        <textarea class="form-control" id="markets" placeholder="Which markets are you interested in" name="markets" rows="3"></textarea>
                                    </div>

                                </div>

                            </div>
                          </div>
                            <div class="modal-footer">
                                <button class="btn btn-indigo" type="submit">Register Mentor</button>
                                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                            </div>
                          </form>

                    </div>
                 </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
<!-- Initialize Quill editor -->
<script>
     $(document).ready(function() {
        $('#about').summernote({height: 100});
        $('#what_i_do').summernote({height: 50});
        $('#looking_for').summernote({height: 50});
        $('#achievements').summernote({height: 80});
    });
  </script>
@endsection
