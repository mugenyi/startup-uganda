@extends('build.admin')
@section('profile-summary')

<div class="row">
    <div class="container">
        <div class="page-header">
            <div class="page-leftheader">
                <h4 class="page-title mb-0">Registered Programs</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.programs')}}">Programs</a></li>
                </ol>
            </div>
            <div class="page-rightheader">
                <a href="" class="text-primary" data-target="#create_program" data-toggle="modal"><i class="fa fa-plus"></i> Create Program</a>

            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-content')
<div class="container">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            @include('admin.include.menu')
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="row">

                <div class="card">
                    <div class="card-header">
                       <h3 class="card-title">Registered Programs</h3>
                    </div>
                    <div class="card-body p-0">
                       <div class="table-responsive">
                          <table class="table table-striped text-wrap">
                             <thead>
                                <tr class="text-blue">
                                   <th>Program Name</th>
                                   <th>Organiser</th>
                                   <th>From - To</th>
                                   <th>Venue</th>
                                   <th>Status</th>
                                   <th>Action</th>
                                </tr>
                             </thead>
                             <tbody>
                                 @foreach ($programs as $program)
                                 <tr>
                                 <th scope="row">{{$program['program_name']}}</th>
                                    <td>{{$program['organiser']}}</td>
                                    <td>{{$program->starts.' - '.$program->ends}}</td>
                                    <td>{{$program['location']}}</td>
                                    <td>{{$program['status']}}</td>
                                    <td>
                                    <a href="{{route('admin.program.view',['id'=>$program->id])}}" class="text-primary mr-2">View</a>

                                        @if ($program['id']==env('PROGRAM'))
                                            <br/><a href="{{route('admin.program.registrations.export',['program'=>$program->id])}}" class="text-primary mr-2"><i class="fa fa-download"></i> Export </a>
                                            @else
                                            <a href="{{$program->link}}" class="text-success" target="_blank">Apply</a><br/>
                                        @endif
                                    </td>
                                 </tr>
                                 @endforeach

                             </tbody>
                          </table>
                       </div>
                       <!-- bd -->
                    </div>
                    <!-- bd -->
                 </div>
            </div>

            <div class="row">
                {{$programs->links()}}
            </div>
        </div>

    </div>

</div>
<div class="modal" id="create_program">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header">
                <h6 class="modal-title">Create New Program</h6>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
            </div>
            <form action="{{route('admin.program.create')}}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Program Name <span class="text-red">*</span></label>
                                <input type="text" required class="form-control" placeholder="Program Name" name="program_name">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Program Start Date <span class="text-red">*</span></label>
                                <input type="date"  min="{{date('Y-m-d')}}" required class="form-control" id="date" placeholder="Start Date" name="start_date">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Organiser <span class="text-red">*</span></label>
                                <input type="text" required class="form-control" placeholder="Program Organiser" name="organiser">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Program End Date <span class="text-red">*</span></label>
                                <input type="date"  min="{{date('Y-m-d')}}" required class="form-control" id="date" placeholder="Start Date" name="end_date">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputPhone" class="form-label">Describe this program</label>
                                <textarea class="form-control" id="description" required name="description" rows="4"></textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputPhone" class="form-label">Program Summary</label>
                                <input type="text" required class="form-control" id="date" placeholder="Program Summary" maxlength="120" name="summary" placeholder="Summarize program ambitions">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputPhone" class="form-label">Target Audience</label>
                                <textarea class="form-control" required name="target_audience" rows="2" placeholder="Describe target audience "></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Apply Until <span class="text-red">*</span></label>
                                <input type="date"  min="{{date('Y-m-d')}}" required class="form-control" placeholder="Deadline" name="apply_before">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Program Location <span class="text-red">*</span></label>
                                <input type="text" required class="form-control" placeholder="Program Venue" name="location">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputPhone" class="form-label">Registration Link</label>
                                <input type="text" required class="form-control" placeholder="Registration Link" name="link">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-indigo" type="submit">Register Program</button>
                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection
@section('script')
    <script>
        function exportTasks(_this) {
            let _url = $(_this).data('href');
            window.location.href = _url;
        }
    </script>
    <!-- Initialize Quill editor -->
    <script>

        $(document).ready(function() {
            $('#description').summernote({height: 150});
        });
    </script>
@endsection
