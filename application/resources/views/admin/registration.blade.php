@extends('build.admin')
@section('profile-summary')

    <div class="row">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title mb-0">Registered Events</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.programs')}}">Programs</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.program.view',['id'=>$reg->programDetail->id])}}">{{$reg->programDetail->program_name}}</a></li>
                        <li class="breadcrumb-item active"><a href="">{{$reg['name']}}</a></li>
                    </ol>
                </div>
                <div class="page-rightheader">

                </div>
            </div>
        </div>
    </div>
@endsection
@section('admin-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('admin.include.menu')
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="row">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Program Registrations</h3>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-striped card-table table-vcenter text-wrap">
                                    <tbody>
                                    <tr><th>Program Name</th><td><a href="{{route('admin.program.view',['id'=>$reg->programDetail->id])}}">{{$reg->programDetail->program_name}}</a></td></tr>
                                        <tr><th>Name</th><td>{{$reg['name']}}</td></tr>
                                        <tr><th>Email</th><td>{{$reg['email']}}</td></tr>
                                        <tr><th>Phone</th><td>{{$reg['phone']}}</td></tr>
                                        <tr><th>Sector</th><td>{{$reg['sector']}}</td></tr>
                                        <tr><th>Company Name</th><td>{{$reg['company']}}</td></tr>
                                        <tr><th>Is company registered?</th><td>{{$reg['company_registration']}}</td></tr>
                                        <tr><th>Years of Operation</th><td>{{$reg['operation_years']}}</td></tr>
                                        <tr><th>Investment Ask</th><td>{{$reg['investment_size']}}</td></tr>
                                        <tr><th>Affiliation</th><td>{{$reg['affliation']}}</td></tr>
                                        <tr><th>Pitch deck</th><td><a href="{{secure_asset('public/uploads/full/'.$reg['pitchdeck'])}}" target="_blank">View Pitchdeck</a></td></tr>
                                        <tr><th>Team Members</th><td>{{$reg['team_members']}}</td></tr>
                                        <tr><th>Created At</th><td>{{$reg['created_at']}}</td></tr>

                                    </tbody>
                                </table>
                            </div>
                            <!-- bd -->
                        </div>
                        <!-- bd -->
                    </div>
                </div>


            </div>

        </div>

    </div>



@endsection
@section('script')


@endsection
