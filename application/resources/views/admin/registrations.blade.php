@extends('build.admin')
@section('profile-summary')

<div class="row">
    <div class="container">
        <div class="page-header">
            <div class="page-leftheader">
                <h4 class="page-title mb-0">Registered Events</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('admin.programs')}}">Programs</a></li>
                    <li class="breadcrumb-item active"><a href="">Registrations</a></li>
                </ol>
            </div>

        </div>
    </div>
</div>
@endsection
@section('admin-content')
<div class="container">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            @include('admin.include.menu')
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="row">

                <div class="card">
                    <div class="card-header">
                       <h3 class="card-title">Program Registrations</h3>
                    </div>
                    <div class="card-body p-0">
                       <div class="table-responsive">
                          <table class="table table-striped card-table table-vcenter text-wrap">
                             <thead>
                                <tr>
                                   <th>Name</th>
                                   <th>Company</th>
                                   <th>Years</th>
                                   <th>Investment</th>
                                   <th>Affiliation</th>
                                   <th>Date</th>
                                   <th>Event</th>
                                   <th>Action</th>
                                </tr>
                             </thead>
                             <tbody>
                                 @foreach ($registrations as $reg)

                                 <tr>
                                 <th scope="row">{{$reg['name']}}</th>
                                    <td>{{$reg['company']}} ({{$reg['sector']}})</td>
                                    <td>{{$reg['operation_years']}}</td>
                                    <td>{{$reg['investment_size']}}</td>
                                    <td>{{$reg['affliation']}}</td>
                                    <td>{{$reg['created_at']}}</td>
                                     <td>
                                         <a href="{{route('admin.program.view',['id'=>$reg->programDetail->id])}}">
                                             {{$reg->programDetail->program_name}}
                                         </a>
                                     </td>
                                    <td>
                                    <a href="{{route('admin.view.registration',['id'=>$reg['id']])}}" class="text-success" >View Details</a>
                                    </td>
                                 </tr>
                                 @endforeach

                             </tbody>
                          </table>
                       </div>
                       <!-- bd -->
                    </div>
                    <!-- bd -->
                 </div>
            </div>

            <div class="row">
                {{$registrations->links()}}
            </div>
        </div>

    </div>

</div>

@endsection
@section('script')


@endsection
