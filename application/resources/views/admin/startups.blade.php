@extends('build.admin')
@section('profile-summary')

<div class="row">
    <div class="container">
        <div class="page-header">
            <div class="page-leftheader">
                <h4 class="page-title mb-0">Registered Startups</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.startups')}}">Startups</a></li>
                </ol>
            </div>
            <div class="page-rightheader">
                <a href="{{ route('admin.add.listing',['category'=>'startup'])}}" class=""><i class="fa fa-plus"></i> Create Startup</a>
            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-content')
<div class="container">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            @include('admin.include.menu')
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="row">
                @foreach ($startups as $startup)
                    <div class="col-xl-4 col-lg-4 col-md-12">
                    <a href="{{route('admin.startup.view',['id'=>$startup['id']])}}">
                            <div class="card box-widget widget-user">
                                <div class="widget-user-image mt-5">
                                    <img alt="{{$startup['name']}}" class="rounded-circle" src="{{secure_asset('public/uploads/logo/'.$startup['logo'])}}">
                                </div>
                                <div class="card-body text-center">
                                    <div class="pro-user">
                                        <h6 class="pro-user-username text-dark mb-1 font-weight-bold">{{$startup['name']}}</h6>
                                        <p class="pro-user-desc text-muted"> Status: {{$startup['status']}}</p>
                                    </div>
                                </div>
                            </div>
                        </a>

                    </div>
                @endforeach
            </div>

            <div class="row">
                {{$startups->links()}}
            </div>
        </div>

    </div>

</div>

 @endsection
