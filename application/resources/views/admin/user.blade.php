@extends('build.admin')
@section('profile-summary')

<div class="row">
    <div class="container">
        <div class="page-header">
            <div class="page-leftheader">
                <h4 class="page-title mb-0">Registered Users</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.users')}}">Users</a></li>
                </ol>
            </div>
            <div class="page-rightheader">

            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-content')
<div class="container">
    <div class="row bg-white">
        <div class="container">
            <div class="main-proifle">
                <div class="row">
                   <div class="col-lg-8">
                      <div class="box-widget widget-user">
                         <div class="widget-user-image1 d-sm-flex">
                            <img alt="User Avatar" class="rounded-circle border p-0" style="width:128px;height:128px;" src="{{secure_asset('public/uploads/'.$user['image'])}}">
                            <div class="mt-1 ml-lg-5">
                               <h4 class="pro-user-username mb-3 font-weight-bold">{{$user['name']}}</h4>
                               <p>{{substr($user['about_me'],0,250)}}...</p>
                               <ul class="mb-0 pro-details">
                                  <li><span class="h6 mt-3"><i class="fa fa-envelope text-gray"></i> {{$user['email']}}</span></li>
                                  <li><span class="h6 mt-3"><i class="fa fa-phone text-gray"></i> {{$user['phone_number']}}</span></li>
                                  <li><span class="h6 mt-3"><i class="fa fa-location-arrow text-gray"></i> {{$user['address'].' - '.$user['city'].', '.$user['district']}}</span></li>
                               </ul>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="col-lg-4 col-md-auto">
                      <div class="text-lg-right btn-list mt-4 mt-lg-0">
                          <a href="#" class="btn btn-light">Message</a>
                          <a href="{{route('user.edit-profile')}}" class="btn btn-primary">Edit Profile</a>
                    </div>
                      <div class="mt-5">
                         <div class="main-profile-contact-list row">
                            <div class="media col-sm-4">
                               <div class="media-icon bg-primary text-white mr-3"> <i class="fa fa-comments fs-18 pt-3"></i> </div>
                               <div class="media-body">
                                  <small class="text-muted">Posts</small>
                                  <div class="font-weight-bold number-font"> 245 </div>
                               </div>
                            </div>
                            <div class="media col-sm-4">
                               <div class="media-icon bg-secondary text-white mr-3"> <i class="fa fa-users fs-18 pt-3"></i> </div>
                               <div class="media-body">
                                  <small class="text-muted">Followers</small>
                                  <div class="font-weight-normal1"> 689k </div>
                               </div>
                            </div>
                            <div class="media col-sm-4">
                               <div class="media-icon bg-info text-white mr-3"> <i class="fa fa-feed fs-18 pt-3"></i> </div>
                               <div class="media-body">
                                  <small class="text-muted">Following</small>
                                  <div class="font-weight-bold number-font"> 3,765 </div>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                </div>

                <!-- /.profile-cover -->
             </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-3 col-lg-4">
            @include('admin.include.menu')
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="row">
                <div class="container mt-4">
                    {{-- User startups --}}
                    @if ($data['startups']->count() > 0)
                    <h4>YOUR REGISTERED STARTUPS</h4>
                    <div class="row">
                        @foreach ($data['startups'] as $startup)
                        <div class="col-md-6 col-lg-4">
                            <a href="{{route('admin.startup.view',['id'=>$startup['id']])}}">
                            <div class="card overflow-hidden startup-card">
                            <img src="{{secure_asset('public/uploads/banner/'.$startup['profile_photo'])}}" style="height: 220px" alt="image">
                            <div class="card-body">
                                <h4 class="card-title text-uppercase" style="font-size: 13px">{{$startup['name']}}</h4>

                            </div>
                            </div>
                            </a>
                        </div>

                        @endforeach
                    </div>
                    @else

                    @endif
                    {{-- User startups --}}
                {{-- User investors --}}
                @if ($data['investors']->count() > 0)
                <h4>YOUR REGISTERED INVESTORS</h4>
                <div class="row">
                    @foreach ($data['investors'] as $investor)
                    <div class="col-md-6 col-lg-4">
                        <a href="{{route('admin.investor.view',['id'=>$investor['id']])}}">
                        <div class="card overflow-hidden startup-card">
                            <img src="{{secure_asset('public/uploads/banner/'.$investor['profile_photo'])}}" style="height: 220px" alt="image">
                            <div class="card-body">
                                <h4 class="card-title text-uppercase" style="font-size: 13px">{{$investor['name']}}</h4>
                                <h6 class="text-gray text-uppercase">{{$investor['investor_type']}} INVESTOR </h6>
                            </div>
                        </div>
                        </a>
                    </div>

                    @endforeach
                </div>
                @endif
                {{-- User investors --}}
                {{-- User Government --}}
                @if ($data['governments']->count() > 0)
                <h4>YOUR REGISTERED GOVERNMENT</h4>
                <div class="row">
                    @foreach ($data['governments'] as $government)
                    <div class="col-md-6 col-lg-4">
                        <a href="{{route('admin.government.view',['id'=>$government['id']])}}">
                        <div class="card overflow-hidden startup-card">
                            <img src="{{secure_asset('public/uploads/banner/'.$government['image'])}}" style="height: 220px" alt="image">
                            <div class="card-body">
                                <h4 class="card-title text-uppercase" style="font-size: 13px">{{$government['name']}}</h4>
                                {{-- <h6 class="text-gray text-uppercase">{{$government['investor_type']}} INVESTOR </h6> --}}
                            </div>
                        </div>
                        </a>
                    </div>

                    @endforeach
                </div>
                @endif
                {{-- User Government --}}

                </div>

            </div>
        </div>

    </div>

</div>

 @endsection
