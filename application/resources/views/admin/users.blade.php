@extends('build.admin')
@section('profile-summary')

<div class="row">
    <div class="container">
        <div class="page-header">
            <div class="page-leftheader">
                <h4 class="page-title mb-0">Registered Users</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.users')}}">Users</a></li>
                </ol>
            </div>
            <div class="page-rightheader">

            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-content')
<div class="container">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            @include('admin.include.menu')
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="row">
                <table class="table table-striped card-table table-vcenter fs-13">
                    <thead>
                        <tr>
                            <th colspan="2">Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Phone</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td>
                                <img src="{{secure_asset('public/uploads/'.$user['image'])}}" class="avatar left-align avatar-sm mr-2 brround">
                            </td>
                            <td>
                            <a href="{{route('admin.user.details',['id'=>$user['id']])}}"><b>{{$user['name']}}</b></a>
                           </td>
                            <td>{{$user['email']}}</td>
                            <td>{{$user['role']}}</td>
                            <td>{{$user['phone']}}</td>
                            <td><a href="{{route('admin.user.details',['id'=>$user['id']])}}">View User</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>

            <div class="row">
                {{$users->links()}}
            </div>
        </div>

    </div>

</div>

 @endsection
