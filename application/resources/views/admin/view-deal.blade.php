@extends('build.admin')
@section('profile-summary')

    <div class="row">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title mb-0">Registered Offers</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                        <li class="breadcrumb-item "><a href="{{route('admin.deals')}}">Offers</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('admin.deals')}}">{{$deal->name}}</a></li>
                    </ol>
                </div>
                <div class="page-rightheader">
                    @if($deal->status =='Active')
                        <a href="{{route('admin.deal.action',['id'=>$deal->id,'state'=>'Inactive'])}}">Deactivate</a>
                    @else
                        <a href="{{route('admin.deal.action',['id'=>$deal->id,'state'=>'Active'])}}">Activate</a>
                    @endif

                </div>
            </div>
        </div>
    </div>
@endsection
@section('admin-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('admin.include.menu')
            </div>
            <div class="col-xl-9 col-lg-8">
                    <h4>{{$deal->name}}</h4>

                    <p> Deadline: {{$deal->deadline}} | Status: {{$deal->status}} | Category: {{$deal->category}}</p>
                    <h5>Offer Brief</h5>
                <hr/>
                {!! $deal->brief !!}

                <h5 class="mt-4">Offer Roles</h5>
                <hr/>
                {!! $deal->roles !!}

                <h5 class="mt-4">Required Skills</h5>
                <hr/>
                {!! $deal->skills !!}

                <h5 class="mt-4">Application Guidelines</h5>
                <hr/>
                {!! $deal->application_guidelines !!}

                <h5 class="mt-4">Attachment</h5>
                <hr/>
                <a href="{{secure_asset(env('FULL').$deal['attachment'])}}" target="_blank">View Attachment</a>

            </div>

        </div>

    </div>


@endsection
@section('script')


@endsection
