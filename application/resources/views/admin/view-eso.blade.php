@extends('build.admin')
@section('profile-summary')
    <link rel="stylesheet" href="https://unpkg.com/dropzone/dist/dropzone.css" />
    <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
    <script src="https://unpkg.com/dropzone"></script>
    <script src="https://unpkg.com/cropperjs"></script>

<div class="row">
    <div class="container">
        <div class="page-header">
            <div class="page-leftheader">
                <h4 class="page-title mb-0">{{$eso['name']}} </h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('admin.esos')}}">esos</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.mentors')}}">{{$eso['name']}}</a></li>
                </ol>
            </div>
            <div class="page-rightheader">

            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-content')
<div class="container">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            @include('admin.include.menu')
        </div>
        <div class="col-xl-9 col-lg-8">

            <div class="row" style="width: 100%">
                <div class="col-2">
                    <img alt="{{$eso['legal_name']}}" class="rounded-circle avatar-xxl mt-4 border p-0" src="{{secure_asset('public/uploads/logo/'.$eso['logo'])}}">
                </div>
                <div class="col-3">
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{$eso['legal_name']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Legal Name</span>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{$eso['email']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Email</span>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{$eso['founded_on']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Founded On</span>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{$eso['phone_number']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Phone Number</span>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{$eso['address']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Address</span>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{ $eso['created_at']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Created At</span>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row pull-right mt-5">
                @if($eso->user->role =='admin')
                    <a href="" class="text-warning mr-3" data-target="#upload_Logo" data-toggle="modal"><i class="fa fa-image"></i> Upload Logo</a>
                    <a href="" class="text-success mr-3" data-target="#send_claim" data-toggle="modal"><i class="fa fa-envelope"></i> Send Claim Email</a>
                @endif
                @if (in_array($eso['status'],['Pending','Unpublished']))
                    <a href="{{ route('admin.eso.update',['id'=>$eso['id'],'status'=>'Published'])}}" class="text-success"><i class="fa fa-check"></i> Publish Eso</a>
                @else
                    <a href="{{ route('admin.eso.update',['id'=>$eso['id'],'status'=>'Unpublished'])}}" class="text-danger"><i class="fa fa-times"></i> Unpublish Eso</a>
                @endif

            </div>


            <div class="container mt-7">
                <div class="row">

                    <div class="col-lg-12 col-md-auto">
                        <div class="card-body">
                            <h4 class="pro-user-username centered mb-3 text-gray font-weight-bold">About {{$eso['name']}} </h4>
                            <div class="main-profile-bio mb-0">
                            <p>{!!$eso['about']!!}</p>
                            </div>
                            <div class="main-profile-bio mb-0">
                                <h4 class="text-gray mt-7">Tagline</h4>
                                <p>{{$eso['tagline']}}</p>
                                </div>


                    </div>
                    </div>
                 </div>
            </div>

        </div>

    </div>

</div>
<div class="modal" id="upload_Logo">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header">
                <h6 class="modal-title">Upload {{$eso['name']}} Logo</h6>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
            </div>
            <form action="{{route('admin.eso.logo.upload')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{$eso['id']}}" name="id">
                <input type="hidden" value="ESO" name="type">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="form-label">Add Logo</div>
                        <input type="file" class="form-control" name="upload_logo">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-indigo" type="submit">Save changes</button>
                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="send_claim">
    <div class="modal-dialog" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title">Send Profile Claim Email</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('email.claim')}}" method="POST">
            @csrf
            <input type="hidden" value="{{$eso['id']}}" name="id">
            <input type="hidden" value="eso" name="category">
          <div class="modal-body">
            <div class="form-group">
                <label for="exampleInputEmail1" class="form-label">Email Address</label>
                <input type="email" class="form-control" required id="claimEmail" name="email" placeholder="Enter Email Address">
            </div>

          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Send Email</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>

 @endsection
