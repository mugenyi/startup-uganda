@extends('build.admin')
@section('profile-summary')

<div class="row">
    <div class="container">
        <div class="page-header">
            <div class="page-leftheader">
                <h4 class="page-title mb-0">{{$mentor['name']}} </h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('admin.mentors')}}">Mentors</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.mentors')}}">{{$mentor['name']}}</a></li>
                </ol>
            </div>
            <div class="page-rightheader">

            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-content')
<div class="container">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            @include('admin.include.menu')
        </div>
        <div class="col-xl-9 col-lg-8">

            <div class="row" style="width: 100%">
                <div class="col-2">
                    <img alt="User Avatar" class="rounded-circle avatar-xxl mt-4 border p-0" src="{{secure_asset(env('LOGO').$mentor['photo'])}}">
                </div>
                <div class="col-3">
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{$mentor['name']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Name</span>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{$mentor['email']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Email</span>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{ $mentor['gender']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Gender</span>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{$mentor['phone_number']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Phone Number</span>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{$mentor['location']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Location</span>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{ $mentor['created_at']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Created At</span>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row pull-right mt-5">
                @if($mentor->user->role =='admin')
                    <a href="" class="text-info mr-3" data-target="#send_claim" data-toggle="modal"><i class="fa fa-envelope"></i> Send Claim Email</a>
                @endif
                @if (in_array($mentor['status'],['pending','suspended']))
                    <a href="{{ route('admin.mentor.update',['id'=>$mentor['id'],'status'=>'active'])}}" class="text-success"><i class="fa fa-check"></i> Publish Mentor</a>
                @else
                    <a href="{{ route('admin.mentor.update',['id'=>$mentor['id'],'status'=>'suspended'])}}" class="text-danger"><i class="fa fa-times"></i> Unpublish Mentor</a>
                @endif
            </div>

            <div class="container mt-7">
                <div class="row">

                    <div class="col-lg-12 col-md-auto">
                        <div class="card-body">
                            <h4 class="pro-user-username centered mb-3 font-weight-bold">{{$mentor['name']}} </h4>
                            <div class="main-profile-bio mb-0">
                            <p>{!!$mentor['about']!!}</p>
                            </div>


                            <div class="row mt-5">
                                <div class="col-12">
                                    <h5>ABOUT</h5>
                                    <br/>
                                    <table class="table table-vcenter text-wrap fs-12">
                                        <tr class="p-3 " >
                                            <td class="text-uppercase pl-4" style="width: 20%">What i do</td>
                                            <td class="mb-4 text-justify">{!!$mentor['what_i_do']!!}</td>
                                        </tr>
                                        <tr class="p-3">
                                            <td class="text-uppercase fs-12 pl-4">Achievements</td>
                                            <td class="mb-4 text-justify">{!!$mentor['achievements']!!}</td>
                                        </tr>
                                        <tr class="p-3">
                                            <td class="text-uppercase fs-12 pl-4">What i'm looking for</td>
                                            <td class="mb-4 text-justify">{!!$mentor['looking_for']!!}</td>
                                        </tr>
                                        <tr class="p-3">
                                            <td class="text-uppercase fs-12 pl-4">Markets</td>
                                            <td class="mb-4 text-justify">{!!$mentor['markets']!!}</td>
                                        </tr>
                                    </table>


                                </div>

                            </div>

                    </div>
                    </div>
                 </div>
            </div>

        </div>

    </div>

</div>
<div class="modal" id="send_claim">
    <div class="modal-dialog" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title">Send Profile Claim Email</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('email.claim')}}" method="POST">
            @csrf
            <input type="hidden" value="{{$mentor['id']}}" name="id">
            <input type="hidden" value="mentor" name="category">
          <div class="modal-body">
            <div class="form-group">
                <label for="exampleInputEmail1" class="form-label">Email Address</label>
                <input type="email" class="form-control" required id="claimEmail" name="email" placeholder="Enter Email Address">
            </div>

          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Send Email</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>


 @endsection
