@extends('build.admin')
@section('profile-summary')
    <link rel="stylesheet" href="https://unpkg.com/dropzone/dist/dropzone.css" />
    <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
    <script src="https://unpkg.com/dropzone"></script>
    <script src="https://unpkg.com/cropperjs"></script>

<div class="row">
    <div class="container">
        <div class="page-header">
            <div class="page-leftheader">
                <h4 class="page-title mb-0">{{$program['name']}} </h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('admin.programs')}}">Programs</a></li>
                    <li class="breadcrumb-item active">{{$program['program_name']}}</a></li>
                </ol>
            </div>
            <div class="page-rightheader">

            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-content')
<div class="container">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            @include('admin.include.menu')
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="page-rightheader mb-4">


            </div>


             <div class="container">
                 @if (in_array($program['status'],['Pending','Closed','Ended']))
                     <a href="{{ route('admin.program.update',['id'=>$program['id'],'status'=>'Active'])}}" class="text-success"><i class="fa fa-check"></i> Publish Program</a>
                 @else
                     <a href="{{ route('admin.program.update',['id'=>$program['id'],'status'=>'Closed'])}}" class="text-danger"><i class="fa fa-times"></i> Unpublish Program</a>
                 @endif
                     <a href="{{ route('admin.program.registrations',['program'=>$program['id']])}}" class="text-info ml-2"><i class="fa fa-users"></i> View Registrations</a>
                 <div class="image_area mt-4">
                     <form method="post">
                         @csrf
                         <input type="hidden" value="{{$program['id']}}" name="id">
                         <input type="hidden" value="program" name="type">

                         <label for="upload_image">
                             <img src="{{secure_asset(env('FULL').$program['cover'])}}" id="uploaded_image" alt="Banner" class="img-responsive img-circle" />
                             <span class="overlay1">
                                    <div class="text fs-14 pt-4" style="background: #FFF;padding: 20px;opacity: .8;border-radius: 9px;">Click to Change Banner Image</div>
                                </span>
                             <input type="file" name="image" class="image" id="upload_image" style="display:none" />
                         </label>
                     </form>
                 </div>
                 <div class="row">
                     <div class="col-md-9">
                         <h4>{{$program['program_name']}} </h4>
                         <div class="geodir-category-location fl-wrap">
                             <i class="fa fa-user ml-3"></i> <b>Organiser:</b>  {{$program['organiser']}} <br/>
                             <i class="fa fa-calendar ml-3"></i> <b>Apply Before:</b>  {{$program['apply_before']}} <br/>
                             <i class="fa fa-calendar ml-3"></i> <b>From:</b> {!!date_format(date_create($program['start_date']),"Y/m/d").' <b>To:</b> '.date_format(date_create($program['end_date']),"Y/m/d")!!}
                         </div>


                     </div>
                 </div>
                 <div class="row">

                    <div class="col-md-12">
                        <div class="list-single-main-item fl-wrap block_box">
                            <div class="list-single-main-item-title mt-4">
                                <h5>SUMMARY</h5>
                            </div>
                            <div class="list-single-main-item_content fl-wrap">
                                {!!$program['summary']!!}
                            </div>
                            <div class="list-single-main-item-title mt-5">
                                <h5>ABOUT PROGRAM</h5>
                            </div>
                            <div class="list-single-main-item_content fl-wrap">
                                {!!$program['description']!!}
                            </div>


                        </div>

                    </div>
                 </div>

             </div>
        </div>

    </div>

</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crop Image Before Upload</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-8">
                            <img src="" id="sample_image" />
                        </div>
                        <div class="col-md-4">
                            <div class="preview"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="crop" class="btn btn-primary">Crop</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

 @endsection

@section('script')
    <!-- Initialize Quill editor -->
    <script>

        $(document).ready(function() {
            $('#description').summernote({height: 150});
        });
    </script>
    <script>

        $(document).ready(function(){
            var $modal = $('#modal');
            var image = document.getElementById('sample_image');
            var cropper;

            $('#upload_image').change(function(event){
                var files = event.target.files;

                var done = function(url){
                    image.src = url;
                    $modal.modal('show');
                };

                if(files && files.length > 0)
                {
                    reader = new FileReader();
                    reader.onload = function(event)
                    {
                        done(reader.result);
                    };
                    reader.readAsDataURL(files[0]);
                }
            });

            $modal.on('shown.bs.modal', function() {
                cropper = new Cropper(image, {
                    aspectRatio: 860/ 360,
                    viewMode:3,
                    preview:'.preview'
                });
            }).on('hidden.bs.modal', function(){
                cropper.destroy();
                cropper = null;
            });

            $('#crop').click(function(){
                canvas = cropper.getCroppedCanvas({
                    width:900,
                    height:360
                });

                canvas.toBlob(function(blob){
                    url = URL.createObjectURL(blob);
                    var reader = new FileReader();
                    reader.readAsDataURL(blob);
                    reader.onloadend = function(){
                        var base64data = reader.result;
                        $.ajax({
                            url:'{{route('admin.program.banner')}}',
                            method:'POST',
                            data:{image:base64data,id:{{$program['id']}},'type':'program'},
                            success:function(data)
                            {
                                $modal.modal('hide');
                                history.go(0);
                            }
                        });
                    };
                });
            });

        });
    </script>
@endsection
