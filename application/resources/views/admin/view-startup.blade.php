@extends('build.admin')
@section('profile-summary')
    <link rel="stylesheet" href="https://unpkg.com/dropzone/dist/dropzone.css" />
    <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
    <script src="https://unpkg.com/dropzone"></script>
    <script src="https://unpkg.com/cropperjs"></script>

<div class="row">
    <div class="container">
        <div class="page-header mt-7">
            <div class="page-leftheader">
                <h4 class="page-title mb-0">{{$startup['name']}} </h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                    <li class="breadcrumb-item"><a href="{{route('admin.startups')}}">Startups</a></li>
                    <li class="breadcrumb-item active"><a href="">{{$startup['name']}}</a></li>
                </ol>
            </div>
            <div class="page-rightheader">

            </div>
        </div>
    </div>
</div>
@endsection
@section('admin-content')
<div class="container">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            @include('admin.include.menu')
        </div>
        <div class="col-xl-9 col-lg-8">

            <div class="row" style="width: 100%">
                <div class="col-2">
                    <img alt="User Avatar" class="rounded-circle avatar-xxl mt-4  border p-0" src="{{secure_asset(env('LOGO').$startup['logo'])}}">
                </div>
                <div class="col-3">
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{$startup['name']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Name</span>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{$startup['email']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Email</span>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{!! $startup['registration_status']?'<span class="text-success">Registered</span>':'<span class="text-warning">Not Registered</span>'!!}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Registration Status</span>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{$startup['phone_number']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Phone Number</span>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{$startup['stage']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Stage</span>
                        </div>
                    </div>
                    <div class="media mb-1">
                        <div class="media-body">
                            <div class="d-md-flex align-items-center mt-1">
                            <h6 class="mb-1">{{ $startup['created_at']}}</h6>
                        </div>
                            <span class="mb-0 fs-13 text-muted">Created At</span>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row pull-right mt-5">
            @if($startup->user->role =='admin')
                <a href="" class="text-warning mr-3" data-target="#upload_Logo" data-toggle="modal"><i class="fa fa-image"></i> Upload Logo</a>
                <a href="" class="text-info mr-3" data-target="#send_claim" data-toggle="modal"><i class="fa fa-envelope"></i> Send Claim Email</a>
                @endif
                @if (in_array($startup['status'],['Pending','Unpublished']))
                    <a href="{{ route('admin.startup.update',['id'=>$startup['id'],'status'=>'Published'])}}" class="text-success mr-3"><i class="fa fa-check"></i> Publish Startup</a>
                @else
                    <a href="{{ route('admin.startup.update',['id'=>$startup['id'],'status'=>'Unpublished'])}}" class="text-danger mr-3"><i class="fa fa-times"></i> Unpublish Startup</a>
                @endif

                @if($startup->is_featured)
                    <a href="{{route('admin.startup.feature',['startupId'=>$startup->id,'feature'=>'false'])}}" class="text-black-50 mr-3" ><i class="fa fa-times"></i> Unfeature startup</a>
                @else
                    <a href="{{route('admin.startup.feature',['startupId'=>$startup->id,'feature'=>'true'])}}" class="text-black-50 mr-3" ><i class="fa fa-check"></i> Feature Startup</a>
                @endif

            </div>


            <div class="container mt-7">
                <div class="row">

                    <div class="image_area mt-4">
                        <form method="post">
                            @csrf
                            <input type="hidden" value="{{$startup['id']}}" name="id">
                            <input type="hidden" value="startup" name="type">

                            <label for="upload_image">
                                <img src="{{secure_asset(env('FULL').$startup['banner'])}}" id="uploaded_image" class="img-responsive img-circle" />
                                <div class="overlay1">
                                    <div class="text fs-14 pt-4" style="background: #FFF;padding: 20px;opacity: .8;border-radius: 9px;">Click to Change Banner Image</div>
                                </div>
                                <input type="file" name="image" class="image" id="upload_image" style="display:none" />
                            </label>
                        </form>
                    </div>

                    <div class="col-lg-12 col-md-auto">
                        <div class="card-body">
                            <h4 class="pro-user-username centered mb-3 text-gray text-uppercase font-weight-bold">About {{$startup['name']}} </h4>
                            <div class="main-profile-bio mb-0">
                            <p>{!!$startup['about']!!}</p>
                            </div>
                            <h4 class="pro-user-username centered mb-3 text-gray font-weight-bold">TRACTION </h4>
                            <div class="main-profile-bio mb-0">
                            <p>{!!$startup['traction']!!}</p>
                            </div>

                            <h4 class="pro-user-username centered mb-3 text-gray font-weight-bold">PROBLEM </h4>
                            <div class="main-profile-bio mb-0">
                            <p>{!!$startup['problem']!!}</p>
                            </div>
                            <h4 class="pro-user-username centered mb-3 text-gray font-weight-bold">SOLUTION </h4>
                            <div class="main-profile-bio mb-0">
                            <p>{!!$startup['solution']!!}</p>
                            </div>
                            <div class="list-single-main-item-title mt-5 text-gray"><h5>INDUSTRIES YOU SERVE</h5></div>
                            <div class="list-single-main-item_content fl-wrap">
                                {!!$startup['industries']!!}
                            </div>
                            <div class="list-single-main-item-title mt-5 text-gray"><h5>SECTORS YOU SERVE</h5></div>
                            <div class="list-single-main-item_content fl-wrap">
                                {!!$startup['sectors']!!}
                            </div>

                            <div class="row mt-5">
                                <div class="col-12">
                                    <h5>OUR TEAM</h5>

                                    @foreach ($teams as $team)
                                    <div class="media mt-0 mb-3">
                                        <figure class="rounded-circle align-self-start mb-0">
                                            <img src="{{secure_asset('public/uploads/user/'.$team['photo'])}}" alt="Generic placeholder image" class="avatar brround avatar-md mr-3">
                                        </figure>
                                        <div class="media-body">
                                            <h6 class="time-title p-0 mb-0 font-weight-semibold leading-normal">{{$team->member_name}}</h6>
                                            {{$team['designation']}} - {{$team['memberType']=='teamMember'?'Team Member':'Board Member'}}
                                        </div>
                                     </div>
                                    @endforeach


                                </div>

                            </div>

                    </div>
                    </div>
                 </div>
            </div>

        </div>

    </div>

</div>

<div class="modal" id="upload_Logo">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header">
                <h6 class="modal-title">Upload {{$startup['name']}} Logo</h6>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
            </div>
            <form action="{{route('admin.startup.logo')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{$startup['id']}}" name="id">
                <input type="hidden" value="startup" name="type">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="form-label">Add Logo</div>
                        <input type="file" class="form-control" name="upload_logo"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-indigo" type="submit">Save changes</button>
                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="send_claim">
    <div class="modal-dialog" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title">Send Profile Claim Email</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('email.claim')}}" method="POST">
            @csrf
            <input type="hidden" value="{{$startup['id']}}" name="id">
            <input type="hidden" value="startup" name="category">
          <div class="modal-body">
            <div class="form-group">
                <label for="exampleInputEmail1" class="form-label">Email Address</label>
                <input type="email" class="form-control" required id="claimEmail" name="email" placeholder="Enter Email Address">
            </div>

          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Send Email</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crop Image Before Upload</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-8">
                            <img src="" id="sample_image" />
                        </div>
                        <div class="col-md-4">
                            <div class="preview"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="crop" class="btn btn-primary">Crop</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
 @endsection

@section('script')
    <script>

        $(document).ready(function(){
            var $modal = $('#modal');
            var image = document.getElementById('sample_image');
            var cropper;

            $('#upload_image').change(function(event){
                var files = event.target.files;

                var done = function(url){
                    image.src = url;
                    $modal.modal('show');
                };

                if(files && files.length > 0)
                {
                    reader = new FileReader();
                    reader.onload = function(event)
                    {
                        done(reader.result);
                    };
                    reader.readAsDataURL(files[0]);
                }
            });

            $modal.on('shown.bs.modal', function() {
                cropper = new Cropper(image, {
                    aspectRatio: 860/ 360,
                    viewMode:3,
                    preview:'.preview'
                });
            }).on('hidden.bs.modal', function(){
                cropper.destroy();
                cropper = null;
            });

            $('#crop').click(function(){
                canvas = cropper.getCroppedCanvas({
                    width:900,
                    height:360
                });

                canvas.toBlob(function(blob){
                    url = URL.createObjectURL(blob);
                    var reader = new FileReader();
                    reader.readAsDataURL(blob);
                    reader.onloadend = function(){
                        var base64data = reader.result;
                        $.ajax({
                            url:'{{route('admin.startup.banner')}}',
                            method:'POST',
                            data:{image:base64data,id:{{$startup['id']}},'type':'startup'},
                            success:function(data)
                            {
                                $modal.modal('hide');
                                history.go(0);
                             },
                            error: function (request, status, error) {
                                alert(request.responseText);
                            }
                        });
                    };
                });
            });

        });
    </script>
@endsection
