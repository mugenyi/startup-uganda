@extends('build.master')
@section('content')
        <div class="container mt-3">
    <div class="row">
            <div class="col-md-9">
                <h2>{{$post['post_title']}}</h2>
                <div class="row py-2 px-4 mb-2">
                    <img src="{{secure_asset(env('FULL').$post->image_name)}}" alt="{{$post->post_title}}" />
                </div>
                <div class="row">
                    {!! $post->body !!}
                </div>
            </div>
            <div class="col-md-3 pt-9">
                <h4>LATEST ARTICLES</h4>
                <hr/>
                @foreach($posts as $p)
                   <div class="row mb-2">
                    <a href="{{route('post.view',['slug'=>Illuminate\Support\Str::slug($p['post_title']).'_'.$p['id']])}}" >
                        <img src="{{secure_asset(env('FULL').$p['image_name'])}}" class="pt-3" style="width: 100%; height:200px;"  alt="">

                        <p class="pt-2">{{$p['post_title']??" Visit Article"}}</p>
                    </a>
                   </div>

                    @endforeach
            </div>

        </div>
    </div>

@endsection

