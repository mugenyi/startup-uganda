@extends('build.app')

@section('content')
<div class="container">
    <div class="row">
       <div class="col-md-6">
          <div class="">
             <div class="">
                <div class="card-body">
                   <h2 class="display-4 mb-2 font-weight-bold text-orange text-center"><strong>Login</strong></h2>
                <h4 class="mb-7 text-center">Sign In to your account | Back to <a href="{{route('index')}}">Home</a></h4>
                   <form method="POST" action="{{ route('login') }}">
                     @csrf

                     <div class="row">
                        <div class="col-md-9 col-lg-9 col-sm-12 d-block mx-auto">
                            <div class="input-group mb-4">
                               <div class="input-group-prepend">
                                  <div class="input-group-text"> <i class="fa fa-user"></i> </div>
                               </div>
                               <input type="text" class="form-control" name="email" placeholder="Email address">
                            </div>
                            <div class="input-group mb-4">
                               <div class="input-group-prepend">
                                  <div class="input-group-text"> <i class="fa fa-lock"></i> </div>
                               </div>
                               <input type="password" class="form-control" name="password" placeholder="Password">
                            </div>
                            <div class="row">
                               <div class="col-12"> <button type="submit" class="btn btn-secondary btn-block px-4">Login</button> </div>
                               <div class="col-12 text-center">
                                   <a href="{{ route('password.request') }}" class="btn btn-link box-shadow-0 px-0 ">Forgot password?</a>
                                </div>
                            </div>
                         </div>
                      </div>
                 </form>

                   <div class="text-center pt-4">
                   <div class="font-weight-normal fs-16">I don't have an account <a class="btn-link font-weight-normal" href="{{route('register')}}">Register Here</a></div>
                   </div>
                </div>
                <div class="custom-btns text-center">
                   <a href="{{url('login/facebook')}}"><button class="btn btn-icon" style="background-color: #4267B2" type="button"><span class="btn-inner-icon"><i class="fa fa-facebook"></i></span></button></a>
                   <a href="{{url('login/google')}}"><button class="btn btn-icon" style="background-color: #DB4437" type="button"><span class="btn-inner-icon"><i class="fa fa-google"></i></span></button></a>
                   <a href="{{url('login/twitter')}}"><button class="btn btn-icon" style="background-color: #1DA1F2" type="button"><span class="btn-inner-icon"><i class="fa fa-twitter"></i></span></button></a>
                </div>
             </div>
          </div>
       </div>
       <div class="col-md-6 d-none d-md-flex align-items-center">
          <img src="{{secure_asset('public/startup/assets/images/png/login-pana.png')}}" alt="img">
       </div>
    </div>
 </div>
@endsection
