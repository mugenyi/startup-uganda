

@extends('build.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="">
                    <div class="">
                        <div class="card-body mt-9">

                            <h2 class="display-4 mb-2 font-weight-bold text-orange text-center"><strong>{{ __('Reset Password') }}</strong></h2>
                            <h4 class="mb-7 text-center"><a href="{{route('login')}}">Sign In</a> to your account | Back to <a href="{{route('index')}}">Home</a></h4>

                        @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form method="POST" action="{{ route('password.email') }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Send Password Reset Link') }}
                                        </button>
                                    </div>
                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 d-none d-md-flex align-items-center">
                <img src="{{secure_asset('public/startup/assets/images/png/login-pana.png')}}" alt="img">
            </div>
        </div>
    </div>
@endsection

