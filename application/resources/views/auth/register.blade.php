@extends('build.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="">
                    <div class="">
                        <div class="card-body">
                            <h2 class="display-4 mb-2 font-weight-bold text-center"><strong>Register</strong></h2>
                            <h4 class="mb-5 text-center">Create New Account | Back to <a href="{{route('index')}}">Home</a></h4>
                            @if($errors->any())
                                <p class="font-weight-bold text text-danger text-center mb-3">{{$errors->first()}}</p>
                            @endif

                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                @if (isset($business))
                                    <input type="hidden" name="category_id" value="{{$business['id']}}">
                                    <input type="hidden" name="category" value="{{$business['category']}}">
                                @endif

                                <div class="row">
                                    <div class="col-md-9 col-lg-9 col-sm-12  d-block mx-auto">
                                        <div class="input-group mb-4">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"> <i class="fa fa-user"></i>
                                                </div>
                                            </div>
                                            <input type="text" class="form-control" name="name" placeholder="Full Name">
                                        </div>
                                        <div class="input-group mb-4">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"> <i class="fa fa-envelope"></i>
                                                </div>
                                            </div>
                                            <input type="text" class="form-control" name="email" placeholder="Enter Email">
                                        </div>
                                        <div class="input-group mb-4">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"> <i class="fa fa-lock"></i> </div>
                                            </div>
                                            <input type="password" name="password" id="password" required minlength="8" class="form-control" placeholder="Password">
                                        </div>

                                        <div class="input-group mb-4">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"> <i class="fa fa-lock"></i> </div>
                                            </div>
                                            <input type="password" name="password_confirmation" id="confirm_password" required minlength="8" class="form-control" placeholder="Confirm Password">
                                        </div>
                                        <div class="form-group">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" required class="custom-control-input">
                                                <span class="custom-control-label"><a href="terms.html" class="text-gray">Agree the Terms and policy</a></span> </label>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <span id='message'></span>
                                            </div>
                                            <div class="col-12">
                                                <button type="submit" id="" class="btn btn-secondary btn-block px-4 register_btn">Create New Account</button>
                                            </div>
                                            <div class="col-12 text-center">
                                                <a href="" class="btn btn-link box-shadow-0 px-0">Forgot password?</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <div class="text-center pt-4">
                                <div class="font-weight-normal fs-16">You Already have an account <a class="btn-link font-weight-normal" href="{{route('login')}}">Login Here</a></div>
                            </div>
                        </div>
                        <div class="custom-btns text-center">
                            <a href="{{url('login/facebook')}}"><button class="btn btn-icon" style="background-color: #4267B2" type="button"><span class="btn-inner-icon"><i class="fa fa-facebook"></i></span></button></a>
                            <a href="{{url('login/google')}}"><button class="btn btn-icon" style="background-color: #DB4437" type="button"><span class="btn-inner-icon"><i class="fa fa-google"></i></span></button></a>
                            <a href="{{url('login/twitter')}}"><button class="btn btn-icon" style="background-color: #1DA1F2" type="button"><span class="btn-inner-icon"><i class="fa fa-twitter"></i></span></button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 pt-8 align-items-center">
                @if (isset($business))
                    <div class="row mb-3">
                        <h4>Register to Claim this profile</h4>
                    </div>
                    <div class="row">
                        <div class="card box-widget widget-user">
                            <div class="widget-user-image mx-auto mt-5"><img alt="User Avatar" class="rounded-circle" src="{{secure_asset('public/uploads/logo/'.$business['logo'])}}"></div>
                            <div class="card-body text-center">
                                <div class="pro-user">
                                    <h4 class="pro-user-username text-dark mb-1 font-weight-bold">{{$business['name']}}</h4>
                                    <h6 class="pro-user-desc text-muted">{{$business->address}}</h6>
                                </div>
                            </div>

                        </div>




                    </div>


                @else
                    <div class="d-none d-md-flex ">
                        <img src="{{secure_asset('public/startup/assets/images/png/login.png')}}" alt="img">
                    </div>
                @endif
            </div>



        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            //$('#register_btn').prop('disabled', true);

            $('#password, #confirm_password').on('keyup', function () {
                if ($('#password').val() == $('#confirm_password').val()) {
                    // $(".register_btn").prop('disabled', false );
                    $('.register_btn').button("enabled");
                    $('#message').html('Matching').css('color', 'green');

                } else
                    $('#message').html('Passwords don\'t Match').css('color', 'red');
                // attr() method applied here
                $('.register_btn').button("disabled");
            });
        });
    </script>

@endsection
