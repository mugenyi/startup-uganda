@extends('build.master')
@section('content')
    <div class="container mt-2">
        <div class="row" style="background-image: url('{{secure_asset(env('FULL').$blog->image)}}');background-size: cover;background-position: center; height: 400px"  >

        </div>
        <div class="row mt-3">

                <h3 class="mb-2 text-orange">{{$blog->title}}</h3>

                <div class="mt-3 mb-4" style="width: 100%">
                    {!! $blog->content !!}
                </div>

            @foreach(explode(',',$blog->meta_tags) as $tag)
                <span class="badge badge-dark mr-1 text-md">#{{$tag}}</span>
            @endforeach
        </div>
        <div class="row" style="width: 100%">
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="media mb-1">
                    <div class="media-body">
                        <div class="d-md-flex align-items-center mt-1">
                            <p class="mb-1 font-weight-semibold">{{$blog->author_name}} </p>
                        </div>
                        <span class="mb-0 fs-13 text-muted">Author Name</span>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="media mb-1">
                    <div class="media-body">
                        <div class="d-md-flex align-items-center mt-1">
                            <p class="mb-1 font-weight-semibold">{{$blog->author_email}}</p>
                        </div>
                        <span class="mb-0 fs-13 text-muted">Author Emaill</span>
                    </div>
                </div>

            </div>
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="media mb-1">
                    <div class="media-body">
                        <div class="d-md-flex align-items-center mt-1">
                            <p class="mb-1 font-weight-semibold">{{$blog->created_at}}</p>
                        </div>
                        <span class="mb-0 fs-13 text-muted">Created On</span>
                    </div>
                </div>

            </div>

        </div>

    </div>
@endsection
