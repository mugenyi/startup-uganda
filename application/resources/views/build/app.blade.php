<html lang="en" dir="ltr">

    <head><!-- Meta data -->
    <meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta content="Startup Uganda" name="description">
    <meta content="Startup Uganda" name="author">
    <meta name="keywords" content="admin panel ui, user dashboard template, web application templates, premium admin templates, html css admin templates, premium admin templates, best admin template bootstrap 4, dark admin template, bootstrap 4 template admin, responsive admin template, bootstrap panel template, bootstrap simple dashboard, html web app template, bootstrap report template, modern admin template, nice admin template">
    <!-- Title --> <title>Startup Uganda</title>
    <!--Favicon --> <link rel="icon" href="{{secure_asset('public/⁨theme⁩/⁨images⁩/wish-list.png')}}" type="image/x-icon"> <!--Bootstrap css -->
    <link href="{{secure_asset('public/startup/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet"> <!-- Style css -->
    <link href="{{secure_asset('public/startup/assets/css/style.css')}}" rel="stylesheet">
    <link href="{{secure_asset('public/startup/assets/css/dark.css')}}" rel="stylesheet">
    <link href="{{secure_asset('public/startup/assets/css/skin-modes.css')}}" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Animate css -->
    <link href="{{secure_asset('public/startup/assets/css/animated.css')}}" rel="stylesheet">
    <!-- P-scroll bar css--> <link href="{{secure_asset('public/startup/assets/plugins/p-scrollbar/p-scrollbar.css')}}" rel="stylesheet">
    <!---Icons css-->
    <link href="{{secure_asset('public/startup/assets/css/icons.css')}}" rel="stylesheet">
    <!-- Simplebar css -->
    <link rel="stylesheet" href="{{secure_asset('public/startup/assets/plugins/simplebar/css/simplebar.css')}}">
    <!-- INTERNAL Select2 css -->
    <link href="{{secure_asset('public/startup/assets/plugins/select2/select2.min.css')}}" rel="stylesheet">
    <!-- Color Skin css -->
    <link id="theme" href="{{secure_asset('public/startup/assets/colors/color1.css')}}" rel="stylesheet" type="text/css">
    <!-- Switcher css -->
    <link href="{{secure_asset('public/startup/assets/switcher/css/switcher.css')}}" rel="stylesheet" type="text/css">
    <link href="{{secure_asset('public/startup/assets/switcher/demo.css')}}" rel="stylesheet" type="text/css">

</head>
<body class="h-100vh bg-primary232" cz-shortcut-listen="true">
    <div class="horizontalMenucontainer">
        <div class="page">
            <div class="page-main"  style="margin-top: 70px">
                @yield('content')
            </div>

        </div>

    </div>

    <script src="{{secure_asset('public/startup/assets/js/jquery-3.5.1.min.js')}}"></script>
        <script src="{{secure_asset('public/startup/assets/plugins/bootstrap/popper.min.js')}}"></script>
        <script src="{{secure_asset('public/startup/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{secure_asset('public/startup/assets/plugins/othercharts/jquery.sparkline.min.js')}}"></script>
        <script src="{{secure_asset('public/startup/assets/js/circle-progress.min.js')}}"></script>
        <script src="{{secure_asset('public/startup/assets/plugins/rating/jquery.rating-stars.js')}}"></script>
        <script src="{{secure_asset('public/startup/assets/plugins/horizontal-menu/horizontal-menu.js')}}"></script>

        @yield('script')
</body>
</body>

</html>
