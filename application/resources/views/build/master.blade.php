<html lang="en" dir="ltr">

    <head><!-- Meta data -->
    <meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    {!! SEO::generate() !!}
    <meta content="Startup Uganda" name="author">
    <meta name="keywords" content="{{env('KEYWORDS')}}">
        <meta name="google-site-verification" content="_QlgHXLGjMNK1SV5FDCoCycvKvonTp949aWX1jvfAQ0" />
    <!-- Title --> <title>Startup Uganda</title>
        <!--=============== favicons ===============-->
        <link rel="apple-touch-icon" sizes="180x180" href="{{secure_asset('public/images/favicon/apple-touch-icon.png')}}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{secure_asset('public/images/favicon/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{secure_asset('public/images/favicon/favicon-16x16.png')}}">
        <link rel="manifest" href="{{secure_asset('public/images/favicon/site.webmanifest')}}">
        <!--Bootstrap css -->
    <link href="{{secure_asset('public/startup/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet"> <!-- Style css -->
    <link href="{{secure_asset('public/startup/assets/css/style.css')}}" rel="stylesheet">
    <link href="{{secure_asset('public/startup/assets/css/custom-style.css')}}" rel="stylesheet">
    <link href="{{secure_asset('public/startup/assets/css/dark.css')}}" rel="stylesheet">
    <link href="{{secure_asset('public/startup/assets/css/skin-modes.css')}}" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Animate css -->
    <link href="{{secure_asset('public/startup/assets/css/animated.css')}}" rel="stylesheet">
    <!-- P-scroll bar css--> <link href="{{secure_asset('public/startup/assets/plugins/p-scrollbar/p-scrollbar.css')}}" rel="stylesheet">
    <!---Icons css-->
    <link href="{{secure_asset('public/startup/assets/css/icons.css')}}" rel="stylesheet">
    <!-- Simplebar css -->
    <link rel="stylesheet" href="{{secure_asset('public/startup/assets/plugins/simplebar/css/simplebar.css')}}">
    <!-- INTERNAL Select2 css -->
    <link href="{{secure_asset('public/startup/assets/plugins/select2/select2.min.css')}}" rel="stylesheet">
    <!-- Color Skin css -->
    <link id="theme" href="{{secure_asset('public/startup/assets/colors/color1.css')}}" rel="stylesheet" type="text/css">
    <!-- Switcher css -->
    <link href="{{secure_asset('public/startup/assets/switcher/css/switcher.css')}}" rel="stylesheet" type="text/css">
    <link href="{{secure_asset('public/startup/assets/switcher/demo.css')}}" rel="stylesheet" type="text/css">
        <script src="{{ secure_asset('public/js/share.js') }}"></script>

</head>
<body class="app" cz-shortcut-listen="true">
    <div class="horizontalMenucontainer">
        <div class="page">
            <div class="page-main">
                @include('includes.header')

                @yield('content')
            </div>

            @include('includes.footer')
        </div>

    </div>
    <script>
        function displayFilters(){
            var nFilter = document.getElementById('content-filter');
            nFilter.setAttribute("style", "display:block !important;");
            var nFilter = document.getElementById('filter');
            nFilter.setAttribute("style", "display:none !important;");

        }
    </script>

            <script src="{{secure_asset('public/startup/assets/js/jquery-3.5.1.min.js')}}"></script>
            <script src="{{secure_asset('public/startup/assets/plugins/bootstrap/popper.min.js')}}"></script>
            <script src="{{secure_asset('public/startup/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
            <script src="{{secure_asset('public/startup/assets/plugins/othercharts/jquery.sparkline.min.js')}}"></script>
            <script src="{{secure_asset('public/startup/assets/js/circle-progress.min.js')}}"></script>
            <script src="{{secure_asset('public/startup/assets/plugins/rating/jquery.rating-stars.js')}}"></script>
            <script src="{{secure_asset('public/startup/assets/plugins/horizontal-menu/horizontal-menu.js')}}"></script>
            <script src="{{secure_asset('public/startup/assets/js/stiky.js')}}"></script>
            {{-- <script src="{{secure_asset('public/startup/assets/plugins/p-scrollbar/p-scrollbar.js')}}"></script>
            <script src="{{secure_asset('public/startup/assets/plugins/p-scrollbar/p-scroll.js')}}"></script> --}}
            <script src="{{secure_asset('public/startup/assets/plugins/peitychart/jquery.peity.min.js')}}"></script>
            <script src="{{secure_asset('public/startup/assets/plugins/peitychart/peitychart.init.js')}}"></script>
            <script src="{{secure_asset('public/startup/assets/js/apexcharts.js')}}"></script>
{{--            <script src="{{secure_asset('public/startup/assets/plugins/echarts/echarts.js')}}"></script>--}}
{{--            <script src="{{secure_asset('public/startup/assets/plugins/chart/chart.bundle.js')}}"></script>--}}
{{--            <script src="{{secure_asset('public/startup/assets/plugins/chart/utils.js')}}"></script>--}}
            <script src="{{secure_asset('public/startup/assets/plugins/select2/select2.full.min.js')}}"></script>
            <script src="{{secure_asset('public/startup/assets/js/select2.js')}}"></script>
            <script src="{{secure_asset('public/startup/assets/plugins/moment/moment.js')}}"></script>
{{--            <script src="{{secure_asset('public/startup/assets/js/index1.js')}}"></script>--}}
            <script src="{{secure_asset('public/startup/assets/plugins/simplebar/js/simplebar.min.js')}}"></script>
            <script src="{{secure_asset('public/startup/assets/js/custom.js')}}"></script>
            <script src="{{secure_asset('public/startup/assets/switcher/js/switcher.js')}}"></script>
            <script src="{{secure_asset('public/startup/assets/switcher/js/switcher.js')}}"></script>
</body>

</html>
