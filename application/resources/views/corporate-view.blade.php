    @extends('build.master')
@section('content')
@include('includes.menus.network-menu')

<div class="row">
    <div class="col-lg-6" style="background: url({{secure_asset('public/uploads/full/'.$corporate['banner'])}}) no-repeat center center;height:320px">
    </div>
    <div class="col-lg-6 p-6" style="background-color: #FFF">
        <div class="container">
            <div class="row mt-0">

                <div class="media" style="height: 110px;">
                    <div class=" mr-3 mt-1">
                        <img src="{{secure_asset('public/uploads/logo/'.$corporate['logo'])}}" alt="img" class="avatar-xxl mr-2 brround p-1 shadow2">
                    </div>
                    <div class="media-body mt-4">
                        <div class="fs-25 text-bolder text-gray3 font-weight-normal1" style="font-weight:bolder !important"> {{$corporate['name']}} </div>
                        <small class="fs-14 text-gray2">{{$corporate['tagline']}}</small>
                    </div>
                </div>
                <hr/>
                <div class="row" style="width: 100%">

                    <div class="col-lg-6">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex text-gray3 align-items-center mt-1">
                                <h6 class="mb-1">{{$corporate['address']}}</h6>
                            </div>
                                <span class="mb-0fs-13 text-muted">Address</span>
                            </div>
                        </div>
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex text-gray3  align-items-center mt-1">
                                <h6 class="mb-1">{{$corporate['phone_number']}}</h6>
                            </div>
                                <span class="mb-0 fs-13 text-muted">Phone Number</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex text-gray3 align-items-center mt-1">
                                <h6 class="mb-1">{{ $corporate['website']}}</h6>
                            </div>
                                <span class="mb-0 fs-13 text-muted">Website</span>
                            </div>
                        </div>

                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex text-gray3 align-items-center mt-1">
                                <h6 class="mb-1">{{ $corporate['email']}}</h6>
                            </div>
                                <span class="mb-0 fs-13 text-muted">Email</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>
<div class="container">
    <div class="row mt-4">
        <div class="col-md-7 fs-13 text-muted">
            <p>
                {{-- <span class="mb-0 fs-13 text-muted">Address: </span> {{ $corporate['address']}}<br/>
                <span class="mb-0 fs-13 text-muted">Website: </span> {{ $corporate['website']}}<br/> --}}
{{--                <span class="mb-0 fs-13 text-muted">Share: </span>--}}
{{--                <i class="fa fa-facebook fa-1x p-2" data-toggle="tooltip" title=""></i>--}}
{{--                <i class="fa fa-twitter fa-1x p-2" data-toggle="tooltip" title=""></i>--}}
{{--                <i class="fa fa-linkedin fa-1x p-2" data-toggle="tooltip" title=""></i>--}}
           </p>
            <span style="width: 100%" class="mt-3">
                @if(Auth::check() && App\Traits\HelperTrait::showChatButton(Auth::user()->id,$corporate->id))
                <a href="{{route('initiate.chat',['sender'=>Auth::user()->id,'receiver'=>$corporate->id,'type'=>'corporate'])}}"><i class="fa fa-comments text-orange"></i> Initiate Chat</a>
            @endif
            </span>
     </div>
     <div class="col-5 pull-right">
        <div class="row centered">
            @if ($corporate->jobs->count() >0)
            <a href="{{route('jobs')}}" data-toggle="tab" class="btn border-orange text-orange pull-right fs-13" style="padding: 10px 180px; margin-left:70px;font-weight:bold"> APPLICATION CALL</a>
            @endif

        </div>
     </div>
    </div>
</div>


 <div class="container mt-6">

     <div class="row mb-3">
         <div class="container">
             <h5 class="">ABOUT CORPORATION</h5>
             {!! $corporate['about']!!}
         </div>
     </div>
 </div>

@endsection
