@extends('build.master')
@section('content')
    <div class="container">

        <div class="row mt-8">
            <div class="col-lg-4 col-xl-3 pr-6 mb-5 d-block d-sm-none" id="filter">
                <a href="javascript:void(0)" onclick="displayFilters()" ><div class="fs-13 mb-4 text-orange"><i class="fa fa-search"></i> Filter Results</div></a>

            </div>
            <div class="col-lg-4 col-xl-3 pr-6 mb-5 d-none d-sm-block" id="content-filter">
                <div class="fs-16 mb-4 text-gray3 font-weight-bold">
                    <img src="{{secure_asset('public/images/filter.svg')}}" style="width: 18px" class="text-gray3 d-inline"> Filters
                </div>

                <form class="" method="POST" action="{{route('eso.search')}}">
                    @csrf
                    <div class="search-element">
                        <input type="search" class="form-control" name="name" placeholder="Enter description Keyword…" aria-label="Search" tabindex="1">

                    </div>

                    <h5 class="text-gray3 mt-6">Demographics</h5>
                    <select class="form-control mb-2" name="district">
                        <option value="">Search By District...</option>

                        {{ $districts= App\Traits\HelperTrait::getDistricts() }}
                        @foreach($districts as $district)
                            <option value="{{$district['district']}}">{{$district['district']}}</option>
                        @endforeach
                    </select>

                    <select class="form-control mb-2" name="industry">
                        <option value="">Search By Industry...</option>
                        {{ $industries= App\Traits\HelperTrait::getIndustries() }}
                        @foreach ($industries as $industry)
                            <option value="{{$industry['industry']}}">
                                {{$industry['industry']}}
                            </option>
                        @endforeach
                    </select>
                    <select class="form-control mb-2" name="sector">
                        <option value="">Search By Sector...</option>
                        {{ $sectors= App\Traits\HelperTrait::getSectors() }}
                        @foreach ($sectors as $sector)
                            <option value="{{$sector['sector_name']}}">
                                {{$sector['sector_name']}}
                            </option>
                        @endforeach

                    </select>



                    <button class="btn btn-orange mt-2" type="submit">Apply Filters</button>

                </form>
            </div>

            <div class="col-lg-8 col-xl-9 pl-5">
                <h4 class="text-gray3">Available offers from Startup Uganda</h4>

                <div class="text-gray2">
                    Showing {{($deals->currentpage()-1)*$deals->perpage()+1}} to {{$deals->currentpage()*$deals->perpage()}}
                    of  {{$deals->total()}} entries
                </div>
                <div class="row mt-5">
                    @foreach($deals as $deal)
                        <a href="{{route('deal.view',['id'=>$deal->id,'slug'=>App\Traits\HelperTrait::slugify($deal->name)])}}">
                            <div class="row mb-4 rounded p-3 shadow-lg" style="border: 0px solid #b3bdca ">
                                <div class="col-md-3">
                                    @if ($deal->category == "Job")
                                        <img alt="{{$deal['name']}}" class="rounded mx-auto d-block" style="height: 120px;width: auto" src="{{secure_asset('public/theme/images/job.png')}}">
                                    @elseif($deal->category == "Bid")
                                        <img alt="{{$deal['name']}}" class="rounded mx-auto d-block" style="height: 120px;width: auto" src="{{secure_asset('public/theme/images/bid.png')}}">
                                    @else
                                        <img alt="{{$deal['name']}}" class="rounded mx-auto d-block" style="height: 120px;width: auto" src="{{secure_asset('public/theme/images/other.png')}}">
                                    @endif
                                </div>
                                <div class="col-md-9">
                                    <h5 class="text-orange">{{$deal->category}}: {{$deal->name}}</h5>
                                    <hr/>
                                    {!!  strlen($deal->brief) > 200 ? substr($deal->brief,0,200)."..." : $deal->brief !!}
                                    <p class="mt-3">Deadline: {{$deal->deadline}}</p>
                                </div>
                            </div>
                        </a>


                    @endforeach

                </div>
                <div class="row">
                    {{$deals->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
