@extends('build.master')
@section('content')
@include('includes.menus.network-menu')

<div class="mb-4" style="background: url('https://images.idgesg.net/images/article/2020/02/ihub-100833090-large.jpg'); background-repeat: no-repeat;
    background-attachment: fixed;background-attachment: fixed;background-size: cover;
    background-repeat:no-repeat;
background-position: center center;height:360px;">

    <div class="row" >

        <div class="container text-center" style="padding-top:200px;z-index:99">
            <h2 class="fs-18 text-white font-weight-bold  " style="font-weight:bolder !important">{{$eso['name']}}</h2>
            <p class="fs-14 text-orange font-weight-bold ">{{$eso['tagline']}}</p>
            <img src="{{secure_asset('public/uploads/logo/'.$eso['logo'])}}"  name="{{$eso['name']}}" alt="img" style="width: 9rem;height:9rem" class="avatar-xxxl shadow2 center-block brround">

        </div>
    </div>
    </div>
    <div class="row mb-8">
        <div class="container">
            <div class="col-md-7 fs-13 mt-3 text-gray2">
                {{--             <div style="margin-bottom: 12px"><strong class="text-gray3 font-weight-semibold">Address</strong> {{$eso['address']}} </div>--}}
                {{--             <div style="margin-bottom: 12px"><strong class="text-gray3 mb-4 font-weight-semibold">Website: </strong>{{$eso['website']}}</div>--}}
                <div class="fl-wrap">
                    <strong class="text-gray3 font-weight-semibold">Social Media: </strong>
                    @if(!is_null($eso['facebook']))<a href="{{$eso['facebook']}}" target="_blank"><i class="fa fa-facebook fa-1x ml-3"></i></a>@endif
                    @if(!is_null($eso['twitter']))<a href="{{$eso['twitter']}}" target="_blank"><i class="fa fa-twitter ml-3"></i></a>@endif
                    @if(!is_null($eso['linkedin']))<a href="{{$eso['linkedin']}}" target="_blank"><i class="fa fa-linkedin ml-3"></i></a>@endif
                    @if(!is_null($eso['youtube']))<a href="{{$eso['youtube']}}" target="_blank"><i class="fa fa-youtube ml-3"></i></a>@endif

                </div>

            </div>
            <div class="col-md-5 pull-right">
                <div class="row centered">
                    @if ($eso->jobs->count() >0)
                        <a href="#jobs" data-toggle="tab" class="btn border-orange text-orange pull-right fs-13" style="padding: 10px 180px; margin-left:70px;font-weight:bold"> APPLICATION CALL</a>
                    @endif

                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="container">

            <div class="bb"></div>
                <h2 class="fs-14 text-orange text-center font-weight-bold ">ABOUT AGENCY</h2>
                <span class="text-center">{!! $eso['about']!!}</span>



                <h2 class="fs-14 text-center mt-8 font-weight-bold ">Address</h2>
            <p class="fs-14 font-weight-bold text-center ">{{$eso['address']}}</p>


            <h2 class="fs-14 mt-8 text-center font-weight-bold ">Contact Us:</h2>
            <p class="text-center">
                Phone Number: {{$eso['phone_number']}}<br/>
                Website: {{ $eso['website']}}<br/>
                Email: {{ $eso['email']}}
            </p>

    </div>



@endsection
