    @extends('build.master')
@section('content')
<div class="row">
    <div class="col-md-6" style="background: url({{secure_asset('public/uploads/banner/'.$event['cover'])}}) !important;background-size: cover;height:300px">
    </div>
    <div class="col-md-6 p-7" style="background-color: #FFF">
        @if(!is_null($event['link']))
        <button class="btn btn-warning pull-right" style="margin-top: -40px">Register</button>
        @endif
        <div class="container">
            <div class="row mt-0">
                <h5 class="text-uppercase text-gray3" style="width: 100%">{{$event['event_name']}}</h5>
                <hr/>
                <div class="row" style="width: 100%">
                    <div class="col-md-3">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex text-gray3  align-items-center mt-1">
                                <h6 class="mb-1">{{ date_format(date_create($event['start_date']),'Y m d')}}</h6>
                            </div>
                                <span class="mb-0 fs-13 text-muted">Start Date</span>
                            </div>
                        </div>
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex text-gray3 align-items-center mt-1">
                                <h6 class="mb-1">{{ $event['organiser']}}</h6>
                            </div>
                                <span class="mb-0 fs-13 text-muted">Organiser</span>
                            </div>
                        </div>


                    </div>
                    <div class="col-md-3">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex text-gray3 align-items-center mt-1">
                                <h6 class="mb-1">{{ date_format(date_create($event['end_date']),'Y m d')}}</h6>
                            </div>
                                <span class="mb-0 fs-13 text-muted">End Date</span>
                            </div>
                        </div>
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex text-gray3 align-items-center mt-1">
                                <h6 class="mb-1"><a href="{{$event['link']}}" target="_blank">Visit Website</a></h6>
                            </div>
                                <span class="mb-0 fs-13 text-muted">Website</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex text-gray3 align-items-center mt-1">
                                    <h6 class="mb-1">{{ $event['location']}}</h6>
                                </div>
                                <span class="mb-0 fs-13 text-muted">Location</span>
                            </div>
                        </div>
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex text-gray3 align-items-center mt-1">
                                <h6 class="mb-1">{{ $event['target_audience']}}</h6>
                            </div>
                                <span class="mb-0 fs-13 text-muted">Targets</span>
                            </div>
                        </div>

                    </div>
                </div>
                <h5 class="mt-4">Share: </h5>

                    <i class="fa fa-facebook fa-1x p-4" data-toggle="tooltip" title=""></i>
                    <i class="fa fa-twitter fa-1x p-4" data-toggle="tooltip" title=""></i>
                    <i class="fa fa-linkedin fa-1x p-4" data-toggle="tooltip" title=""></i>

            </div>


        </div>

    </div>
</div>

 <div class="container mt-6">
     <div class="row mb-3">
         <h5 class="">ABOUT THIS EVENT</h5>
     </div>
     <div class="row p-2">
        {!! $event['description']!!}
     </div>
 </div>


@endsection
