@extends('build.master')
@section('content')
<div class="container">

    <div class="row mt-8">
        <div class="col-lg-4 col-xl-3 pr-6 mb-5 d-block d-sm-none" id="filter">
            <a href="javascript:void(0)" onclick="displayFilters()" ><div class="fs-13 mb-4 text-orange"><i class="fa fa-search"></i> Filter Results</div></a>

        </div>
        <div class="col-lg-4 col-xl-3 pr-6 mb-5 d-none d-sm-block" id="content-filter">
            <div class="fs-16 mb-4 text-gray3 font-weight-bold">
                <img src="{{secure_asset('public/images/filter.svg')}}" style="width: 18px" class="text-gray3 d-inline"> Filters
            </div>
            <form class="" method="POST" action="{{route('events.search')}}">
                @csrf
                <div class="search-element">
                    <input type="search" class="form-control" name="name" placeholder="Enter description Keyword…" aria-label="Search" tabindex="1">
                </div>

                <h5 class="text-gray3 mt-6"> Event Details</h5>
                <div class="form-group">
                    <label class="form-label">Event Start Date</label>
                    <input class="form-control  mb-4" placeholder="Event Start Date" type="date" name="startDate">
                </div>
                <div class="form-group">
                    <label class="form-label">Event End Date</label>
                    <input class="form-control  mb-4" placeholder="Event End Date" type="date" name="endDate">
                </div>
                <div class="form-group">
                    <label class="form-label">Event Location</label>
                    <input class="form-control  mb-4" placeholder="Event Location" type="text" name="location">
                </div>
                <div class="form-group">
                    <label class="form-label">Organised By</label>
                    <input class="form-control  mb-4" placeholder="Event Organiser" type="text" name="organiser">
                </div>

                <button class="btn btn-orange mt-2" type="submit">Apply Filters</button>

            </form>
        </div>
        <div class="col-md-9 col-lg-9 col-sm-12">
            <div class="row">
                @foreach ($events as $event)
                    @include('includes.partial.event-col-4')
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
