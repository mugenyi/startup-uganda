@extends('build.master')
@section('content')
@include('includes.menus.network-menu')
    <div class="container">

        <div class="row mt-8">
            <div class="col-lg-4 col-xl-3 pr-6 mb-5 d-block d-sm-none" id="filter">
                <a href="javascript:void(0)" onclick="displayFilters()" ><div class="fs-13 mb-4 text-orange"><i class="fa fa-search"></i> Filter Results</div></a>

            </div>
            <div class="col-lg-4 col-xl-3 pr-6 mb-5 d-none d-sm-block" id="content-filter">
                <div class="fs-16 mb-4 text-gray3 font-weight-bold">
                    <img src="{{secure_asset('public/images/filter.svg')}}" style="width: 18px" class="text-gray3 d-inline"> Filters
                </div>

                <form class="" method="POST" action="{{route('government.search')}}">
                    @csrf
                    <div class="search-element">
                        <input type="search" class="form-control" name="name" placeholder="Enter description Keyword…" aria-label="Search" tabindex="1">

                    </div>

                    <h5 class="text-gray3 mt-6">Demographics</h5>
                    <select class="form-control mb-2" name="district">
                        <option value="">Search By District...</option>

                        {{ $districts= App\Traits\HelperTrait::getDistricts() }}
                        @foreach($districts as $district)
                            <option value="{{$district['district']}}">{{$district['district']}}</option>
                        @endforeach
                    </select>

                    {{--                    <select class="form-control mb-2" name="industry">--}}
                    {{--                        <option value="">Search By Industry...</option>--}}
                    {{--                        {{ $industries= App\Traits\HelperTrait::getIndustries() }}--}}
                    {{--                        @foreach ($industries as $industry)--}}
                    {{--                            <option value="{{$industry['industry']}}">--}}
                    {{--                                {{$industry['industry']}}--}}
                    {{--                            </option>--}}
                    {{--                        @endforeach--}}
                    {{--                    </select>--}}
                    <select class="form-control mb-2" name="sector">
                        <option value="">Search By Sector...</option>
                        {{ $sectors= App\Traits\HelperTrait::getSectors() }}
                        @foreach ($sectors as $sector)
                            <option value="{{$sector['sector_name']}}">
                                {{$sector['sector_name']}}
                            </option>
                        @endforeach

                    </select>



                    <button class="btn btn-orange mt-2" type="submit">Apply Filters</button>

                </form>
            </div>

            <div class="col-lg-8 col-xl-9 pl-5">
                <h4 class="text-gray3">Browse Government Agencies</h4>

                <div class="text-gray2">
                    Showing {{($governments->currentpage()-1)*$governments->perpage()+1}} to {{$governments->currentpage()*$governments->perpage()}}
                    of  {{$governments->total()}} entries
                </div>
                <div class="row mt-5">
                    <div class="table-responsive">
                        <table class="table table-striped card-table table-vcenter">
                            <thead style="background-color:#F4F4F4;color:#333333">
                            <tr  style="height: 54px;text-align: left;margin: 0;">
                                <th  style="vertical-align: middle;font-size:14px;text-transform: none;" colspan="2"><b>Name</b></th>
                                <th class="d-none d-sm-block" style="vertical-align: middle;font-size:14px;text-transform: none;"><b>One liner</b></th>
                                <th  style="vertical-align: middle;font-size:14px;text-transform: none;"><b>Sectors of Interest</b></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($governments as $government)
                                <tr class="fs-12" style="padding: 20px">
                                    <td style="height: 112px; width:100px">
                                        <img src="{{secure_asset(env('LOGO').''.$government['logo'])}}" alt="{{$government['name']}}"  style="min-width: 50px;min-height: 50px" class="avatar avatar-lg mr-2 brround">
                                    </td>
                                    <td style="width: 140px;">
                                        <a href="{{route('view.government',['id'=>$government['id'],'slug'=>App\Traits\HelperTrait::slugify($government['name'])])}}"><b>{{$government['name']}}</b></a>
                                    </td>
                                    <td class="d-none d-sm-block" style="width: 450px;padding-top: 40px;">{{$government['tagline']}}</td>
                                    <td class="text-capitalize">{{$government['sectors']}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="row">
                    {{$governments->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
