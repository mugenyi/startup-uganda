@extends('build.master')
@section('content')
@include('includes.menus.network-menu')

    <div class="row">
        <div class="col-md-12 col-lg-6" style="background: url({{secure_asset(env('BANNER').''.$government['cover'])}}) no-repeat center center;height:340px">
        </div>
        <div class="col-md-12 col-lg-6 px-6 py-2" style="background-color: #FFF">
            <div class="container">
                <div class="row mt-0">
                    <div class="media" style="height: 110px;margin-top: 0px !important;">
                        <div class="mr-3 mt-1">
                            <img src="{{secure_asset('public/uploads/logo/'.$government['logo'])}}" alt="img" class="avatar-xxl mr-2 brround p-1 shadow2">
                        </div>
                        <div class="media-body mt-3">
                            <div class="fs-25 text-bolder text-gray3 font-weight-normal1" style="font-weight:bolder !important"> {{$government['name']}} </div>
                            <small class="fs-14 text-muted">{{$government['address']}}</small>
                        </div>
                    </div>

                    <hr/>
                    <div class="row" style="width: 100%">

                        <div class="col-md-12 col-lg-6">
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex text-gray3 align-items-center mt-1">
                                        <h6 class="mb-1">{{$government['department']}}</h6>
                                    </div>
                                    <span class="mb-0 fs-13 text-muted">Department</span>
                                </div>
                            </div>
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex text-gray3 align-items-center mt-1">
                                        <h6 class="mb-1">{{$government['phone_number']}}</h6>
                                    </div>
                                    <span class="mb-0 fs-13 text-muted">Phone Number</span>
                                </div>
                            </div>
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex text-gray3 align-items-center mt-1">
                                        <h6 class="mb-1">{{$government['website']}}</h6>
                                    </div>
                                    <span class="mb-0 fs-13 text-muted">Website</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex text-gray3 align-items-center mt-1">
                                        <h6 class="mb-1">{{ $government['email']}}</h6>
                                    </div>
                                    <span class="mb-0 fs-13 text-muted">Email</span>
                                </div>
                            </div>
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex text-gray3 align-items-center mt-1">
                                        <h6 class="mb-1">{{ substr($government['address'],0,40)}}</h6>
                                    </div>
                                    <span class="mb-0 fs-13 text-muted">Address</span>
                                </div>
                            </div>
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex text-gray3 align-items-center mt-1">
                                        @if(!is_null($government['facebook']))<a href="{{$government['facebook']}}" target="_blank"><i class="fa fa-facebook fa-1x ml-3"></i></a>@endif
                                        @if(!is_null($government['twitter']))<a href="{{$government['twitter']}}" target="_blank"><i class="fa fa-twitter ml-3"></i></a>@endif
                                        @if(!is_null($government['linkedin']))<a href="{{$government['linkedin']}}" target="_blank"><i class="fa fa-linkedin ml-3"></i></a>@endif
                                        @if(!is_null($government['youtube']))<a href="{{$government['youtube']}}" target="_blank"><i class="fa fa-youtube ml-3"></i></a>@endif

                                    </div>
                                    <span class="mb-0 fs-13 text-muted">Social Media </span>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-4">
            <div class="col-md-7 fs-13 text-gray2">

                <span style="width: 100%" class="mt-3">
                @if(Auth::check() && App\Traits\HelperTrait::showChatButton(Auth::user()->id,$government->id))
                        <a href="{{route('initiate.chat',['sender'=>Auth::user()->id,'receiver'=>$government->id,'type'=>'government'])}}"><i class="fa fa-comments text-orange"></i> Initiate Chat</a>
                    @endif
            </span>
            </div>
            <div class="col-5 pull-right">
                <div class="row centered">
                    @if ($government->jobs->count() >0)
                        <a href="#jobs" data-toggle="tab" class="btn border-orange text-orange pull-right fs-13" style="padding: 10px 180px; margin-left:70px;font-weight:bold"> APPLICATION CALL</a>
                    @endif

                </div>
            </div>
        </div>

    </div>


    <div class="container mt-6">

        <div class="row mb-3">
            <div class="container">
                <h5 class="">ABOUT AGENCY</h5>
                {!! $government['about']!!}
            </div>
        </div>

        {{-- TABS --}}
        <div class="d-md-flex mt-5">
            <div class="row">
                <div class="col-xl-3 col-md-12 col-lg-6 col-md-12">
                    <div class="tab-menu-heading border-top-0 border-0 ">
                        <div class="tabs-menu ">
                            <!-- Tabs -->
                            <ul class="nav panel-tabs fs-12">
                                <li><a href="#team" data-toggle="tab" class="active"> TEAM MEMBERS</a></li>
                                <li><a href="#board" data-toggle="tab" class=""> BOARD MEMBERS</a></li>
                                <li><a href="#jobs" data-toggle="tab" class=""> JOBS</a></li>
                                <li><a href="#feeds" data-toggle="tab" class=""> UPDATES</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-md-12 col-lg-6 col-md-12">
                    <div class="tabs-style-4">
                        <div class="panel-body tabs-menu-body1 border-0">
                            <div class="tab-content">

                                <div class="tab-pane" id="jobs">
                                    <div class="row">
                                        @foreach ($government->jobs as $job)
                                            @include('includes.partial.job-list')
                                        @endforeach
                                    </div>
                                </div>
                                <div class="tab-pane active" id="team">
                                    <div class="text-gray3 "><h5 class="fs-28">Our Team</h5></div>
                                    <hr/>
                                    <div class="row">
                                        @foreach ($teams as $team)
                                            <div class="col-md-4 pt-3 mb-5" style="">
                                                <div class="media mt-0 mb-3">
                                                    <figure class="rounded-circle align-self-start mb-0">
                                                        <img src="{{secure_asset('public/uploads/user/'.$team['photo'])}}" alt="Generic placeholder image" style="width: 80px !important; height: 80px !important;"  class="avatar brround avatar-lg mr-3">
                                                    </figure>
                                                    <div class="media-body">
                                                        <h6 class="time-title p-0 mb-0  pt-3 font-weight-semibold text-gray3 leading-normal">{{$team->member_name}}</h6>
                                                        <p>{{$team['designation']}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="tab-pane" id="board">
                                    <div class="text-gray3 "><h5 class="fs-28">Board Members & Advisors</h5></div>
                                    <hr/>
                                    <div class="row">

                                        @foreach ($boards as $board)
                                            <div class="col-md-4 pt-3" style="">
                                                <div class="media mt-0 mb-3">
                                                    <figure class="rounded-circle align-self-start mb-0">
                                                        <img src="{{secure_asset('public/uploads/user/'.$board['photo'])}}" alt="Generic placeholder image" style="width: 80px !important; height: 80px !important;"  class="avatar brround avatar-lg mr-3">
                                                    </figure>
                                                    <div class="media-body">
                                                        <h6 class="time-title p-0 mb-0 pt-3 font-weight-semibold text-gray3 leading-normal">{{$board->member_name}}</h6>
                                                        <p>{{$team['designation']}} </p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="tab-pane" id="feeds">
                                    <div class="text-gray3 "><h5 class="fs-28">Updates</h5></div>
                                    <hr/>
                                    <div class="row">
                                        <?php $feeds = App\Traits\HelperTrait::getFeeds('government',$government['id'],4)?>
                                        @foreach ($feeds as $feed)
                                                <div class="row mb-6" style="width:100%; border-radius: 5px; border: 0.5px solid #BFBFBF ">
                                                    <div class="col-md-12">
                                                        <div class="row" style="border-bottom: 1px solid rgba(0,0,0,0) !important;">
                                                            <div class="col-lg-8 col-xl-9 col-md-12 col-sm-12">
                                                                <div class="card-header" style="padding: .75rem 0;border-bottom: 1px solid rgba(0,0,0,0) !important;">
                                        <span>
                                            <img src="{{secure_asset(env('LOGO').$feed['company']['logo'])}}" alt="img" class="avatar avatar-lg  mr-2 brround">
                                        </span>
                                                                    <h6 class="text-gray-300 pt-2">
                                                                        {{$feed['company']['name']}} <br/>
                                                                        <small class="text-gray-100">{{$feed['company']['address']}}</small>
                                                                    </h6>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-xl-3 col-md-12 col-sm-12">
                                                                <span class=" ml-6 mt-3" style="float: right;">{{ Carbon\Carbon::parse($feed->created_at)->diffForHumans()}}</span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-8 col-xl-9 col-md-12 col-sm-12">
                                                                <h6 class="pt-3" style="color: #0E59DE">
                                                                    <a href="{{$feed['url']}}" style="font-size: 21px" target="_blank" >{{$feed['title']??" Visit Article"}}</a>
                                                                </h6>
                                                            </div>
                                                            <div class="col-lg-4 col-xl-3 col-md-12 col-sm-12">
                                                                @if (is_null($feed['image']))
                                                                    <img src="{{secure_asset('public/uploads/logo/business-logo.png')}}" class="pt-3" style="width: 131px;"  alt="">
                                                                @else
                                                                    <img src="{{$feed['image']}}" class=""  alt="">
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <h6 class="text-gray fs-13 mt-3">THOUGHTS</h6>
                                                        <p class="fs-12">{{$feed['thought']}}</p>
                                                    </div>


                                                </div>

                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="border border-right-0 br-tl-7 br-bl-7">
                <div class="panel panel-primary tabs-style-4">

                </div>
            </div>

        </div>
        {{-- END TABS --}}

    </div>

@endsection
