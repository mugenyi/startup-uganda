
<footer class="bg-black-9 footer" style="margin-top: 50px;padding-top: 40px;background-color: rgba(0, 0, 0, 0.9);">
    <section class=" text-white">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img src="{{secure_asset('public/theme/images/logo-white-uganda.png')}}" class="header-brand-img desktop-lgo d-none d-sm-block" alt="Startup Uganda Logo">
                    <p class="mt-4">Startup Uganda (SU) is an association of innovation and entrepreneurship support organizations (IESO) working towards strengthening the startup support ecosystem and sector.</p>

                </div>
                <div class="col-md-2">
                    <h4><b>Quick Links</b></h4>
                    <ul class="list-unstyle-d mt-3">
                        <li><a href="{{route('startups')}}" class="text-white">Startups</a></li>
                        <li><a href="{{route('investors')}}" class="text-white">Investors</a></li>
                        <li><a href="{{route('esos')}}" class="text-white">ESOs</a></li>
                        <li><a href="{{route('programs')}}" class="text-white">Programs</a></li>
                        <li><a href="{{route('about')}}" class="text-white">About Us</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4><b>Subscribe</b></h4>
                    <p>Don’t miss to subscribe to our new feeds, kindly fill the form below</p>
                    @if(Session::has('success'))
                        <script>
                            $(function() {$('#success-pop').modal('show');});
                        </script>
                    @endif

                    @if (\Session::has('failure'))
                        <script>
                            $(function() { $('#failure-pop').modal('show'); });
                        </script>
                    @endif


                    <form method="post" action="{{route('subscribe')}}">
                        @csrf

                        <div class="row">
                            <div class="input-group mb-2">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i></span>
                                <input type="text" class="form-control input-lg" name="email" id="email"  placeholder="Enter your Email"/>
                            </div>
                            <button class="btn btn-warning btn-sm">Subscribe Now!</button>

                        </div>

                    </form>
                    <!-- End form -->



                </div>
                <div class="col-md-3">
                    <div class="">
                        <h4><b>Follow us</b></h4>
                        <ul class="list-unstyle-d list-inline social text-left">
                            <li class="list-inline-item"><a href="https://www.facebook.com/Startup256" target="_blank"><img src="{{secure_asset('public/images/icons/facebook.png')}}" style=""/></a></li>
                            <li class="list-inline-item"><a href="https://twitter.com/StartupUganda_" target="_blank"><img src="{{secure_asset('public/images/icons/twitter.png')}}" style=""/></a></li>
                            <li class="list-inline-item"><a href="https://www.linkedin.com/company/startupug/" target="_blank"><img src="{{secure_asset('public/images/icons/linkedin.png')}}" style=""/></a></li>
                            <li class="list-inline-item"><a href="mailto:info@startupug.com" target="_blank"><img src="{{secure_asset('public/images/icons/mail.png')}}" style=""/></li>
                        </ul>
                    </div>
                    <!-- End Social link -->
                </div>
            </div>
        </div
    </section>
    <div class="container">
        <div class="row align-items-center flex-row-reverse">
            <div class="col-md-12 col-sm-12 text-center"> Copyright © 2020  All rights reserved. </div>
        </div>
    </div>

    <div class="modal fade" id="success-pop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-center">
            <div class="modal-dialog .modal-align-center">
                <div class="modal-content">
                    <div class="modal-header bg-green-dark text-white">
                        <h4 class="modal-title" id="myModalLabel">Success</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body">
                        {{ Session::get('success', '') }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="failure-pop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-center">
            <div class="modal-dialog .modal-align-center">
                <div class="modal-content">
                    <div class="modal-header bg-red-dark text-white">
                        <h4 class="modal-title" id="myModalLabel">Success</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body">
                        {{ Session::get('failure', '') }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
