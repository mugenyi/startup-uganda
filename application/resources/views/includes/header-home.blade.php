<div class="horizontal-main-build clearfix" style="background-color: none !important">
    <div class="container">
        <div class="d-flex pt-2 pb-2">
            <a class="animated-arrow hor-toggle horizontal-navtoggle pt-1"><span></span></a>
            <a class="header-brand" href="/">
                 <img src="{{secure_asset('public/theme/images/logo-white-uganda.png')}}" class="header-brand-img desktop-lgo d-none d-sm-block" alt="Startup Uganda Logo">
                <img src="{{secure_asset('public/theme/images/logo-white-uganda.png')}}" class="header-brand-img mobile-logo" alt="Startup Uganda Logo" style="height: 1.5rem;">
            </a>
            <div class="mt-3 ml-6 ">
                <div class="horizontal-mainwrapper container clearfix">
                    <!--Nav-->
                    <nav class="horizontalMenu clearfix text-white">
                       <div class="horizontal-overlapbg "></div>
                       <ul class="horizontalMenu-list ">

                        <li aria-haspopup="false" class="">
                            <a href="{{route('feeds')}}" class="sub-icon text-white ">Feeds </a>
                            </li>
                          <li aria-haspopup="true" class="">
                            <a href="{{route('startups')}}" class="text-white sub-icon ">Startups </a>
                          </li>
                          <li aria-haspopup="true" class="">
                            <a href="{{route('investors')}}" class="text-white sub-icon ">Investors </a>
                         </li>
                         <li aria-haspopup="true" class="">
                            <a href="{{route('esos')}}" class= "text-white sub-icon ">ESOs </a>
                         </li>
                         <li aria-haspopup="true" class="">
                            <a href="{{route('programs')}}" class= "text-white sub-icon ">Programs </a>
                         </li>
                         <li aria-haspopup="true" class="">
                         <a href="{{route('jobs')}}" class= "text-white sub-icon ">Jobs </a>
                         </li>
                         <li aria-haspopup="true" class="">
                         <a href="{{route('about')}}" class= "text-white sub-icon ">About Us </a>
                         </li>

                         <li aria-haspopup="true">
                            <span class="horizontalMenu-click"><i class="horizontalMenu-arrow fa fa-angle-down"></i></span>
                            <a href="#" class="sub-icon text-white ">
                               More <i class="fa fa-angle-down horizontal-icon"></i>
                            </a>
                            <ul class="sub-menu">
                               <li aria-haspopup="true"><a href="{{route('governments')}}" class="sub-icon ">Governments </a></li>
                               <li aria-haspopup="true"><a href="{{route('corporations')}}"> Corporations</a></li>
                               <li aria-haspopup="true"><a href="{{route('mentors')}}">Mentors</a></li>
                               <li aria-haspopup="true"><a href="{{route('events')}}"> Events</a></li>
                               <li aria-haspopup="true"><a href="{{route('blogs')}}"> Blogs</a></li>
                            </ul>
                         </li>



                       </ul>
                    </nav>
                    <!--Nav-->
                 </div>

                </div>
                        <!-- SEARCH -->

                        <div class="d-flex order-lg-2 ml-auto">

                            @if (Auth::check())
{{--                            <div class="dropdown   header-fullscreen" style="margin-top: .75rem;margin-right: .5rem;">--}}
{{--                                <a href="{{route('user.dashboard')}}" class="ml-3"><button class="btn btn-sm border-warning text-warning text-bold fs-12">List your Venture</button></a>--}}

{{--                            </div>--}}
                                <div class="dropdown pull-right" style="margin-top: .75rem;margin-right: .5rem;">
                                    <button type="button" class="btn btn-sm border-warning text-warning text-bold fs-12 dropdown-toggle d-none d-sm-block" data-toggle="dropdown" aria-expanded="true"> <i class="fa fa-briefcase mr-2"></i>List your Venture </button>
                                    <div class="dropdown-menu fs-12" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 36px, 0px);" x-placement="bottom-start">
                                        <a class="dropdown-item" href="{{route('user.new.startup')}}">Startup</a>
                                        <a class="dropdown-item" href="{{route('user.mentors')}}">Mentor</a>

                                        <a class="dropdown-item" href="{{route('user.new.individual.investor')}}">Individual Investor</a>
                                        <a class="dropdown-item" href="{{route('user.new.investor')}}">Company Investor</a>
                                        <a class="dropdown-item" href="{{route('user.esos')}}">ESO</a>
                                        <a class="dropdown-item" href="{{route('user.government.register')}}">Government Agency</a>
                                        <a class="dropdown-item" href="{{route('user.corporates')}}">Corporation</a>
                                    </div>
                                </div>

                            <div class="dropdown profile-dropdown">
                            <a href="{{route('user.dashboard')}}" class="nav-link pr-0 leading-none" data-toggle="dropdown"> <span>
                                   <img src="{{secure_asset('public/uploads/'.Auth::user()->image)}}" alt="img" class="avatar avatar-md brround"> </span>
                                </a>
                               <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow animated">
                                  <div class="text-center">
                                     <a href="#" class="dropdown-item text-center user pb-0 font-weight-bold">{{Auth::user()->name}}</a> <span class="text-center user-semi-title">{{Auth::user()->role}}</span>
                                     <div class="dropdown-divider"></div>
                                  </div>
{{--                                  <a class="dropdown-item d-flex" href="{{route('user.profile')}}">--}}
{{--                                     <svg class="header-icon mr-3" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">--}}
{{--                                        <path d="M0 0h24v24H0V0z" fill="none"></path>--}}
{{--                                        <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zM7.07 18.28c.43-.9 3.05-1.78 4.93-1.78s4.51.88 4.93 1.78C15.57 19.36 13.86 20 12 20s-3.57-.64-4.93-1.72zm11.29-1.45c-1.43-1.74-4.9-2.33-6.36-2.33s-4.93.59-6.36 2.33C4.62 15.49 4 13.82 4 12c0-4.41 3.59-8 8-8s8 3.59 8 8c0 1.82-.62 3.49-1.64 4.83zM12 6c-1.94 0-3.5 1.56-3.5 3.5S10.06 13 12 13s3.5-1.56 3.5-3.5S13.94 6 12 6zm0 5c-.83 0-1.5-.67-1.5-1.5S11.17 8 12 8s1.5.67 1.5 1.5S12.83 11 12 11z"></path>--}}
{{--                                     </svg>--}}
{{--                                     <div class="">Profile</div>--}}
{{--                                  </a>--}}

                                   <a class="dropdown-item d-flex" href="{{Auth::user()->role=='admin'?route('admin.dashboard'):route('user.dashboard')}}">
                                       <svg class="header-icon mr-3" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                                           <path d="M0 0h24v24H0V0z" fill="none"></path>
                                           <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zM7.07 18.28c.43-.9 3.05-1.78 4.93-1.78s4.51.88 4.93 1.78C15.57 19.36 13.86 20 12 20s-3.57-.64-4.93-1.72zm11.29-1.45c-1.43-1.74-4.9-2.33-6.36-2.33s-4.93.59-6.36 2.33C4.62 15.49 4 13.82 4 12c0-4.41 3.59-8 8-8s8 3.59 8 8c0 1.82-.62 3.49-1.64 4.83zM12 6c-1.94 0-3.5 1.56-3.5 3.5S10.06 13 12 13s3.5-1.56 3.5-3.5S13.94 6 12 6zm0 5c-.83 0-1.5-.67-1.5-1.5S11.17 8 12 8s1.5.67 1.5 1.5S12.83 11 12 11z"></path>
                                       </svg>
                                       <div class="">Dashboard</div>
                                   </a>


                                  @if(Auth::check() && Auth::user()->role =='user')
                                    <a class="dropdown-item d-flex" href="{{route('user.chat')}}">
                                        <svg class="header-icon mr-3" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                                            <path d="M0 0h24v24H0V0z" fill="none"></path>
                                            <path d="M4 4h16v12H5.17L4 17.17V4m0-2c-1.1 0-1.99.9-1.99 2L2 22l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2H4zm2 10h12v2H6v-2zm0-3h12v2H6V9zm0-3h12v2H6V6z"></path>
                                        </svg>
                                        <div class="">Messages</div>
                                    </a>
                                  @endif
                                  <a class="dropdown-item d-flex" href="{{route('user.logout')}}">
                                     <svg class="header-icon mr-3" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24">
                                        <g>
                                           <rect fill="none" height="24" width="24"></rect>
                                        </g>
                                        <g>
                                           <path d="M11,7L9.6,8.4l2.6,2.6H2v2h10.2l-2.6,2.6L11,17l5-5L11,7z M20,19h-8v2h8c1.1,0,2-0.9,2-2V5c0-1.1-0.9-2-2-2h-8v2h8V19z"></path>
                                        </g>
                                     </svg>
                                     <div class="">Sign Out</div>
                                  </a>
                               </div>
                            </div>
                            @else
                            <div class="mt-3" style="font-weight:bold">
                            <a href="{{route('login')}}" class="text-primary text-bold mr-3"><i class="fa fa-signin"></i> Sign in</a><span class="text-white  d-none d-md-inline-block"> | </span>
                            <a href="{{route('user.dashboard')}}" class="ml-3 text-bold d-none  d-md-inline-block"><button class="btn btn-sm border-warning text-warning text-bold fs-12" style="font-weight:bold">List your Venture</button></a>

                            </div>

                            @endif
                         </div>
                        </div>
                    </div>
                </div>
