<style>
    .secondary-menu li.active{
        color: #FFFFFF !important;
        background-color:  #F29109 !important ;
        border-radius: 5px;
    }
    .secondary-menu li a.sub-icon{
        color: #FFFFFF;
        font-weight: bold;
    }
</style>
<div class="row bg-dark-gray" style=" height: 40px;padding: 7px;">
    <div class="container">
        <nav class="horizontalMenu clearfix">
            <ul class="horizontalMenu-list secondary-menu" >
                <li aria-haspopup="true" class="{{ request()->route()->named('network') ? 'active' : '' }}">
                    <a href="{{route('network')}}" class="sub-icon ">Overview </a>
                </li>
                <li aria-haspopup="true" class="{{ request()->route()->named('startups') ? 'active' : '' }}">
                    <a href="{{route('startups')}}" class="sub-icon ">Startups </a>
                </li>
                <li aria-haspopup="true" class="{{ request()->route()->named('investors') ? 'active' : '' }}">
                    <a href="{{route('investors')}}" class="sub-icon ">Investors </a>
                </li>
                <li aria-haspopup="true" class="{{ request()->route()->named('esos') ? 'active' : '' }}">
                    <a href="{{route('esos')}}" class="sub-icon ">ESOs </a>
                </li>
                <li aria-haspopup="true" class="{{ request()->route()->named('governments') ? 'active' : '' }}">
                    <a href="{{route('governments')}}" class="sub-icon ">Governments </a>
                </li>
                <li aria-haspopup="true" class="{{ request()->route()->named('corporations') ? 'active' : '' }}">
                    <a href="{{route('corporations')}}" class="sub-icon "> Corporations</a>
                </li>
                <li aria-haspopup="true" class="{{ request()->route()->named('mentors') ? 'active' : '' }}">
                    <a href="{{route('mentors')}}" class="sub-icon ">Mentors</a>
                </li>

            </ul>
        </nav>
    </div>
</div>
