<div class="col-md-6 col-lg-3">
    <a href="{{route('view.corporation',['id'=>$corporate['id'],'slug'=>App\Traits\HelperTrait::slugify($corporate['name'])])}}">
    <div class="card overflow-hidden startup-card">
        <img src="{{env('COVER').''.$corporate['image']}}" alt="image">
        <div class="card-body">
            <h4 class="card-title text-uppercase" style="font-size: 13px">{{$corporate['name']}}</h4>
            <p class="text-gray">{{$corporate['address']}} </p>
        </div>
    </div>
    </a>
</div>
