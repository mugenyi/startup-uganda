<div class="col-md-6 col-lg-3">
    <a href="{{route('view.eso',['id'=>$eso['id'],'slug'=>App\Traits\HelperTrait::slugify($eso['name'])])}}">
    <div class="card overflow-hidden startup-card">
        <img src="{{secure_asset('public/uploads/banner/'.$eso['image'])}}" alt="image">
        <div class="card-body">
            <h4 class="card-title text-uppercase" style="font-size: 13px">{{$eso['name']}}</h4>
            <p class="text-gray">{{$eso['address']}} </p>
        </div>
    </div>
    </a>
</div>
