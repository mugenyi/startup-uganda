<div class="col-md-6 col-lg-4">
    <a href="{{route('view.event',['id'=>$event['id'],'slug'=>App\Traits\HelperTrait::slugify($event['event_name'])])}}">
    <div class="card overflow-hidden startup-card">
{{--        <img src="{{secure_asset(env('BANNER').$event['image'])}}" alt="image">--}}
        <img src="{{secure_asset(env('COVER').$event['image'])}}" style="background-size: cover;height:230px !important" alt="{{$event['event_name']}}; ">

        <div class="card-body">
            <h4 class="card-title text-uppercase" style="font-size: 13px">{{$event['event_name']}}</h4>
            <p class="text-gray">{{ strlen($event['summary']) > 110 ? substr($event['summary'],0,110)."..." : $event['summary']}}</p>
            <p class="text-gray-300"><b>Starts On: </b>{{$event['start_date']}} </p>
        </div>
    </div>
    </a>
</div>
