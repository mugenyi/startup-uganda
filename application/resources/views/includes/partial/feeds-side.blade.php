<div class="row">
    <div class="col fs-15 font-weight-semibold text-gray2">STARTUP UGANDA PROGRAMS</div>
    <div class="col-auto">
        <a href="{{route('programs')}}"><span class="">See more</span></a>
    </div>
</div>
<div class="row shadow3 mt-4">
    <?php $others = App\Traits\HelperTrait::getProgramFeeds(3) ?>
    @foreach ($others as $key=>$program)
        <div class="p-3" @if($key==1) style="background-color:#F4F4F4;width:100%" @endif >
            <div class="row pt-4 pb-4 pr-3 pl-3">
                <div class="col-md-3">
                    <span class="avatar avatar-xl" style="background-image: url('{{secure_asset(env('FULL').''.$program['image'])}}')"></span>
                </div>
                <div class="col-md-9 pt-2">
                    <h5 class="fs-12">
                        <a class="fs-16" href="{{route('view.program',['id'=>$program['id'],'slug'=>App\Traits\HelperTrait::slugify($program['program_name'])])}}">
                            {{$program['program_name']}}
                        </a>
                    </h5>
                    <p class="fs-11 text-gray"><b>Apply Before:</b> {{$program['apply_before']}} </p>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="progress progress-xs mb-2">
                        <div class="progress-bar bg-yellow" style="width: {{App\Traits\HelperTrait::calculateDaysLeft($program['created_at'],$program['apply_before'])}}%"></div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

<div class="row mt-8">
    <div class="col fs-15 font-weight-semibold text-gray2">VENTURES OF THE WEEK</div>
    <div class="col col-auto">
        <a href="{{route('startups')}}"><span class="">See more</span></a>
    </div>
</div>
<div class="row mt-4 shadow3">
    <?php $startups = App\Traits\HelperTrait::getStartupsOfWeek(3) ?>
    @foreach ($startups as $key=>$startup)
        <div class="" @if($key==1) style="background-color:#F4F4F4;width:100%" @endif>
            <a class="font-weight-semibold" href="{{route('startup.details',['slug'=>$startup['slug']])}}">
                <div class="row p-4">
                    <div class="col-3 pt-4">
                        <img src="{{secure_asset(env('LOGO').''.$startup['logo'])}}" alt="{{$startup['name']}}" style="min-width: 50px;min-height: 50px"  class="avatar avatar-lg mr-2 brround">
                    </div>
                    <div class="col-9">
                        {{$startup['name']}} <br>
                        <div class="fs-11 text-gray-100 pt-2 pb-2">Stage: {{$startup['stage']}}</div>
                        <p class="fs-12">{!! strip_tags(strlen($startup['about']) > 100 ? substr($startup['about'],0,100)."..." : $startup['about']) !!} </p>

                    </div>
                </div>
            </a>

        </div>
    @endforeach
</div>


<div class="row mt-8">
    <div class="col fs-15 font-weight-semibold text-gray2">INVESTORS OF THE WEEK</div>
    <div class="col col-auto">
        <a href="{{route('investors')}}"><span class="">See more</span></a>
    </div>
</div>
<div class="row mt-4 shadow3">
    <?php $investors = App\Traits\HelperTrait::getInvestorsOfWeek(3) ?>
    @foreach ($investors as $key=>$investor)
        <div class="" @if($key==1) style="background-color:#F4F4F4;width:100%" @endif>
            <a class="font-weight-semibold" href="{{route('investor.details',['slug'=>$investor['slug']])}}">
                <div class="row p-4">
                    <div class="col-3 pt-3">
                        <img src="{{secure_asset(env('LOGO').''.$investor['logo'])}}" alt="{{$investor['name']}}"  style="min-width: 50px;min-height: 50px"  class="avatar avatar-lg mr-2 brround">
                    </div>
                    <div class="col-9">

                        {{$investor['name']}} <br>
                        <div class="fs-11 text-gray-100 text-capitalize pt-2 pb-2">Type: {{$investor['investor_type']}}</div>
                        <p class="fs-12">{!! strip_tags(strlen($investor['about']) > 100 ? substr($investor['about'],0,100)."..." : $investor['about']) !!} </p>

                    </div>
                </div>
            </a>

        </div>

    @endforeach
</div>



