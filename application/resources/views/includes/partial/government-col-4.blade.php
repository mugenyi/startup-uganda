<div class="col-md-6 col-lg-3">
    <a href="{{route('view.government',['id'=>$government['id'],'slug'=>App\Traits\HelperTrait::slugify($government['name'])])}}">
    <div class="card overflow-hidden startup-card">
        <img src="{{secure_asset('public/uploads/banner/'.$government['image'])}}" alt="image">
        <div class="card-body">
            <h4 class="card-title text-uppercase" style="font-size: 13px">{{$government['name']}}</h4>
            <p class="text-gray">{{$government['address']}} </p>
        </div>
    </div>
    </a>
</div>
