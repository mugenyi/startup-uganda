<div class="col-md-6 col-lg-3">
    <a href="{{route('investor.details',['slug'=>$investor['slug']])}}">
        <div class="card overflow-hidden startup-card">
            <img src="{{secure_asset('public/uploads/banner/'.$investor['profile_photo'])}}" alt="image">
            <div class="card-body">
                <h4 class="card-title text-uppercase startup-title">{{$investor['name']}}</h4>
                <h6 class="text-gray text-capitalize">{{$investor['investor_type']}}</h6>
                {{-- <p class="card-text">{{$investor['tagline']}} </p> --}}
            </div>
        </div>
    </a>
</div>
