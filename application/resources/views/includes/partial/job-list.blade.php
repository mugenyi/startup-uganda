<?php $company = App\Traits\HelperTrait::getCompany($job['company_type'],$job['company']); ?>
<?php $slug = App\Traits\HelperTrait::slugify($job['job_title']); ?>
<div class="col-md-12 col-lg-12">
    <div class="card">
        <div class="card-header">
            <div class="row" style="border-bottom: 1px solid rgba(0,0,0,0) !important;width: 100%">
                <div class="col-lg-8 col-xl-9 col-md-12 col-sm-12">
                    <div class="card-header" style="padding: .75rem 0;border-bottom: 1px solid rgba(0,0,0,0) !important;">
                        <span>
                            <img src="{{secure_asset(env('LOGO').$company['logo'])}}" alt="img" class="avatar avatar-lg mr-2 brround">
                        </span>
                        <h6 class="text-gray-300 pl-2 pt-2">
                            {{$company['name']}} <br/>
                            <small class="text-gray-100">{{$company['address']}}</small>
                        </h6>
                    </div>
                </div>
                <div class="col-lg-4 col-xl-3 col-md-12 col-sm-12">
                    {{-- <a href="{{route('job.view',['id'=>$job['id'],'slug'=>$slug])}}" class="btn btn-primary btn-sm mt-3" >Apply</a> --}}
                    <span class=" ml-6 mt-4" style="float: right;">{{ Carbon\Carbon::parse($job->created_at)->diffForHumans()}}</span>
                </div>
            </div>

        </div>
        <div class="card-body">
            <a href="{{route('job.view',['id'=>$job['id'],'slug'=>$slug])}}"  style="color: #1DACF8"><h5 class="card-title">{{$job['job_title']}}</h5></a>
            <p><b>Location: </b>{{$job['location']}}</p>
            <p>{{ strlen(strip_tags($job['description'])) > 110 ? substr(strip_tags($job['description']),0,110)."..." : strip_tags($job['description'])}}
            </p>
        </div>
    </div>
</div>
