<div class="col-sm-6 col-md-4 col-lg-4 mt-4">
    <a href="{{route('view.program',['id'=>$program['id'],'slug'=>App\Traits\HelperTrait::slugify($program['program_name'])])}}">
    <div class="card">
        <img class="card-img-top" src="{{secure_asset(env('COVER').$program['image'])}}" name="{{$program['program_name']}}">
        <div class="card-block p-2"  style="height: 120px">
            <h4 class="card-title" style="font-size: 14px !important">{{$program['program_name']}}</h4>

            <div class="card-text">
                {{ strlen($program['summary']) > 75 ? substr($program['summary'],0,75)."..." : $program['summary']}}
                <div class="progress mb-3 mt-3" style="height: .6rem;">
                    <div class="progress-bar bg-yellow" style="width: {{App\Traits\HelperTrait::calculateDaysLeft($program['created_at'],$program['apply_before'])}}%"></div>
                </div>

            </div>
        </div>
        <div class="card-footer">
            <span> {{$program['apply_before']}}</span>
            <span class="float-left">Apply Before: </span>

        </div>
    </div>
    </a>
</div>
