<div class="col-md-6 col-lg-3">
    <a href="{{route('startup.details',['slug'=>$startup['slug']])}}">
        <div class="card overflow-hidden startup-card">
            <img src="{{secure_asset('public/uploads/cover/'.$startup['profile_photo'])}}" alt="image">
            <div class="card-body">
                <h4 class="card-title startup-title">{{$startup['name']}}</h4>
                <h6 class="text-gray text-capitalize">{{$startup['stage']}}</h6>
                {{-- <p class="card-text">{{$startup['tagline']}} </p> --}}
            </div>
        </div>
    </a>
</div>
