<?php
    $partners = [
        ['name'=>"Starthub","img"=>"starthub.png","link"=>"https://starthubafrica.org/"],
        ['name'=>"Hive colab","img"=>"hivecolab.png","link"=>"https://hivecolab.org/"],
        ['name'=>"NFT Consults","img"=>"nft.png","link"=>"https://nftconsult.com/"],
        ['name'=>"Response Innovation Lab","img"=>"response.png","link"=>"https://www.responseinnovationlab.com/uganda"],
        ['name'=>"Outbox","img"=>"outbox.png","link"=>"https://outbox.co.ug/"],
        ['name'=>"Innovation Village","img"=>"innovation_village.png","link"=>"https://innovationvillage.co.ug/"],
        ['name'=>"Tribe Kampala","img"=>"tribe_kampala.png","link"=>"https://www.coworker.com/uganda/kampala/tribe-kampala"],
        ['name'=>"Refactory","img"=>"refactory.png","link"=>"https://www.refactory.ug/"],
        ['name'=>"Amara Group","img"=>"mara.png","link"=>"https://www.linkedin.com/company/mara-group"],
        ['name'=>"Amarin","img"=>"amarin.png","link"=>"http://www.amarinfinancial.com/"],
        ['name'=>"Shona","img"=>"shona.png","link"=>"https://shona.co/"],
        ['name'=>"United Social Ventures Internship","img"=>"United-Social-Ventures-Internship.png","link"=>"https://unitedsocialventures.org/"],
        ['name'=>"Growth Africa","img"=>"growthafrica.png","link"=>"https://growthafrica.com/"],
        ['name'=>"Design Hub","img"=>"designHub.png","link"=>"https://designhubkampala.com/"],
        ['name'=>"Makerere Innovation Center","img"=>"makerere.png","link"=>""],
        ['name'=>"Sina","img"=>"sina.png","link"=>"https://socialinnovationacademy.org/"],
        ['name'=>"Iventure Africa","img"=>"iventure.png","link"=>""],
        ['name'=>"Women in Technology Uganda","img"=>"womenInTech.png","link"=>"https://starthubafrica.org/"],
        ['name'=>"Tech Buzz Hub","img"=>"techBuzz.png","link"=>"http://techbuzzhub.org/"],
    ];

?>


{{-- <div class="row pl-2 pr-2">


    <div class="col-md-2 col-xs-6 col-sm-6 shadow1 br-3 h-130 center">
        <a href="http://techbuzzhub.org/" target="_blank">
            <img src="{{secure_asset('public/images/partners/techBuzz.png')}}" class="center-block p-3" style="max-height:100px;">
        </a>
    </div>
</div> --}}
<style>
    #clients {
     padding: 60px 0
 }

.clients-wrap {

     margin-bottom: 30px
 }

.client-logo {
     padding: 20px;
     display: -webkit-box;
     display: -webkit-flex;
     display: -ms-flexbox;
     display: flex;
     -webkit-box-pack: center;
     -webkit-justify-content: center;
     -ms-flex-pack: center;
     justify-content: center;
     -webkit-box-align: center;
     -webkit-align-items: center;
     -ms-flex-align: center;
     align-items: center;
     box-shadow: 0px 5px 5px 50px rgba(179, 186, 212, 0), 5px 5px 8px -5px #b0b8d6 !important;
     overflow: hidden;
     background: #fff;
     border-radius: 10px;
     height: 130px;
     margin-bottom:30px;
     margin-right: 20px;
 }
.client-logo:hover{
    box-shadow: 0px 10px 10px 20px rgba(179, 186, 212, 0), 10px 10px 15px -5px #b0b8d6 !important;
}

 #clients img {
     transition: all 0.4s ease-in-out
 }
</style>

<div class="row no-gutters clients-wrap clearfix wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
    @foreach ($partners as $item)
        <div class="col-lg-2 col-md-3 col-xs-6">
            <a href="{{$item['link']}}" target="_blank" class="partnerLink">
            <div class="client-logo"> <img src="{{secure_asset('public/images/partners/'.$item['img'])}}" class="img-fluid" alt=""> </div>
            </a>
        </div>
    @endforeach


</div>
