@extends('build.master')
@section('content')
    <div class="container">
        <div class="row mt-7">
            <div class="col-lg-4 col-xl-3 mb-5 text-center">
                <img src="{{secure_asset('public/uploads/logo/'.$investor['logo'])}}" alt="img" style="width: 12rem; height: 12rem;background: #FFFFFF no-repeat center/cover;" class="avatar mb-4 avatar-xxxl mr-2 p-2 brround shadow2">


            </div>
            <div class="col-md-9 col-lg-9 col-sm-12  mt-5">
                <h4>{{$investor['name']}}</h4>
                <p>{!!$investor['tagline']!!}</p>
                <div class="row" style="width: 100%">
                    <div class="col-xl-4 col-lg-4 col-md-12">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <p class="mb-1 font-weight-semibold">{{$investor['legal_name']}}</p>
                                </div>
                                <span class="mb-0 fs-13 text-muted">Legal Name</span>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <p class="mb-1 font-weight-semibold">{{$investor['phone_number']}}</p>
                                </div>
                                <span class="mb-0 fs-13 text-muted">Phone Number</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <p class="mb-1 font-weight-semibold">{{ $investor['address']}}</p>
                                </div>
                                <span class="mb-0 fs-13 text-muted">Location</span>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <p class="mb-1 font-weight-semibold">{{number_format(App\Traits\HelperTrait::getFundings('investor',$investor['id'],'count'))}}</p>
                                </div>
                                <span class="mb-0 fs-13 text-muted">Number of Investments</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <p class="mb-1 font-weight-semibold">{{ $investor['email']}}</p>
                                </div>
                                <span class="mb-0 fs-13 text-muted">Email</span>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <p class="mb-1 font-weight-semibold">{{ $investor['website']}}</p>
                                </div>
                                <span class="mb-0 fs-13 text-muted">Website</span>
                            </div>
                        </div>
                    </div>

                </div>
{{--                <div class="row pl-2 pr-2 fl-wrap">--}}
{{--                    <strong class="font-weight-semibold">Social Media: </strong>--}}
{{--                    @if(!is_null($investor['facebook']))<a href="{{$investor['facebook']}}" target="_blank"><i class="fa fa-facebook fa-1x ml-3"></i></a>@endif--}}
{{--                    @if(!is_null($investor['twitter']))<a href="{{$investor['twitter']}}" target="_blank"><i class="fa fa-twitter ml-3"></i></a>@endif--}}
{{--                    @if(!is_null($investor['linkedin']))<a href="{{$investor['linkedin']}}" target="_blank"><i class="fa fa-linkedin ml-3"></i></a>@endif--}}
{{--                    @if(!is_null($investor['youtube']))<a href="{{$investor['youtube']}}" target="_blank"><i class="fa fa-youtube ml-3"></i></a>@endif--}}

{{--                </div>--}}
            </div>
        </div>

        <div class="row">
            {{-- TABS --}}
            <div class="d-md-flex mt-5">
                <div class="row">
                    <div class="col-lg-4 col-xl-3">
                        <div class="tab-menu-heading border-0">
                            <div class="tabs-menu ">
                                <!-- Tabs -->
                                <ul class="nav panel-tabs fs-12">
                                    <li class=""><a href="#funding" class="active" data-toggle="tab">INVESTMENT PORTFOLIO</a></li>
                                    <li class=""><a href="#experience"  data-toggle="tab">EXPERIENCE</a></li>
                                    <li class=""><a href="#about"  data-toggle="tab">ABOUT</a></li>
                                    <li><a href="#industries" data-toggle="tab" class=""> INDUSTRIES & SECTORS</a></li>
                                    <li><a href="#jobs" data-toggle="tab" class=""> JOBS</a></li>
                                    <li><a href="#feeds" data-toggle="tab" class=""> UPDATES</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-lg-9 col-sm-12 ">
                        <div class="tabs-style-4">
                            <div class="panel-body tabs-menu-body1 border-0">
                                <div class="tab-content">
                                    <div class="tab-pane " id="experience">

                                        <h4 class="text-muted">EXPERIENCE</h4>
                                        {!!$investor['experience']!!}


                                    </div>
                                    <div class="tab-pane " id="about">
                                        <h4 class="text-muted">ABOUT</h4>
                                        {!!$investor['about']!!}


                                    </div>
                                    <div class="tab-pane active" id="funding">

                                        <h4 class="text-muted">INVESTMENT PORTIFOLIO</h4>
                                        <div class="row">
                                            <div class="col-12 mt-3">

                                                <div class="row">
                                                    <?php $funds= App\Traits\HelperTrait::getFundings('investor',$investor['id'],'list'); ?>

                                                    <table class="table table-vcenter text-nowrap mb-0 table-striped fs-13">
                                                        <thead>
                                                        <tr>
                                                            <th>Date</th><th>Company Name</th>
                                                            <th>Company Type</th><th>Amount</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($funds as $fund)
                                                            <tr>
                                                                <td>{{date_format(date_create($fund['created_at']),"M-Y")}}</td>
                                                                <td>{{$fund['company_name']}}</td>
                                                                <td>{{$fund['company_type']}}</td>
                                                                <td>$ {{number_format($fund['amount'])}}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>




                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="tab-pane" id="industries">
                                        <div class="list-single-main-item-title text-gray"><h5>INDUSTRIES</h5></div>
                                        <div class="list-single-main-item_content fl-wrap">
                                            <?php $industries = explode(',',$investor['investment_industries']); array_pop($industries);?>
                                            @foreach ($industries as $ind)
                                                <a class="btn btn-sm btn-light mt-1" href="#">{{$ind}}</a>
                                            @endforeach
                                        </div>
                                        <div class="list-single-main-item-title text-gray mt-5"><h5>SECTORS</h5></div>
                                        <div class="list-single-main-item_content fl-wrap">
                                            <?php $sectors = explode(',',$investor['investment_sectors']); array_pop($sectors); ?>
                                            @foreach ($sectors as $sec)
                                                <a class="btn btn-sm btn-light mt-1" href="#">{{$sec}}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="jobs">
                                        <div class="row">
                                            <h5 class="mb-3 text-gray" style="width: 100%">JOB OPENINGS</h5>
                                            @foreach ($investor->jobs as $job)
                                                @include('includes.partial.job-list')
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="feeds">
                                        <div class="row">
                                            <h5 class="mb-3 text-muted" style="width: 100%">UPDATES</h5>
                                            <hr/>
                                            <?php $feeds = App\Traits\HelperTrait::getFeeds('investor',$investor['id'],4)?>
                                            @foreach ($feeds as $feed)
                                                <div class="row mb-4">
                                                    <div class="col-3" style="">
                                                        @if (is_null($feed['image']))
                                                            <img src="{{secure_asset('public/uploads/logo/business-logo.png')}}" class=""  alt="">
                                                        @else
                                                            <img src="{{$feed['image']}}" class=""  alt="Thought">
                                                        @endif
                                                    </div>
                                                    <div class="col-9">
                                                        <div class="row card-header">
                                                            <div class="col-9">
                                                                <div class="card-header" style="border-bottom: 0px !important">
                                                            <span>
                                                                <img src="{{secure_asset('public/uploads/logo/'.$feed['company']['logo'])}}" alt="img" class="avatar avatar-lg mr-2 brround">
                                                            </span>
                                                                    <h6 class="text-gray-300 pt-2">
                                                                        {{$feed['company']['name']}} <br/>
                                                                        <small class="text-gray-100">{{$feed['company']['address']}}</small>
                                                                    </h6>
                                                                </div>
                                                            </div>
                                                            <div class="col-3">
                                                                <span class=" ml-6 mt-3">{{ Carbon\Carbon::parse($feed->created_at)->diffForHumans()}}</span>
                                                            </div>
                                                        </div>

                                                        <h6 class="pt-3" style="color: #0E59DE">
                                                            <a href="{{$feed['url']}}" target="_blank" >{{$feed['title']??" Visit Article"}}</a>
                                                        </h6>
                                                        <h6 class="text-gray fs-13">THOUGHTS</h6>
                                                        <p class="fs-12">{{$feed['thought']}}</p>

                                                    </div>


                                                </div>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border border-right-0 br-tl-7 br-bl-7">
                    <div class="panel panel-primary tabs-style-4">

                    </div>
                </div>

            </div>
            {{-- END TABS --}}

        </div>
    </div>
@endsection
