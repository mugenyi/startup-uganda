@extends('build.master')
@section('content')
@include('includes.menus.network-menu')

<div class="container">

    <div class="row mt-8">

        <div class="col-lg-4 col-xl-3 pr-6 mb-5 d-block d-sm-none" id="filter">
            <a href="javascript:void(0)" onclick="displayFilters()" ><div class="fs-13 mb-4 text-orange"><i class="fa fa-search"></i> Filter Results</div></a>

        </div>
        <div class="col-lg-4 col-xl-3 pr-6 mb-5 d-none d-sm-block" id="content-filter">
            <div class="fs-16 mb-4 text-gray3 font-weight-bold">
                <img src="{{secure_asset('public/images/filter.svg')}}" style="width: 18px" class="text-gray3 d-inline"> Filters
            </div>

            <form class="" method="POST" action="{{route('investor.search')}}">
                @csrf
                <div class="search-element">
                    <input type="search" class="form-control" name="name" placeholder="Enter description Keyword…" aria-label="Search" tabindex="1">
                    <button class="btn btn-primary-color" type="submit"> </button>
            </div>

            <h5 class="text-gray3 mt-7 ">Demographics</h5>


            <select class="form-control mb-2" name="industry">
                <option value="">Search By Industry...</option>
                {{ $industries= App\Traits\HelperTrait::getIndustries() }}
                @foreach ($industries as $industry)
                <option value="{{$industry['industry']}}">
                    {{$industry['industry']}}
                </option>
                @endforeach
            </select>
            <select class="form-control mb-2" name="sector">
                <option value="">Search By Sector...</option>
                {{ $sectors= App\Traits\HelperTrait::getSectors() }}
                @foreach ($sectors as $sector)
                <option value="{{$sector['sector_name']}}">
                    {{$sector['sector_name']}}
                </option>
                @endforeach

            </select>


            <h5 class="text-gray3 mt-7">Funding Calls</h5>
            <p>Looking to invest: (USD)</p>
            <div class="range range-primary">
                <input type="range" name="range" min="0" max="500000" name="investValue" value="2000" onchange="rangePrimary.value=value">
                <output id="rangePrimary">2000</output>
              </div>

              <h5 class="text-gray3 mt-7">General Information</h5>
              <div class="form-group ">
                  <div class="form-label">Type of Investor</div>
              <div class="custom-controls-stacked">
                  <label class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" name="investor_type" value="Organisation">
                  <span class="custom-control-label">Organisation</span>
                </label>

                <label class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input" name="investor_type" value="Individual"> <span class="custom-control-label">Individual</span>
                </label>

            </div>

            <button class="btn btn-orange mt-2" type="submit">Apply Filters</button>
            </div>
        </form>
        </div>

        <div class="col-lg-8 col-xl-9 pl-5">
            <h3 class="text-gray3">Browse Investors</h3>

            <div class="text-gray2">
                Showing {{($investors->currentpage()-1)*$investors->perpage()+1}} to {{$investors->currentpage()*$investors->perpage()}}
                of  {{$investors->total()}} entries
            </div>
            <div class="row mt-5">
                <div class="table-responsive">
                    <table class="table table-striped card-table table-vcenter">
                        <thead class="text-gray3" style="background-color:#F4F4F4;">
                            <tr  style="height: 54px;text-align: left;margin: 0;">
                               <th  style="vertical-align: middle;font-size:14px;text-transform: none;" colspan="2"><b>Name</b></th>
                               <th  style="vertical-align: middle;font-size:14px;text-transform: none;"><b>One liner</b></th>
                               <th class="d-none d-sm-block" style="vertical-align: middle;font-size:14px;text-transform: none;"><b>Type</b></th>
                               <th  style="vertical-align: middle;font-size:14px;text-transform: none;"><b>Investments</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($investors as $investor)
                            <tr class="fs-12" style="padding: 20px">
                                <td style="height: 112px;">
                                <img src="{{secure_asset(env('LOGO').''.$investor['logo'])}}" alt="{{$investor['name']}}"  style="min-width: 50px;min-height: 50px"  class="avatar avatar-lg mr-2 brround">
                            </td>
                            <td>
                                <a href="{{route('investor.details',['slug'=>$investor['slug']])}}"><b>{{$investor['name']}}</b></a>
                           </td>
                            <td>{{$investor['tagline']}}</td>
                            <td class="text-capitalize d-none d-sm-block" style="padding-top: 40px;">{{$investor['investor_type']}}</td>
                            <td>$ {{number_format(App\Traits\HelperTrait::getFundings('investor',$investor['id'],'sum'))}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="row">
                {{$investors->links()}}
            </div>
        </div>
    </div>
</div>
@endsection
