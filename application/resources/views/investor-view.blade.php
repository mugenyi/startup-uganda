@extends('build.master')
@section('content')
@include('includes.menus.network-menu')

<div class="row">
    <div class="col-lg-6 d-none d-sm-block" style="background: url({{secure_asset(env('BANNER').''.$investor['banner'])}}) no-repeat ;height:360px">
    </div>
    <div class="col-lg-6 pl-5 pt-1 mt-4" style="background-color: #FFF">
        <div class="container">
            <div class="row mt-0">
                <div class="media" style="height: 110px;">
                    <div class="mr-3 mt-1">
                        <img src="{{secure_asset('public/uploads/logo/'.$investor['logo'])}}" alt="img" class="avatar-xxl mr-2 brround p-1 shadow2">
                    </div>
                    <div class="media-body mt-4 pr-4">
                        <div class="fs-20 text-bolder text-gray3 font-weight-normal1" style="font-weight:bolder !important"> {{$investor['name']}} </div>
                        <small class="fs-13 text-gray2">{{$investor['tagline']}}</small>
                    </div>
                </div>
                <hr/>
                <hr/>
                <div class="row" style="width: 100%">
                    <div class="col-lg-6">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                <p class="mb-1  text-gray3 font-weight-semibold">{{$investor['legal_name']}}</p>
                            </div>
                                <span class="mb-0 fs-13  text-gray2">Legal Name</span>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                <p class="mb-1  text-gray3 font-weight-semibold">{{$investor['phone_number']}}</p>
                            </div>
                                <span class="mb-0 fs-13  text-gray2">Phone Number</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">

                        <div class="media">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                <p class="mb-1  text-gray3 font-weight-semibold">{{number_format(App\Traits\HelperTrait::getFundings('investor',$investor['id'],'count'))}}</p>
                            </div>
                                <span class="mb-0 fs-13  text-gray2">Number of Investments</span>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <p class="mb-1  text-gray3 font-weight-semibold">{{$investor['email']}}</p>
                                </div>
                                <span class="mb-0 fs-13  text-gray2">Email</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center">
                                <p class="mb-1  text-gray3 font-weight-semibold">{{ substr($investor['investment_industries'],0,300)}}</p>
                            </div>
                                <span class="mb-0 fs-13  text-gray2">Industries</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
<div class="container pr-4 pl-4">
    <div class="row mt-4">
        <div class="col-md-7 fs-13  text-gray2">
            <div style="margin-bottom: 12px"><strong class="text-gray3 font-weight-semibold">Address</strong> {{$investor['address']}} </div>
            <div style="margin-bottom: 12px"><strong class="text-gray3 mb-4 font-weight-semibold">Website: </strong>{{$investor['website']}}</div>

        <div class="fl-wrap">
            <strong class=" text-gray3 font-weight-semibold">Social Media: </strong>
            @if(!is_null($investor['facebook']))<a href="{{$investor['facebook']}}" target="_blank"><i class="fa fa-facebook fa-1x ml-3"></i></a>@endif
            @if(!is_null($investor['twitter']))<a href="{{$investor['twitter']}}" target="_blank"><i class="fa fa-twitter ml-3"></i></a>@endif
            @if(!is_null($investor['linkedin']))<a href="{{$investor['linkedin']}}" target="_blank"><i class="fa fa-linkedin ml-3"></i></a>@endif
            @if(!is_null($investor['youtube']))<a href="{{$investor['youtube']}}" target="_blank"><i class="fa fa-youtube ml-3"></i></a>@endif

            </div>
            <span style="width: 100%" class="mt-3">
                @if(Auth::check() && App\Traits\HelperTrait::showChatButton(Auth::user()->id,$investor->id))
                    <a href="{{route('initiate.chat',['sender'=>Auth::user()->id,'receiver'=>$investor->id,'type'=>'investor'])}}"><i class="fa fa-comments text-orange"></i> Initiate Chat</a>
                @endif
            </span>
     </div>
     <div class="col-md-5 pull-right">
        @if ($investor->jobs->count() >0)
        <a href="#jobs" data-toggle="tab" class="btn border-orange text-orange pull-right fs-13" style="padding: 10px 180px; margin-left:70px;font-weight:bold"> APPLICATION CALL</a>
        @endif
     </div>
    </div>
</div>

 <div class="container mt-3 pr-4 pl-4">
     <div class="row">
        <div class="col-md-12">
            <div class="list-single-main-item fl-wrap block_box mt-4">
                <div class="list-single-main-item-title">
                    <h3 class=" text-gray3">Company Information</h3>
                </div>
                <div class="list-single-main-item_content fl-wrap">
                    {!!$investor['about']!!}
                </div>

                {{-- TABS --}}
            <div class="d-md-flex mt-5">
                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-md-12">
                        <div class="tab-menu-heading border-0">
                            <div class="tabs-menu ">
                               <!-- Tabs -->
                               <ul class="nav panel-tabs fs-12">
                                  <li class=""><a href="#traction" class="active" data-toggle="tab">DETAILS</a></li>
                                  <li class=""><a href="#funding"  data-toggle="tab">INVESTMENT PORTFOLIO</a></li>
                                  <li><a href="#industries" data-toggle="tab" class=""> INDUSTRIES & SECTORS</a></li>
                                  <li><a href="#team" data-toggle="tab" class=""> TEAM MEMBERS</a></li>
                                  <li><a href="#board" data-toggle="tab" class=""> BOARD MEMBERS</a></li>
                                  <li><a href="#jobs" data-toggle="tab" class=""> JOBS</a></li>
                                  <li><a href="#feeds" data-toggle="tab" class=""> UPDATES</a></li>
                               </ul>
                            </div>
                         </div>
                    </div>
                    <div class="col-xl-9 col-lg-6 col-md-12">
                        <div class="tabs-style-4">
                            <div class="panel-body tabs-menu-body1 border-0">
                               <div class="tab-content">
                                  <div class="tab-pane active" id="traction">

                                      <div class="text-gray3 "><h5 class="fs-28">Experience</h5></div>
                                      <hr/>
                                      <div class="mt-5 list-single-main-item_content fl-wrap">
                                          {!!$investor['experience']!!}
                                      </div>

                                  </div>
                                  <div class="tab-pane" id="funding">
                                      <div class="text-gray3 "><h5 class="fs-28">Investment Portifolio</h5></div>
                                      <hr/>
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 mt-3">
                                            <div class="row">
                                                <?php $funds= App\Traits\HelperTrait::getFundings('investor',$investor['id'],'list'); ?>

                                                <table class="table table-vcenter text-nowrap mb-0 table-striped fs-13">
                                                    <thead>
                                                        <tr>
                                                            <th>Date</th><th>Company Name</th>
                                                            <th>Company Type</th><th>Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($funds as $fund)
                                                        <tr>
                                                        <td>{{date_format(date_create($fund['created_at']),"M-Y")}}</td>
                                                            <td>{{$fund['company_name']}}</td>
                                                            <td>{{$fund['company_type']}}</td>
                                                            <td>$ {{number_format($fund['amount'])}}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>




                                            </div>
                                        </div>
                                    </div>

                                </div>

                                  <div class="tab-pane" id="industries">
                                      <div class="text-gray3 "><h5 class="fs-28">Industries</h5></div>
                                      <hr/>
                                        <div class="list-single-main-item_content fl-wrap">
                                            <?php $industries = explode(',',$investor['investment_industries']); array_pop($industries);?>
                                            @foreach ($industries as $ind)
                                            <a class="btn btn-sm btn-light mt-1" href="#">{{$ind}}</a>
                                            @endforeach
                                        </div>
                                      <div class="text-gray3 mt-7 "><h5 class="fs-28">Sectors</h5></div>
                                      <hr/>
                                            <div class="list-single-main-item_content fl-wrap">
                                                <?php $sectors = explode(',',$investor['investment_sectors']); array_pop($sectors); ?>
                                                @foreach ($sectors as $sec)
                                                <a class="btn btn-sm btn-light mt-1" href="#">{{$sec}}</a>
                                                @endforeach
                                        </div>
                                  </div>
                                  <div class="tab-pane" id="jobs">
                                      <div class="text-gray3 "><h5 class="fs-28">Job openings</h5></div>
                                      <hr/>
                                     <div class="row mt-4">
                                         @foreach ($investor->jobs as $job)
                                             @include('includes.partial.job-list')
                                         @endforeach
                                     </div>
                                  </div>
                                  <div class="tab-pane" id="team">
                                      <div class="text-gray3 "><h5 class="fs-28">Our Team</h5></div>
                                      <hr/>
                                    <div class="row mt-4">

                                            @foreach ($teams as $team)
                                            <div class="col-md-4 pt-3" style="border-top: 1px solid #dedede">
                                            <div class="media mt-0 mb-3">
                                                <figure class="rounded-circle align-self-start mb-0">
                                                    <img src="{{secure_asset('public/uploads/user/'.$team['photo'])}}" style="width: 80px !important; height: 80px !important;" alt="Generic placeholder image" class="avatar brround avatar-md mr-3">
                                                </figure>
                                                <div class="media-body">
                                                    <h6 class="time-title  pt-3 p-0 mb-0  text-gray3 font-weight-semibold leading-normal">{{$team->member_name}}</h6>
                                                    {{$team['designation']}} - Team Member
                                                </div>
                                            </div>
                                            </div>
                                            @endforeach
                                    </div>
                                 </div>
                                 <div class="tab-pane" id="board">
                                     <div class="text-gray3 "><h5 class="fs-28">Board Members & Advisors</h5></div>
                                     <hr/>
                                    <div class="row mt-4">

                                            @foreach ($boards as $board)
                                            <div class="col-md-4 pt-3" style="border-top: 1px solid #dedede">
                                            <div class="media mt-0 mb-3">
                                                <figure class="rounded-circle align-self-start mb-0">
                                                    <img src="{{secure_asset('public/uploads/user/'.$board['photo'])}}" alt="Generic placeholder image" class="avatar brround avatar-md mr-3">
                                                </figure>
                                                <div class="media-body">
                                                    <h6 class="time-title p-0 mb-0  pt-3 text-gray3 font-weight-semibold leading-normal">{{$board->member_name}}</h6>
                                                    {{$team['designation']}} - Board Member
                                                </div>
                                             </div>
                                            </div>
                                            @endforeach
                                    </div>
                                 </div>
                                 <div class="tab-pane" id="feeds">
                                     <div class="text-gray3 "><h5 class="fs-28">Updates</h5></div>
                                     <hr/>
                                    <div class="row mt-4">

                                        <?php $feeds = App\Traits\HelperTrait::getFeeds('investor',$investor['id'],4)?>
                                        @foreach ($feeds as $feed)
                                        <div class="row mb-4">
                                            <div class="col-xl-3 col-lg-6 col-md-12" style="">
                                                @if (is_null($feed['image']))
                                                <img src="{{secure_asset('public/uploads/logo/business-logo.png')}}" class=""  alt="">
                                                @else
                                                    <img src="{{$feed['image']}}" class=""  alt="Thought">
                                                @endif
                                            </div>
                                            <div class="col-xl-9 col-lg-6 col-md-12">
                                                <div class="row card-header">
                                                    <div class="col-xl-9 col-lg-6 col-md-12">
                                                        <div class="card-header" style="border-bottom: 0px !important">
                                                            <span>
                                                                <img src="{{secure_asset('public/uploads/logo/'.$feed['company']['logo'])}}" alt="img" class="avatar avatar-lg mr-2 brround">
                                                            </span>
                                                            <h6 class="text-gray-300 pt-2">
                                                                {{$feed['company']['name']}} <br/>
                                                                <small class="text-gray-100">{{$feed['company']['address']}}</small>
                                                            </h6>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3 col-lg-6 col-md-12">
                                                        <span class=" ml-6 mt-3">{{ Carbon\Carbon::parse($feed->created_at)->diffForHumans()}}</span>
                                                    </div>
                                                </div>

                                                <h6 class="pt-3" style="color: #0E59DE">
                                                    <a href="{{$feed['url']}}" target="_blank" >{{$feed['title']??" Visit Article"}}</a>
                                                </h6>
                                                <h6 class="text-gray fs-13">THOUGHTS</h6>
                                                <p class="fs-12">{{$feed['thought']}}</p>

                                            </div>


                                        </div>

                                    @endforeach
                                    </div>
                                 </div>
                               </div>
                            </div>
                         </div>
                    </div>
                </div>
                <div class="border border-right-0 br-tl-7 br-bl-7">
                   <div class="panel panel-primary tabs-style-4">

                   </div>
                </div>

             </div>
             {{-- END TABS --}}


            </div>
        </div>
     </div>
     @if($teams->count()>0)
     <div class="row mt-5">

            <h5 class="mb-3" style="width: 100%">OUR TEAM</h5>
            <hr/>
                @foreach ($teams as $team)
                <div class="col-md-4 pt-3" style="border-top: 1px solid #dedede">
                <div class="media mt-0 mb-3">
                    <figure class="rounded-circle align-self-start mb-0">
                        <img src="{{secure_asset('public/uploads/user/'.$team['photo'])}}" alt="Generic placeholder image" class="avatar brround avatar-md mr-3">
                    </figure>
                    <div class="media-body">
                        <h6 class="time-title p-0 mb-0  text-gray3 font-weight-semibold leading-normal">{{$team->member_name}}</h6>
                        {{$team['designation']}} - Team Member
                    </div>
                 </div>
                </div>
                @endforeach

     </div>
     @endif
     @if($boards->count()>0)
     <div class="row mt-5">
            <h5 class="mb-3" style="width: 100%">BOARD MEMBERS & ADVISORS</h5>
            <hr/>
                @foreach ($boards as $board)
                <div class="col-md-4 pt-3" style="border-top: 1px solid #dedede">
                <div class="media mt-0 mb-3">
                    <figure class="rounded-circle align-self-start mb-0">
                        <img src="{{secure_asset('public/uploads/user/'.$board['photo'])}}" alt="Generic placeholder image" class="avatar brround avatar-md mr-3">
                    </figure>
                    <div class="media-body">
                        <h6 class="time-title p-0 mb-0  text-gray3 font-weight-semibold leading-normal">{{$board->member_name}}</h6>
                        {{$team['designation']}} - Board Member
                    </div>
                 </div>
                @endforeach
            </div>
     </div>
     @endif
 </div>


@endsection
