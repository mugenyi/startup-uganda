@extends('build.master')
@section('content')
    <div class="container">

        <div class="row mt-7">

            <div class="col-md-9 col-lg-9 col-sm-12">
                <div class="card-2">
                    <div class="card-body">
                        <h3 class="text-gray3">{{$job['job_title']}}</h3>

                        <h5 class="text-gray3 fs-15">About the job</h5>
                        <hr/>
                        <p class="card-text"> {!!$job['description']!!}</p>

                    </div>
                </div>

            </div>
            <div class="col-xl-3 col-lg-3 col-md-12 col-xs-12">
                <a href="{{$job['application_link']}}" target="_blank">
                    <button class="btn btn-orange mt-7 mb-2" style="width: 100%">Apply Now</button>
                </a>

                <div class="card">
                    <div class="card-header">
                        <span>
                            <img src="{{secure_asset('public/uploads/logo/'.$company['logo'])}}" alt="img" class="avatar avatar-lg mr-2 brround">
                        </span>
                        <h6 class="text-gray-300 pt-2">
                            {{$company['name']}} <br/>
                            <small class="text-gray-100">{{$company->address}}</small>
                        </h6>

                    </div>
                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item-0">
                                <h5 class="mb-0 text-gray3">Location</h5>
                                <p class="text-gray-500">{{ $job['location']}}</p>
                            </li>
                            <li class="list-group-item-0">
                                <h5 class="mb-0 text-gray3">Job type</h5>
                                <p class="text-gray-500">{{ $job['job_type']}}</p>
                            </li>
                            <li class="list-group-item-0">
                                <h5 class="mb-0 text-gray3">Valid Until</h5>
                                <p class="text-gray-500">{{ $job['ends_at']}}</p>
                            </li>
                        </ul>



                    </div>
                </div>
                <h5 class="text-gray3 fs-15">Required Skills</h5>
                <hr/>
                <?php $skills = explode(',',$job['skills']);?>
                @foreach ($skills as $skill)
                    <a class="btn btn-sm btn-light mt-1" href="#">{{$skill}}</a>
                @endforeach
            </div>
        </div>
    </div>
@endsection
