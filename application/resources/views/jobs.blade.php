@extends('build.master')
@section('content')
<div class="container">

    <div class="row mt-8">
        <div class="col-lg-4 col-xl-3 pr-6 mb-5 d-block d-sm-none" id="filter">
            <a href="javascript:void(0)" onclick="displayFilters()" ><div class="fs-13 mb-4 text-orange"><i class="fa fa-search"></i> Filter Results</div></a>

        </div>
        <div class="col-lg-4 col-xl-3 pr-6 mb-5 d-none d-sm-block" id="content-filter">
            <div class="fs-16 mb-4 text-gray3 font-weight-bold">
                <img src="{{secure_asset('public/images/filter.svg')}}" style="width: 18px" class="text-gray3 d-inline"> Filters
            </div>

            <form class="" method="POST" action="{{route('job.search')}}">
                @csrf
                <div class="search-element">
                    <input type="search" class="form-control" name="name" placeholder="Enter description Keyword…" aria-label="Search" tabindex="1">

                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Job Location" name="location">
                    </div>


                <h5 class="text-gray3 mt-6">General Information</h5>
              <div class="form-group ">
                  <div class="form-label">Company type</div>
                    <div class="custom-controls-stacked">
                        <?php $cos = ['startup','investor','government','esos']; ?>
                        @foreach ($cos as $co)
                            <label class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input tect-capitalize" name="company_type" value="{{$co}}"> <span class="custom-control-label text-capitalize">{{$co}}</span>
                            </label>
                        @endforeach
                    </div>
              </div>
              <div class="form-group mt-3">
                <div class="form-label">Job type</div>
                    <div class="custom-controls-stacked">

                    <?php $types = ['Full time','Part time','Contract','Remote']; ?>
                    @foreach ($types as $type)
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="job_type" value="{{$type}}"> <span class="custom-control-label">{{$type}}</span>
                        </label>
                    @endforeach


                </div>
            </div>

            <button class="btn btn-orange mt-2" type="submit">Apply Filters</button>

        </form>
        </div>
        <div class="col-lg-8 col-xl-9">
            <h4>Browse Jobs</h4>
            <div class="text-muted">
                Showing {{($jobs->currentpage()-1)*$jobs->perpage()+1}} to {{$jobs->currentpage()*$jobs->perpage()}}
                of  {{$jobs->total()}} entries
            </div>
            <div class="row mt-5">
                @foreach ($jobs as $job)
                    @include('includes.partial.job-list')
                @endforeach
            </div>
            <div class="row">
                {{$jobs->links()}}
            </div>
        </div>
    </div>
</div>
@endsection
