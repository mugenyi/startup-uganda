
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
    <title>{{env('APP_NAME')}}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->
        <link type="text/css" rel="stylesheet" href="{{assets('public/theme/css/reset.css')}}">
        <link type="text/css" rel="stylesheet" href="{{secure_asset('public/theme/css/plugins.css')}}">
        <link type="text/css" rel="stylesheet" href="{{secure_asset('public/theme/css/main-style.css')}}">
        <link type="text/css" rel="stylesheet" href="{{secure_asset('public/theme/css/color.css')}}">
        <link type="text/css" rel="stylesheet" href="{{secure_asset('public/theme/css/style.css')}}">
        @yield('custom-css')
        <!--=============== favicons ===============-->
        <link rel="apple-touch-icon" sizes="180x180" href="{{secure_asset('public/images/favicon/apple-touch-icon.png')}}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{secure_asset('public/images/favicon/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{secure_asset('public/images/favicon/favicon-16x16.png')}}">
        <link rel="manifest" href="{{secure_asset('public/images/favicon/site.webmanifest')}}">
    </head>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            {{-- <div class="loader-inner">
                <div class="loader-inner-cirle"></div>
            </div> --}}
        </div>
        <!--loader end-->
        <!-- main start  -->

        <div id="main">

            @include('includes.header')
            <!-- header end-->
            <div id="wrapper">
                <!-- content-->
                <div class="content">
                    @yield('content')
                </div>
                <!--content end-->
            </div>
            <!-- wrapper end-->
             <!--footer -->
             @include('includes.footer')
             <!--footer end -->

             <!--register form -->
            @include('includes.modal.register')
            <!--register form end -->

            <a class="to-top"><i class="fas fa-caret-up"></i></a>
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <script src="{{secure_asset('public/theme/js/jquery.min.js')}}"></script>
        <script src="{{secure_asset('public/theme/js/plugins.js')}}"></script>
        <script src="{{secure_asset('public/theme/js/scripts.js')}}"></script>
    </body>
</html>
