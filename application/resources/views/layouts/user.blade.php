@extends('layouts.master')
@section('custom-css')
<link type="text/css" rel="stylesheet" href="{{secure_asset('public/theme/css/dashboard-style.css')}}">
@endsection
@section('content')
     <!-- wrapper-->
     <div id="wrapper">
        <!-- content-->
        <div class="content">
            @include('user.partial.profile')

            <section class="gray-bg main-dashboard-sec" id="sec1">
                <div class="container">
                    @include('user.partial.menu')
                    <!-- dashboard content-->
                    <div class="col-md-9">
                        @yield('user-content')
                        <!-- dashboard-list-box end-->
                    </div>
                    <!-- dashboard content end-->
                </div>
            </section>

        </div>
        <!--content end-->
    </div>
    <!-- wrapper end-->
    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
        </script>
@endsection
