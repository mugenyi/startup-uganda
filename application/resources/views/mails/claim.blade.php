@extends('build.email')
<div class="row p-6 bg-black-1">
    <img src="{{secure_asset('public/theme/images/logo.png')}}" style="width:80px" class="" alt="Startup Uganda Logo">
</div>
<div class="container text-center">
    <div class="row">
    <h4>Welcome to {{env('APP_NAME')}}</h4>
    <p>Startup Uganda (SU) is an association of innovation and entrepreneurship support organizations
        (IESO) working towards strengthening the startup support ecosystem and sector.</p>

    <p>Rooted in the fundamental belief that innovators and entrepreneurs are a driving source of economic,
        social, and environmental sustainable development, the association is committed to creating an enabling
        environment where startups can access the support they need to start and grow. As an association of
        organizations, SU’s approach stems from the conviction that we are stronger and more impactful together
        than we are alone. SU endeavours to deepen the trust, sharing and collaboration between members and the
        ecosystem at large.</p>

    <a href="{{route('claim.link',['endpoint'=>$endpoint])}}" class="btn btn-sm btn-info">Claim your profile </a>
    </div>

</div>
