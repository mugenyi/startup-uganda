@extends('build.email')
<div class="row p-6 bg-black-1">
    <img src="{{secure_asset('public/theme/images/logo.png')}}" style="width:80px" class="" alt="Startup Uganda Logo">
</div>
<div class="container text-center">
    <div class="row">
    <h4>Hello,</h4>
    <p>Client with the details below jyst registered for {{$program['program_name']}}</p>

    <p>
    <b>Name:</b> {{$registry['name']}}<br/>
    <b>Email:</b> {{$registry['email']}}<br/>
    <b>Phone Number:</b> {{$registry['phone']}}<br/>
    <b>Sector:</b> {{$registry['sector']}}<br/>
    <b>Company:</b> {{$registry['company']}}<br/>
    <b>Company Registration:</b> {{$registry['company_registration']}}<br/>
    <b>Operational years:</b> {{$registry['operation_years']}}<br/>
    <b>Investment Type & Size:</b> {{$registry['investment_size']}}<br/>
    <b>Affliated to:</b> {{$registry['affliation']}}<br/>
    <b>Pitchdeck:</b> <a href="{{secure_asset('public/uploads/full/'.$registry['pitchdeck'])}}" target="_blank">View Pitchdeck</a><br/>
    <b>Team members:</b> {{$registry['team_members']}}<br/>
    </p>

    </div>

</div>
