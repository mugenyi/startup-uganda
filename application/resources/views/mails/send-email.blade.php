@extends('build.email')
<div class="row p-6 bg-black-1">
    <img src="{{secure_asset('public/theme/images/logo.png')}}" style="width:80px" class="" alt="Startup Uganda Logo">
</div>
<div class="container text-center">
    <div class="row">
        <p>Hello,</p>
        <p>Name: {{$data['name']}}</p>
        <p>Email: {{$data['email']}}</p>
        <p>Message: {{$data['message']}}</p>
    </div>

</div>
