@extends('build.master')
@section('content')
@include('includes.menus.network-menu')

<div class="container mt-7">
    <div class="row">
        <div class="col-lg-2">

           <div class="box-widget widget-user text-center">
           <div class="row mb-5 mt-5">
            <img alt="User Avatar" class="avatar avatar-xxxl brround border p-0" style="max-width: 70%;  margin-left:auto;margin-right:auto" src="{{secure_asset('public/uploads/logo/'.$mentor['photo'])}}">

           </div>
            <span style="width: 100%" class="mt-3">
                @if(Auth::check() && App\Traits\HelperTrait::showChatButton(Auth::user()->id,$mentor->id))
                    <a href="{{route('initiate.chat',['sender'=>Auth::user()->id,'receiver'=>$mentor->id,'type'=>'mentor'])}}"><i class="fa fa-comments text-orange"></i> Initiate Chat</a>
                @endif
            </span>

           </div>
        </div>
        <div class="col-lg-10 col-md-auto">
            <div class="card-body">
                <h4 class="pro-user-username centered text-gray3 mb-3 font-weight-bold">{{$mentor['name']}} </h4>
                <div class="main-profile-bio mb-0">
                <p>{!!$mentor['about']!!}</p>
                </div>
                <div class="row" style="width: 100%">

                    <div class="col-lg-4 col-sm-12">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex text-gray3 align-items-center mt-1">
                                <h6 class="mb-1">{{$mentor['location']}}</h6>
                            </div>
                                <span class="mb-0 fs-13 text-muted">Location</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-12">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex text-gray3 align-items-center mt-1">
                                <h6 class="mb-1">{{ $mentor['gender']}}</h6>
                            </div>
                                <span class="mb-0 fs-13 text-muted">Gender</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-5 pr-4 pl-4">
                    <div class="col-ms-12">
                        <h5>ABOUT</h5>
                        <br/>
                        <table class="table table-vcenter text-wrap fs-12">
                            <tr class="p-3 " >
                                <td class="text-uppercase text-gray3 pl-4" style="width: 20%">What i do</td>
                                <td class="mb-4 text-justify">{!!$mentor['what_i_do']!!}</td>
                            </tr>
                            <tr class="p-3">
                                <td class="text-uppercase text-gray3 fs-12 pl-4">Achievements</td>
                                <td class="mb-4 text-justify">{!!$mentor['achievements']!!}</td>
                            </tr>
                            <tr class="p-3">
                                <td class="text-uppercase text-gray3 fs-12 pl-4">What i'm looking for</td>
                                <td class="mb-4 text-justify">{!!$mentor['looking_for']!!}</td>
                            </tr>
                            <tr class="p-3">
                                <td class="text-uppercase text-gray3 fs-12 pl-4">Markets</td>
                                <td class="mb-4 text-justify">{!!$mentor['markets']!!}</td>
                            </tr>
                        </table>


                    </div>

                </div>

        </div>
        </div>
     </div>
</div>


@endsection
