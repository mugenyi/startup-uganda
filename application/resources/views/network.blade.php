@extends('build.master')
@section('content')
    @include('includes.menus.network-menu')
    <div class="row" style="height:380px;background-repeat: no-repeat;
  background-position: center center;   background-size: cover;background-image: url('https://media-exp1.licdn.com/dms/image/C4D1BAQHsE9-1RslyCQ/company-background_10000/0/1625475469611?e=1635512400&v=beta&t=nXDLHeednxxT4j1Oz5K8uyuP83-VWkYupCo7LuILQJA')">

    </div>
    <div class="row">
        {{-- <div class="container text-center ">
            <h1 class="text-white font-weight-bolder pt-8">Uganda’s Startup Ecosystem</h1>
            <h3>Explore Our Network Today</h3>
            <div class="row text-white">
                <div class="col-md-4">
                    <h1>{{rand(111,999)}}</h1>
                    <h4>Startups</h4>
                </div>
                <div class="col-md-4">
                    <h1>{{rand(111,999)}}</h1>
                    <h4>Investors</h4>
                </div>
                <div class="col-md-4">
                    <h1>{{rand(111,999)}}</h1>
                    <h4>ESOs</h4>
                </div>
            </div>

        </div> --}}
        <div class="container mt-7 mb-7">
            <h2 class="startupTitleHr fs-18 mb-6 text-dark-gray font-weight-bold"><span>What the Network Offers</span></h2>
            <div class="row">
                <div class="col-md-4">
                    <img src="https://www.startupsg.gov.sg/_nuxt/37320755f43a6530522e86e427a326c2.svg">
                    <h4>Discover</h4>
                    <p>Search for companies or people through the Network, and connect with them for opportunities. Once connected, you will be able to see their latest activity or message them directly.</p>
                </div>
                <div class="col-md-4">
                    <img src="https://www.startupsg.gov.sg/_nuxt/d66e0b9e29e82ae08e69be3ca902c658.svg">
                    <h4>Get Recommended</h4>
                    <p>Expand your network through our recommendations. Through machine learning, our recommendation engine considers your preferences and associations to suggest potential business partners.</p>
                </div>
                <div class="col-md-4">
                    <img src="https://www.startupsg.gov.sg/_nuxt/e4bae6d772fcc0c0228a0e01f1950cb6.svg">
                    <h4>Pitch</h4>
                    <p>Profile yourself on a global stage through the Network. Startups looking to raise funds can also upload pitch materials and deal make with investors through the platform.
                    </p>
                </div>
            </div>
        </div>

    </div>
    <div class="row bg-dark-gray">
        <div class="container text-white  mt-7 mb-7">
            <h2 class="fs-18 bolder mb-4">How it works</h2>
            <p>Plug into the startup Uganda ecosystem with these simple steps:</p>
            <img src="https://www.startupsg.gov.sg/_nuxt/img/steps_how-it-works.e121ae2.png" class="mt-2 mb-2">
        </div>
    </div>
    <div class="row" style="">
        <div class="container mt-7" >
            <h2 class="startupTitleHr fs-18 mb-6 text-dark-gray font-weight-bold"><span>Sectors</span></h2>
            <div class="row">
                @foreach($sectors as $sector)

                    <a href="#" class="">
                        <div  class="rounded border-orange br-5 p-1 mx-1 my-1">{{$sector->sector_name}}</div>
                    </a>
                @endforeach
            </div>
        </div>

    </div>

@endsection
