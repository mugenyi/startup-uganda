@extends('build.master')
@section('content')
    <div class="row">
        <div class="col-md-6 col-xm-12" style="background: url({{secure_asset(env('BANNER').''.$program['cover'])}}) !important;background-size: cover;height:330px">
        </div>
        <div class="col-md-6 col-xm-12 p-7" style="background-color: #FFF">
           @if($program['id']==env('PROGRAM') )
            <a href="" class="text-primary" data-target="#register" data-toggle="modal">
                <button class="btn btn-primary pull-right" style="margin-top: -40px">Register Event</button>
            </a>
            @else
             <a href="{{$program['link']}}" target="_blank"><button class="btn btn-warning pull-right" style="margin-top: -40px">Register</button></a>
            @endif
            <div class="container">
                <div class="row mt-5">
                    <h5 class="text-uppercase text-gray3" style="width: 100%">{{$program['program_name']}}</h5>
                    <p class="text-gray">{{$program['summary']}} </p>
                    <hr/>
                    <div class="row" style="width: 100%">
                        <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12">
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex text-gray3 align-items-center mt-1">
                                        <h6 class="mb-1">{{ date_format(date_create($program['start_date']),'Y m d')}}</h6>
                                    </div>
                                    <span class="mb-0 fs-13 text-muted">Start Date</span>
                                </div>
                            </div>
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex text-gray3 align-items-center mt-1">
                                        <h6 class="mb-1">{{ $program['organiser']}}</h6>
                                    </div>
                                    <span class="mb-0 fs-13 text-muted">Organiser</span>
                                </div>
                            </div>


                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12">
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex text-gray3 align-items-center mt-1">
                                        <h6 class="mb-1">{{ date_format(date_create($program['end_date']),'Y m d')}}</h6>
                                    </div>
                                    <span class="mb-0 fs-13 text-muted">End Date</span>
                                </div>
                            </div>
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex text-gray3 align-items-center mt-1">
                                        <h6 class="mb-1"><a href="{{$program['link']}}" target="_blank">Visit Website</a></h6>
                                    </div>
                                    <span class="mb-0 fs-13 text-muted">Website</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12">

                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex text-gray3 align-items-center mt-1">
                                        <h6 class="mb-1">{{ $program['target_audience']}}</h6>
                                    </div>
                                    <span class="mb-0 fs-13 text-muted">Targets</span>
                                </div>
                            </div>
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex text-gray3 align-items-center mt-1">
                                        <h6 class="mb-1">{{ $program['location']}}</h6>
                                    </div>
                                    <span class="mb-0 fs-13 text-muted">Location</span>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>


            </div>

        </div>
    </div>

    <div class="container mt-6  pl-4 pr-4">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <h5 class="text-gray3 fs-20">About this Program</h5>
                <hr/>
                {!! $program['description']!!}
            </div>

        </div>
    </div>

<div class="modal" id="register">
    <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header" style="background-color:#f29109; color:#FFF">
             <h6 class="modal-title">Register for {{$program['program_name']}} Program</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('program.register')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="program" value="{{$program['id']}}" />
          <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">Name <span class="text-red">*</span></label>
                        <input type="text" required class="form-control" placeholder="Enter your name" value="{{Auth::user()->name??""}}" name="name">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                       <label class="form-label">Email <span class="text-red">*</span></label>
                       <input type="text" required class="form-control" placeholder="Email Email" value="{{Auth::user()->email??""}}" name="email">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                         <label class="form-label">Phone Number <span class="text-red">*</span></label>
                         <input type="text" required class="form-control" id="date" placeholder="Phone Number" name="phone">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Sector <span class="text-red">*</span></label>
                        <select class="form-control" required name="sector">
                           <option value="">Select Sector</option>
                           <option value="Fintech">Fintech</option>
                           <option value="Health">Health</option>
                           <option value="Agriculture">Agriculture</option>
                           <option value="Education">Education</option>
                        </select>
                    </div>

                    <div class="form-group">
                         <label class="form-label">Is your company Registered? <span class="text-red">*</span></label>
                         <label class="radio-inline" style="margin-top:10px; margin-right:25px">
                            <input type="radio" value="Yes" name="company_registration" checked>&nbsp;&nbsp;Yes
                         </label>
                         <label class="radio-inline">
                            <input type="radio" value="No" name="company_registration">&nbsp;&nbsp;No
                         </label>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Startup Uganda Member Affiliation <span class="text-red">*</span></label>
                        <select class="form-control" required name="affliation">
                           <option value="">Select Member you are affliated to</option>
                           <option value="Outbox">Outbox</option>
                           <option value="The innovation Village">The innovation Village</option>
                           <option value="Design Hub">Design Hub</option>
                           <option value="TechBuzz Hub">TechBuzz Hub</option>
                           <option value="Iventure Africa">Iventure Africa</option>
                           <option value="MIIC">MIIC</option>
                           <option value="RIL">RIL</option>
                           <option value="Start Hub">Start Hub</option>
                           <option value="NFT Consult">NFT Consult</option>
                           <option value="Tribe Kampala">Tribe Kampala</option>
                           <option value="Shona">Shona</option>
                           <option value="Refactory">Refactory</option>
                           <option value="Growth Africa">Growth Africa</option>
                           <option value="Amara Hub">Amara Hub</option>
                           <option value="Hive Colab">Hive Colab</option>
                           <option value="Amarin Financial">Amarin Financial</option>
                           <option value="United Social Ventures">United Social Ventures</option>
                           <option value="Women in Technology Uganda">Women in Technology Uganda</option>
                           <option value="Einstein Rising">Einstein Rising</option>
                           <option value="Other">Others</option>
                        </select>
                     </div>

                </div>
                <div class="col-md-6">
                    <div class="form-group">
                     <label class="form-label">Company Name <span class="text-red">*</span></label>
                     <input type="text" required class="form-control" id="date" placeholder="Enter Company Name" name="company">
                    </div>
                    <div class="form-group">
                       <label class="form-label">How long have you been operational? <span class="text-red">*</span></label>
                       <select class="form-control" required name="operation_years">
                          <option value="">Select Years</option>
                          <option value="<2years">Less than 2 years</option>
                          <option value="2-4 years">2-4 years</option>
                          <option value="4-7 years">4-7 years</option>
                          <option value="7-10 years">7-10 years</option>
                          <option value="10+ years">10+ years</option>
                       </select>
                    </div>
                    <div class="form-group">
                        <label class="form-label">What type & Size of Investment are you looking for? <span class="text-red">*</span></label>
                        <input type="text" required class="form-control" id="invest" placeholder="Investment type & size" name="investment_size">
                    </div>

                    <div class="form-group">
                        <label class="form-label">Your Company pitchdeck <span class="text-red">*</span></label>
                        <input type="file" required class="form-control" id="pitchdeck" placeholder="Pitchdeck" name="pitchdeck"aria-describedby="emailHelp">
                        <div id="emailHelp" class="form-text">Not more than 10 slides</div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPhone" class="form-label">Team Members</label>
                        <textarea class="form-control" name="team_members" id="team_members-editor"  placeholder="Enter team members and titles" rows="2"></textarea>
                    </div>
                </div>

            </div>
          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Register For Event</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>


@endsection
