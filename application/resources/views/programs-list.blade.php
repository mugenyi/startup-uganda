@extends('build.master')
@section('content')
<div class="container">

    <div class="row mt-8">
        <div class="col-lg-4 col-xl-3 pr-6 mb-5 d-block d-sm-none" id="filter">
            <a href="javascript:void(0)" onclick="displayFilters()" ><div class="fs-13 mb-4 text-orange"><i class="fa fa-search"></i> Filter Results</div></a>

        </div>
        <div class="col-lg-4 col-xl-3 pr-6  d-none d-sm-block"  id="content-filter">
            <div class="fs-16 mb-4 text-gray3 font-weight-bold"><img src="{{secure_asset('public/images/filter.svg')}}" style="width: 18px" class="text-gray3 d-inline"> Filters</div>

            <form class="" method="POST" action="{{route('search.program')}}">
                @csrf
                <div class="search-element">
                    <input type="search" class="form-control" name="name" placeholder="Enter description Keyword…" aria-label="Search" tabindex="1">

            </div>

                <h5 class="text-gray3 mt-6">Demographics</h5>
            <select class="form-control mb-2" name="district">
                <option value="">Search By District...</option>

                {{ $districts= App\Traits\HelperTrait::getDistricts() }}
                @foreach($districts as $district)
                    <option value="{{$district['district']}}">{{$district['district']}}</option>
                 @endforeach
            </select>

            <select class="form-control mb-2" name="industry">
                <option value="">Search By Industry...</option>
                {{ $industries= App\Traits\HelperTrait::getIndustries() }}
                @foreach ($industries as $industry)
                <option value="{{$industry['industry']}}">
                    {{$industry['industry']}}
                </option>
                @endforeach
            </select>
            <select class="form-control mb-2" name="sector">
                <option value="">Search By Sector...</option>
                {{ $sectors= App\Traits\HelperTrait::getSectors() }}
                @foreach ($sectors as $sector)
                <option value="{{$sector['sector_name']}}">
                    {{$sector['sector_name']}}
                </option>
                @endforeach

            </select>



            <button class="btn btn-orange mt-2" type="submit">Apply Filters</button>

        </form>
        </div>

        <div class="col-lg-8 col-xl-9 ">
            <?php $startPrograms = \App\Traits\HelperTrait::getStartupUgPrograms(4); ?>
            @if ($startPrograms->count() > 0)
                <h5 class="text-gray3 fs-25">Startup-Uganda Programs</h5>
                <div class="row mb-7">
                    @foreach ($startPrograms as $program)
                        @include('includes.partial.program-col-4')
                    @endforeach
                </div>
                @endif

            <h5 class="text-gray3 fs-25">Browse Programs</h5>
            <div class="row">
                @foreach ($programs as $program)
                    @include('includes.partial.program-col-4')
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
