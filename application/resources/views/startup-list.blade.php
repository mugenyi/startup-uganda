@extends('build.master')
@section('content')
@include('includes.menus.network-menu')
    <div class="container">

        <div class="row mt-8">

            <div class="col-lg-4 col-xl-3 pr-6 mb-5 d-block d-sm-none" id="filter">
                <a href="javascript:void(0)" onclick="displayFilters()" ><div class="fs-13 mb-4 text-orange"><i class="fa fa-search"></i> Filter Results</div></a>

            </div>
            <div class="col-lg-4 col-xl-3 pr-6 mb-5 d-none d-sm-block" id="content-filter">
                <div class="fs-16 mb-4 text-gray3 font-weight-bold"><img src="{{secure_asset('public/images/filter.svg')}}" style="width: 18px" class="text-gray3 d-inline"> Filters</div>

                {{-- SEARCH BY NAME --}}
                <form class="" method="POST" action="{{route('startup.search')}}">
                    @csrf
                    <div class="search-element">
                        <input type="search" class="form-control" name="name" placeholder="Enter description Keyword…" aria-label="Search" tabindex="1">
                        <button class="btn btn-primary-color" type="submit"> </button>
                    </div>

                    <h5 class="text-gray3 mt-6">Demographics</h5>
                    <select class="form-control mb-2" name="district">
                        <option value="">Search By District...</option>

                        {{ $districts= App\Traits\HelperTrait::getDistricts() }}
                        @foreach($districts as $district)
                            <option value="{{$district['district']}}">{{$district['district']}}</option>
                        @endforeach
                    </select>
                    <select class="form-control mb-2" name="industry">
                        <option value="">Search By Industry...</option>
                        {{ $industries= App\Traits\HelperTrait::getIndustries() }}
                        @foreach ($industries as $industry)
                            <option value="{{$industry['industry']}}">
                                {{$industry['industry']}}
                            </option>
                        @endforeach
                    </select>
                    <select class="form-control mb-2" name="sector">
                        <option value="">Search By Sector...</option>
                        {{ $sectors= App\Traits\HelperTrait::getSectors() }}
                        @foreach ($sectors as $sector)
                            <option value="{{$sector['sector_name']}}">
                                {{$sector['sector_name']}}
                            </option>
                        @endforeach

                    </select>


                    <h5  class="text-gray3 mt-8">Funding Information</h5>
                    <div class="form-group ">
                        <div class="form-label">Fundraising</div>
                        <div class="custom-controls-stacked">
                            <label class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" name="fundraising" value="Yes">
                                <span class="custom-control-label">Yes</span>
                            </label>

                            <label class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" name="fundraising" value="No"> <span class="custom-control-label">No</span>
                            </label>
                        </div>
                    </div>
                    <p class="mt-7">Looking to raise: $0 - $500K(USD)</p>
                    <div class="range range-warning">
                        <input type="range" name="range" min="0" max="500000" name="lookingToRaise" value="2000" onchange="rangePrimary.value=numberWithCommas(value)">
                        <output id="rangePrimary">2,000</output>
                    </div>
                    <div class="form-group mt-7 ">
                        <div class="form-label">Revenue Generating</div>
                        <div class="custom-controls-stacked">
                            <label class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" name="revenue" value="Yes">
                                <span class="custom-control-label">Yes</span>
                            </label>

                            <label class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" name="revenue" value="No">
                                <span class="custom-control-label">No</span>
                            </label>
                        </div>
                    </div>

                    <button class="btn btn-orange mt-2" type="submit">Apply Filters</button>

                </form>

            </div>
            <div class="col-lg-8 col-xl-9 pl-5">
                <h3 class="text-gray3">Browse Ventures</h3>
                <div class="text-gray2">
                    Showing {{($startups->currentpage()-1)*$startups->perpage()+1}} to {{$startups->currentpage()*$startups->perpage()}}
                    of  {{$startups->total()}} entries
                </div>
                <div class="row ">
                    <div class="table-responsive mt-4">
                        <table class="table table-striped card-table table-vcenter">
                            <thead class="text-gray3" style="background-color:#F4F4F4;">
                            <tr  style="height: 54px;text-align: left;margin: 0;">
                                <th colspan="2" style="vertical-align: middle;font-size:14px;text-transform: none;"><b>Name</b></th>
                                <th style="vertical-align: middle;font-size:14px;text-transform: none;"><b>One liner</b></th>
                                <th class="d-none d-sm-block" style="vertical-align: middle;font-size:14px;text-transform: none;"><b>Industries</b></th>
                                <th style="vertical-align: middle;font-size:14px;text-transform: none;"><b>Stage</b></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($startups as $startup)
                                <tr class="fs-12" style="padding: 20px">
                                   <td style="height: 112px;">
                                        <img src="{{secure_asset(env('LOGO').''.$startup['logo'])}}" alt="{{$startup['name']}}" style="min-width: 50px;min-height: 50px"  class="avatar avatar-lg mr-2 brround">
                                    </td>
                                    <td>
                                        <a href="{{route('startup.details',['slug'=>$startup['slug']])}}" class="fs-14"><b>{{$startup['name']}}</b></a>
                                    </td>
                                    <td>{{$startup['tagline']}}</td>
                                    <td class="d-none d-sm-block">{{$startup['industries']}}</td>
                                    <td>{{$startup['stage']}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="row">
                    {{$startups->links()}}
                </div>
            </div>
        </div>
    </div>

    <script>
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
        }


    </script>
@endsection
