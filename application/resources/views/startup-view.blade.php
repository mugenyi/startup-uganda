@extends('build.master')
@section('content')
@include('includes.menus.network-menu')

    <div class="mb-4" style="background: url('https://images.squarespace-cdn.com/content/v1/558da567e4b0813627c44d54/1600363437350-NRE4C3O88SPYFQC3LL66/Empty+Startup+Space-Web--1.jpg'); background-repeat: no-repeat;
    background-attachment: fixed;background-attachment: fixed;background-size: cover;
    background-repeat:no-repeat;
background-position: center center;height:360px;">

    <div class="row" >

        <div class="container text-center" style="padding-top:150px;z-index:99">
            <h2 class="fs-18 text-white font-weight-bold  " style="font-weight:bolder !important"> {{$startup['name']}} </h2>
            <p class="fs-14 text-orange font-weight-bold ">{{$startup['tagline']}}</p>
            <img src="{{secure_asset('public/uploads/logo/'.$startup['logo'])}}" alt="img" style="width: 9rem; margin-top:50px;height:9rem" class="avatar-xxxl shadow2 center-block brround">

        </div>
    </div>

    </div>
    <div class="row">
        <div class="container pl-4 pr-4">
            <div class="row mt-4">
                <div class="col-md-7 fs-13 text-gray2">
                   <div class="fl-wrap">
                        <strong class="text-gray3 font-weight-semibold">Social Media: </strong>
                        @if(!is_null($startup['facebook']))<a href="{{$startup['facebook']}}" target="_blank"><i class="fa text-orange fa-facebook fa-1x ml-3"></i></a>@endif
                        @if(!is_null($startup['twitter']))<a href="{{$startup['twitter']}}" target="_blank"><i class="fa text-orange fa-twitter ml-3"></i></a>@endif
                        @if(!is_null($startup['linkedin']))<a href="{{$startup['linkedin']}}" target="_blank"><i class="fa text-orange fa-linkedin ml-3"></i></a>@endif
                        @if(!is_null($startup['youtube']))<a href="{{$startup['youtube']}}" target="_blank"><i class="fa text-orange fa-youtube ml-3"></i></a>@endif

                    </div>
                    {{--            {!!  Share::page(Request::url(), $startup['name'].' - Startup Uganda')--}}
                    {{--                ->facebook()--}}
                    {{--                ->twitter()--}}
                    {{--                ->linkedin('Extra linkedin summary can be passed here')--}}
                    {{--                ->whatsapp() !!}--}}


                </div>
                <div class="col-md-5 pull-right">
                    <div class="row centered pr-4">

                        @if ($startup['fundraising'])

                            <a href="#funding"  data-toggle="tab"   style="width: 40%" class="btn  btn-orange pull-right fs-13 text-white mr-3 mt-3 mb-4 ml-3" > WE ARE RAISING</a>


                        @endif

                        @if ($startup->jobs->count() >0)
                            <a href="#jobs" data-toggle="tab" style="width: 40%"  class="btn border-orange text-orange mr-3 mb-3 mb-4 ml-3"> APPLICATION CALL</a>

                        @endif


                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row mt-0">
                <div class="row" style="width: 100%">
                    <div class="col-xl-3 col-lg-3 col-md-12">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <p class="mb-1 text-gray3 font-weight-semibold">{{$startup['stage']}}</p>
                                </div>
                                <span class="mb-0 fs-13 text-gray2">Stage</span>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <p class="mb-1 text-gray3 font-weight-semibold">{{$startup['phone_number']}}</p>
                                </div>
                                <span class="mb-0 fs-13 text-gray2">Phone Number</span>
                            </div>
                        </div>
                    </div>
                    <div class="`col-xl-3 col-lg-3 col-md-12`">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <p class="mb-1 text-gray3 font-weight-semibold">{{  $startup['district']}}</p>
                                </div>
                                <span class="mb-0 fs-13 text-gray2">District</span>
                            </div>
                        </div>
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <p class="mb-1 text-gray3 font-weight-semibold">{{ $startup['rev_generating']==true?'Yes':'No'}}</p>
                                </div>
                                <span class="mb-0 fs-13 text-gray2">Revenue Generating</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-12">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <p class="mb-1 text-gray3 font-weight-semibold">
                                        {{ strlen($startup['consumer_model']) > 15 ? substr($startup['consumer_model'],0,15)."..." : $startup['consumer_model']}}
                                    </p>
                                </div>
                                <span class="mb-0 fs-13 text-gray2">Customer Model</span>
                            </div>
                        </div>
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <p class="mb-1 text-gray3 font-weight-semibold">{{ $startup['founded_on']}}</p>
                                </div>
                                <span class="mb-0 fs-13 text-gray2">Founded On</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-12">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <p class="mb-1 text-gray3 font-weight-semibold">
                                        {{$startup['address']}}
                                    </p>
                                </div>
                                <span class="mb-0 fs-13 text-gray2">Address</span>
                            </div>
                        </div>
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <p class="mb-1 text-gray3 font-weight-semibold">
                                        @if(!is_null($startup['website']))
                                            <a href="{{$startup['website']}}" target="_blank">{{$startup['website']}}</a>
                                        @else
                                            {{$startup['website']}}
                                        @endif
                                    </p>
                                </div>
                                <span class="mb-0 fs-13 text-gray2">Website</span>
                            </div>
                        </div>
                    </div>


                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="media mt-2">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center">
                                    <p class="mb-1 text-gray3 font-weight-semibold">
                                        {{ strlen($startup['industries']) > 130 ? substr($startup['industries'],0,130)."..." : $startup['industries']}}
                                    </p>
                                </div>
                                <span class="mb-0 fs-13 text-dark-gray">Industries</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    {{-- <div class="row">
        <div class="col-xs-9 col-md-7 " style="background: url({{secure_asset(env('BANNER').''.$startup['banner'])}}) no-repeat;height:360px">
        </div>
        <div class="col-xs-3 col-md-5 pl-6 pt-3" style="background-color: #FFF">
            <div class="container">
                <div class="row mt-0">
                    <div class="media" style="">
                        <div class=" mr-3">
                            <img src="{{secure_asset('public/uploads/logo/'.$startup['logo'])}}" alt="img" class="avatar-xxl mr-2 brround p-1 shadow2">
                        </div>
                        <div class="media-body pr-4">
                            <div class="fs-20 text-bolder text-gray3 font-weight-normal1" style="font-weight:bolder !important"> {{$startup['name']}} </div>
                            <small class="fs-14 text-gray2">{{$startup['tagline']}}</small><br/>
                            <span style="width: 100%" class="mt-3">
                                @if(Auth::check() && App\Traits\HelperTrait::showChatButton(Auth::user()->id,$startup->id))
                                    <a href="{{route('initiate.chat',['sender'=>Auth::user()->id,'receiver'=>$startup->id,'type'=>'startup'])}}"><i class="fa fa-comments text-orange"></i> Initiate Chat</a>
                                @endif
                            </span>

                        </div>
                    </div>
                    <hr/>
                    <div class="row" style="width: 100%">
                        <div class="col-xl-4 col-lg-4 col-md-12">
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex align-items-center mt-1">
                                        <p class="mb-1 text-gray3 font-weight-semibold">{{$startup['stage']}}</p>
                                    </div>
                                    <span class="mb-0 fs-13 text-gray2">Stage</span>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-body">
                                    <div class="d-md-flex align-items-center mt-1">
                                        <p class="mb-1 text-gray3 font-weight-semibold">{{$startup['phone_number']}}</p>
                                    </div>
                                    <span class="mb-0 fs-13 text-gray2">Phone Number</span>
                                </div>
                            </div>
                        </div>
                        <div class="`col-xl-4 col-lg-4 col-md-12`">
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex align-items-center mt-1">
                                        <p class="mb-1 text-gray3 font-weight-semibold">{{  $startup['district']}}</p>
                                    </div>
                                    <span class="mb-0 fs-13 text-gray2">District</span>
                                </div>
                            </div>
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex align-items-center mt-1">
                                        <p class="mb-1 text-gray3 font-weight-semibold">{{ $startup['rev_generating']==true?'Yes':'No'}}</p>
                                    </div>
                                    <span class="mb-0 fs-13 text-gray2">Revenue Generating</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-12">
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex align-items-center mt-1">
                                        <p class="mb-1 text-gray3 font-weight-semibold">
                                            {{ strlen($startup['consumer_model']) > 15 ? substr($startup['consumer_model'],0,15)."..." : $startup['consumer_model']}}
                                        </p>
                                    </div>
                                    <span class="mb-0 fs-13 text-gray2">Customer Model</span>
                                </div>
                            </div>
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex align-items-center mt-1">
                                        <p class="mb-1 text-gray3 font-weight-semibold">{{ $startup['founded_on']}}</p>
                                    </div>
                                    <span class="mb-0 fs-13 text-gray2">Founded On</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12">
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex align-items-center mt-1">
                                        <p class="mb-1 text-gray3 font-weight-semibold">
                                            {{$startup['address']}}
                                        </p>
                                    </div>
                                    <span class="mb-0 fs-13 text-gray2">Address</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12">
                            <div class="media mb-1">
                                <div class="media-body">
                                    <div class="d-md-flex align-items-center mt-1">
                                        <p class="mb-1 text-gray3 font-weight-semibold">
                                            @if(!is_null($startup['website']))
                                                <a href="{{$startup['website']}}" target="_blank">{{$startup['website']}}</a>
                                            @else
                                                {{$startup['website']}}
                                            @endif
                                        </p>
                                    </div>
                                    <span class="mb-0 fs-13 text-gray2">Website</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12">
                            <div class="media mt-2">
                                <div class="media-body">
                                    <div class="d-md-flex align-items-center">
                                        <p class="mb-1 text-gray3 font-weight-semibold">
                                            {{ strlen($startup['industries']) > 130 ? substr($startup['industries'],0,130)."..." : $startup['industries']}}
                                        </p>
                                    </div>
                                    <span class="mb-0 fs-13 text-gray2">Industries</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div> --}}


    <div class="container mt-8">
        <div class="row">
            <div class="col-md-12">
                <div class="list-single-main-item fl-wrap block_box mb-9">
                    <div class="list-single-main-item-title">
                        <h3 class="text-gray3">Company Information</h3>
                    </div>
                    <div class="list-single-main-item_content fl-wrap">
                        {!!$startup['about']!!}
                    </div>

                </div>

                {{-- TABS --}}
                <div class="d-md-flex">
                    <div class="row">
                        <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12">
                            <div class="tab-menu-heading border-0 br-tr-0">
                                <div class="tabs-menu ">
                                    <!-- Tabs -->
                                    <ul class="nav panel-tabs">
                                        <li class=""><a href="#traction" class="active" data-toggle="tab">OVERVIEW</a></li>
                                        @if ($startup['fundraising'])
                                            <li class=""><a href="#funding"  data-toggle="tab">FUNDING ROUND</a></li>
                                        @endif
                                        <li><a href="#pitchDeck" data-toggle="tab" class=""> PITCHDECK</a></li>
                                        <li><a href="#industries" data-toggle="tab" class=""> INDUSTRIES & SECTORS</a></li>
                                        <li><a href="#team" data-toggle="tab" class=""> TEAM MEMBERS</a></li>
                                        <li><a href="#board" data-toggle="tab" class=""> BOARD MEMBERS</a></li>
                                        <li><a href="#jobs" data-toggle="tab" class=""> JOBS</a></li>
                                        <li><a href="#feeds" data-toggle="tab" class=""> UPDATES</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-6 col-md-6 col-xm-12">
                            <div class="tabs-style-4">
                                <div class="panel-body tabs-menu-body1 border-0">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="traction">
                                            <h2 class="startupTitleHr fs-18 mb-3 text-dark-gray font-weight-bold"><span>Problem</span></h2>
                                            {{-- <div class="text-gray3 "><h5 class="fs-28">Problem</h5></div> --}}

                                            <div class="mb-5 list-single-main-item_content fl-wrap">
                                                {!!$startup['problem']!!}
                                            </div>
                                            <h2 class="startupTitleHr fs-18 mb-3 mt-5 text-dark-gray font-weight-bold"><span>Solution</span></h2>
                                            <div class="mt-5 mb-4 list-single-main-item_content fl-wrap">
                                                {!!$startup['solution']!!}
                                            </div>
                                            <h2 class="startupTitleHr fs-18 mb-3 mt-5 text-dark-gray font-weight-bold"><span>Traction</span></h2>
                                            <div class="mt-5 list-single-main-item_content fl-wrap">
                                                {!!$startup['traction']!!}
                                            </div>


                                        </div>
                                        <div class="tab-pane" id="funding">
                                            <a id="tips"></a>
                                            <h2 class="startupTitleHr fs-18 mb-3 mt-5 text-dark-gray font-weight-bold"><span>FUNDING ROUND</span></h2>

                                            <div class="row mt-7">
                                                <div class="col-xl-8 col-lg-8 col-md-12">
                                                    <div class="row">
                                                        <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12">
                                                            <div class="media">
                                                                <div class="media-body">
                                                                    <div class="d-md-flex align-items-center mt-1">
                                                                        <p class="mb-1 fs-15 text-gray3 font-weight-semibold">{{ date('M Y')}}</p>
                                                                    </div>
                                                                    <span class="mb-0 fs-13 text-gray2">Date</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12">
                                                            <div class="media">
                                                                <div class="media-body">
                                                                    <div class="d-md-flex align-items-center mt-1">
                                                                        <p class="mb-1 fs-15 text-gray3 font-weight-semibold">{{ $startup['round']}}</p>
                                                                    </div>
                                                                    <span class="mb-0 fs-13 text-gray2">Round</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12">
                                                            <div class="media">
                                                                <div class="media-body">
                                                                    <div class="d-md-flex align-items-center mt-1">
                                                                        <p class="mb-1 fs-15 text-gray3 font-weight-semibold">${{ number_format($startup['raise_amount'])}}</p>
                                                                    </div>
                                                                    <span class="mb-0 fs-13 text-gray2">Amount to Raise</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-xl-4 col-lg-4 col-md-12">
                                                    <a href="" data-target="#get_in_touch" data-toggle="modal"><button style="margin-top: 20px;" class="btn btn-sm border-warning pull-right text-warning p-3 text-bold fs-12"><i class="fa fa-envelope-o text-orange"></i> GET IN TOUCH</button></a>
                                                </div>
                                                <div class="col-xl-12 col-lg-12 col-md-12 mt-4">
                                                    <div class="progress progress-md mb-3"> <div class="progress-bar bg-green" style="width: {{App\Traits\HelperTrait::calculateFundingPercentage(App\Traits\HelperTrait::getFundings('startup',$startup['id'],'sum'),$startup['raise_amount']) }}%"></div> </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="media" style="margin-top: 5px">
                                                        <div class="media-body">
                                                            <div class="d-md-flex align-items-center mt-1">
                                                                <p class="mb-1 text-gray3  fs-20 font-weight-semibold">${{ number_format(App\Traits\HelperTrait::getFundings('startup',$startup['id'],'sum')) }}</p>
                                                            </div>
                                                            <span class="mb-0 fs-13 text-gray2">Amount Raised</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6 pull-right">
                                                    <div class="media pull-right" style="margin-top: 5px">
                                                        <div class="media-body">
                                                            <div class="">
                                                                <p class="mb-1 text-gray3 fs-20 pull-right font-weight-semibold">${{ number_format($startup['raise_amount'])}}</p>
                                                            </div>
                                                            <span class="mb-0 fs-13 text-right text-gray2 ">Target Raise</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-12 col-lg-12 col-md-12 mt-9">
                                                    <h2 class="startupTitleHr fs-18 mb-3 mt-5 text-dark-gray font-weight-bold"><span>INVESTORS</span></h2>

                                                    <div class="row">
                                                        <?php $funds= App\Traits\HelperTrait::getFundings('startup',$startup['id'],'list'); ?>
                                                        @foreach ($funds as $fund)
                                                            <div class="col-xl-4 col-lg-4 col-md-12">
                                                                <div class="media">
                                                                    {{--                                                                    <div class="mr-3">--}}
                                                                    {{--                                                                        <img src="{{$fund['company_logo']}}" alt="img" class="avatar avatar-md mr-2 brround">--}}
                                                                    {{--                                                                    </div>--}}
                                                                    <div class="media-body">
                                                                        <div class="font-weight-normal1"><b>{{$fund['company_name']}}</b> </div>
                                                                        <small class="text-gray2">{{$fund['company_type']}}</small>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="tab-pane" id="pitchDeck">
                                            <h2 class="startupTitleHr fs-18 mb-3 mt-5 text-dark-gray font-weight-bold"><span>PITCH DECK</span></h2>

                                            @if (is_null($startup['pitchDeck']))
                                                <p>No uploaded Pitch deck for this startup</p>
                                            @else
                                                <div class="row">
                                                    <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12">
                                                        <img src="{{secure_asset('public/images/Pichdeck-image.png')}}" alt="">
                                                    </div>
                                                    <div class="col-xl-9 col-lg-6 col-md-12">
                                                        <h4>Document</h4>
                                                        <a href="{{secure_asset('public/uploads/pitch/'.$startup['pitchDeck'])}}" target="_blank">
                                                            <p>View Pitchdeck to {{$startup['name']}}</p>
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="tab-pane" id="industries">
                                            <h2 class="startupTitleHr fs-18 mb-3 mt-5 text-dark-gray font-weight-bold"><span>Industries</span></h2>

                                            <div class="list-single-main-item_content fl-wrap">
                                                <?php $industries = explode(',',$startup['industries']); array_pop($industries);?>
                                                @foreach ($industries as $ind)
                                                    <a class="btn btn-sm btn-light mt-1" href="#">{{$ind}}</a>
                                                @endforeach
                                            </div>
                                            <h2 class="startupTitleHr fs-18 mb-3 mt-5 text-dark-gray font-weight-bold"><span>Sectors</span></h2>

                                            <div class="list-single-main-item_content fl-wrap">
                                                <?php $sectors = explode(',',$startup['sectors']); array_pop($sectors); ?>
                                                @foreach ($sectors as $sec)
                                                    <a class="btn btn-sm btn-light mt-1" href="#">{{$sec}}</a>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="jobs">
                                            <h2 class="startupTitleHr fs-18 mb-3 mt-5 text-dark-gray font-weight-bold"><span>JOB OPENINGS</span></h2>

                                            <div class="row pt-4">
                                                @foreach ($startup->jobs as $job)
                                                    @include('includes.partial.job-list')
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="team">
                                            <h2 class="startupTitleHr fs-18 mb-3 mt-5 text-dark-gray font-weight-bold"><span>The Team</span></h2>

                                            <div class="row mt-7">
                                                @if(isset($teams))
                                                @foreach ($teams as $team)
                                                    <div class="col-md-4 pt-3 mb-5">
                                                        <div class="media mt-0 mb-3">
                                                            <figure class="rounded-circle align-self-start mb-0">
                                                                <img src="{{secure_asset('public/uploads/user/'.$team['photo'])}}" alt="Generic placeholder image" style="width: 80px !important; height: 80px !important;" class="avatar brround avatar-lg mr-3">
                                                            </figure>
                                                            <div class="media-body">
                                                                <h6 class="time-title pt-3  p-0 fs-16 mb-0 text-gray3 font-weight-semibold leading-normal">{{$team->member_name}}</h6>
                                                                <p>
                                                                    @if(isset($team['designation']) && !is_null($team['designation']))
                                                                    {{$team['designation']}}
                                                                        @endif
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                                    @endif
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="board">
                                            <h2 class="startupTitleHr fs-18 mb-3 mt-5 text-dark-gray font-weight-bold"><span>Board Members & Advisors</span></h2>

                                            <div class="row mt-7">
                                                @foreach ($boards as $board)
                                                    <div class="col-md-4 pt-3 mb-5" >
                                                        <div class="media mt-0 mb-3">
                                                            <figure class="rounded-circle align-self-start mb-0">
                                                                <img src="{{secure_asset('public/uploads/user/'.$board['photo'])}}"  alt="Generic placeholder image" style="width: 80px !important; height: 80px !important;"  class="avatar brround avatar-lg mr-3">
                                                            </figure>
                                                            <div class="media-body">
                                                                <h6 class="time-title pt-3 p-0 mb-0 fs-16 text-gray3 font-weight-semibold leading-normal">{{$board->member_name}}</h6>
                                                                <p>
                                                                    @if(isset($team['designation']) && !is_null($team['designation']))
                                                                        {{$team['designation']}}
                                                                    @endif
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="feeds">
                                            <h2 class="startupTitleHr fs-18 mb-3 mt-5 text-dark-gray font-weight-bold"><span>Updates</span></h2>

                                            <div class="row">
                                                <?php $feeds = App\Traits\HelperTrait::getFeeds('startup',$startup['id'],4)?>
                                                @foreach ($feeds as $feed)
                                                    <div class="row mb-4">
                                                        <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12" style="">
                                                            @if (is_null($feed['image']))
                                                                <img src="{{secure_asset('public/uploads/logo/business-logo.png')}}" class=""  alt="">
                                                            @else
                                                                <img src="{{$feed['image']}}" class=""  alt="Thought">
                                                            @endif
                                                        </div>
                                                        <div class="col-xl-9 col-lg-6 col-md-12">
                                                            <div class="row card-header">
                                                                <div class="col-xl-9 col-lg-6 col-md-12">
                                                                    <div class="card-header" style="border-bottom: 0px !important">
                                                                        <span><img src="{{secure_asset('public/uploads/logo/'.$feed['company']['logo'])}}" alt="img" class="avatar avatar-lg mr-2 brround"></span>
                                                                        <h6 class="text-gray-300 pt-2">
                                                                            {{$feed['company']['name']}} <br/>
                                                                            <small class="text-gray-100">{{$feed['company']['address']}}</small>
                                                                        </h6>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-3 col-lg-6 col-md-6 col-xm-12">
                                                                    <span class=" ml-6 mt-3">{{ Carbon\Carbon::parse($feed->created_at)->diffForHumans()}}</span>
                                                                </div>
                                                            </div>

                                                            <h6 class="pt-3" style="color: #0E59DE">
                                                                <a href="{{$feed['url']}}" target="_blank" >{{$feed['title']??" Visit Article"}}</a>
                                                            </h6>
                                                            <h6 class="text-gray fs-13">THOUGHTS</h6>
                                                            <p class="fs-12">{{$feed['thought']}}</p>
                                                        </div>
                                                    </div>

                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="border border-right-0 br-tl-7 br-bl-7">
                        <div class="panel panel-primary tabs-style-4">

                        </div>
                    </div>

                </div>
                {{-- END TABS --}}

            </div>
        </div>

    </div>

    {{-- GET IN TOUCH MODAL --}}
    <div class="modal" id="get_in_touch">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title"> Get in touch with {{$startup['name']}}</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
                </div>
                <form action="{{route('user.contact.startup')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="startupId" value="{{$startup['id']}}">

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="form-label">Name</label>
                                    <input type="text" class="form-control" id="contact_name" name="name" required placeholder="Enter your Name">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="form-label">Email Address</label>
                                    <input type="email" class="form-control" id="contact_email" name="email" required placeholder="Enter Email Address ">
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Message</label>
                                    <textarea class="form-control" required placeholder="Type your message here" name="message" rows="6"></textarea>
                                </div>

                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-orange" type="submit">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- END GET IN TOUCH MODAL --}}

    <script>
        // Select all links with hashes
        $('a[href*="#"]')
            // Remove links that don't actually link to anything
            .not('[href="#"]')
            .not('[href="#0"]')
            .click(function(event) {
                // On-page links
                if (
                    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                    &&
                    location.hostname == this.hostname
                ) {
                    // Figure out element to scroll to
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000, function() {
                            // Callback after animation
                            // Must change focus!
                            var $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                                $target.focus(); // Set focus again
                            };
                        });
                    }
                }
            });
    </script>
@endsection
