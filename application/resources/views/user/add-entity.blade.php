@extends('layouts.user')
@section('user-content')
<div class="row">
    <div class="col-md-12">

        <div class="ab_text">
            <div class="ab_text-title fl-wrap">
                <h3>Add Listing</h3>
                <span class="section-separator fl-sec-sep"></span>
            </div>
            <p>Creating a venture is simple and only takes 5 minutes. Provide  basic information about your company and enrich the profile with additional infomation after publishing the page.
                Take a moment to review our guidelines to be sure your venture is a good fit.
            </p>
        </div>
        <!-- dashboard-list-box-->
    <form class="custom-form  " action="{{route('user.add.startup')}}" method="POST" name="registerStartup" id="registerStartup" style="margin-top: 30px;">
            <fieldset>
                @csrf
                <input type="hidden" name="user_id" value="1">
                <div class="row m-t-5">
                    <div class="col-md-6">
                        <label>Startup Name</label>
                        <input type="text" name="name" id="name" placeholder="Startup name *" required value="">
                    </div>
                    <div class="col-md-6">
                        <label>Is your Business legally registered</label>
                        <select class="nice-select" required name="registration">
                            <option value="">Select status</option>
                            <option value="true">Yes</option>
                            <option value="false">No</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Contact Phone Number</label>
                        <input type="text" name="phone_number" required id="phone_number" placeholder="Phone number *" required value="">
                    </div>
                    <div class="col-md-6">
                        <label>Contact Email</label>
                        <input type="email" name="email" required id="name" placeholder="Your Name *" value="">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label>Tagline</label>
                        <input type="text" name="tagline" required id="tagline" placeholder="Company Tagline *" required value="">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label>Address</label>
                        <input type="text" name="address" required id="address" placeholder="Company Tagline *" required value="">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <label>Venture consumer model</label>
                        <select class="nice-select" required name="consumer_model" >
                            <option value="">Select models</option>
                            <option value="B2B">B2B</option>
                            <option value="B2B2B">B2B2B</option>
                            <option value="B2B2C">B2B2C</option>
                            <option value="B2B2G">B2B2G</option>
                            <option value="B2C">B2C</option>
                            <option value="C2C">C2C</option>
                            <option value="Government(B2C)">Government(B2C)</option>
                            <option value="Non-Profit">Non-Profit</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label>Venture stage</label>
                        <select class="nice-select" required name="stage">
                            <option value="">Select stage</option>
                            <option value="Ideation">Ideation</option>
                            <option value="Validation">Validation</option>
                            <option value="Early Traction">Early Traction</option>
                            <option value="Scaling">Scaling</option>
                        </select>
                    </div>
                </div>
                <div class="row" style="margin:17px 0">
                    <h3 class="pull-left">Company Information</h3>
                    <hr/>
                </div>
                <div class="row">

                    <div class="col-md-12">
                        <label>Problem</label>
                        <textarea name="about" id="about" cols="10" style="height: 100px !important" rows="2" placeholder="Tell us about your venture:"></textarea>
                    </div>
                    <div class="col-md-12">
                        <label>Problem</label>
                        <textarea name="problem" id="problem" cols="10" style="height: 100px !important" rows="2" placeholder="What problem does your venture solve?:"></textarea>
                    </div>
                    <div class="col-md-12">
                        <label>Solution</label>
                        <textarea name="solution" id="solution" cols="10" style="height: 100px !important" rows="2" placeholder="What problem does your venture solve?:"></textarea>
                    </div>
                    <div class="col-md-12">
                        <label>Traction</label>
                        <textarea name="traction" id="traction" cols="10" style="height: 100px !important" rows="2" placeholder="What problem does your venture solve?:"></textarea>
                    </div>

                </div>





                <div class="row" style="margin:17px 0">
                    <h3 class="pull-left">Social Media  Links</h3>
                    <hr/>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Facebook link</label>
                        <input type="text" name="facebook" required id="facebook" placeholder="Venture Facebook page link (optional)">
                    </div>
                    <div class="col-md-6">
                        <label>Twitter link</label>
                        <input type="text" name="twitter" required id="twitter" placeholder="Venture twitter link (optional)" >
                    </div>
                    <div class="col-md-6">
                        <label>LinkedIn link</label>
                        <input type="text" name="linkedIn" required id="linkedIn" placeholder="LinkedIn page" >
                    </div>
                    <div class="col-md-6">
                        <label>Youtube link</label>
                        <input type="text" name="youtube" required id="youtube" placeholder="Youtube channel link" value="">
                    </div>
                </div>
            </fieldset>
            <button class="btn float-btn color2-bg" id="submit">Send Message<i class="fal fa-paper-plane"></i></button>
        </form>



    </div>

</div>


@endsection
