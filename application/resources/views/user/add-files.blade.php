@extends('build.user')
@section('profile-summary')
    <div class="row">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                <h4 class="page-title mb-0">Add Files</h4>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                <li class="breadcrumb-item"><a href="#">{{$category}}</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="#">Add Files</a></li>
            </ol>
            </div>
            <div class="page-rightheader">
                <div class="btn btn-list">
                @include('user.partial.new')
            </div>
        </div>
        </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('user.partial.menu')
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="card">
                    <div class="card-header">
                    <h3 class="card-title">Add files to {{$entity['name']}}</h3>
                     </div>
                     <div class="card-body">
                         <form action="" method="POST">
                             <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputName" class="form-label">Startup Name</label>
                                        <input type="text" class="form-control" name="name" id="exampleInputEmail1" value="{{$entity['name']}}" readonly placeholder="Startup name *">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName" class="form-label">Upload Pitch Deck</label>
                                        <div class="input-group file-browser mb-5">
                                            <input type="text" class="form-control border-right-0 browse-file" placeholder="choose" name="pitchDeck" readonly="">
                                            <label class="input-group-btn"> <span class="btn btn-primary"> Browse <input type="file" style="display: none;" multiple=""> </span>
                                            </label>
                                        </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputName" class="form-label">Upload Logo</label>
                                        <div class="input-group file-browser mb-5">
                                            <input type="text" class="form-control border-right-0 browse-file" placeholder="choose" name="logo" readonly="">
                                            <label class="input-group-btn"> <span class="btn btn-primary"> Browse <input type="file" style="display: none;" multiple=""> </span>
                                            </label>
                                        </div>
                                    </div>
                                 </div>
                             </div>
                         </form>

                     </div>

                </div>
            </div>
        </div>
    </div>

@endsection
