@extends('build.master')
@section('custom-css')
<link type="text/css" rel="stylesheet" href="{{secure_asset('theme/css/dashboard-style.css')}}">
@endsection
@section('content')
     <!-- wrapper-->
     <div id="wrapper">
        <!-- content-->
        <div class="content">
            @include('user.partial.profile')

            <section class="gray-bg main-dashboard-sec" id="sec1">
                <div class="container">
                    @include('user.partial.menu')
                    <!-- dashboard content-->
                    <div class="col-md-9">
                        <div class="dashboard-title fl-wrap">
                            <h3>Your Statistics</h3>
                        </div>
                        <!-- list-single-facts -->
                        <div class="list-single-facts fl-wrap">
                            <div class="row">
                                <div class="col-md-4">
                                    <!-- inline-facts -->
                                    <div class="inline-facts-wrap gradient-bg " style="height: 112px;">
                                        <div class="inline-facts">
                                            <i class="fal fa-chart-bar"></i>
                                            <div class="milestone-counter">
                                                <div class="stats animaper">
                                                    <div class="num" data-content="0" data-num="1054">1054</div>
                                                </div>
                                            </div>
                                            <h6>Listing Views</h6>
                                        </div>
                                        <div class="stat-wave">
                                            <svg viewBox="0 0 100 25">
                                                <path fill="#fff" d="M0 30 V12 Q30 17 55 2 T100 11 V30z"></path>
                                            </svg>
                                        </div>
                                    </div>
                                    <!-- inline-facts end -->
                                </div>
                                <div class="col-md-4">
                                    <!-- inline-facts  -->
                                    <div class="inline-facts-wrap gradient-bg " style="height: 112px;">
                                        <div class="inline-facts">
                                            <i class="fal fa-comments-alt"></i>
                                            <div class="milestone-counter">
                                                <div class="stats animaper">
                                                    <div class="num" data-content="0" data-num="2557">2557</div>
                                                </div>
                                            </div>
                                            <h6>Total Reviews</h6>
                                        </div>
                                        <div class="stat-wave">
                                            <svg viewBox="0 0 100 25">
                                                <path fill="#fff" d="M0 30 V12 Q30 17 55 12 T100 11 V30z"></path>
                                            </svg>
                                        </div>
                                    </div>
                                    <!-- inline-facts end -->
                                </div>
                                <div class="col-md-4">
                                    <!-- inline-facts  -->
                                    <div class="inline-facts-wrap gradient-bg " style="height: 112px;">
                                        <div class="inline-facts">
                                            <i class="fal fa-envelope-open-dollar"></i>
                                            <div class="milestone-counter">
                                                <div class="stats animaper">
                                                    <div class="num" data-content="0" data-num="125">125</div>
                                                </div>
                                            </div>
                                            <h6>Bookings </h6>
                                        </div>
                                        <div class="stat-wave">
                                            <svg viewBox="0 0 100 25">
                                                <path fill="#fff" d="M0 30 V12 Q30 12 55 5 T100 11 V30z"></path>
                                            </svg>
                                        </div>
                                    </div>
                                    <!-- inline-facts end -->
                                </div>
                            </div>
                        </div>
                        <!-- list-single-facts end -->
                        <div class="list-single-main-item fl-wrap block_box">
                            <!-- chart-wra-->
                            <div class="chart-wrap   fl-wrap"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                                <div class="chart-header fl-wrap">
                                    <div class="listsearch-input-item">
                                        <select data-placeholder="Week" class="chosen-select no-search-select" style="display: none;">
                                            <option>Week</option>
                                            <option>Month</option>
                                            <option>Year</option>
                                        </select><div class="nice-select chosen-select no-search-select" tabindex="0"><span class="current">Week</span><div class="nice-select-search-box"><input type="text" class="nice-select-search" placeholder="Search..."></div><ul class="list"><li data-value="Week" class="option selected">Week</li><li data-value="Month" class="option">Month</li><li data-value="Year" class="option">Year</li></ul></div>
                                    </div>
                                    <div id="myChartLegend"><ul class="0-legend"><li><span style="background-color:rgba(94, 207, 177, 0.2)"></span>Listings Views</li><li><span style="background-color:rgba(77, 183, 254, 0.2)"></span>Bookings</li></ul></div>
                                </div>
                                <canvas id="canvas-chart" style="display: block; height: 413px; width: 826px;" width="1652" height="826" class="chartjs-render-monitor"></canvas>
                            </div>
                            <!--chart-wrap end-->
                        </div>
                        <div class="dashboard-title dt-inbox fl-wrap">
                            <h3>Recent Activities</h3>
                        </div>
                        <!-- dashboard-list-box-->
                        <div class="dashboard-list-box  fl-wrap">
                            <!-- dashboard-list end-->
                            <div class="dashboard-list fl-wrap">
                                <div class="dashboard-message">
                                    <span class="new-dashboard-item"><i class="fal fa-times"></i></span>
                                    <div class="dashboard-message-text">
                                        <i class="far fa-check green-bg"></i>
                                        <p>Your listing <a href="#">Park Central</a> has been approved! </p>
                                    </div>
                                    <div class="dashboard-message-time"><i class="fal fa-calendar-week"></i> 28 may 2020</div>
                                </div>
                            </div>
                            <!-- dashboard-list end-->
                            <!-- dashboard-list end-->
                            <div class="dashboard-list fl-wrap">
                                <div class="dashboard-message">
                                    <span class="new-dashboard-item"><i class="fal fa-times"></i></span>
                                    <div class="dashboard-message-text">
                                        <i class="far fa-heart purp-bg"></i>
                                        <p>Someone bookmarked your <a href="#">Holiday Home</a> listing!</p>
                                    </div>
                                    <div class="dashboard-message-time"><i class="fal fa-calendar-week"></i> 28 may 2020</div>
                                </div>
                            </div>
                            <!-- dashboard-list end-->
                            <!-- dashboard-list end-->
                            <div class="dashboard-list fl-wrap">
                                <div class="dashboard-message">
                                    <span class="new-dashboard-item"><i class="fal fa-times"></i></span>
                                    <div class="dashboard-message-text">
                                        <i class="fal fa-comments-alt red-bg"></i>
                                        <p> Someone left a review on <a href="#">Park Central</a> listing!</p>
                                    </div>
                                    <div class="dashboard-message-time"><i class="fal fa-calendar-week"></i> 28 may 2020</div>
                                </div>
                            </div>
                            <!-- dashboard-list end-->
                            <!-- dashboard-list end-->
                            <div class="dashboard-list fl-wrap">
                                <div class="dashboard-message">
                                    <span class="new-dashboard-item"><i class="fal fa-times"></i></span>
                                    <div class="dashboard-message-text">
                                        <i class="far fa-check green-bg"></i>
                                        <p> Your listing <a href="#">Holiday Home</a> has been approved! </p>
                                    </div>
                                    <div class="dashboard-message-time"><i class="fal fa-calendar-week"></i> 28 may 2020</div>
                                </div>
                            </div>
                            <!-- dashboard-list end-->
                            <!-- dashboard-list end-->
                            <div class="dashboard-list fl-wrap">
                                <div class="dashboard-message">
                                    <span class="new-dashboard-item"><i class="fal fa-times"></i></span>
                                    <div class="dashboard-message-text">
                                        <i class="far fa-heart purp-bg"></i>
                                        <p>Someone bookmarked your <a href="#">Moonlight Hotel</a> listing!</p>
                                    </div>
                                    <div class="dashboard-message-time"><i class="fal fa-calendar-week"></i> 28 may 2020</div>
                                </div>
                            </div>
                            <!-- dashboard-list end-->
                        </div>
                        <!-- dashboard-list-box end-->
                    </div>
                    <!-- dashboard content end-->
                </div>
            </section>

        </div>
        <!--content end-->
    </div>
    <!-- wrapper end-->

@endsection
