@extends('build.user')
@section('profile-summary')
    <div class="row pt-6 pr-4 pl-4">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title mb-0">MESSAGES</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                        <li class="breadcrumb-item active"><a href="">Chats</a></li>
                    </ol>
                </div>
                <div class="page-rightheader">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('user-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('user.partial.menu')
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tile tile-alt mb-0" id="messages-main">
                            <div class="ms-menu" style="height: 100%;overflow-y: auto;overflow-x: hidden ">
                                <div class="tab-menu-heading border-top-0">
                                    <h4>CHAT MESSAGES</h4>
                                </div>
                                <ul class="list-group lg-alt chat-contact-list ps ps--active-y" id="ChatList">
                                    <?php $i=0;?>
                                    @foreach ($conversations as $conv)
                                        <?php $i+=1; ?>

                                        <a href="#chat-tab_{{$i}}" @if($i==1) class="active" @endif data-toggle="tab">

                                            <li class="list-group-item media p-4 mt-0 border-0">
                                                <div class="float-left pr-2">
                                                    <img src="{{secure_asset('public/uploads/'.App\Traits\HelperTrait::getModelProfilePhoto(substr($conv->conversation->participants[1]->messageable_type,4),$conv->conversation->participants[1]->messageable->id))}}" alt="" class="avatar avatar-md brround">
                                                </div>
                                                <div class="media-body">
                                                    @if(isset($model))
                                                        <div class="list-group-item-heading text-default font-weight-semibold">{{$conv->conversation->participants[0]->messageable->name}}</div>
                                                        <small class="list-group-item-text text-muted">
                                                            {{substr($conv->conversation->participants[0]->messageable_type,4)}}
                                                            <?php $unreadCount =  App\Traits\HelperTrait::getConversationWithTwoUsers($category.'_'.$id,$conv->conversation_id); ?>
                                                            {{ $unreadCount.' Unread Messages'}}
                                                        </small>
                                                    @else
                                                        <div class="list-group-item-heading text-default font-weight-semibold">{{$conv->conversation->participants[1]->messageable->name}}</div>
                                                        <small class="list-group-item-text text-muted">
                                                            {{substr($conv->conversation->participants[1]->messageable_type,4)}}
                                                            <?php $unreadCount = App\Traits\HelperTrait::countUnreadConversationMessages('user_'.Auth::user()->id,$conv->conversation_id); ?>
                                                            @if ($unreadCount >0)
                                                                <span class="ml-5 text-bold text-green">{{ 'No Unread Messages'}}</span>
                                                            @else
                                                                <span class="ml-5">{{ $unreadCount.' Unread Messages'}}</span>
                                                            @endif

                                                        </small>
                                                    @endif

                                                </div>
                                            </li>
                                        </a>

                                    @endforeach
                                </ul>
                            </div>
                            <div class="ms-body">

                                <div class="chat-body-style ps ps--active-y" id="ChatBody">
                                    <div class="tab-content">
                                        <?php $j=0;?>
                                        @foreach ($conversations as $message)


                                            <div id="chat-tab_{{$j+=1}}" @if($j==1) class="tab-pane active" @else class="tab-pane" @endif  style="height:450px;overflow-y: scroll;" >
                                                {{-- Participants --}}
                                                <div class="action-header clearfix">
                                                    <div class="float-left hidden-xs d-flex ml-4 chat-user">

                                                        @if(isset($model))
                                                            <img src="{{secure_asset('public/uploads/'.App\Traits\HelperTrait::getModelProfilePhoto($category,$id))}}" alt="" class="avatar avatar-lg brround mr-2">
                                                            <div class="align-items-center mt-1">
                                                                <p class="text-orange fs-12">You are chatting as:  <br/>
                                                                    <span class="font-weight-bold text-black-50 mb-0 fs-16">{{$model->name}} ({{substr($conv->conversation->participants[0]->messageable_type,4)}})</span>
                                                                </p>
                                                            </div>
                                                        @else
                                                            <img src="{{secure_asset('public/uploads/'.Auth::user()->image)}}" alt="" class="avatar avatar-lg brround mr-2">
                                                            <div class="align-items-center mt-1">
                                                                <p class="text-orange fs-12">You are chatting as:  <br/>
                                                                    <span class="font-weight-bold text-black-50 mb-0 fs-16">{{Auth::user()->name}} ({{substr($conv->conversation->participants[0]->messageable_type,4)}})</span>
                                                                </p>


                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>

                                                @if(isset($category))
                                                    <?php $chat_messages = App\Traits\HelperTrait::getConversationWithTwoUsers($category.'_'.$id,$message->conversation_id); ?>
                                                @else
                                                    <?php $chat_messages = App\Traits\HelperTrait::getConversationWithTwoUsers('user_'.Auth::user()->id,$message->conversation_id); ?>
                                                @endif
                                                @foreach($chat_messages as $chat)
                                                    @if($chat->sender->name == $chat->posted )
                                                        <div class="message-feed right" style="clear: both">
                                                            {{-- <div class="float-right pl-2">
                                                                <img src="../../assets/images/users/10.jpg" alt="" class="avatar avatar-md brround">
                                                            </div> --}}
                                                            <div class="media-body" style="float: right;">
                                                                <div class="mf-content">
                                                                    {{$chat->body}}
                                                                </div>
                                                                <small class="mf-date"><i class="fa fa-clock-o"></i> {{$chat->created_at}}</small>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="message-feed media mt-0">
                                                            {{-- <div class="float-left pr-2">
                                                                @if(isset($chat->sender->role) && $chat->sender->role=='user')
                                                                <img src="{{secure_asset('public/uploads/'.$chat->sender->image)}}" alt="{{$chat->sender->name}}" class="avatar avatar-md brround">
                                                                @else
                                                                @if(isset($category))
                                                                <img src="{{secure_asset('public/uploads/'.App\Traits\HelperTrait::getModelProfilePhoto($category,$id))}}" alt="{{$chat->sender->name}}" class="avatar avatar-md brround">
                                                                @endif
                                                                @endif
                                                            </div> --}}
                                                            <div class="media-body">
                                                                <div class="mf-content"> {{$chat->body}} </div>
                                                                <small class="mf-date"><i class="fa fa-clock-o"></i> {{$chat->created_at}}</small>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach

                                                <div class="msb-reply" style="    position: absolute;
                                background: #fff;
                                border: 2px solid #eceff9;
                                height: 50px;
                                border-bottom: 0;
                                width: 65%;
                                top: 100%;">
                                                    <form action="{{route('chat.send.message')}}" method="POST">
                                                        @csrf

                                                        <textarea placeholder="What's on your mind..." name="message"></textarea>
                                                        <input type="hidden" name="conversation_id" value="{{$message->conversation_id}}">
                                                        @if(isset($category))
                                                            <input type="hidden" name="category" value="{{$category}}">
                                                            <input type="hidden" name="id" value="{{$id}}">
                                                        @endif
                                                        <button type="submit"><i class="fa fa-paper-plane-o"></i></button>
                                                    </form>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
