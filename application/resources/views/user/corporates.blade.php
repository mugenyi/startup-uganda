@extends('build.user')
@section('profile-summary')

    <div class="row pt-6 pr-4 pl-4">
    <div class="container">
        <div class="page-header">
            <div class="page-leftheader">
                <h4 class="page-title mb-0 d-none d-sm-block">Corporations</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('user.corporates')}}">Corporations</a></li>
                </ol>
            </div>
            <div class="page-rightheader">
                <a href="" class="text-primary" data-target="#create_corporates" data-toggle="modal"><i class="fa fa-plus"></i> Create Corporation</a>
            </div>
        </div>
    </div>
</div>
@endsection
@section('user-content')
<div class="container">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            @include('user.partial.menu')
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="row">
                @foreach ($corporates as $corporate)
                    @include('user.includes.corporate-col-4')
                @endforeach
            </div>
            <div class="row">
                {{$corporates->links()}}
            </div>
        </div>

    </div>

</div>

<div class="modal" id="create_corporates">
    <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title">Create New Corporation</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('user.corporates.create')}}" method="POST" enctype="multipart/form-data">
            @csrf

          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="form-label">Corporation Name <span class="text-red">*</span></label>
                        <input type="text" required class="form-control" placeholder="Corporate Company Name" name="name">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Company Tagline <span class="text-red">*</span></label>
                        <input type="text" required class="form-control" id="date" placeholder=" Company Tagline" name="tagline">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">Address <span class="text-red">*</span></label>
                        <input type="text" required class="form-control" placeholder="Company Address" name="address">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Email <span class="text-red">*</span></label>
                        <input type="text" required class="form-control" placeholder="Company Email" name="email">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Facebook </label>
                        <input type="text" class="form-control" id="facebook" placeholder="Facebook link" name="facebook">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Twitter </label>
                        <input type="text" class="form-control" id="twitter" placeholder="Twitter link" name="twitter">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">Phone Number <span class="text-red">*</span></label>
                        <input type="text" required class="form-control" placeholder="Company Phone Number" name="phone_number">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Website </label>
                        <input type="text" class="form-control" id="date" placeholder="Website" name="website">
                    </div>
                    <div class="form-group">
                        <label class="form-label">LinkedIn </label>
                        <input type="text" class="form-control" id="linkedIn" placeholder="LinkedIn link" name="linkedIn">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Youtube </label>
                        <input type="text" class="form-control" id="twitter" placeholder="Youtube link" name="youtube">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPhone" class="form-label">Describe this Corporation</label>
                        <textarea class="form-control" id="description" name="about" rows="4"></textarea>
                    </div>
                </div>



            </div>
          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Register Corporation</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>

 @endsection
@section('script')
<!-- Initialize Quill editor -->
<script>

    $(document).ready(function() {
        $('#description').summernote({height: 150});
    });
  </script>
@endsection
