@extends('build.user')

@section('user-content')
<div class="container">
    <div class="row mt-7">
        <div class="col-xl-3 col-lg-3 col-md-12">
            @include('user.partial.menu')
        </div>
        <div class="col-md-9 col-lg-9 col-sm-12">

            @include('user.partial.profile')

            <div class="row">
                @include('user.partial.new')
            </div>

            <div class="container mt-4">

                {{-- User startups --}}
                @if ($data['startups']->count() > 0)
                    <h4>YOUR REGISTERED STARTUPS</h4>
                    <div class="row">
                        @foreach ($data['startups'] as $startup)
                            @include('user.includes.startup-col-4')
                        @endforeach
                    </div>
                @else

                @endif
                {{-- User startups --}}
                {{-- User investors --}}
                @if ($data['investors']->count() > 0)
                    <h4>YOUR REGISTERED INVESTORS</h4>
                    <div class="row">
                        @foreach ($data['investors'] as $investor)
                            @include('user.includes.investor-col-4')
                        @endforeach
                    </div>
                @endif
                {{-- User investors --}}
                {{-- User Government --}}
                @if ($data['governments']->count() > 0)
                    <h4>YOUR REGISTERED GOVERNMENT</h4>
                    <div class="row">
                        @foreach ($data['governments'] as $government)
                            @include('user.includes.government-col-4')
                        @endforeach
                    </div>
                @endif
                {{-- User Government --}}
                {{-- User Mentors --}}
                @if ($data['mentors']->count() > 0)
                    <h4>YOUR REGISTERED MENTORS</h4>
                    <div class="row">
                        @foreach ($data['mentors'] as $mentor)
                            @include('user.includes.mentor-col-4')
                        @endforeach
                    </div>
                @endif
                {{-- User Mentors --}}

            </div>

        </div>
    </div>
</div>


@endsection
