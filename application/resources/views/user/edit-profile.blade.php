@extends('build.user')
@section('user-content')
<div class="row pt-6 pr-4 pl-4">
    <div class="container">
        <div class="page-header">
            <div class="page-leftheader">
            <h4 class="page-title mb-0">Edit Profile</h4>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="#">Dashboard</a></li>
        </ol>
        </div>
        <div class="page-rightheader">
            <div class="btn btn-list">
            {{-- <a href="#" class="btn btn-info"><i class="fa fa-cogs mr-1"></i> General Settings </a>
            <a href="#" class="btn btn-warning"><i class="fa fa-plus mr-1"></i> Add Entity </a> --}}
        </div>
    </div>
    </div>
    </div>
</div>
<div class="container">


<div class="row">
    <div class="col-xl-3 col-lg-4">
       <div class="card box-widget widget-user">
          <div class="widget-user-image mx-auto mt-5"><img alt="User Avatar" class="rounded-circle" src="{{secure_asset('public/uploads/'.Auth::user()->image)}}"></div>
          <div class="card-body text-center pt-2">
             <div class="pro-user">
                <h3 class="pro-user-username text-dark mb-1 fs-22">{{Auth::user()->name}}</h3>
                <h6 class="pro-user-desc text-muted">{{Auth::user()->role}}</h6>
                <div class="text-center mb-4">
                    <a href="" class="text-primary" data-target="#upload-Image" data-toggle="modal">Upload Image</a>
                </div>
             <a href="{{route('user.dashboard')}}" class="btn btn-primary mt-3">View Profile</a>
             </div>
          </div>

       </div>
       <div class="card">
          <div class="card-header">
             <div class="card-title">Edit Password</div>
          </div>
           <div class="text-center mb-5">
               <div class="widget-user-image"> <img alt="User Avatar" class="rounded-circle  mr-3" src="{{secure_asset('public/uploads/'.Auth::user()->image)}}"> </div>
           </div>
           <form action="{{route('user.password.update')}}" method="post">
               @csrf
               <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
               <div class="card-body">

                   <div class="form-group"> <label class="form-label">Change Password</label> <input type="password" class="form-control" name="current" placeholder="Current password"> </div>
                   <div class="form-group"> <label class="form-label">New Password</label> <input type="password" class="form-control" name="new_password" placeholder="New password"> </div>
                   <div class="form-group"> <label class="form-label">Confirm Password</label> <input type="password" class="form-control" name="confirm_password" placeholder="Retype new password"> </div>
               </div>
               <div class="card-footer text-right">
                   <button type="submit" href="#" class="btn btn-primary">Update</button>
                   <a href="#" class="btn btn-danger">Cancel</a>
               </div>
           </form>

       </div>
    </div>
    <div class="col-xl-9 col-lg-8">
    <form action="{{route('user.update.profile')}}" method="POST">
        @csrf
        <input type="hidden" name="userId" value="{{$user['id']}}" />
            <div class="card">
                <div class="card-header">
                   <div class="card-title">Edit Profile</div>
                </div>
                <div class="card-body">
                   <div class="card-title font-weight-bold">Basic info:</div>
                   <div class="row">
                      <div class="col-sm-12 col-md-12">
                      <div class="form-group"> <label class="form-label">Name</label> <input type="text" class="form-control" name="name" value="{{$user['name']}}" placeholder="Full Name"> </div>
                      </div>

                      <div class="col-sm-6 col-md-6">
                      <div class="form-group"> <label class="form-label">Email address</label>
                          <input type="email" name="email" value="{{$user['email']}}" class="form-control" placeholder="Email"> </div>
                      </div>
                      <div class="col-sm-6 col-md-6">
                         <div class="form-group"> <label class="form-label">Phone Number</label> <input type="number" name="phone_number" value="{{$user['phone_number']}}" class="form-control" placeholder="Number"> </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group"> <label class="form-label">Address</label> <input type="text" class="form-control" name="address" value="{{$user['address']}}" placeholder="Home Address"> </div>
                      </div>
                      <div class="col-sm-6 col-md-4">
                         <div class="form-group"> <label class="form-label">City</label> <input type="text" class="form-control" name="city" value="{{$user['city']}}" placeholder="City"> </div>
                      </div>
                      <div class="col-sm-6 col-md-3">
                         <div class="form-group"> <label class="form-label">Postal Code</label> <input type="text" class="form-control" name="postal_code" value="{{$user['postal_code']}}" placeholder="Postal Code"> </div>
                      </div>
                      <div class="col-md-5">
                         <div class="form-group">
                            <label class="form-label">Select District</label>
                            <select class="form-control" name="district">
                                @foreach($districts as $district)
                                    <option @if(Auth::user()->district==$district['district']) selected=selected @endif value="{{$district['district']}}">{{$district['district']}}</option>
                                @endforeach

                            </select>
                            </div>
                      </div>
                   </div>
                   <div class="card-title font-weight-bold mt-5">External Links:</div>
                   <div class="row">
                      <div class="col-sm-6 col-md-6">
                         <div class="form-group"> <label class="form-label">Facebook</label> <input type="text" class="form-control" value="{{$user['facebook']}}" name="facebook" placeholder="https://www.facebook.com/"> </div>
                      </div>
                      <div class="col-sm-6 col-md-6">
                         <div class="form-group"> <label class="form-label">Twitter</label> <input type="text" class="form-control" value="{{$user['twitter']}}" name="twitter" placeholder="https://twitter.com/"> </div>
                      </div>
                   </div>
                   <div class="card-title font-weight-bold mt-5">About:</div>
                   <div class="row">
                      <div class="col-md-12">
                         <div class="form-group"> <label class="form-label">About Me</label>
                          <textarea rows="5" name="about_me" class="form-control" placeholder="Enter About your description">{{$user['about_me']}}
                             </textarea> </div>
                      </div>
                   </div>
                </div>
                <div class="card-footer text-right"> <button type="submit" class="btn btn-primary">Update</button> <a href="#" class="btn btn-danger">Cancel</a> </div>
             </div>
        </form>

    </div>
 </div>



</div>

<div class="modal" id="upload-Image">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content modal-content-demo">
            <div class="modal-header">
                <h6 class="modal-title">Upload Profile Photo</h6>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
            </div>
            <form action="{{route('user.profile.photo')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-label">Profile Photo <span class="text-red">*</span></label>
                                <input type="file" required class="form-control" placeholder="Upload profile photo" name="photo">
                            </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-indigo" type="submit">Upload Profile Image</button>
                    <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
 @endsection
