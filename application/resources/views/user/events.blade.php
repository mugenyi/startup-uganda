@extends('build.user')
@section('profile-summary')

<div class="row pt-6 pr-4 pl-4">
    <div class="container">
        <div class="page-header">
            <div class="page-leftheader">
                <h4 class="page-title mb-0">Events</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('user.events')}}">Events</a></li>
                </ol>
            </div>
            <div class="page-rightheader">
                <a href="" class="text-primary" data-target="#create_events" data-toggle="modal"><i class="fa fa-plus"></i> Create Event</a>
            </div>
        </div>
    </div>
</div>
@endsection
@section('user-content')
<div class="container">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            @include('user.partial.menu')
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="row">
                @foreach ($events as $event)
                    @include('user.includes.event-col-4')
                @endforeach
            </div>
            <div class="row">
                {{$events->links()}}
            </div>
        </div>

    </div>

</div>

<div class="modal" id="create_events">
    <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title">Create Event</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('user.event.create')}}" method="POST" enctype="multipart/form-data">
            @csrf

          <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">Event Name <span class="text-red">*</span></label>
                        <input type="text" required class="form-control" placeholder="event Name" name="event_name">
                    </div>
                    <div class="form-group">
                        <label class="form-label">event Start Date <span class="text-red">*</span></label>
                        <input type="date"  min="{{date('Y-m-d')}}" required class="form-control" id="date" placeholder="Start Date" name="start_date">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">Organiser <span class="text-red">*</span></label>
                        <input type="text" required class="form-control" placeholder="event Organiser" name="organiser">
                    </div>
                    <div class="form-group">
                        <label class="form-label">event End Date <span class="text-red">*</span></label>
                        <input type="date"  min="{{date('Y-m-d')}}" required class="form-control" id="date" placeholder="Start Date" name="end_date">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPhone" class="form-label">Describe this event</label>
                        <textarea class="form-control" id="description" name="description" rows="4"></textarea>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPhone" class="form-label">Event Summary</label>
                        <textarea class="form-control" required name="summary" rows="2" placeholder="Event Summary "></textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPhone" class="form-label">Target Audience</label>
                        <textarea class="form-control" required name="target_audience" rows="2" placeholder="Describe target audience "></textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">Apply Until <span class="text-red">*</span></label>
                        <input type="date"  min="{{date('Y-m-d')}}" required class="form-control" placeholder="Deadline" name="apply_before">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">Event Location <span class="text-red">*</span></label>
                        <input type="text" required class="form-control" placeholder="Event Location" name="location">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPhone" class="form-label">Registration Link</label>
                        <input type="text" required class="form-control" placeholder="Registration Link" name="link">
                    </div>
                </div>

            </div>
          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Register event</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>


 @endsection
@section('script')
<!-- Initialize Quill editor -->
<script>

    $(document).ready(function() {
        $('#description').summernote({height: 150});
    });
  </script>
@endsection
