@extends('build.user')
@section('profile-summary')

<div class="row pt-6 pr-4 pl-4">
    <div class="container">
        <div class="page-header">
            <div class="page-leftheader">
            <h4 class="page-title mb-0">FEEDS</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                <li class="breadcrumb-item active"><a href="{{route('user.feeds')}}">Feeds</a></li>
        </ol>
        </div>
        <div class="page-rightheader">

        </div>
    </div>
    </div>
    </div>



@endsection
@section('user-content')
<div class="container">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            @include('user.partial.menu')
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="row mt-5">

                <div class="col-md-12">
                    @if (Auth::check())
                        <div class="row pl-4 pr-4">
                            <h6 class="text-gray">WHAT'S HAPPENING?</h6>
                        </div>
                        <div class="row pl-4 pr-4">
                            <form action="{{route('feed.create')}}" method="POST" style=" width: 100%;">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-sm-12 col-md-8">
                                        <label class="form-label">URL</label>
                                        <input class="form-control input-sm" required type="url" placeholder="Copy past link here........" name="url">
                                    </div>
                                    <div class="form-group col-sm-12 col-md-4">
                                        <label class="form-label">SELECT POST AS</label>
                                        <select class="form-control input-sm" required name="company">
                                            <option value="">Select Venture</option>
                                            <?php $companies= App\Traits\HelperTrait::getUserCompanies() ?>

                                            @foreach ($companies as $company)
                                                <option value="{{$company['param']}}">
                                                    {{$company['name']}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <textarea class="form-control br-br-0 br-bl-0" name="thought" placeholder="What are you doing right now?" rows="5"></textarea>

                                    </div>

                                    <div class="profile-share  ">
                                        <button type="submit" class="btn btn-orange pull-right mt-1"><i class="fa fa-share ml-1"></i> SHARE</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    @else

                    @endif
                    <div class="row">
                        @foreach ($feeds as $feed)
                            <div class="mb-6 pr-4 pl-4" style="width:100%; border-radius: 5px; border: 0.5px solid #BFBFBF ">

                                <div class="row" style="border-bottom: 1px solid rgba(0,0,0,0) !important;">
                                    <div class="col-lg-7 col-xl-8 col-md-12 col-sm-12">
                                        <div class="card-header" style="padding: .75rem 0;border-bottom: 1px solid rgba(0,0,0,0) !important;">
                                        <span>
                                            <img src="{{secure_asset(env('LOGO').$feed['company']['logo'])}}" alt="img" class="avatar avatar-lg  mr-2 brround">
                                        </span>
                                            <h6 class="text-gray-300 pt-2">
                                                {{$feed['company']['name']}} <br/>
                                                <small class="text-gray-100">
                                                    {{ strlen($feed['company']['address']) > 25 ? substr($feed['company']['address'],0,25)."..." : $feed['company']['address']}}
                                                </small>
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-xl-4 pt-3 col-md-12 col-sm-12">
                                        @if(Auth::check() && $feed['user_id']==Auth::user()->id)
                                            @if ($feed['status']=='published')
                                                <a class="text-danger" href="{{ route('user.feed.update',['id'=>$feed['id'],'status'=>'unpublished']) }}">
                                                    <i class="fa fa-trash"></i> Unpublish Post
                                                </a>
                                            @else
                                                <a class="text-danger" href="{{ route('user.feed.update',['id'=>$feed['id'],'status'=>'published']) }}">
                                                    <i class="fa fa-trash"></i> Unpublish Post
                                                </a>
                                            @endif

                                        @endif
                                        <span class=" ml-6" style="float: right;">{{ Carbon\Carbon::parse($feed->created_at)->diffForHumans()}}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-8 col-xl-9 col-md-12 col-sm-12">
                                        <h6 class="pt-3" style="color: #0E59DE">
                                            <a href="{{$feed['url']}}" style="font-size: 21px" target="_blank" >{{$feed['title']??" Visit Article"}}</a>
                                        </h6>
                                    </div>
                                    <div class="col-lg-4 col-xl-3 col-md-12 col-sm-12">
                                        @if (is_null($feed['image']))
                                            <img src="{{secure_asset(env('LOGO').'business-logo.png')}}" class="pt-3" style="width: 131px;"  alt="">
                                        @else
                                            <img src="{{$feed['image']}}" class=""  alt="">
                                        @endif
                                    </div>
                                </div>

                                <h6 class="text-gray fs-13 mt-3">THOUGHTS</h6>
                                <p class="fs-12">{{$feed['thought']}}</p>


                            </div>

                        @endforeach

                    </div>
                    <div class="row">
                        {{$feeds->links()}}
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>


@endsection
@section('script')
<!-- Initialize Quill editor -->
<script>

    $(document).ready(function() {
        $('#job-editor').summernote({height: 100});
    });
  </script>
@endsection
