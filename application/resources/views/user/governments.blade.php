@extends('build.user')
@section('profile-summary')

<div class="row pt-6 pr-4 pl-4">
    <div class="container">
        <div class="page-header">
            <div class="page-leftheader">
                <h4 class="page-title mb-0">GOVERNMENT</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('user.programs')}}">Government</a></li>
                </ol>
            </div>
            <div class="page-rightheader">
            <a href="{{route('user.government.register')}}" class="text-primary"><i class="fa fa-plus"></i> Create Program</a>
            </div>
        </div>
    </div>
</div>
@endsection
@section('user-content')
<div class="container">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            @include('user.partial.menu')
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="row">
                @foreach ($programs as $program)
                    @include('user.includes.program-col-4')
                @endforeach
            </div>
            <div class="row">
                {{$programs->links()}}
            </div>
        </div>

    </div>

</div>

@section('script')
<!-- Initialize Quill editor -->
<script>

    $(document).ready(function() {
        $('#description').summernote({height: 150});
    });
  </script>
@endsection
