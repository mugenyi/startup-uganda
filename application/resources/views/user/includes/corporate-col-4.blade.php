{{--<div class="col-md-6 col-lg-4">--}}
{{--    <a href="{{route('user.view.corporate',['id'=>$corporate['id'],'slug'=>App\Traits\HelperTrait::slugify($corporate['name'])])}}">--}}
{{--    <div class="card overflow-hidden startup-card">--}}
{{--        <img src="{{secure_asset(env('FULL').''.$corporate['image'])}}" style="height: 220px" alt="image">--}}
{{--        <img src="{{secure_asset(env('COVER').$corporate['image'])}}" style="height: 220px" alt="image">--}}
{{--        <div class="card-body">--}}
{{--            <h4 class="card-title text-uppercase" style="font-size: 13px">{{$corporate['name']}}</h4>--}}
{{--            --}}{{-- <h6 class="text-gray text-uppercase">{{$government['investor_type']}} INVESTOR </h6> --}}
{{--            <a href="{{route('user.category.chat',['category'=>'corporate','id'=>$corporate['id']])}}">--}}
{{--                <i class="fa fa-comments-o"></i>--}}
{{--                <span class="text-muted app-sidebar__user-name fs-12">--}}
{{--                    View Messages ({{App\Traits\HelperTrait::countUnreadMessages('corporate',$corporate['id'])}} Unread)--}}
{{--                </span>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    </a>--}}
{{--</div>--}}

<div class="col-xl-4 col-lg-4 col-md-12">
    <div class="card box-widget widget-user">
        <div class="widget-user-image mx-auto mt-5">
            <img alt="User Avatar" class="rounded-circle" src="{{secure_asset(env('LOGO').$corporate['logo'])}}">
        </div>
        <div class="card-body text-center">
            <div class="pro-user">
                <a href="{{route('user.view.corporate',['id'=>$corporate['id'],'slug'=>App\Traits\HelperTrait::slugify($corporate['name'])])}}">
                    <h4 class="pro-user-username text-dark mb-1 font-weight-bold">{{$corporate['name']}}</h4>
                </a>
                <h6 class="pro-user-desc text-muted text-uppercase">{{$corporate['location']}}</h6>
                <a href="{{route('user.category.chat',['category'=>'corporate','id'=>$corporate['id']])}}">
                    <i class="fa fa-comments-o"></i>
                    <span class="text-muted app-sidebar__user-name fs-12">
                View Messages ({{App\Traits\HelperTrait::countUnreadMessages('corporate',$corporate['id'])}} Unread)
            </span>
                </a>

            </div>
        </div>
    </div>

</div>
