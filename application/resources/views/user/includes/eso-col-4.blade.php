{{--<div class="col-md-6 col-lg-4">--}}
{{--    <a href="{{route('user.view.eso',['id'=>$eso['id'],'slug'=>App\Traits\HelperTrait::slugify($eso['name'])])}}">--}}
{{--    <div class="card overflow-hidden startup-card">--}}

{{--        <img src="{{secure_asset(env('COVER').$eso['image'])}}" style="height: 220px" alt="image">--}}
{{--        <div class="card-body">--}}
{{--            <h4 class="card-title text-uppercase" style="font-size: 13px">{{$eso['name']}}</h4>--}}
{{--            --}}{{-- <h6 class="text-gray text-uppercase">{{$government['investor_type']}} INVESTOR </h6> --}}
{{--            <a href="{{route('user.category.chat',['category'=>'eso','id'=>$eso['id']])}}">--}}
{{--                <i class="fa fa-comments-o"></i>--}}
{{--                <span class="text-muted app-sidebar__user-name fs-12">--}}
{{--                    View Messages ({{App\Traits\HelperTrait::countUnreadMessages('eso',$eso['id'])}} Unread)--}}
{{--                </span>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    </a>--}}
{{--</div>--}}

<div class="col-xl-4 col-lg-4 col-md-12">
    <div class="card box-widget widget-user">
        <div class="widget-user-image mx-auto mt-5">
            <img alt="User Avatar" class="rounded-circle" src="{{secure_asset(env('LOGO').$eso['logo'])}}">
        </div>
        <div class="card-body text-center">
            <div class="pro-user">
                <a href="{{route('user.view.eso',['id'=>$eso['id'],'slug'=>App\Traits\HelperTrait::slugify($eso['name'])])}}">
                    <h4 class="pro-user-username text-dark mb-1 font-weight-bold">{{$eso['name']}}</h4>
                </a>
                <h6 class="pro-user-desc text-muted text-uppercase">{{$eso['location']}}</h6>
                <a href="{{route('user.category.chat',['category'=>'eso','id'=>$eso['id']])}}">
                    <i class="fa fa-comments-o"></i>
                    <span class="text-muted app-sidebar__user-name fs-12">
                View Messages ({{App\Traits\HelperTrait::countUnreadMessages('eso',$eso['id'])}} Unread)
            </span>
                </a>

            </div>
        </div>
    </div>

</div>
