<div class="col-md-6 col-lg-4">
    <a href="{{route('user.view.event',['id'=>$event['id'],'slug'=>App\Traits\HelperTrait::slugify($event['event_name'])])}}">
    <div class="card overflow-hidden startup-card">
        <img src="{{secure_asset('public/uploads/banner/'.$event['image'])}}" style="height: 220px" alt="image">
        <div class="card-body">
            <h4 class="card-title text-uppercase" style="font-size: 13px">{{$event['event_name']}}</h4>
            <p class="text-gray">{{ strlen($event['summary']) > 110 ? substr($event['summary'],0,110)."..." : $event['summary']}}</p>
        </div>
    </div>
    </a>
</div>
