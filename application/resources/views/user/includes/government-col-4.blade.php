{{--<div class="col-md-6 col-lg-4">--}}
{{--    <a href="{{route('user.view.government',['id'=>$government['id'],'slug'=>App\Traits\HelperTrait::slugify($government['name'])])}}">--}}
{{--    <div class="card overflow-hidden startup-card">--}}
{{--        <img src="{{secure_asset(env('COVER').$government['image'])}}" style="height: 220px" alt="image">--}}
{{--        <div class="card-body">--}}
{{--            <h4 class="card-title text-uppercase" style="font-size: 13px">{{$government['name']}}</h4>--}}
{{--            --}}{{-- <h6 class="text-gray text-uppercase">{{$government['investor_type']}} INVESTOR </h6> --}}
{{--            <a href="{{route('user.category.chat',['category'=>'government','id'=>$government['id']])}}">--}}
{{--                <i class="fa fa-comments-o"></i>--}}
{{--                <span class="text-muted app-sidebar__user-name fs-12">--}}
{{--                    View Messages ({{App\Traits\HelperTrait::countUnreadMessages('government',$government['id'])}} Unread)--}}
{{--                </span>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    </a>--}}
{{--</div>--}}


<div class="col-xl-4 col-lg-4 col-md-12">
    <div class="card box-widget widget-user">
        <div class="widget-user-image mx-auto mt-5">
            <img alt="User Avatar" class="rounded-circle" src="{{secure_asset(env('LOGO').$government['logo'])}}">
        </div>
        <div class="card-body text-center">
            <div class="pro-user">
                <a href="{{route('user.view.government',['id'=>$government['id'],'slug'=>App\Traits\HelperTrait::slugify($government['name'])])}}">
                    <h4 class="pro-user-username text-dark mb-1 font-weight-bold">{{$government['name']}}</h4>
                </a>
                <h6 class="pro-user-desc text-muted">{{$government['address']}}</h6>
                <a href="{{route('user.category.chat',['category'=>'government','id'=>$government['id']])}}">
                    <i class="fa fa-comments-o"></i>
                    <span class="text-muted app-sidebar__user-name fs-12">
                View Messages ({{App\Traits\HelperTrait::countUnreadMessages('government',$government['id'])}} Unread)
            </span>
                </a>

            </div>
        </div>
    </div>

</div>
