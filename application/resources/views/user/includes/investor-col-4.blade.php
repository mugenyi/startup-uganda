{{--<div class="col-md-6 col-lg-4">--}}
{{--    <a href="{{route('user.view.investor',['slug'=>$investor['slug']])}}">--}}
{{--    <div class="card overflow-hidden startup-card">--}}
{{--        <img src="{{secure_asset(env('COVER').$investor['profile_photo'])}}" style="height: 220px" alt="image">--}}
{{--        <div class="card-body">--}}
{{--            <h4 class="card-title text-uppercase" style="font-size: 13px">{{$investor['name']}}</h4>--}}
{{--            <h6 class="text-gray text-uppercase">{{$investor['investor_type']}} INVESTOR </h6>--}}
{{--            <a href="{{route('user.category.chat',['category'=>'investor','id'=>$investor['id']])}}">--}}
{{--                <i class="fa fa-comments-o"></i>--}}
{{--                <span class="text-muted app-sidebar__user-name fs-12">--}}
{{--                    View Messages ({{App\Traits\HelperTrait::countUnreadMessages('investor',$investor['id'])}} Unread)--}}
{{--                </span>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    </a>--}}
{{--</div>--}}


<div class="col-xl-4 col-lg-4 col-md-12">
    <div class="card box-widget widget-user">
        <div class="widget-user-image mx-auto mt-5">
            <img alt="User Avatar" class="rounded-circle" src="{{secure_asset(env('LOGO').$investor['logo'])}}">
        </div>
        <div class="card-body text-center">
            <div class="pro-user">
                <a href="{{route('user.view.investor',['slug'=>$investor['slug']])}}">
                    <h4 class="pro-user-username text-dark mb-1 font-weight-bold">{{$investor['name']}}</h4>
                </a>
                <h6 class="pro-user-desc text-muted text-uppercase">{{$investor['investor_type']}} INVESTOR</h6>
                <a href="{{route('user.category.chat',['category'=>'investor','id'=>$investor['id']])}}">
                    <i class="fa fa-comments-o"></i>
                    <span class="text-muted app-sidebar__user-name fs-12">
                View Messages ({{App\Traits\HelperTrait::countUnreadMessages('investor',$investor['id'])}} Unread)
            </span>
                </a>

            </div>
        </div>
    </div>

</div>
