<div class="modal" id="upload_Logo">
    <div class="modal-dialog" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title">Upload {{$investor['name']}} Logo</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('user.investor.logo')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" value="{{$investor['id']}}" name="id">
            <input type="hidden" value="startup" name="type">
          <div class="modal-body">
            <div class="form-group">
                <div class="form-label">Add Logo</div>
                <input type="file" class="form-control" required name="upload_logo"/>

            </div>
          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Save changes</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>

 <div class="modal" id="upload_Cover">
    <div class="modal-dialog" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title">Upload {{$investor['name']}} Banner Image</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('user.investor.cover')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" value="{{$investor['id']}}" name="id">
            <input type="hidden" value="startup" name="type">
          <div class="modal-body">
            <div class="form-group">
                <div class="form-label">Add Banner</div>
                <input type="file" class="form-control" name="upload_cover"/>
                <small id="emailHelp" class="form-text text-muted">Preferably an image 780px * 340px.</small>
            </div>
          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Save changes</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>

 <div class="modal" id="upload_PitchDeck">
    <div class="modal-dialog" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title">Upload {{$investor['name']}} Pitch Deck</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('user.startup.pitch')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" value="{{$investor['id']}}" name="id">
            <input type="hidden" value="startup" name="type">
          <div class="modal-body">
            <div class="form-group">
                <div class="form-label">Add Pitch Deck</div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" placeholder="" name="pitch-deck">
                    <label class="custom-file-label">Choose file</label>
                </div>
            </div>
          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Save changes</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>


 <div class="modal" id="add_team">
    <div class="modal-dialog" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title">Add Team Member</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('user.investor.member.add')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" value="{{$investor['id']}}" name="category_id">
            <input type="hidden" value="investor" name="category">
          <div class="modal-body">
            <div class="form-group">
                <label for="exampleInputEmail1" class="form-label">Team Member Name</label>
                <input type="text" class="form-control" id="memberName" name="name" placeholder="Enter member name">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1" class="form-label">Designation</label>
                <input type="text" class="form-control" id="designation" name="designation" placeholder="Enter member position">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1" class="form-label">Designation</label>
                <select class="form-control" name="memberType">
                    <option value="teamMember">Team Member</option>
                    <option value="boardMember">Board Member</option>
                </select>
            </div>

            <div class="form-group">
                <div class="form-label">Add Team member </div>
                <input type="file" class="form-control" name="photo"/>
            </div>
          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Save changes</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>

 <div class="modal" id="record_funding">
    <div class="modal-dialog" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title"> {{$investor['name']}}</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('user.fund.record')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" value="{{$investor['id']}}" name="category_id">
            <input type="hidden" value="" name="round">
            <input type="hidden" value="investor" name="category">
          <div class="modal-body">
              <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                        <label class="form-label">Company Type</label>
                        <select class="form-control select2 select2-hidden-accessible" data-placeholder="Choose Company type" required name="company_type" aria-hidden="true">
                            <option value="">Select Company Type</option>
                            <option value="Startup">Startup</option>
                            <option value="Eso">Eso</option>
                            <option value="Incubator">Incubator</option>
                            <option value="Government">Government</option>

                        </select>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                        <label class="form-label">Amount (USD)</label>
                        <input type="number" class="form-control" id="raising" name="amount" required placeholder="Enter amount ">
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                        <label class="form-label">Company Name</label>
                        <input type="text" class="form-control" id="raising" name="company_name" required placeholder="Enter Investor Name ">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Company Logo</label>
                        <input type="text" class="form-control" id="raising" name="company_logo" required placeholder="Paste link to investor logo ">
                    </div>
                  </div>
              </div>


          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Create</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>
