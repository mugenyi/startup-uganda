<div class="col-xl-4 col-lg-4 col-md-12">
    <div class="card box-widget widget-user">
        <div class="widget-user-image mx-auto mt-5">
            <img alt="{{$mentor['name']}}" class="rounded-circle" src="{{secure_asset(env('LOGO').$mentor['photo'])}}">
        </div>
        <div class="card-body text-center">
            <div class="pro-user">
                <a href="{{route('user.view.mentor',['id'=>$mentor['id'],'slug'=>App\Traits\HelperTrait::slugify($mentor['name'])])}}">
                    <h4 class="pro-user-username text-dark mb-1 font-weight-bold">{{$mentor['name']}}</h4>
                </a>
                <h6 class="pro-user-desc text-muted">{{$mentor['location']}}</h6>
                <a href="{{route('user.category.chat',['category'=>'mentor','id'=>$mentor['id']])}}">
                    <i class="fa fa-comments-o"></i>
                    <span class="text-muted app-sidebar__user-name fs-12">
                View Messages ({{App\Traits\HelperTrait::countUnreadMessages('mentor',$mentor['id'])}} Unread)
            </span>
                </a>
                {{-- <span class="mb-0 fs-13 text-muted">{{$mentor['focus_areas']}}</span> --}}
                {{-- <a href="profile.html" class="btn btn-primary  mt-3"><i class="fa fa-pencil"></i> Edit Profile</a>
                <a href="#" class="btn btn-success  mt-3"><i class="fa fa-rss"></i> Follow</a> --}}
            </div>
        </div>
    </div>

</div>
