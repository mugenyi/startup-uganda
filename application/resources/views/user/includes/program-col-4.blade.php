<div class="col-md-6 col-lg-4">
    <a href="{{route('user.view.program',['id'=>$program['id'],'slug'=>App\Traits\HelperTrait::slugify($program['program_name'])])}}">
    <div class="card overflow-hidden startup-card">
{{--        <img src="{{secure_asset('public/uploads/banner/'.$program['image'])}}" style="height: 220px" alt="image">--}}

        <img src="{{secure_asset(env('COVER').$program['image'])}}" style="height: 220px" alt="image">
        <div class="card-body">
            <h4 class="card-title text-uppercase" style="font-size: 13px">{{$program['program_name']}}</h4>
            <p class="text-gray">{{$program['summary']}} </p>
        </div>
    </div>
    </a>
</div>
