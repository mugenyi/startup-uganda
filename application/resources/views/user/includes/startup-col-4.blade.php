{{--<div class="col-md-6 col-lg-4">--}}
{{--    <a href="{{route('user.view.startup',['slug'=>$startup['slug']])}}">--}}
{{--    <div class="card overflow-hidden startup-card">--}}
{{--    <img src="{{secure_asset(env('COVER').$startup['profile_photo'])}}" style="height: 220px" alt="image">--}}
{{--    <div class="card-body">--}}
{{--        <h4 class="card-title text-uppercase" style="font-size: 13px">{{$startup['name']}}</h4>--}}
{{--        <h6 class="text-gray">STARTUP</h6>--}}
{{--        <a href="{{route('user.category.chat',['category'=>'startup','id'=>$startup['id']])}}">--}}
{{--            <i class="fa fa-comments-o"></i>--}}
{{--            <span class="text-muted app-sidebar__user-name fs-12">--}}
{{--                View Messages ({{App\Traits\HelperTrait::countUnreadMessages('startup',$startup['id'])}} Unread)--}}
{{--            </span>--}}
{{--        </a>--}}

{{--    </div>--}}
{{--    </div>--}}
{{--    </a>--}}
{{--</div>--}}

<div class="col-xl-4 col-lg-4 col-md-12">
    <div class="card box-widget widget-user">
        <div class="widget-user-image mx-auto mt-5">
            <img alt="User Avatar" class="rounded-circle" src="{{secure_asset(env('LOGO').$startup['logo'])}}">
        </div>
        <div class="card-body text-center">
            <div class="pro-user">
                <a href="{{route('user.view.startup',['slug'=>$startup['slug']])}}">
                    <h4 class="pro-user-username text-dark mb-1 font-weight-bold">{{$startup['name']}}</h4>
                </a>
                <h6 class="pro-user-desc text-muted">{{$startup['status']}}</h6>
                <a href="{{route('user.category.chat',['category'=>'startup','id'=>$startup['id']])}}">
                    <i class="fa fa-comments-o"></i>
                    <span class="text-muted app-sidebar__user-name fs-12">
                View Messages ({{App\Traits\HelperTrait::countUnreadMessages('startup',$startup['id'])}} Unread)
            </span>
                </a>

            </div>
        </div>
    </div>

</div>
