<div class="modal" id="upload_Logo">
    <div class="modal-dialog" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title">Upload {{$startup['name']}} Logo</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('user.startup.logo')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" value="{{$startup['id']}}" name="id">
            <input type="hidden" value="startup" name="type">
          <div class="modal-body">
            <div class="form-group">
                <div class="form-label">Add Logo</div>
                <input type="file" class="form-control" name="upload_logo"/>
            </div>
          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Save changes</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>


 <div class="modal" id="upload_Cover">
    <div class="modal-dialog" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title">Upload {{$startup['name']}} Banner Image</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('user.startup.cover')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" value="{{$startup['id']}}" name="id">
            <input type="hidden" value="startup" name="type">
          <div class="modal-body">
            <div class="form-group">
                <div class="form-label">Add Banner</div>
                <input type="file" class="form-control" name="upload_cover"/>
                <small id="emailHelp" class="form-text text-muted">Preferably an image 780px * 340px.</small>

            </div>
          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Save changes</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>

 <div class="modal" id="upload_PitchDeck">
    <div class="modal-dialog" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title">Upload {{$startup['name']}} Pitch Deck</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('user.startup.pitch')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" value="{{$startup['id']}}" name="id">
            <input type="hidden" value="startup" name="type">
          <div class="modal-body">
            <div class="form-group">
                <div class="form-label">Add Pitch Deck</div>
                <input type="file" class="form-control" required name="pitch-deck"/>
            </div>
          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Save changes</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>


 <div class="modal" id="add_team">
    <div class="modal-dialog" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title">Add Team Member</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('user.startup.member.add')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" value="{{$startup['id']}}" name="category_id">
            <input type="hidden" value="startup" name="category">
          <div class="modal-body">
            <div class="form-group">
                <label for="exampleInputEmail1" class="form-label">Team Member Name</label>
                <input type="text" class="form-control" id="memberName" name="name" placeholder="Enter member name">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1" class="form-label">Designation</label>
                <input type="text" class="form-control" id="designation" name="designation" placeholder="Enter member position">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1" class="form-label">Designation</label>
                <select class="form-control" name="memberType">
                    <option value="teamMember">Team Member</option>
                    <option value="boardMember">Board Member</option>
                </select>
            </div>

            <div class="form-group">
                <div class="form-label">Add Team member </div>
                <input type="file" class="form-control" name="photo"/>

            </div>
          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Add Team Member</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>

 <div class="modal" id="add_sectors_industries">
    <div class="modal-dialog" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title">Add Industries & Sectors</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('user.startup.sectors')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" value="{{$startup['id']}}" name="startup_id">
          <div class="modal-body">
            <div class="form-group">
                <label class="form-label">Industries you serve</label>
                <select class="form-control select2 select2-hidden-accessible" data-placeholder="Choose Industries" name="industries[]" multiple="" tabindex="-1" aria-hidden="true">
                    {{ $industries= App\Traits\HelperTrait::getIndustries() }}
                    @foreach ($industries as $industry)
                    <option value="{{$industry['industry']}}">
                        {{$industry['industry']}}
                    </option>
                    @endforeach

                </select>
            </div>
            <div class="form-group">
                <label class="form-label">Sector you serve</label>
                <select class="form-control select2 select2-hidden-accessible" data-placeholder="Choose Sector" name="sectors[]" multiple="" tabindex="-1" aria-hidden="true">
                    {{ $sectors= App\Traits\HelperTrait::getSectors() }}
                    @foreach ($sectors as $sector)
                    <option value="{{$sector['sector_name']}}">
                        {{$sector['sector_name']}}
                    </option>
                    @endforeach

                </select>
            </div>
          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Save changes</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>


 <div class="modal" id="initiate_funding">
    <div class="modal-dialog" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title"> {{$startup['name']}}</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('user.fund.raising')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" value="{{$startup['id']}}" name="id">
            <input type="hidden" value="startup" name="type">
          <div class="modal-body">
              <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                        <label class="form-label">Fund Raising Round</label>
                        <select class="form-control select2 select2-hidden-accessible" data-placeholder="Choose Round" required name="round" aria-hidden="true">
                            <option value="">Select Round</option>
                            <option value="Pre-seed">Pre Seed</option>
                            <option value="Serie A">Serie A</option>
                            <option value="Serie B">Serie B</option>
                            <option value="Serie C">Serie C</option>

                        </select>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                        <label class="form-label">Looking to Raise (USD)</label>
                        <input type="number" class="form-control" id="raising" name="raise_amount" required placeholder="Enter amount you want to raise">
                    </div>
                  </div>
              </div>

          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Create</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>


 <div class="modal" id="record_funding">
    <div class="modal-dialog" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title"> {{$startup['name']}}</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('user.fund.record')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" value="{{$startup['id']}}" name="category_id">
            <input type="hidden" value="{{$startup['round']}}" name="round">
            <input type="hidden" value="startup" name="category">
          <div class="modal-body">
              <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                        <label class="form-label">Company Type</label>
                        <select class="form-control select2 select2-hidden-accessible" data-placeholder="Choose Round" required name="company_type" aria-hidden="true">
                            <option value="">Select Company Type</option>
                            <option value="Venture Capitalist Firm">Venture Capitalist Firm</option>
                            <option value="Incubator">Incubator</option>

                        </select>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                        <label class="form-label">Amount (USD)</label>
                        <input type="number" class="form-control" id="raising" name="amount" required placeholder="Enter amount ">
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                        <label class="form-label">Company Name</label>
                        <input type="text" class="form-control" id="raising" name="company_name" required placeholder="Enter Investor Name ">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Company Logo</label>
                        <input type="text" class="form-control" id="raising" name="company_logo" required placeholder="Paste link to investor logo ">
                    </div>
                  </div>
              </div>


          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Create</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>
