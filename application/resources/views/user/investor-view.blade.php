@extends('build.user')
@section('profile-summary')
    <div class="row pt-6 pr-4 pl-4">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                <h4 class="page-title mb-0">{{$investor['name']}}</h4>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Startup</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="#">{{$investor['name']}}</a></li>
            </ol>
            </div>
            <div class="page-rightheader">
            <a href="{{route('user.edit.startup',['slug'=>$investor['slug']])}}" class="text-primary"><i class="fa fa-pencil"></i> Edit Investor</a>
            </div>
        </div>
        </div>
        </div>
    </div>
@endsection
@section('user-content')
<div class="row">
    <div class="card" style="background: url({{secure_asset('public/uploads/full/'.$investor['banner'])}}) !important;background-size: cover;">
        <div class="card-body text-white" style="background-color: rgba(0,0,0,0.5);">
          <div class="container" style="padding-top:160px">
             <div class="row">

                <div class="col-md-8">
                   <div class="row">
                       <div class="col-md-2">
                    <img alt="" class="avatar avatar-xxl brround" src="{{secure_asset('public/uploads/logo/'.$investor['logo'])}}">
                    </div>
                      <div class="col-md-8">
                         <h3>{{$investor['name']}} </h3>
                         <div class="geodir-category-location fl-wrap">
                            <i class="fa fa-map-marker ml-3"></i>  {{$investor['address'].', '.$investor['district']}} <br/>
                            <i class="fa fa-globe ml-3"></i> {{$investor['website']}}
                         </div>
                         <div class="fl-wrap text-white">
                         @if(!is_null($investor['facebook']))<a href="{{$investor['facebook']}}" target="_blank"><i class="fa fa-facebook text-white fa-1x ml-3"></i></a>@endif
                         @if(!is_null($investor['twitter']))<a href="{{$investor['twitter']}}" target="_blank"><i class="fa fa-twitter text-white  ml-3"></i></a>@endif
                         @if(!is_null($investor['linkedin']))<a href="{{$investor['linkedin']}}" target="_blank"><i class="fa fa-linkedin text-white  ml-3"></i></a>@endif
                         @if(!is_null($investor['youtube']))<a href="{{$investor['youtube']}}" target="_blank"><i class="fa fa-youtube text-white  ml-3"></i></a>@endif
                         </div>

                      </div>
                   </div>
                </div>
                <div class="col-md-4">
                   <strong>Stage</strong> {{$investor['stage']}} <br/>
                   <strong>Consumer Model: </strong>{{$investor['consumer_model']}} <br/>
                   <strong>Revenue Generating: </strong>YES<br/>
                   <strong>Founded On: </strong>{{$investor['founded_on']}}<br/>
                   <strong>Industries:</strong> E-commenrce, Manufucturing, Logistics, Consumer Technology
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>

 <div class="container">
     <div class="row">
        <div class="col-md-12">
            <div class="list-single-main-item fl-wrap block_box">
                <div class="list-single-main-item-title">
                    <h3>Company Information</h3>
                </div>
                <div class="list-single-main-item_content fl-wrap">
                    {!!$investor['about']!!}
                </div>
              <div class="list-single-main-item-title"><h5>EXPERIENCE</h5></div>
                <div class="list-single-main-item_content fl-wrap">
                    {!!$investor['experience']!!}
                </div>

              <div class="list-single-main-item-title"><h5>INVESTOR INDUSTRIES</h5></div>
              <div class="list-single-main-item_content fl-wrap">
                {!!$investor['investment_industries']!!}
            </div>
              <div class="list-single-main-item-title"><h5>INVESTOR SECTOR</h5></div>
              <div class="list-single-main-item_content fl-wrap">
                {!!$investor['investment_sectors']!!}
            </div>
            </div>
        </div>
     </div>
     @if($teams->count()>0)
     <div class="row mt-5">

            <h5 class="mb-3" style="width: 100%">OUR TEAM</h5>
            <hr/>
                @foreach ($teams as $team)
                <div class="col-md-4 pt-3" style="border-top: 1px solid #dedede">
                <div class="media mt-0 mb-3">
                    <figure class="rounded-circle align-self-start mb-0">
                        <img src="{{secure_asset('public/uploads/user/'.$team['photo'])}}" alt="Generic placeholder image" class="avatar brround avatar-md mr-3">
                    </figure>@extends('build.user')
                    @section('profile-summary')
                        <div class="row">
                            <div class="container">
                                <div class="page-header">
                                    <div class="page-leftheader">
                                    <h4 class="page-title mb-0">{{$investor['name']}}</h4>
                                    <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                                    <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Investor</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="#">{{$investor['name']}}</a></li>
                                </ol>
                                </div>
                                <div class="page-rightheader">
                                <a href="{{route('user.edit.startup',['slug'=>$investor['slug']])}}" class="text-primary"><i class="fa fa-pencil"></i> Edit Startup</a>
                                </div>
                            </div>
                            </div>
                            </div>
                        </div>
                    @endsection
                    @section('user-content')
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-3 col-lg-4">
                                    @include('user.partial.menu')
                                </div>
                                <div class="col-xl-9 col-lg-8">
                                    <div class="page-rightheader mb-4">
                                        <a href="" class="text-primary mr-3" data-target="#upload_Logo" data-toggle="modal"><i class="fa fa-image"></i> Upload Logo</a>
                                        <a href="" class="text-primary mr-3" data-target="#upload_Cover" data-toggle="modal"><i class="fa fa-image"></i> Upload Cover Image</a>
                                        <a href="" class="text-primary mr-3" data-target="#upload_PitchDeck" data-toggle="modal"><i class="fa fa-file"></i> Upload Pitch Deck</a>


                                    </div>
                                    <div class="row">
                                        <div class="card" style="background: url({{secure_asset('public/uploads/full/'.$investor['banner'])}}) !important;background-size: cover;">
                                            <div class="card-body text-white" style="background-color: rgba(0,0,0,0.5);">
                                              <div class="container" style="padding-top:30px">
                                                 <div class="row">
                                                    <div class="col-md-2">
                                                    <img alt="" class="avatar avatar-xxl brround" src="{{secure_asset('public/uploads/logo/'.$investor['logo'])}}">
                                                    </div>
                                                    <div class="col-md-6">
                                                       <div class="row">
                                                          <div class="col-md-9">
                                                             <h5>{{$investor['name']}} </h5>
                                                             <div class="geodir-category-location fl-wrap">
                                                                <i class="fa fa-map-marker ml-3"></i>  {{$investor['address'].', '.$investor['district']}} <br/>
                                                                <i class="fa fa-globe ml-3"></i> {{$investor['website']}}
                                                             </div>
                                                             <div class="fl-wrap text-white">
                                                             @if(!is_null($investor['facebook']))<a href="{{$investor['facebook']}}" target="_blank"><i class="fa fa-facebook text-white fa-1x ml-3"></i></a>@endif
                                                             @if(!is_null($investor['twitter']))<a href="{{$investor['twitter']}}" target="_blank"><i class="fa fa-twitter text-white  ml-3"></i></a>@endif
                                                             @if(!is_null($investor['linkedin']))<a href="{{$investor['linkedin']}}" target="_blank"><i class="fa fa-linkedin text-white  ml-3"></i></a>@endif
                                                             @if(!is_null($investor['youtube']))<a href="{{$investor['youtube']}}" target="_blank"><i class="fa fa-youtube text-white  ml-3"></i></a>@endif
                                                             </div>

                                                          </div>
                                                       </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                       <strong>Stage</strong> {{$investor['stage']}} <br/>
                                                       <strong>Consumer Model: </strong>{{$investor['consumer_model']}} <br/>
                                                       <strong>Revenue Generating: </strong>YES<br/>
                                                       <strong>Founded On: </strong>{{$investor['founded_on']}}<br/>
                                                       <strong>Industries:</strong> E-commenrce, Manufucturing, Logistics, Consumer Technology
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                     </div>

                                     <div class="container">
                                         <div class="row">
                                             @if(!is_null($investor['pitchDeck']))
                                             <a href="{{secure_asset('public/uploads/pitch/'.$investor['pitchDeck'])}}" target="_blank" class="text-blue pull-right text-uppercase" style="margin-left: 80%">View Pitch Deck</a>
                                             @endif
                                            <div class="col-md-12">
                                                <div class="list-single-main-item fl-wrap block_box">
                                                    <div class="list-single-main-item-title">
                                                        <h3>Company Information</h3>
                                                    </div>
                                                    <div class="list-single-main-item_content fl-wrap">
                                                        {!!$investor['about']!!}
                                                    </div>
                                                  <div class="list-single-main-item-title"><h5>TRACTION</h5></div>
                                                    <div class="list-single-main-item_content fl-wrap">
                                                        {!!$investor['traction']!!}
                                                    </div>

                                                  <div class="list-single-main-item-title"><h5>INVESTMENT INDUSTRIES</h5></div>
                                                  <div class="list-single-main-item_content fl-wrap">
                                                    {!!$investor['investment_industries']!!}
                                                </div>
                                                  <div class="list-single-main-item-title"><h5>INVESTMENT SECTORS</h5></div>
                                                  <div class="list-single-main-item_content fl-wrap">
                                                    {!!$investor['investment_sectors']!!}
                                                </div>
                                                </div>

                                            </div>
                                         </div>
                                         <div class="row mt-6">
                                             <div class="col-md-6">
                                                <h5>OUR TEAM</h5>
                                                <div class="row m-4 right">
                                                    <a href="" class="mr-3" data-target="#add_team" data-toggle="modal"><i class="fa fa-user-plus text-orange"></i> Add Team Member</a>
                                                </div>

                                                @foreach ($teams as $team)
                                                <div class="media mt-0 mb-3">
                                                    <figure class="rounded-circle align-self-start mb-0">
                                                        <img src="{{secure_asset('public/uploads/user/'.$team['photo'])}}" alt="Generic placeholder image" class="avatar brround avatar-md mr-3">
                                                    </figure>
                                                    <div class="media-body">
                                                        <h6 class="time-title p-0 mb-0 font-weight-semibold leading-normal">{{$team->member_name}}</h6>
                                                        {{$team['designation']}} - {{$team['memberType']=='teamMember'?'Team Member':'Board Member'}}
                                                    </div>
                                                 </div>
                                                @endforeach
                                             </div>
                                         </div>
                                     </div>
                                </div>
                            </div>

                        @include('user.includes.startup-modals')

                    @endsection

                    <div class="media-body">
                        <h6 class="time-title p-0 mb-0 font-weight-semibold leading-normal">{{$team->member_name}}</h6>
                        {{$team['designation']}} - Team Member
                    </div>
                 </div>
                </div>
                @endforeach

     </div>
     @endif
     @if($boards->count()>0)
     <div class="row mt-5">
            <h5 class="mb-3" style="width: 100%">BOARD MEMBERS & ADVISORS</h5>
            <hr/>
                @foreach ($boards as $board)
                <div class="col-md-4 pt-3" style="border-top: 1px solid #dedede">
                <div class="media mt-0 mb-3">
                    <figure class="rounded-circle align-self-start mb-0">
                        <img src="{{secure_asset('public/uploads/user/'.$board['photo'])}}" alt="Generic placeholder image" class="avatar brround avatar-md mr-3">
                    </figure>
                    <div class="media-body">
                        <h6 class="time-title p-0 mb-0 font-weight-semibold leading-normal">{{$board->member_name}}</h6>
                        {{$team['designation']}} - Board Member
                    </div>
                 </div>
                @endforeach
            </div>
     </div>
     @endif
 </div>


@endsection
