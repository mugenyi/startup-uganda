@extends('build.user')
@section('profile-summary')

    <div class="row pt-6 pr-4 pl-4">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title mb-0">JOBS</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('user.jobs')}}">Jobs</a></li>
                    </ol>
                </div>
                <div class="page-rightheader">
                    <a href="" class="text-primary" data-target="#create_job" data-toggle="modal"><i class="fa fa-plus"></i> Create Jobs</a>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('user-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('user.partial.menu')
            </div>
            <div class="col-xl-9 col-lg-8">

                        <table class="table table-striped card-table table-vcenter">
                            <thead>
                            <tr>
                                <th>JOB TITLE</th>
                                <th>COMPANY</th>
                                <th>LOCATION</th>
                                <th class="">JOB TYPE</th>
                                <th class="d-none d-sm-block">DEADLINE</th>
                                <th>ACTION</th>
                            </tr>

                            </thead>
                            <tbody>
                            @foreach ($jobs as $job)
                                <?php $company = App\Traits\HelperTrait::getCompany($job['company_type'],$job['company']); ?>
                                <?php $slug = App\Traits\HelperTrait::slugify($job['job_title']); ?>
                                <tr class="fs-13">
                                    <td><a href="{{route('job.view',['id'=>$job['id'],'slug'=>$slug])}}" class="text-blue" target="_blank">{{$job['job_title']}}</a></td>
                                    <td>
                                        {{$company['name']}}
                                    </td>
                                    <td>{{$job['location']}}</td>
                                    <td class="">{{$job['job_type']}}</td>
                                    <td class="d-none d-sm-block">{{ date('m.d.Y',strtotime($job['ends_at']))}}</td>
                                    <td>
                                        @if ($job['status']=='Pending')
                                            <a href="{{route('user.job.status.update',['jobId'=>$job['id'],'status'=>'Active'])}}" target=""><i class="fa fa-check text-green"></i>Activate</a>
                                        @elseif ($job['status']=='Active')
                                            <a href="{{route('user.job.status.update',['jobId'=>$job['id'],'status'=>'Expired'])}}" target=""><i class="fa fa-times text-danger"></i>Expire</a>
                                        @else
                                            <a href="{{route('user.job.status.update',['jobId'=>$job['id'],'status'=>'Expired'])}}" target=""><i class="fa fa-times text-danger"></i>Expire</a>
                                        @endif


                                    </td>
                                </tr>

                            @endforeach
                            </tbody>

                        </table>

            </div>

        </div>

    </div>

    <div class="modal" id="create_job">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Create New Job</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
                </div>
                <form action="{{route('user.job.create')}}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Job Title <span class="text-red">*</span></label>
                                    <input type="text" class="form-control" required placeholder="Job Title" name="job_title">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Select Company <span class="text-red">*</span></label>
                                    <select class="form-control" required name="company_type">
                                        <?php $companies= App\Traits\HelperTrait::getUserCompanies() ?>

                                        @foreach ($companies as $company)
                                            <option value="{{$company['param']}}">
                                                {{$company['name']}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputPhone" class="form-label">Describe the job</label>
                                    <textarea class="form-control" id="job-editor" required name="description" rows="4"></textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputPhone" class="form-label">Required Skills</label>
                                    <textarea class="form-control" id="skill-editor" required name="skills" rows="2" placeholder="Separate each skill with commas"></textarea>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">Job Type <span class="text-red">*</span></label>
                                    <select class="form-control" required name="job_type">
                                        <option value="Full time">Full time</option>
                                        <option value="Part time">Part time</option>
                                        <option value="Contract">Contract</option>
                                        <option value="Contract">Remote</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">Apply Until <span class="text-red">*</span></label>
                                    <input type="date"  min="{{date('Y-m-d')}}" required class="form-control" placeholder="Application Until" name="ends_at">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">Job Location <span class="text-red">*</span></label>
                                    <input type="text" class="form-control" required placeholder="Job Location" name="location">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputPhone" class="form-label">Application URL</label>
                                    <input type="text" class="form-control" required placeholder="Application URL" name="application_link">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-indigo" type="submit">Save changes</button>
                        <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <!-- Initialize Quill editor -->
    <script>

        $(document).ready(function() {
            $('#job-editor').summernote({height: 100});
        });
    </script>
@endsection
