@extends('build.user')
@section('profile-summary')
    <div class="row pt-6 pr-4 pl-4">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                <h4 class="page-title mb-0">My Profile</h4>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="#">Profile</a></li>
            </ol>
            </div>
            <div class="page-rightheader">
                <div class="btn btn-list">
                @include('user.partial.new')
            </div>
        </div>
        </div>
        </div>
    </div>
    @include('user.partial.profile')
@endsection
@section('content')
<div class="row" style="margin-top: 20px">
    <div class="container">
        <div class="card">
            <div class="card-body">
               <h5 class="font-weight-bold">Biography</h5>
               <div class="main-profile-bio mb-0">
               <p>{{$user['about_me']}}</p>
               </div>
            </div>
            <div class="card-body border-top">
               <h5 class="font-weight-bold">Work &amp; Education</h5>
               <div class="main-profile-contact-list d-lg-flex">
                  <div class="media mr-5">
                     <div class="media-icon bg-success text-white mr-4"> <i class="fa fa-whatsapp "></i> </div>
                     <div class="media-body">
                        <h6 class="font-weight-bold mb-1">Web Designer at <a href="" class="btn-link">Spruko</a></h6>
                        <span>2018 - present</span>
                        <p>Past Work: Spruko, Inc.</p>
                     </div>
                  </div>
                  <div class="media mr-5">
                     <div class="media-icon bg-danger text-white mr-4"> <i class="fa fa-briefcase"></i> </div>
                     <div class="media-body">
                        <h6 class="font-weight-bold mb-1">Studied at <a href="" class="btn-link">University</a></h6>
                        <span>2004-2008</span>
                        <p>Graduation: Bachelor of Science in Computer Science</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="card-body border-top">
               <h5 class="font-weight-bold">Skills</h5>
               <a class="btn  btn-sm btn-light mt-1" href="#">HTML5</a> <a class="btn  btn-sm btn-light mt-1" href="#">CSS</a> <a class="btn  btn-sm btn-light mt-1" href="#">Java Script</a> <a class="btn  btn-sm btn-light mt-1" href="#">Photo Shop</a> <a class="btn  btn-sm btn-light mt-1" href="#">Php</a> <a class="btn  btn-sm btn-light mt-1" href="#">Wordpress</a> <a class="btn  btn-sm btn-light mt-1" href="#">Sass</a> <a class="btn  btn-sm btn-light mt-1" href="#">Angular</a>
            </div>
            <div class="card-body border-top">
               <h5 class="font-weight-bold">Contact</h5>
               <div class="main-profile-contact-list d-lg-flex">
                  <div class="media mr-4">
                     <div class="media-icon bg-primary text-white mr-3"> <i class="fa fa-phone pt-3"></i> </div>
                     <div class="media-body">
                        <small class="text-muted">Mobile</small>
                        <div class="font-weight-normal1"> {{$user['phone_number']}} </div>
                     </div>
                  </div>
                  <div class="media mr-4">
                    <div class="media-icon bg-warning text-white mr-3"> <i class="fa fa-facebook pt-3"></i> </div>
                    <div class="media-body">
                       <small class="text-muted">Facebook</small>
                       <div class="font-weight-normal1"> {{$user['facebook']}} </div>
                    </div>
                 </div>
                  <div class="media mr-4">
                     <div class="media-icon bg-warning text-white mr-3"> <i class="fa fa-twitter pt-3"></i> </div>
                     <div class="media-body">
                        <small class="text-muted">Twitter</small>
                        <div class="font-weight-normal1"> {{$user['twitter']}} </div>
                     </div>
                  </div>
                  <div class="media mr-4">
                    <div class="media-icon bg-warning text-white mr-3"> <i class="fa fa-map-marker-alt pt-3"></i> </div>
                    <div class="media-body">
                       <small class="text-muted">Postal Address</small>
                       <div class="font-weight-normal1"> {{$user['postal_code']}} </div>
                    </div>
                 </div>
                  <div class="media">
                     <div class="media-icon bg-info text-white mr-3"> <i class="fa fa-map pt-3"></i> </div>
                     <div class="media-body">
                        <small class="text-muted">Current Address</small>
                        <div class="font-weight-normal1"> {{$user['address'].' - '.$user['city'].', '.$user['district']}} </div>
                     </div>
                  </div>
               </div>
               <!-- main-profile-contact-list -->
            </div>
         </div>
    </div>
</div>
</div>

@endsection
