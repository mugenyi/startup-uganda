@extends('build.user')
@section('profile-summary')
    <div class="row pt-6 pr-4 pl-4">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                <h4 class="page-title mb-0">Register New Government Agency</h4>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                <li class="breadcrumb-item"><a href="#">Government</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="#">Register</a></li>
            </ol>
            </div>
            <div class="page-rightheader">
                <div class="btn btn-list">
                @include('user.partial.new')
            </div>
        </div>
        </div>
        </div>
    </div>
@endsection
@section('user-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('user.partial.menu')
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="card">
                    <div class="card-header">
                       <h3 class="card-title">Register New Government Agency</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{route('user.government.create')}}" method="POST">
                            <div class="row m-t-5">
                                @csrf
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}" />
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="exampleInputName" class="form-label">Government Agency Name</label>
                                    <input type="text" class="form-control" name="name" id="exampleInputEmail1" placeholder="Agency name *">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputName" class="form-label">Agency Tagline</label>
                                    <input type="text" class="form-control" required name="tagline" id="exampleInputEmail1" placeholder="Agency Tagline">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputName" class="form-label">Department incharge of Innovations</label>
                                    <input type="text" class="form-control" name="department" id="exampleInputEmail1" placeholder="Department incharge">
                                </div>
                            </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Contact Phone Number</label>
                                        <input type="text" class="form-control" name="phone_number" required id="phone_number" placeholder="Phone number *" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Contact Email Address</label>
                                        <input type="email" class="form-control" required name="email" id="email" placeholder="Email address *" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Agency Location</label>
                                        <input type="text" class="form-control" name="address" required id="address" placeholder="Location address *" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Website</label>
                                        <input type="text" class="form-control" name="website" required id="website" placeholder="www.ministry.go" >
                                    </div>

                                </div>
                            </div>
                            <div class="row m-t-5">
                                <h5 class="text-uppercase text-gray pl-2 pt-3">Agency Social Links</h5>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Facebook Link</label>
                                        <input type="text" class="form-control" name="facebook" id="facebook" placeholder="https://facebook.com/" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Twitter Link</label>
                                        <input type="text" class="form-control" name="twitter" id="twitter" placeholder="https://twitter.com/" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company linkedIn Link</label>
                                        <input type="text" class="form-control" name="linkedIn" id="linkedIn" placeholder="https://linkedin.com/" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Youtube Link</label>
                                        <input type="text" class="form-control" name="youtube" id="youtube" placeholder="https://youtube.com/" >
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-5">
                                <h5 class="text-uppercase text-gray pl-2 pt-3">About Agency</h5>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">About your Agency</label>
                                        <textarea class="form-control" id="about" name="about" rows="4"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Sectors of Interest</label>
                                        <select class="form-control select2 select2-hidden-accessible" data-placeholder="Choose Sector" name="sectors[]" multiple="" tabindex="-1" aria-hidden="true">
                                            {{ $sectors= App\Traits\HelperTrait::getSectors() }}
                                            <option value="All"> All Sectors</option>
                                            @foreach ($sectors as $sector)
                                            <option value="{{$sector['sector_name']}}">
                                                {{$sector['sector_name']}}
                                            </option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>

                            </div>
                            <button type="submit" class="btn btn-primary mt-4 mb-0">Register Agency</button>
                        </form>

                    </div>
                 </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
<!-- Initialize Quill editor -->
<script>
     $(document).ready(function() {
        $('#about').summernote({height: 100});
    });
  </script>
@endsection

