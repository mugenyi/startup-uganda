@extends('build.user')
@section('profile-summary')
    <div class="row pt-6 pr-4 pl-4">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                <h4 class="page-title mb-0">Register New Investor</h4>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                <li class="breadcrumb-item"><a href="#">Startup</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="#">Register</a></li>
            </ol>
            </div>
            <div class="page-rightheader">
                <div class="btn btn-list">
                @include('user.partial.new')
            </div>
        </div>
        </div>
        </div>
    </div>
@endsection
@section('user-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('user.partial.menu')
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="card">
                    <div class="card-header">
                       <h3 class="card-title">Register New Investor</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{route('user.add.investor')}}" method="POST">
                            <div class="row m-t-5">
                                @csrf
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}" />
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputName" class="form-label">Investor Name</label>
                                        <input type="text" class="form-control" name="name" id="exampleInputEmail1" placeholder="Investor name *">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Founded on</label>
                                        <input type="date" class="form-control" name="founded_on" id="founded_on" placeholder="Founded On" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Contact Phone Number</label>
                                        <input type="text" class="form-control" name="phone_number" required id="phone_number" placeholder="Phone number *" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputName" class="form-label">Investor Legal Name</label>
                                        <input type="text" class="form-control" name="legal_name" id="exampleInputEmail1" placeholder="Legal name *">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Contact Email Address</label>
                                        <input type="email" class="form-control" name="email" required id="phone_number" placeholder="Email Address *" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Investor Type</label>
                                        <select class="form-control" required name="investor_type">
                                            <option value="">Select type</option>
                                            <option value="organisation">Company</option>
                                            <option value="individual">Individual</option>
                                        </select>
                                    </div>


                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Tagline</label>
                                        <input type="text" class="form-control" name="tagline" required id="phone_number" placeholder="Company Tagline *" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Address</label>
                                        <input type="text" class="form-control" name="address" required id="address" placeholder="Company Address *" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Website</label>
                                        <input type="text" class="form-control" name="website" required id="website" placeholder="" >
                                    </div>


                                </div>
                                <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">District</label>
                                        <select class="form-control" name="district">
                                            @foreach($districts as $district)
                                                <option @if(Auth::user()->district==$district['district']) selected=selected @endif required value="{{$district['district']}}">{{$district['district']}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="row m-t-5">
                                <h5 class="text-uppercase text-gray pl-2 pt-3">Company Social Links</h5>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Facebook Link</label>
                                        <input type="text" class="form-control" name="facebook" id="facebook" placeholder="https://facebook.com/" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Twitter Link</label>
                                        <input type="text" class="form-control" name="twitter" id="twitter" placeholder="https://twitter.com/" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company linkedIn Link</label>
                                        <input type="text" class="form-control" name="linkedIn" id="linkedIn" placeholder="https://linkedin.com/" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Youtube Link</label>
                                        <input type="text" class="form-control" name="youtube" id="youtube" placeholder="https://youtube.com/" >
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-5">
                                <h5 class="text-uppercase text-gray pl-2 pt-3">About Investor</h5>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">About investor </label>
                                        <textarea class="form-control" name="about" id="about" rows="4"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Experience</label>
                                        <textarea class="form-control" name="experience" id="experience" rows="4"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label">Investment industries</label>
                                        <select class="form-control select2 select2-hidden-accessible" data-placeholder="Choose Industries" name="investment_industries[]" multiple="" tabindex="-1" aria-hidden="true">
                                            {{ $industries= App\Traits\HelperTrait::getIndustries() }}
                                            @foreach ($industries as $industry)
                                            <option value="{{$industry['industry']}}">
                                                {{$industry['industry']}}
                                            </option>
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Investment sector</label>
                                        <select class="form-control select2 select2-hidden-accessible" data-placeholder="Choose Sector" name="investment_sectors[]" multiple="" tabindex="-1" aria-hidden="true">
                                            {{ $sectors= App\Traits\HelperTrait::getSectors() }}
                                            @foreach ($sectors as $sector)
                                            <option value="{{$sector['sector_name']}}">
                                                {{$sector['sector_name']}}
                                            </option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>

                            </div>
                            <button type="submit" class="btn btn-primary mt-4 mb-0">Submit</button>
                        </form>

                    </div>
                 </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
<!-- Initialize Quill editor -->
<script>
     $(document).ready(function() {
        $('#about').summernote({height: 100});
        $('#experience').summernote();
    });
  </script>
@endsection
