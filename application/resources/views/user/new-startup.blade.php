@extends('build.user')
@section('profile-summary')
    <div class="row pt-6 pr-4 pl-4">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                <h4 class="page-title mb-0">Register New Startup</h4>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                <li class="breadcrumb-item"><a href="#">Startup</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="#">Register</a></li>
            </ol>
            </div>
            <div class="page-rightheader">
                <div class="btn btn-list">
                @include('user.partial.new')
            </div>
        </div>
        </div>
        </div>
    </div>
@endsection
@section('user-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('user.partial.menu')
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="card">
                    <div class="card-header">
                       <h3 class="card-title">Register New Startup</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{route('user.add.startup')}}" method="POST">
                            <div class="row m-t-5">
                                @csrf
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}" />
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputName" class="form-label">Startup Name</label>
                                        <input type="text" class="form-control" required name="name" id="exampleInputEmail1" placeholder="Startup name *">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Contact Phone Number</label>
                                        <input type="text" class="form-control" required name="phone_number" required id="phone_number" placeholder="Phone number *" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Founded on</label>
                                        <input type="date" class="form-control" required name="founded_on" id="founded_on" placeholder="Founded On" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Is your Business legally registered</label>
                                        <select class="form-control" required name="registration">
                                            <option value="">Select status</option>
                                            <option value="true">Yes</option>
                                            <option value="false">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Contact Email Address</label>
                                        <input type="email" class="form-control" name="email" required id="phone_number" placeholder="Email Address *" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Startup stage</label>
                                        <select class="form-control" required name="stage">
                                            <option value="">Select status</option>
                                            <option value="Ideation">Ideation</option>
                                            <option value="Validation">Validation</option>
                                            <option value="Early Traction">Early Traction</option>
                                            <option value="Scaling">Scaling</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Tagline</label>
                                        <input type="text" class="form-control" name="tagline" required id="phone_number" placeholder="Company Tagline *" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Address</label>
                                        <input type="text" class="form-control" name="address" required id="address" placeholder="Company Address *" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Website</label>
                                        <input type="text" class="form-control" name="website" required id="website" placeholder="" >
                                    </div>


                                </div>
                                <div class="col-md-6">
                                     <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">District</label>
                                        <select class="form-control" name="district">
                                                {{ $districts= App\Traits\HelperTrait::getDistricts() }}
                                                @foreach($districts as $district)
                                                    <option  @if(Auth::user()->district==$district['district']) selected=selected @endif  value="{{$district['district']}}">{{$district['district']}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Consumer Model</label>
                                        <div class="selectgroup selectgroup-pills">
                                            <?php $models = ['B2B','B2B2B','B2B2C','B2B2G','B2C','C2C','Government(B2C)','Non-Profit']; ?>
                                            @foreach($models as $a)
                                            <label class="selectgroup-item">
                                                <input type="checkbox" name="consumer_model[]" value="{{$a}}" class="selectgroup-input">
                                                <span class="selectgroup-button">{{$a}}</span>
                                            </label>
                                            @endforeach
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row m-t-5">
                                <h5 class="text-uppercase text-gray pl-2 pt-3">Company Social Links</h5>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Facebook Link</label>
                                        <input type="text" class="form-control" name="facebook" id="facebook" placeholder="https://facebook.com/" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Twitter Link</label>
                                        <input type="text" class="form-control" name="twitter" id="twitter" placeholder="https://twitter.com/" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company linkedIn Link</label>
                                        <input type="text" class="form-control" name="linkedIn" id="linkedIn" placeholder="https://linkedin.com/" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Company Youtube Link</label>
                                        <input type="text" class="form-control" name="youtube" id="youtube" placeholder="https://youtube.com/" >
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-5">
                                <h5 class="text-uppercase text-gray pl-2 pt-3">About Startup</h5>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">About your Startup</label>
                                        <textarea class="form-control" id="about-editor" required name="about" rows="4"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Problem your Startup Addresses</label>
                                        <textarea class="form-control" id="problem-editor" required name="problem" rows="4"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Solution</label>
                                        <textarea class="form-control" id="solution-editor" required name="solution" rows="4"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Traction</label>
                                        <textarea class="form-control" id="traction-editor" required name="traction" rows="4"></textarea>
                                    </div>
                                </div>

                            </div>
                            <button type="submit" class="btn btn-primary mt-4 mb-0">Register Startup</button>
                        </form>

                    </div>
                 </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
<!-- Initialize Quill editor -->
<script>
     $(document).ready(function() {
        $('#about-editor').summernote({height: 100});
        $('#problem-editor').summernote();
        $('#solution-editor').summernote();
        $('#traction-editor').summernote();
    });
  </script>
@endsection

