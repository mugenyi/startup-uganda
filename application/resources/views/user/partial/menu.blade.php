<!--  dashboard-menu-->
<div class="card1 pt-4">
    <div class="d-block d-sm-none" id="filter">
        <a href="javascript:void(0)" onclick="displayFilters()" ><div class="fs-13 mb-4 text-orange"><i class="fa fa-list"></i> Show Menu</div></a>

    </div>
    <div class=" d-none d-sm-block" id="content-filter">
        <div class="card-header">
            <div class="card-title">NAVIGATION MENU</div>
        </div>
        <div class="card-body">
            <ul class="flex-column">
                <li class="nav-item1"><a href="{{route('user.dashboard')}}">Dashboard</a></li>
                <li class="nav-item1"><a href="{{route('user.dashboard')}}">Ventures</a></li>
                <li class="nav-item1"><a href="{{route('user.jobs')}}">Job Adverts</a></li>
                <li class="nav-item1"><a href="{{route('user.feeds')}}">Your feeds</a></li>
                <li class="nav-item1"><a href="{{route('user.programs')}}">Your Programs</a></li>
                <li class="nav-item1"><a href="{{route('user.esos')}}">Your ESOs</a></li>
                <li class="nav-item1"><a href="{{route('user.mentors')}}">Mentors</a></li>
                <li class="nav-item1"><a href="{{route('user.corporates')}}">Your Corporations</a></li>
                <li class="nav-item1"><a href="{{route('user.events')}}">Events</a></li>

            </ul>

        </div>
    </div>

 </div>
