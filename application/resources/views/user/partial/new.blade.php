<div class="row d-block d-sm-none mb-4" style="width: 100%">
    <div class="dropdown pull-right" style="margin-top: .75rem;margin-right: .5rem;">
        <button type="button" class="btn btn-sm border-warning text-warning text-bold fs-12 dropdown-toggle" data-toggle="dropdown" aria-expanded="true"> <i class="fa fa-briefcase mr-2"></i>List your Venture </button>
        <div class="dropdown-menu fs-12" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 36px, 0px);" x-placement="bottom-start">
            <a class="dropdown-item" href="{{route('user.new.startup')}}">Startup</a>
            <a class="dropdown-item" href="{{route('user.mentors')}}">Mentor</a>
            <a class="dropdown-item" href="{{route('user.new.individual.investor')}}">Individual Investor</a>
            <a class="dropdown-item" href="{{route('user.new.investor')}}">Company Investor</a>
            <a class="dropdown-item" href="{{route('user.esos')}}">ESO</a>
            <a class="dropdown-item" href="{{route('user.government.register')}}">Government Agency</a>
            <a class="dropdown-item" href="{{route('user.corporates')}}">Corporation</a>
        </div>
    </div>

</div>
