<div class="row bg-white">
    <div class="container">
        <div class="main-proifle">
            @if (strpos(Auth::user()->email, 'twitterUser_') !== false)
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Hello!</strong> Please consider setting your correct email address by editing your profile
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="row">

               <div class="col-lg-12">
                  <div class="box-widget widget-user">
                      <div class="pull-right fs-14">
                          <a href="{{route('user.chat')}}" class="mr-4"><i class="fa fa-envelope"></i> Messages</a>
                          <a href="{{route('user.edit-profile')}}" class=""><i class="fa fa-pencil"></i> Edit Profile</a>
                      </div>
                     <div class="widget-user-image1 d-sm-flex">
                        <img alt="User Avatar" class="rounded-circle border p-1 brround shadow2" style="width:150px;height:150px; margin-left: auto;margin-right: auto" src="{{secure_asset('public/uploads/'.Auth::user()->image)}}">
                        <div class="mt-1 pt-4 ml-lg-5" style="width: 100%">
                           <h4 class="pro-user-username mb-3 font-weight-bold">{{Auth::user()->name}}</h4>
                           <p>{{Auth::user()->about_me}}</p>
                           <div class="mb-0 row">
                              <span class="h6 col-md-6 col-sm-12 mt-2 "><i class="fa fa-envelope text-gray"></i> {{Auth::user()->email}}</span>
                              <span class="h6 col-md-6 col-sm-12 mt-2"><i class="fa fa-phone text-gray"></i> {{Auth::user()->phone_number}}</span>
                              <span class="h6 col-md-6 col-sm-12 mt-2"><i class="fa fa-location-arrow text-gray"></i> {{Auth::user()->address.' - '.Auth::user()->city}}</span>
                              <span class="h6 col-md-6 col-sm-12 mt-2"><i class="fa fa-location-arrow text-gray"></i> {{Auth::user()->district}}</span>
                           </div>

                        </div>

                     </div>

                  </div>
               </div>

            </div>

         </div>
    </div>
</div>
