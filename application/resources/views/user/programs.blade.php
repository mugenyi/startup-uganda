@extends('build.user')
@section('profile-summary')

<div class="row pt-6 pr-4 pl-4">
    <div class="container">
        <div class="page-header">
            <div class="page-leftheader">
                <h4 class="page-title mb-0">PROGRAMS</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('user.programs')}}">Programs</a></li>
                </ol>
            </div>
            <div class="page-rightheader">
                <a href="" class="text-primary" data-target="#create_program" data-toggle="modal"><i class="fa fa-plus"></i> Create Program</a>
            </div>
        </div>
    </div>
</div>
@endsection
@section('user-content')
<div class="container">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            @include('user.partial.menu')
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="row">
                @foreach ($programs as $program)
                    @include('user.includes.program-col-4')
                @endforeach
            </div>
            <div class="row">
                {{$programs->links()}}
            </div>
        </div>

    </div>

</div>

<div class="modal" id="create_program">
    <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content modal-content-demo">
          <div class="modal-header">
             <h6 class="modal-title">Create New Program</h6>
             <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
          </div>
        <form action="{{route('user.program.create')}}" method="POST" enctype="multipart/form-data">
            @csrf

          <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">Program Name <span class="text-red">*</span></label>
                        <input type="text" required class="form-control" placeholder="Program Name" name="program_name">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Program Start Date <span class="text-red">*</span></label>
                        <input type="date"  min="{{date('Y-m-d')}}" required class="form-control" id="date" placeholder="Start Date" name="start_date">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">Organiser <span class="text-red">*</span></label>
                        <input type="text" required class="form-control" placeholder="Program Organiser" name="organiser">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Program End Date <span class="text-red">*</span></label>
                        <input type="date"  min="{{date('Y-m-d')}}" required class="form-control" id="date" placeholder="Start Date" name="end_date">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPhone" class="form-label">Describe this program</label>
                        <textarea class="form-control" id="description" required name="description" rows="4"></textarea>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPhone" class="form-label">Program Summary</label>
                        <input type="text" required class="form-control" id="date" placeholder="Program Summary" maxlength="120" name="summary" placeholder="Summarize program ambitions">

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPhone" class="form-label">Target Audience</label>
                        <textarea class="form-control" required name="target_audience" rows="2" placeholder="Describe target audience "></textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">Apply Until <span class="text-red">*</span></label>
                        <input type="date"  min="{{date('Y-m-d')}}" required class="form-control" placeholder="Deadline" name="apply_before">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label">Program Location <span class="text-red">*</span></label>
                        <input type="text" required class="form-control" placeholder="Program Venue" name="location">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPhone" class="form-label">Registration Link</label>
                        <input type="text" required class="form-control" placeholder="Registration Link" name="link">
                    </div>
                </div>

            </div>
          </div>
            <div class="modal-footer">
                <button class="btn btn-indigo" type="submit">Register Program</button>
                <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
            </div>
          </form>
       </div>
    </div>
 </div>


 @endsection
@section('script')
<!-- Initialize Quill editor -->
<script>

    $(document).ready(function() {
        $('#description').summernote({height: 150});
    });
  </script>
@endsection
