@extends('build.user')
@section('top_scripts')
    <link rel="stylesheet" href="https://unpkg.com/dropzone/dist/dropzone.css" />
    <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
    <script src="https://unpkg.com/dropzone"></script>
    <script src="https://unpkg.com/cropperjs"></script>
@endsection
@section('profile-summary')

    <div class="container">
        <div class="row pt-6 pr-4 pl-4">

            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title mb-0">{{$startup['name']}}</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Startup</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="#">{{$startup['name']}}</a></li>
                    </ol>
                </div>
                <div class="page-rightheader">

                </div>
            </div>
        </div>
    </div>
@endsection

@section('user-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('user.partial.menu')
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="page-rightheader mb-4">
                    <div class="dropdown mb-4">
                        <button class="btn btn-orange pull-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#" data-target="#upload_Logo" data-toggle="modal"><i class="fa fa-image"></i> Upload Logo</a>
{{--                            <a class="dropdown-item" href="#" data-target="#upload_Cover" data-toggle="modal"><i class="fa fa-image"></i> Upload Cover Image</a>--}}
                            <a class="dropdown-item" href="#" data-target="#upload_PitchDeck" data-toggle="modal"><i class="fa fa-file"></i> Upload Pitch Deck</a>
                            <a class="dropdown-item" href="#" data-target="#add_sectors_industries" data-toggle="modal"><i class="fa fa-cogs"></i> Update Industries</a>
                            <a class="dropdown-item" href="{{route('user.edit.startup',['slug'=>$startup['slug']])}}" class="text-primary"><i class="fa fa-pencil"></i> Edit Startup</a>
                            @if (!$startup['fundraising'])
                                <a class="dropdown-item" href="" data-target="#initiate_funding" data-toggle="modal"> Are you Raising Funds?</a>
                            @else
                                <a class="dropdown-item" href="#" data-target="#record_funding" data-toggle="modal"><i class="fa fa-plus"></i> Record Funds</a>
                                <a class="dropdown-item" href="{{route('fund.raising.close',['id'=>$startup['id']])}}" class="text-primary mr-3"> Close Raising Funds?</a>

                            @endif
                            @if(!is_null($startup['pitchDeck']))
                                <a class="dropdown-item" href="{{secure_asset('public/uploads/pitch/'.$startup['pitchDeck'])}}" target="_blank" class="text-primary mr-3"> View Pitch Deck</a>
                            @endif
                        </div>
                    </div>

                </div>
                <div class="row"  style="width: 100%">

                    <div class="container" style="padding-top:20px;">
                        <div class="row">
                            <div class="col-md-2 col-sm-12 ">
                                <img alt="{{$startup['name']}}" class="rounded-circle border mb-4 brround" style="width:100px;height:100px; margin-left: auto;margin-right: auto" src="{{secure_asset(env('LOGO').$startup['logo'])}}">
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="row">
                                    <div class="col-md-9">
                                        <h5>{{$startup['name']}} </h5>
                                        <span class="">
                                            <i class="fa fa-map-marker ml-3"></i>  {{$startup['address'].', '.$startup['district']}} <br/>
                                            <i class="fa fa-globe ml-3"></i> {{$startup['website']}}
                                        </span>
                                        <div class="fl-wrap text-white">
                                            @if(!is_null($startup['facebook']))<a href="{{$startup['facebook']}}" target="_blank"><i class="fa fa-facebook text-white fa-1x ml-3"></i></a>@endif
                                            @if(!is_null($startup['twitter']))<a href="{{$startup['twitter']}}" target="_blank"><i class="fa fa-twitter text-white  ml-3"></i></a>@endif
                                            @if(!is_null($startup['linkedin']))<a href="{{$startup['linkedin']}}" target="_blank"><i class="fa fa-linkedin text-white  ml-3"></i></a>@endif
                                            @if(!is_null($startup['youtube']))<a href="{{$startup['youtube']}}" target="_blank"><i class="fa fa-youtube text-white  ml-3"></i></a>@endif
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <strong>Stage</strong> {{$startup['stage']}} <br/>
                                <strong>Consumer Model: </strong>{{$startup['consumer_model']}} <br/>
                                <strong>Revenue Generating: </strong>YES<br/>
                                <strong>Founded On: </strong>{{$startup['founded_on']}}<br/>

                            </div>
                        </div>
                    </div>


                </div>

                <div class="container">

                    <div class="row">

                        <div class="image_area mt-4">
                            <form method="post">
                                @csrf
                                <input type="hidden" value="{{$startup['id']}}" name="id">
                                <input type="hidden" value="startup" name="type">

                                <label for="upload_image">
                                    <img src="{{secure_asset(env('FULL').$startup['banner'])}}" id="uploaded_image" class="img-responsive img-circle" />
                                    <div class="overlay1">
                                        <div class="text fs-14 pt-4" style="background: #FFF;padding: 20px;opacity: .8;border-radius: 9px;">Click to Change Banner Image</div>
                                    </div>
                                    <input type="file" name="image" class="image" id="upload_image" style="display:none" />
                                </label>
                            </form>
                        </div>

                        <div class="col-md-12">
                            <div class="list-single-main-item fl-wrap block_box">
                                <div class="list-single-main-item-title mt-5">
                                    <h3>Company Information</h3>
                                </div>
                                <div class="list-single-main-item_content fl-wrap">
                                    {!!$startup['about']!!}
                                </div>
                                <div class="list-single-main-item-title mt-5 text-gray"><h5>TRACTION</h5></div>
                                <div class="list-single-main-item_content fl-wrap">
                                    {!!$startup['traction']!!}
                                </div>

                                <div class="list-single-main-item-title mt-5 text-gray"><h5>PROBLEM</h5></div>
                                <div class="list-single-main-item_content fl-wrap">
                                    {!!$startup['problem']!!}
                                </div>
                                <div class="list-single-main-item-title mt-5 text-gray"><h5>SOLUTION</h5></div>
                                <div class="list-single-main-item_content fl-wrap">
                                    {!!$startup['solution']!!}
                                </div>
                                <div class="list-single-main-item-title mt-5 text-gray"><h5>INDUSTRIES YOU SERVE</h5></div>
                                <div class="list-single-main-item_content fl-wrap">
                                    {!!$startup['industries']!!}
                                </div>
                                <div class="list-single-main-item-title mt-5 text-gray"><h5>SECTORS YOU SERVE</h5></div>
                                <div class="list-single-main-item_content fl-wrap">
                                    {!!$startup['sectors']!!}
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row mt-6">
                        <div class="col-md-6">
                            <h5>OUR TEAM</h5>
                            <div class="row m-4 right">
                                <a href="" class="mr-3" data-target="#add_team" data-toggle="modal"><i class="fa fa-user-plus text-orange"></i> Add Team Member</a>
                            </div>

                            @foreach ($teams as $team)
                                <div class="media mt-0 mb-3">
                                    <figure class="rounded-circle align-self-start mb-0">
                                        <img src="{{secure_asset('public/uploads/user/'.$team['photo'])}}" alt="Generic placeholder image" class="avatar brround avatar-md mr-3">
                                    </figure>
                                    <div class="media-body">
                                        <h6 class="time-title p-0 mb-0 font-weight-semibold leading-normal">{{$team->member_name}}</h6>
                                        {{$team['designation']}} - {{$team['memberType']=='teamMember'?'Team Member':'Board Member'}}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="row mt-6">
                        <div class="col-md-12">
                            <h5>RECORDED FUNDING</h5>

                            <?php $funds= App\Traits\HelperTrait::getFundings('startup',$startup['id'],'list'); ?>
                            <div class="table-responsive">
                                <table class="table table-striped card-table table-vcenter">
                                    <thead>
                                    <tr>
                                        <th colspan="2"><b>Name</b></th>
                                        <th><b>Amount</b></th>
                                        <th><b>Round</b></th>
                                        <th><b>Action</b></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($funds as $fund)
                                        <tr class="fs-12">
                                            <td>
                                                <img src="{{$fund['company_logo']}}" style="width: 50px !important;" alt="{{$fund['company_name']}}" class="avatar left-align avatar-lg mr-2 brround">
                                            </td>
                                            <td>
                                                <b>{{$fund['company_name']}}</b>
                                            </td>
                                            <td>{{$fund['amount']}}</td>
                                            <td>{{$fund['round']}}</td>
                                            <td><a href="{{route('investment.delete',['id'=>$fund['id']])}}">Delete</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('user.includes.startup-modals')

        <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Crop Image Before Upload</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="img-container">
                            <div class="row">
                                <div class="col-md-8">
                                    <img src="" id="sample_image" />
                                </div>
                                <div class="col-md-4">
                                    <div class="preview"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="crop" class="btn btn-primary">Crop</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>


@endsection
@section('script')
            <script>

                $(document).ready(function(){
                    var $modal = $('#modal');
                    var image = document.getElementById('sample_image');
                    var cropper;

                    $('#upload_image').change(function(event){
                        var files = event.target.files;

                        var done = function(url){
                            image.src = url;
                            $modal.modal('show');
                        };

                        if(files && files.length > 0)
                        {
                            reader = new FileReader();
                            reader.onload = function(event)
                            {
                                done(reader.result);
                            };
                            reader.readAsDataURL(files[0]);
                        }
                    });

                    $modal.on('shown.bs.modal', function() {
                        cropper = new Cropper(image, {
                            aspectRatio: 860/ 360,
                            viewMode:3,
                            preview:'.preview'
                        });
                    }).on('hidden.bs.modal', function(){
                        cropper.destroy();
                        cropper = null;
                    });

                    $('#crop').click(function(){
                        canvas = cropper.getCroppedCanvas({
                            width:900,
                            height:360
                        });

                        canvas.toBlob(function(blob){
                            url = URL.createObjectURL(blob);
                            var reader = new FileReader();
                            reader.readAsDataURL(blob);
                            reader.onloadend = function(){
                                var base64data = reader.result;
                                $.ajax({
                                    url:'{{route('user.startup.banner')}}',
                                    method:'POST',
                                    data:{image:base64data,id:{{$startup['id']}},'type':'startup'},
                                    success:function(data)
                                    {
                                        $modal.modal('hide');
                                        history.go(0);
                                    }
                                });
                            };
                        });
                    });

                });
            </script>

@endsection
