@extends('build.user')
@section('top_scripts')
    <link rel="stylesheet" href="https://unpkg.com/dropzone/dist/dropzone.css" />
    <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
    <script src="https://unpkg.com/dropzone"></script>
    <script src="https://unpkg.com/cropperjs"></script>
@endsection
@section('profile-summary')
    <div class="row pt-6 pr-4 pl-4">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title mb-0 d-none d-sm-blockmb-2 ">{{$corporate['name']}}</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('user.corporates')}}">Corporations</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="#">{{$corporate['name']}}</a></li>
                    </ol>
                </div>
                <div class="page-rightheader">

                </div>
            </div>
        </div>
    </div>

@endsection

@section('user-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('user.partial.menu')
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="page-rightheader mb-4">
                    <div class="page-rightheader mb-4">
                        <div class="dropdown mb-4">
                            <button class="btn btn-orange pull-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#" data-target="#upload_Logo" data-toggle="modal"><i class="fa fa-image"></i> Upload Logo</a>
                                <a class="dropdown-item" href="#" data-target="#edit_corporate" data-toggle="modal"><i class="fa fa-pencil"></i> Edit corporation</a>
                                <a class="dropdown-item" href="#" data-target="#add_sectors_industries" data-toggle="modal"><i class="fa fa-cogs"></i> Update Industries</a>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="row">
                    <div class="container" style="padding-top:30px">
                        <div class="row">
                            <div class="col-md-2 col-sm-12">
                                <img alt="{{$corporate['name']}}" class="rounded-circle border mb-4 brround" style="width:100px;height:100px; margin-left: auto;margin-right: auto" src="{{secure_asset(env('LOGO').$corporate['logo'])}}">
                            </div>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-9 col-sm-12">
                                        <h5>{{$corporate['name']}} </h5>
                                        <div class="geodir-category-location fl-wrap">
                                            <i class="fa fa-map-marker ml-3"></i>  {{$corporate['address']}} <br/>
                                            <i class="fa fa-globe ml-3"></i> {{$corporate['website']}}
                                        </div>
                                        <div class="fl-wrap text-white">
                                            @if(!is_null($corporate['facebook']))<a href="{{$corporate['facebook']}}" target="_blank"><i class="fa fa-facebook text-white fa-1x ml-3"></i></a>@endif
                                            @if(!is_null($corporate['twitter']))<a href="{{$corporate['twitter']}}" target="_blank"><i class="fa fa-twitter text-white  ml-3"></i></a>@endif
                                            @if(!is_null($corporate['linkedin']))<a href="{{$corporate['linkedin']}}" target="_blank"><i class="fa fa-linkedin text-white  ml-3"></i></a>@endif
                                            @if(!is_null($corporate['youtube']))<a href="{{$corporate['youtube']}}" target="_blank"><i class="fa fa-youtube text-white  ml-3"></i></a>@endif
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="image_area mt-4">
                        <form method="post">
                            @csrf
                            <input type="hidden" value="{{$corporate['id']}}" name="id">
                            <input type="hidden" value="startup" name="type">

                            <label for="upload_image">
                                <img src="{{secure_asset(env('FULL').$corporate['banner'])}}" id="uploaded_image" alt="Banner" class="img-responsive img-circle" />
                                <span class="overlay1">
                                    <div class="text fs-14 pt-4" style="background: #FFF;padding: 20px;opacity: .8;border-radius: 9px;">Click to Change Banner Image</div>
                                </span>
                                <input type="file" name="image" class="image" id="upload_image" style="display:none" />
                            </label>
                        </form>
                    </div>
                    <div class="row">
                         <div class="col-md-12">
                            <div class="list-single-main-item fl-wrap block_box">
                                <div class="list-single-main-item-title">
                                    <h3>Company Information</h3>
                                </div>
                                <div class="list-single-main-item_content fl-wrap">
                                    {!!$corporate['about']!!}
                                </div>

                                <div class="list-single-main-item-title mt-5 text-gray"><h5>INDUSTRIES</h5></div>
                                <div class="list-single-main-item_content fl-wrap">
                                    {!!$corporate['industries']!!}
                                </div>
                                <div class="list-single-main-item-title mt-5 text-gray"><h5>SECTORS </h5></div>
                                <div class="list-single-main-item_content fl-wrap">
                                    {!!$corporate['sectors']!!}
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="modal" id="upload_Logo">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h6 class="modal-title">Upload {{$corporate['name']}} Logo</h6>
                        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
                    </div>
                    <form action="{{route('user.corporate.logo.upload')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" value="{{$corporate['id']}}" name="id">
                        <input type="hidden" value="corporate" name="type">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="form-label">Add Logo</div>
                                <input type="file" class="form-control" name="upload_logo">

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-indigo" type="submit">Upload Logo</button>
                            <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal" id="add_sectors_industries">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h6 class="modal-title">Add Industries & Sectors</h6>
                        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
                    </div>
                    <form action="{{route('user.corporate.sectors')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" value="{{$corporate['id']}}" name="corporate_id">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="form-label">Industries you serve</label>
                                <select class="form-control select2 select2-hidden-accessible" data-placeholder="Choose Industries" name="industries[]" multiple="" tabindex="-1" aria-hidden="true">
                                    {{ $industries= App\Traits\HelperTrait::getIndustries() }}
                                    @foreach ($industries as $industry)
                                        <option value="{{$industry['industry']}}">
                                            {{$industry['industry']}}
                                        </option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Sector you serve</label>
                                <select class="form-control select2 select2-hidden-accessible" data-placeholder="Choose Sector" name="sectors[]" multiple="" tabindex="-1" aria-hidden="true">
                                    {{ $sectors= App\Traits\HelperTrait::getSectors() }}
                                    @foreach ($sectors as $sector)
                                        <option value="{{$sector['sector_name']}}">
                                            {{$sector['sector_name']}}
                                        </option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-indigo" type="submit">Save changes</button>
                            <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="modal" id="edit_corporate">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h6 class="modal-title">Edit Corporation</h6>
                        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
                    </div>
                    <form action="{{route('user.update.corporate')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{$corporate['id']}}">

                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">Corporation Name <span class="text-red">*</span></label>
                                        <input type="text" required class="form-control" value="{{$corporate['name']}}" placeholder="corporate Company Name" name="name"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Company Tagline <span class="text-red">*</span></label>
                                        <input type="text" required class="form-control" id="date" value="{{$corporate['tagline']}}"  placeholder=" Company Tagline" name="tagline">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Address <span class="text-red">*</span></label>
                                        <input type="text" required class="form-control" placeholder="Company Address" value="{{$corporate['address']}}" name="address">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Email <span class="text-red">*</span></label>
                                        <input type="text" required class="form-control" placeholder="Company Email" value="{{$corporate['email']}}" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Facebook </label>
                                        <input type="text" class="form-control" id="facebook" placeholder="Facebook link" value="{{$corporate['facebook']}}" name="facebook">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Twitter </label>
                                        <input type="text" class="form-control" id="twitter" placeholder="Twitter link" value="{{$corporate['twitter']}}" name="twitter">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Phone Number <span class="text-red">*</span></label>
                                        <input type="text" required class="form-control" placeholder="Company Phone Number" value="{{$corporate['phone_number']}}" name="phone_number"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Website </label>
                                        <input type="text" class="form-control" id="date" placeholder="Website" name="website"  value="{{$corporate['website']}}">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">LinkedIn </label>
                                        <input type="text" class="form-control" id="linkedIn" placeholder="LinkedIn link" value="{{$corporate['linkedIn']}}" name="linkedIn">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Youtube </label>
                                        <input type="text" class="form-control" id="twitter" placeholder="Youtube link" value="{{$corporate['youtube']}}" name="youtube">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputPhone" class="form-label">Describe this Corporation</label>
                                        <textarea class="form-control" id="description" name="about" rows="4"> {!! $corporate['about']  !!}</textarea>
                                    </div>
                                </div>



                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-indigo" type="submit">Update Corporation</button>
                            <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Crop Image Before Upload</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="img-container">
                            <div class="row">
                                <div class="col-md-8">
                                    <img src="" id="sample_image" />
                                </div>
                                <div class="col-md-4">
                                    <div class="preview"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="crop" class="btn btn-primary">Crop</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


        @section("script")
            <script>

                $(document).ready(function() {
                    $('#description').summernote({height: 150});
                });

            </script>
            <script>

                $(document).ready(function(){
                    var $modal = $('#modal');
                    var image = document.getElementById('sample_image');
                    var cropper;


                    $('#upload_image').change(function(event){
                        var files = event.target.files;

                        var done = function(url){
                            image.src = url;
                            $modal.modal('show');
                        };

                        if(files && files.length > 0)
                        {
                            reader = new FileReader();
                            reader.onload = function(event)
                            {
                                done(reader.result);
                            };
                            reader.readAsDataURL(files[0]);
                        }
                    });

                    $modal.on('shown.bs.modal', function() {
                        cropper = new Cropper(image, {
                            aspectRatio: 860/ 360,
                            viewMode:3,
                            preview:'.preview'
                        });
                    }).on('hidden.bs.modal', function(){
                        cropper.destroy();
                        cropper = null;
                    });

                    $('#crop').click(function(){
                        canvas = cropper.getCroppedCanvas({
                            width:900,
                            height:360
                        });

                        canvas.toBlob(function(blob){
                            url = URL.createObjectURL(blob);
                            var reader = new FileReader();
                            reader.readAsDataURL(blob);
                            reader.onloadend = function(){
                                var base64data = reader.result;
                                $.ajax({
                                    url:'{{route('user.corporate.banner')}}',
                                    method:'POST',
                                    data:{image:base64data,id:{{$corporate['id']}},'type':'corporate'},
                                    success:function(data)
                                    {
                                        $modal.modal('hide');
                                        history.go(0);
                                    }
                                });
                            };
                        });
                    });

                });
            </script>

@endsection
