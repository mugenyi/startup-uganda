@extends('build.user')
@section('top_scripts')
    <link rel="stylesheet" href="https://unpkg.com/dropzone/dist/dropzone.css" />
    <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
    <script src="https://unpkg.com/dropzone"></script>
    <script src="https://unpkg.com/cropperjs"></script>
@endsection
@section('profile-summary')
    <div class="row pt-6 pr-4 pl-4">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title mb-0">{{$government['name']}}</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Government</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="#">{{$government['name']}}</a></li>
                    </ol>
                </div>
                <div class="page-rightheader">
                    <a href="" class="text-primary"><i class="fa fa-pencil"></i> Edit Agency</a>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('user-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                @include('user.partial.menu')
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="dropdown mb-4">
                    <button class="btn btn-orange pull-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Actions
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#" data-target="#upload_Logo" data-toggle="modal"><i class="fa fa-image"></i> Upload Logo</a>
                        <a class="dropdown-item" href="#" data-target="#upload_Cover" data-toggle="modal"><i class="fa fa-image"></i> Upload Cover Image</a>
                    </div>
                </div>
                <div class="page-rightheader mb-4">


                </div>
                <div class="row" style="width: 100%">
                    <div class="container" style="padding-top:30px">
                        <div class="row">
                            <div class="col-md-2 col-sm-12">
                                <img alt="{{$government['name']}}" class="rounded-circle border mb-4 brround" style="width:100px;height:100px; margin-left: auto;margin-right: auto" src="{{secure_asset(env('LOGO').$government['logo'])}}">
                            </div>
                            <div class="col-md-10 col-sm-12">
                                <div class="row">
                                    <div class="col-md-9 ">
                                        <h5>{{$government['name']}} </h5>
                                        <div class="geodir-category-location fl-wrap">
                                            <i class="fa fa-map-marker ml-3"></i>  {{$government['address']}} <br/>
                                            <i class="fa fa-globe ml-3"></i> {{$government['website']}}
                                        </div>
                                        <div class="fl-wrap text-white">
                                            @if(!is_null($government['facebook']))<a href="{{$government['facebook']}}" target="_blank"><i class="fa fa-facebook fa-1x ml-3"></i></a>@endif
                                            @if(!is_null($government['twitter']))<a href="{{$government['twitter']}}" target="_blank"><i class="fa fa-twitter ml-3"></i></a>@endif
                                            @if(!is_null($government['linkedin']))<a href="{{$government['linkedin']}}" target="_blank"><i class="fa fa-linkedin ml-3"></i></a>@endif
                                            @if(!is_null($government['youtube']))<a href="{{$government['youtube']}}" target="_blank"><i class="fa fa-youtube ml-3"></i></a>@endif
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="container">


                    <div class="row">
                        <div class="image_area mt-4">
                            <form method="post">
                                @csrf
                                <input type="hidden" value="{{$government['id']}}" name="id">
                                <input type="hidden" value="startup" name="type">

                                <label for="upload_image">
                                    <img src="{{secure_asset(env('FULL').$government['cover'])}}" id="uploaded_image" class="img-responsive img-circle" />
                                    <div class="overlay1">
                                        <div class="text fs-14 pt-4" style="background: #FFF;padding: 20px;opacity: .8;border-radius: 9px;">Click to Change Banner Image</div>
                                    </div>
                                    <input type="file" name="image" class="image" id="upload_image" style="display:none" />
                                </label>
                            </form>
                        </div>

                        <div class="col-md-12">
                            <div class="list-single-main-item fl-wrap block_box">
                                <div class="list-single-main-item-title">
                                    <h3>Company Information</h3>
                                </div>
                                <div class="list-single-main-item_content fl-wrap">
                                    {!!$government['about']!!}
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="row mt-6">
                        <div class="col-md-6">
                            <h5>OUR TEAM</h5>
                            <div class="row m-4 right">
                                <a href="" class="mr-3" data-target="#add_team" data-toggle="modal"><i class="fa fa-user-plus text-orange"></i> Add Team Member</a>
                            </div>
                            <h6 class="mt-4 text-muted">TEAM MEMBERS</h6>
                            @foreach ($teams as $team)
                                <div class="media mt-0 mb-3">
                                    <figure class="rounded-circle align-self-start mb-0">
                                        <img src="{{secure_asset('public/uploads/user/'.$team['photo'])}}" alt="Generic placeholder image" class="avatar brround avatar-md mr-3">
                                    </figure>
                                    <div class="media-body">
                                        <h6 class="time-title p-0 mb-0 font-weight-semibold leading-normal">{{$team->member_name}}</h6>
                                        {{$team['designation']}} - {{$team['memberType']=='teamMember'?'Team Member':'Board Member'}}
                                    </div>
                                </div>
                            @endforeach

                            <h6 class="mt-4 text-muted">BOARD MEMBERS</h6>
                            @foreach ($boards as $board)
                                <div class="media mt-0 mb-3">
                                    <figure class="rounded-circle align-self-start mb-0">
                                        <img src="{{secure_asset('public/uploads/user/'.$board['photo'])}}" alt="Generic placeholder image" class="avatar brround avatar-md mr-3">
                                    </figure>
                                    <div class="media-body">
                                        <h6 class="time-title p-0 mb-0 font-weight-semibold leading-normal">{{$board->member_name}}</h6>
                                        {{$board['designation']}} - {{$board['memberType']=='teamMember'?'Team Member':'Board Member'}}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="modal" id="add_team">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content modal-content-demo">
                                <div class="modal-header">
                                    <h6 class="modal-title">Add Team Member</h6>
                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
                                </div>
                                <form action="{{route('user.startup.member.add')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" value="{{$government['id']}}" name="category_id">
                                    <input type="hidden" value="government" name="category">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1" class="form-label">Team Member Name</label>
                                            <input type="text" class="form-control" id="memberName" name="name" placeholder="Enter member name">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1" class="form-label">Designation</label>
                                            <input type="text" class="form-control" id="designation" name="designation" placeholder="Enter member position">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1" class="form-label">Designation</label>
                                            <select class="form-control" name="memberType">
                                                <option value="teamMember">Team Member</option>
                                                <option value="boardMember">Board Member</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <div class="form-label">Add Team member </div>
                                            <input type="file" class="form-control" name="photo"/>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-indigo" type="submit">Create Member</button>
                                        <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

        {{-- @include('user.includes.investor-modals') --}}

        <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Crop Image Before Upload</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="img-container">
                            <div class="row">
                                <div class="col-md-8">
                                    <img src="" id="sample_image" />
                                </div>
                                <div class="col-md-4">
                                    <div class="preview"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="crop" class="btn btn-primary">Crop</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" id="upload_Logo">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h6 class="modal-title">Upload {{$government['name']}} Logo</h6>
                        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
                    </div>
                    <form action="{{route('user.government.logo.upload')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" value="{{$government['id']}}" name="id">
                        <input type="hidden" value="government" name="type">
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="form-label">Add Logo</div>
                                <input type="file" class="form-control" name="upload_logo"/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-indigo" type="submit">Save changes</button>
                            <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        @endsection

        @section('script')
            <script>

                $(document).ready(function(){
                    var $modal = $('#modal');
                    var image = document.getElementById('sample_image');
                    var cropper;

                    $('#upload_image').change(function(event){
                        var files = event.target.files;

                        var done = function(url){
                            image.src = url;
                            $modal.modal('show');
                        };

                        if(files && files.length > 0)
                        {
                            reader = new FileReader();
                            reader.onload = function(event)
                            {
                                done(reader.result);
                            };
                            reader.readAsDataURL(files[0]);
                        }
                    });

                    $modal.on('shown.bs.modal', function() {
                        cropper = new Cropper(image, {
                            aspectRatio: 860/ 360,
                            viewMode:3,
                            preview:'.preview'
                        });
                    }).on('hidden.bs.modal', function(){
                        cropper.destroy();
                        cropper = null;
                    });

                    $('#crop').click(function(){
                        canvas = cropper.getCroppedCanvas({
                            width:900,
                            height:360
                        });

                        canvas.toBlob(function(blob){
                            url = URL.createObjectURL(blob);
                            var reader = new FileReader();
                            reader.readAsDataURL(blob);
                            reader.onloadend = function(){
                                var base64data = reader.result;
                                $.ajax({
                                    url:'{{route('user.government.banner')}}',
                                    method:'POST',
                                    data:{image:base64data,id:{{$government['id']}},'type':'government'},
                                    success:function(data)
                                    {
                                        $modal.modal('hide');
                                        history.go(0);
                                    }
                                });
                            };
                        });
                    });

                });
            </script>

@endsection
