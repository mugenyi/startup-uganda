@extends('build.user')
@section('profile-summary')
    <div class="row pt-6 pr-4 pl-4">
        <div class="container">
            <div class="page-header">
                <div class="page-leftheader">
                    <h4 class="page-title mb-0 d-none d-sm-block">{{$mentor['name']}}</h4>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}"><i class="fa fa-home mr-2 fs-14"></i>Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Mentor</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><a href="#">{{$mentor['name']}}</a></li>
                    </ol>
                </div>
                <div class="page-rightheader">
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('user-content')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4 col-sm-12">
                @include('user.partial.menu')
            </div>
            <div class="col-xl-9 col-lg-8 col-sm-12">
                <div class="dropdown mb-4">
                    <button class="btn btn-orange pull-right dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Actions
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#" data-target="#edit_mentor" data-toggle="modal"><i class="fa fa-pencil"></i> Edit</a>

                    </div>
                </div>


                <div class="row" style="width: 100%">
                    <div class="col-md-2">
                        <img alt="User Avatar" class="rounded-circle border p-0" style=" margin-left:auto;margin-right:auto" src="{{secure_asset(env('LOGO').''.$mentor['photo'])}}">
                    </div>
                    <div class="col-md-3">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <h6 class="mb-1">{{$mentor['name']}}</h6>
                                </div>
                                <span class="mb-0 fs-13 text-muted">Name</span>
                            </div>
                        </div>
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <h6 class="mb-1">{{$mentor['email']}}</h6>
                                </div>
                                <span class="mb-0 fs-13 text-muted">Email</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <h6 class="mb-1">{{ $mentor['gender']}}</h6>
                                </div>
                                <span class="mb-0 fs-13 text-muted">Gender</span>
                            </div>
                        </div>
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <h6 class="mb-1">{{$mentor['phone_number']}}</h6>
                                </div>
                                <span class="mb-0 fs-13 text-muted">Phone Number</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <h6 class="mb-1">{{$mentor['location']}}</h6>
                                </div>
                                <span class="mb-0 fs-13 text-muted">Location</span>
                            </div>
                        </div>
                        <div class="media mb-1">
                            <div class="media-body">
                                <div class="d-md-flex align-items-center mt-1">
                                    <h6 class="mb-1">{{ $mentor['created_at']}}</h6>
                                </div>
                                <span class="mb-0 fs-13 text-muted">Created At</span>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="container mt-7">
                    <div class="row">
                        @if($mentor->user->role =='admin')
                            <a href="" class="text-success mr-3" data-target="#send_claim" data-toggle="modal"><i class="fa fa-envelope"></i> Send Claim Email</a>
                        @endif
                        <div class="col-lg-12 col-md-auto">
                            <div class="card-body">
                                <h4 class="pro-user-username centered mb-3 font-weight-bold">{{$mentor['name']}} </h4>
                                <div class="main-profile-bio mb-0">
                                    <p>{!!$mentor['about']!!}</p>
                                </div>


                                <div class="row mt-5">
                                    <div class="col-12">
                                        <h5>ABOUT</h5>
                                        <br/>
                                        <table class="table table-vcenter text-wrap fs-12">
                                            <tr class="p-3 " >
                                                <td class="text-uppercase pl-4" style="width: 20%">What i do</td>
                                                <td class="mb-4 text-justify">{!!$mentor['what_i_do']!!}</td>
                                            </tr>
                                            <tr class="p-3">
                                                <td class="text-uppercase fs-12 pl-4">Achievements</td>
                                                <td class="mb-4 text-justify">{!!$mentor['achievements']!!}</td>
                                            </tr>
                                            <tr class="p-3">
                                                <td class="text-uppercase fs-12 pl-4">What i'm looking for</td>
                                                <td class="mb-4 text-justify">{!!$mentor['looking_for']!!}</td>
                                            </tr>
                                            <tr class="p-3">
                                                <td class="text-uppercase fs-12 pl-4">Markets</td>
                                                <td class="mb-4 text-justify">{!!$mentor['markets']!!}</td>
                                            </tr>
                                        </table>


                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="modal" id="edit_mentor">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Create New Mentor</h6>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
                </div>
                <form action="{{route('user.mentor.update')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="mentor_id" value="{{$mentor->id}}">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Mentor Name <span class="text-red">*</span></label>
                                    <input type="text" required class="form-control" value="{{$mentor->name}}" placeholder="Mentor Name" name="name">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Gender<span class="text-red">*</span></label>
                                    <select class="form-control" name="gender">
                                        <option value="Female" @if($mentor->name=='Female') selected="selected" @endif>Female</option>
                                        <option value="Male" @if($mentor->name=='Male') selected="selected" @endif>Male</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Mentor Photo <span class="text-red">*</span></label>
                                    <div class="input-group file-browser mb-5">
                                        <input type="file" class="form-control border-right-0 browse-file" name="image" placeholder="choose" >

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="form-label">Location <span class="text-red">*</span></label>
                                    <input type="text" required class="form-control" value="{{$mentor->location}}" placeholder="Your location" name="location">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Email <span class="text-red">*</span></label>
                                    <input type="text" required class="form-control" value="{{$mentor->email}}" placeholder="Email Address" name="email">
                                </div>

                                <div class="form-group">
                                    <label class="form-label">Phone Number <span class="text-red">*</span></label>
                                    <input type="text" required class="form-control" value="{{$mentor->phone_number}}" placeholder="Phone Number" name="phone_number">
                                </div>

                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputPhone" class="form-label">About Me</label>
                                    <textarea class="form-control" name="about" id="about" rows="5">{!! $mentor->about !!}</textarea>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Investment sector</label>
                                    <select class="form-control select2 select2-hidden-accessible" data-placeholder="Choose Sector" name="sectors[]" multiple="" tabindex="-1" aria-hidden="true">
                                        {{ $sectors= App\Traits\HelperTrait::getSectors() }}
                                        @foreach ($sectors as $sector)
                                            <option value="{{$sector['sector_name']}}">
                                                {{$sector['sector_name']}}
                                            </option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPhone" class="form-label">What i do</label>
                                    <textarea class="form-control" id="what_i_do" name="what_i_do" rows="4">{!! $mentor->what_i_do !!}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPhone" class="form-label">Looking for</label>
                                    <textarea class="form-control" id="looking_for" name="looking_for" rows="4">{!! $mentor->looking_for !!}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPhone" class="form-label">My Achievements</label>
                                    <textarea class="form-control" id="achievements" name="achievements" rows="4">{!! $mentor->achievements !!}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPhone" class="form-label">Interested in Markets</label>
                                    <textarea class="form-control" id="markets" placeholder="Which markets are you interested in" name="markets" rows="3">{!! $mentor->markets !!}</textarea>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-indigo" type="submit">Update Mentor</button>
                        <button class="btn btn-secondary" data-dismiss="modal" type="button">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <!-- Initialize Quill editor -->
    <script>

        $(document).ready(function() {
            $('#description').summernote({height: 150});
            $('#what_i_do').summernote({height: 100});
            $('#achievements').summernote({height: 100});
            $('#about').summernote({height: 150});

        });
    </script>

@endsection

