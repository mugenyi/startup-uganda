@extends('build.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-9 pl-5 pt-1 mt-4" style="background-color: #FFF">
                <div class="container">
                    <div class="row mt-0">
                        <div class="media" style="height: 110px;">
                            <div class="mr-3 mt-1">
                                @if ($deal->category == "Job")
                                    <img alt="{{$deal['name']}}" alt="img" class="avatar-xxl mr-2 brround p-1 shadow2" src="{{secure_asset('public/theme/images/job.png')}}">
                                @elseif($deal->category == "Bid")
                                    <img alt="{{$deal['name']}}" alt="img" class="avatar-xxl mr-2 brround p-1 shadow2" src="{{secure_asset('public/theme/images/bid.png')}}">
                                @else
                                    <img alt="{{$deal['name']}}" alt="img" class="avatar-xxl mr-2 brround p-1 shadow2" src="{{secure_asset('public/theme/images/other.png')}}">
                                @endif
                                 </div>
                            <div class="media-body mt-4 pr-4">
                                <div class="fs-20 text-bolder text-gray3 font-weight-normal1 pt-4" style="font-weight:bolder !important"> {{$deal->name}} </div>
                                <small class="fs-13 text-gray2"></small>
                            </div>
                        </div>
                        <hr/>
                        <div class="row" style="width: 100%">
                            <div class="col-lg-4">
                                <div class="media mb-1">
                                    <div class="media-body">
                                        <div class="d-md-flex align-items-center mt-1">
                                            <p class="mb-1  text-gray3 font-weight-semibold">{{$deal->category}}</p>
                                        </div>
                                        <span class="mb-0 fs-13  text-gray2">Category</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="media">
                                    <div class="media-body">
                                        <div class="d-md-flex align-items-center mt-1">
                                            <p class="mb-1  text-gray3 font-weight-semibold">{{$deal->deadline}}</p>
                                        </div>
                                        <span class="mb-0 fs-13  text-gray2">Deadline</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="media">
                                    <div class="media-body">
                                        <div class="d-md-flex align-items-center" style="">
                                            <a href="{{secure_asset(env('FULL').$deal['attachment'])}}" class="btn btn-primary" target="_blank">
                                                <i class="fa fa-download"></i> Download Document</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-12 mt-5">
                            {!! $deal->brief !!}
                        </div>
                        <div class="col-lg-12 mt-5">
                            <h5>Roles</h5>
                            <hr/>
                            {!! $deal->roles !!}
                        </div>

                        <div class="col-lg-12 mt-5">
                            <h5>Required Skills</h5>
                            <hr/>
                            {!! $deal->roles !!}
                        </div>
                        <div class="col-lg-12 mt-5">
                            <h5>How to Apply</h5>
                            <hr/>
                            {!! $deal->application_guidelines !!}
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-lg-3 d-none d-sm-block mt-8" style="">
                <h4 class="text-orange">Other Available Offers</h4>

                <div class="row mt-3">
                    @foreach ($deals as $other)
                        <div class="col-md-12">
                            <a href="{{route('deal.view',['id'=>$other->id,'slug'=>App\Traits\HelperTrait::slugify($other->name)])}}">
                                <div class="card box-widget widget-user">
                                    <div class="widget-user-image mt-5">
                                        @if ($other->category == "Job")
                                            <img alt="{{$deal['name']}}" class="rounded-circle" src="{{secure_asset('public/theme/images/job.png')}}">
                                        @elseif($other->category == "Bid")
                                            <img alt="{{$other['name']}}" class="rounded-circle" src="{{secure_asset('public/theme/images/bid.png')}}">
                                        @else
                                            <img alt="{{$other['name']}}" class="rounded-circle" src="{{secure_asset('public/theme/images/other.png')}}">
                                        @endif
                                    </div>
                                    <div class="card-body text-center">
                                        <div class="pro-user">
                                            <a href=""><h6 class="pro-user-username text-dark mb-1 font-weight-bold">{{$other['name']}}</h6></a>
                                            <p class="pro-user-desc text-muted"> <i class="fa fa-clock-o"></i> {{$other['deadline']}} </p>

                                        </div>
                                    </div>
                                </div>
                            </a>

                        </div>

                    @endforeach
                </div>
            </div>
        </div>

    </div>
@endsection
