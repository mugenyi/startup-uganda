@extends('build.master')
@section('content')
    <!-- carousel -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">

    <div class="row">
        <div id="carousel-indicators4" class="carousel slide" style="width: 100%; margin-top:-90px" data-ride="carousel">
            <ol class="carousel-indicators carousel-bottom-right-indicators">
                <li data-target="#carousel-indicators4" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-indicators4" data-slide-to="1" class=""></li>
                <li data-target="#carousel-indicators4" data-slide-to="2" class=""></li>
                <li data-target="#carousel-indicators4" data-slide-to="3" class=""></li>
                <li data-target="#carousel-indicators4" data-slide-to="4" class=""></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="col-xs-12 col-md-12" style="background: url({{secure_asset('public/images/slider-1.png')}}) no-repeat;background-size:cover;height: auto;min-height: 600px">
                    </div>
                    <div class="carousel-item-background"></div>
                    <div class="carousel-caption" style="margin-top: -30px">
                        <h2 class="fs-30 bolder mb-6" style="font-weight: bold">Join Uganda's largest<br/>startup community</h2>
                        <p class="fs-18">Experience the strong ecosystem - invest in<br/>startups, research the fastest-growing<br/>companies, and find a job you love<br/>

                            <a href="{{route('register')}}">
                                <button class="mt-4 pl-5 pr-5 pt-2 pb-2 bg-orange-dark text-white fs-15 btn" style="border: none;padding:10px 20px">Register Now</button>
                            </a>
                        </p>


                    </div>
                </div>
                <div class="carousel-item">
                    <div class="col-xs-12 col-md-12" style="background: url({{secure_asset('public/images/slider-2.png')}}) no-repeat;background-size:cover;height: auto;min-height: 600px">
                    </div>
                    {{--                    <img class="d-block img-fluid" alt="" src="{{secure_asset('public/images/slider-2.png')}}" style="min-height:320px;height: auto;width:100%;" data-holder-rendered="true">--}}
                    <div class="carousel-item-background"></div>
                    <div class="carousel-caption" style="margin-top: -40px">
                        <h2 class="fs-30 bolder mb-6" style="font-weight: bold">Join Uganda's largest<br/>startup community</h2>
                        <p class="fs-18">Experience the strong ecosystem - invest in<br/>startups, research the fastest-growing<br/>companies, and find a job you love<br/>

                            <a href="{{route('register')}}">
                                <button class="mt-4 pl-5 pr-5 pt-2 pb-2 bg-orange-dark text-white fs-15 btn" style="border: none;padding:10px 20px">Register Now</button>
                            </a>
                        </p>


                    </div>
                </div>
                <div class="carousel-item">
                    <div class="col-xs-12 col-md-12" style="background: url({{secure_asset('public/images/slider-3.png')}}) no-repeat;background-size:cover;height: auto;min-height: 600px">
                    </div>
                    {{--                    <img class="d-block img-fluid center" alt="" src="{{secure_asset('public/images/slider-3.png')}}" style="min-height:320px;height: auto;width:100%;" data-holder-rendered="true">--}}
                    <div class="carousel-item-background"></div>
                    <div  class="carousel-caption" style="margin-top: -40px">
                        <h2 class="fs-30 bolder mb-6" style="font-weight: bold">Join Uganda's largest<br/>startup community</h2>
                        <p class="fs-18">Experience the strong ecosystem - invest in<br/>startups, research the fastest-growing<br/>companies, and find a job you love<br/>

                            <a href="{{route('register')}}">
                                <button class="mt-4 pl-5 pr-5 pt-2 pb-2 bg-orange-dark text-white fs-15 btn" style="border: none;padding:10px 20px">Register Now</button>
                            </a>
                        </p>

                    </div>
                </div>
                <div class="carousel-item ">
                    <div class="col-xs-12 col-md-12" style="background: url({{secure_asset('public/images/slider-4.png')}}) no-repeat;background-size:cover;height: auto;min-height: 600px">
                    </div>
                    {{--                    <img class="d-block img-fluid" alt="" src="{{secure_asset('public/images/slider-4.png')}}" style="min-height:320px;height: auto;width:100%;" data-holder-rendered="true">--}}
                    <div class="carousel-item-background"></div>
                    <div  class="carousel-caption" style="margin-top: -40px">
                        <h2 class="fs-30 bolder mb-6" style="font-weight: bold">Join Uganda's largest<br/>startup community</h2>
                        <p class="fs-18">Experience the strong ecosystem - invest in<br/>startups, research the fastest-growing<br/>companies, and find a job you love<br/>

                            <a href="{{route('register')}}">
                                <button class="mt-4 pl-5 pr-5 pt-2 pb-2 bg-orange-dark text-white fs-15 btn" style="border: none;padding:10px 20px">Register Now</button>
                            </a>
                        </p>

                    </div>
                </div>
                <div class="carousel-item">
                    <div class="col-xs-12 col-md-12" style="background: url({{secure_asset('public/images/slider-5.png')}}) no-repeat;background-size:cover;height: auto;min-height: 600px">
                    </div>
                    {{--                    <img class="d-block img-fluid" alt="" src="{{secure_asset('public/images/slider-5.png')}}" style="min-height:320px;height: auto;width:100%;" data-holder-rendered="true">--}}
                    <div class="carousel-item-background"></div>
                    <div class="carousel-caption" style="margin-top: -40px">
                        <h2 style="font-size: 40px">Join Uganda's <span class="text-orange">largest</span><br/><span class="text-orange">startup</span> community</h2>
                        <p class="fs-18">Experience the strong ecosystem - invest in<br/>startups, research the fastest-growing<br/>companies, and find a job you love<br/>

                            <a href="{{route('register')}}">
                                <button class="mt-4 pl-5 pr-5 pt-2 pb-2 bg-orange-dark text-white fs-15 btn" style="border: none;padding:10px 20px">Register Now</button>
                            </a>
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- HIGHLIGHTS --}}
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8 ">
                    <div class="row mt-6 mb-1">
                        <div class="col-md-6 col-lg-4 p-2">
                            <div  class="flex md4 section-explore">
                                <a href="{{route('startups')}}" class="explore-card" style="background-image: url({{secure_asset('public/images/network-2.jpg')}});">
                                    <img src="{{secure_asset('public/images/icons/network.png')}}" style="height: 50px"/>
                                    <h3>NETWORK</h3>
                                    <div class="subtitle">
                                        within the startup ecosystem
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 p-2 br-3">
                            <div  class="flex md4 section-explore">
                                <a href="{{route('programs')}}" class="explore-card" style="background-image: url({{secure_asset('public/images/network-1.jpg')}});">
                                    <img src="{{secure_asset('public/images/icons/participate.svg')}}" style="height: 50px"/>
                                    <h3 data-v-1cb787d0="">PARTICIPATE</h3>
                                    <div data-v-1cb787d0="" class="subtitle">
                                        in challenges and programs
                                    </div>
                                </a>

                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 p-2">
                            <div  class="flex md4 section-explore"  >
                                <a href="{{route('investors')}}" class="explore-card" style="background-image: url({{secure_asset('public/images/network-3.jpg')}});">
                                    <img src="{{secure_asset('public/images/icons/market-access.png')}}" style="height: 50px"/>
                                    <h3 >MARKET ACCESS</h3>
                                    <div class="subtitle">
                                        find an investor or partner to scale
                                    </div>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-2"></div>
            </div>

        </div>
    </div>

    {{-- END HIGHLIGHTS --}}


    {{--    <div class="row bg-white">--}}
    {{--        <div class="container">--}}
    {{--            <div class="row mt-6 mb-1">--}}
    {{--                <div class="col-md-6 col-lg-4 p-6">--}}
    {{--                    <div class="card-2 overflow-hidden1">--}}
    {{--                        <div class="mb-2 shadow2">--}}
    {{--                            <img src="{{secure_asset('public/images/network-image.png')}}" alt="image">--}}
    {{--                        </div>--}}
    {{--                        <div class="card-body text-center">--}}
    {{--                            <h5 class="card-title text-orange" style="font-size: 16px !important">NETWORK</h5>--}}
    {{--                            <p class="card-text text-gray2 text-uppercase">with the startup<br/>ecosystem</p>--}}
    {{--                            <a href="{{route('startups')}}" class="text-orange">Learn more</a>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-md-6 col-lg-4 p-6">--}}
    {{--                    <div class="card-2 overflow-hidden1">--}}
    {{--                        <div class="mb-2 shadow2">--}}
    {{--                            <img src="{{secure_asset('public/images/participate-image.png')}}" alt="image">--}}
    {{--                        </div>--}}
    {{--                        <div class="card-body text-center">--}}
    {{--                            <h5 class="card-title text-orange " style="font-size: 16px !important">PARTICIPATE</h5>--}}
    {{--                            <p class="card-text text-gray2 text-uppercase">in challenges &<br/>programs</p>--}}
    {{--                            <a href="{{route('programs')}}" class="text-orange">Learn more</a>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-md-6 col-lg-4 p-6">--}}
    {{--                    <div class="card-2 overflow-hidden1">--}}
    {{--                        <div class="mb-2 shadow2">--}}
    {{--                            <img src="{{secure_asset('public/images/job-image.png')}}"  alt="image">--}}
    {{--                        </div>--}}
    {{--                        <div class="card-body text-center">--}}
    {{--                            <h5 class="card-title text-orange" style="font-size: 16px !important">FUNDING</h5>--}}
    {{--                            <p class="card-text text-gray2 text-uppercase">useful tools &<br/>resources for free</p>--}}
    {{--                            <a href="{{route('investors')}}" class="text-orange">Learn more</a>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}

    <div class="row bg-white pt-3">
        <div class="container">
            <h2 class="startupTitleHr fs-18 mb-6 text-dark-gray font-weight-bold"><span>NEWS</span></h2>

            <div class="container">
                <div class="row">
                    <div class="new-arrival">
                       <div class="titlebar">
                           <div class="next-back">
                               <span><a data-slide="prev" href="#Carousel" class="left carousel-control">Back</a></span>
                                <span><a data-slide="next" href="#Carousel" class="pull-right carousel-control">Next</a></span>
                            </div>
                        </div>
                        <div class="arrival-product">
                            <div id="Carousel" class="carousel slide" data-ride="carousel">
                                <!-- Carousel items -->
                                <div class="carousel-inner">

                                    <?php $posts = array_chunk($feeds,4); ?>
                                        @for ($i = 0; $i <= count($posts)-1; $i++)
                                                @if($i==0)
                                            <div class="carousel-item active">
                                                @else
                                                    <div class="carousel-item">
                                                    @endif
                                                <div class="arrival-item row">

                                                        @foreach ($posts[$i] as $feed)
                                                            <div class="col-md-3">
                                                                <a href="{{route('post.view',['slug'=>Illuminate\Support\Str::slug($feed['post_title']).'_'.$feed['id']])}}" style="">
                                                                    <img src="{{secure_asset(env('FULL').$feed['image_name'])}}" class="pt-3" style="width: 100%; height:200px;"  alt="">

                                                                    <p class="pt-4" style="text-transform: capitalize;">{{$feed['post_title']??" Visit Article"}}</p>
                                                                </a>
                                                            </div>
                                                        @endforeach




                                                </div>
                                            </div>
                                        @endfor


                                </div>
                            </div>
                            <!--Carousel-->
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row bg-white mt-4 pt-3">
        <div class="container">
            <h2 class="startupTitleHr fs-18 mb-6 text-dark-gray font-weight-bold"><span>EVENTS AND PROGRAMS</span></h2>
            <div class="row">
                <div class="col-md-6">
                    <h3 class="fs-16 text-gray3 py-4">Featured Programs</h3>
                    @foreach ($programs as $program)
                        <div class="mb-6 px-1" style="width:100%; ">
                            <div class="row">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <img src="{{secure_asset(env('COVER').$program['image'])}}" alt="{{$program['program_name']}}; " class="" style="">

                                </div>
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <h6 class="pt-6" style="font-weight:bold">
                                        <a href="{{route('view.program',['id'=>$program['id'],'slug'=>App\Traits\HelperTrait::slugify($program['program_name'])])}}" style="font-size: 16px" target="_blank" >{{$program['program_name']}}</a>
                                    </h6>
                                    <p>{{ Carbon\Carbon::parse($program->start_date)->diffForHumans()}}</p>
                                </div>
                            </div>
                        </div>

                    @endforeach

                    <a href="{{route('programs')}}" class="btn btn-outline-primary">View all programs</a>
                </div>
                <div class="col-md-6">
                    <h3 class="fs-16 text-gray3 py-4">Upcoming Events</h3>
                    @foreach ($events as $event)
                        <div class="mb-5 px-1" style="width:100%; ">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="event_date">
                                        <span class="day">{{date('d', strtotime($event['start_date']))}}</span>
                                        <span class="month">{{date('M', strtotime($event['start_date']))}}</span>
                                    </div>
                                </div>
                                <div class="col-md-9  col-sm-9 col-xs-9">
                                    <h6 class="pt-3" style="font-weight:bold">
                                        <a href="{{route('view.event',['id'=>$event['id'],'slug'=>App\Traits\HelperTrait::slugify($event['event_name'])])}}" style="font-size: 16px" target="_blank" >{{$event['event_name']}}</a>
                                    </h6>
                                    <p>Hosted by {{$event['organiser'].' - '.$event['location']}}</p>
                                </div>
                            </div>
                        </div>

                    @endforeach
                    <a href="{{route('events')}}" class="btn btn-outline-primary">View all events</a>
                </div>

            </div>

        </div>
    </div>

    <div class="row bg-dark-gray text-white pt-8 mt-7">
        <div class="container">
            <h2 class="fs-18 bolder text-white mb-4" style="font-weight: bold">USE OUR VAST <span class="text-orange">ONLINE DIRECTORY</span><br/> TO CONNECT WITH OTHER STAKEHOLDERS</h2>
            <hr style="height: 1px;background-color: #FFF;border: none;"/>
            <div class="row mt-4">
                <div class="col-md-6 col-lg-4">
                    <div class="d-flex mb-4">
                        <div class="pr-6">
                            <img src="{{secure_asset('public/images/icons/startups.png')}}" style="height: 50px"/>
                            <span class="fs-15 text-bold font-weight-bold text-orange">Startups</span>
                            <p class="mb-1">Connect with 50K+ Startup spread across all sectors and stages</p>
                        </div>
                    </div>
                    <div class="d-flex mb-8">
                        <div class="pr-6">
                            <img src="{{secure_asset('public/images/icons/accelerators.png')}}" style="height: 50px"/>
                            <span class="fs-15 text-bold font-weight-bold text-orange">Accelerators</span>
                            <p class="mb-1">Explore a wide range of Accelerators & connect for your chance to scale up</p>
                        </div>
                    </div>

                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="d-flex mb-4">
                        <div class="pr-6">
                            <img src="{{secure_asset('public/images/icons/investors.png')}}" style="height: 50px"/>
                            <span class="fs-15 text-bold font-weight-bold text-orange">Investors</span>
                            <p class="mb-1">An opportunity to get in touch with individual / institutional investors</p>
                        </div>
                    </div>
                    <div class="d-flex mb-8">
                        <div class="pr-6">
                            <img src="{{secure_asset('public/images/icons/government.png')}}" style="height: 50px"/>
                            <span class="fs-15 text-bold font-weight-bold text-orange">Government Bodies</span>
                            <p class="mb-1">30+ Government Departments registered on the portal. Register and connect!</p>
                        </div>
                    </div>

                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="d-flex mb-4">
                        <div class="pr-6">
                            <img src="{{secure_asset('public/images/icons/mentors.png')}}" style="height: 50px"/>
                            <span class="fs-15 text-bold font-weight-bold text-orange">Mentors</span>
                            <p class="mb-1">Get a chance to connect with a wide range to Mentors ready to mentor a startup</p>
                        </div>
                    </div>
                    <div class="d-flex mb-8">
                        <div class="pr-6">
                            <img src="{{secure_asset('public/images/icons/incubators.png')}}" style="height: 50px"/>
                            <span class="fs-15 text-bold font-weight-bold text-orange">Incubator</span>
                            <p class="mb-1">Connect with Government, Private and Institutional Incubators registered with us </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="container mt-6 ">
            <h2 class="fs-24 font-weight-bolder text-dark-gray" style="font-weight: bold"><span class="text-orange">STARTUP</span> UGANDA</h2>
            <h2 class="startupTitleHr fs-18 text-dark-gray font-weight-bold"><span>MEMBERS</span></h2>

        </div>
    </div>
    <div class="row bg-white pt-6 pl-4 pr-4">
        <div class="container">
            @include('includes.partners')
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#Carousel').carousel({
                interval: 10000
            })
        });
    </script>
@endsection
