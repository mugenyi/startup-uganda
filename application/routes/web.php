<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@home')->name('index');
Route::get('/about', 'FrontController@aboutUs')->name('about');

Route::get('/test', function () { return view('user.blank');});
Route::get('/network', 'FrontController@network')->name('network');

Route::get('startups','StartupController@startupListing')->name('startups');
Route::get('startup/{slug}','StartupController@startupDetails')->name('startup.details');
Route::post('startup/search','StartupController@searchStartups')->name('startup.search');


Route::get('investors','InvestorController@investorListing')->name('investors');
Route::get('investor/{slug}','InvestorController@investorDetails')->name('investor.details');
Route::post('investor/search','InvestorController@searchInvestors')->name('investor.search');

Route::get('jobs','JobController@jobsListing')->name('jobs');
Route::get('job/{id}/{slug}','JobController@viewJob')->name('job.view');
Route::post('jobs/search', 'JobController@searchJobs')->name('job.search');

Route::get('programs','ProgramController@programsListing')->name('programs');
Route::get('program/{id}/{slug}','ProgramController@programDetails')->name('view.program');
Route::post('program/search','ProgramController@searchPrograms')->name('search.program');
//Program Registry
Route::post('program/register', 'ProgramRegistrationController@register')->name('program.register');

Route::get('events','EventController@eventsListing')->name('events');
Route::get('event/{id}/{slug}','EventController@eventDetails')->name('view.event');
Route::post('events/search','EventController@searchEvents')->name('events.search');

Route::get('/governments','GovernmentController@getAgencies')->name('governments');
Route::get('/government/{id}/{slug}','GovernmentController@viewGovernment')->name('view.government');
Route::post('government/search','GovernmentController@searchGovernments')->name('government.search');

Route::get('/esos','EsoController@getEsos')->name('esos');
Route::get('/eso/{id}/{slug}','EsoController@viewEso')->name('view.eso');
Route::post('eso/search','EsoController@searchEsos')->name('eso.search');

Route::get('/corporations','CorporateController@getCorporates')->name('corporations');
Route::get('/corporation/{id}/{slug}','CorporateController@viewCorporate')->name('view.corporation');
Route::post('corporation/seach','CorporateController@searchCorporations')->name('corporation.search');


Route::get('/mentors','MentorController@getAllMentors')->name('mentors');
Route::get('/mentor/{id}/{slug}','MentorController@viewMentor')->name('view.mentor');
Route::post('/mentors/search','MentorController@searchMentors')->name('mentors.search');

//Feeds
Route::get('feeds','FeedController@index')->name('feeds');
Route::post('feed/create', 'FeedController@store')->name('feed.create');

//Posts
Route::get('post/{slug}', 'PostController@viewPost')->name('post.view');

//Deals
Route::get('deals', 'DealController@getStartUpOffers')->name('deals');
Route::get('deal/{id}/{slug}', 'DealController@viewStartUpOffer')->name('deal.view');


//Claim
Route::get('/claim/profile/{endpoint}','UserController@getClaimProfile')->name('claim.link');

Route::get('login/{provider}', 'SocialController@redirect');
Route::get('login/{provider}/callback','SocialController@Callback');
Route::get('logout', 'UserController@logout')->name('user.logout');

//Newsletter
Route::post('newsletter/store','NewsletterController@store')->name('subscribe');

//Blogs
Route::get('/blogs', 'BlogController@getBlogs')->name('blogs');
Route::get('/blog/{slug}', 'BlogController@viewBlog')->name('blog.view');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/chat/message','ChatController@sendMessage')->name('chat.send.message');

Route::group(['prefix'=>'user','middleware'=>['auth','user']], function(){
    Route::get('/dashboard', 'UserController@dashboard')->name('user.dashboard');
    Route::get('/profile', 'UserController@userProfile')->name('user.profile');
    Route::get('/edit-profile','UserController@editProfile')->name('user.edit-profile');
    Route::post('/edit/profile','UserController@editUserProfile')->name('user.update.profile');
    Route::post('/upload/profile','UserController@profilePhotoUpdate')->name('user.profile.photo');
    Route::post('/password/update','UserController@PasswordUpdate')->name('user.password.update');

    /*********Startup************/
    Route::get('/register/startup', 'UserController@registerStartup')->name('user.new.startup');
    Route::get('add/listing', 'UserController@addListing')->name('user.add.listing');
    Route::post('add/startup', 'StartupController@createStartup')->name('user.add.startup');
    Route::get('startup/{slug}', 'StartupController@getUserStartup')->name('user.view.startup');
    Route::post('startup/edit/startup', 'StartupController@UpdateStartup')->name('user.update.startup');
    Route::get('startup/edit/startup/{slug}','StartupController@editStartup')->name('user.edit.startup');
    Route::post('startup/upload/logo','StartupController@uploadLogo')->name('user.startup.logo');
    Route::post('startup/upload/cover','StartupController@uploadCover')->name('user.startup.cover');
    Route::post('startup/upload/banner','StartupController@bannerUpload')->name('user.startup.banner');
    Route::post('startup/upload/pitch','StartupController@uploadPitchDeck')->name('user.startup.pitch');
    Route::post('startup/add/member', 'TeamController@addTeamMember')->name('user.startup.member.add');
    Route::post('startup/update/sectors', 'StartupController@editSectors')->name('user.startup.sectors');

    /********** Investor ************/
    Route::get('/investors', 'InvestorController@index')->name('user.investors');
    Route::get('/register/investor', 'InvestorController@create')->name('user.new.investor');
    Route::get('/register/individual/investor', 'InvestorController@createIndividual')->name('user.new.individual.investor');
    Route::post('/register/new/investor', 'InvestorController@store')->name('user.add.investor');
    Route::post('/register/new/individual/investor', 'InvestorController@storeIndividualInvestor')->name('user.add.ind.investor');
    Route::get('/investor/{slug}', 'InvestorController@show')->name('user.view.investor');
    Route::get('/investor/edit/{slug}', 'InvestorController@edit')->name('user.edit.investor');
    Route::post('/investor/update', 'InvestorController@update')->name('user.update.investor');
    Route::post('investor/upload/logo','InvestorController@uploadLogo')->name('user.investor.logo');
    Route::post('investor/upload/cover','InvestorController@uploadCover')->name('user.investor.cover');
    Route::post('investor/upload/banner','InvestorController@bannerUpload')->name('user.investor.banner');
    Route::post('investor/add/member', 'TeamController@addTeamMember')->name('user.investor.member.add');

    Route::get('{category}/add/files/{slug}', 'UserController@addFiles')->name('user.startup.images');

    //Jobs
    Route::get('/jobs','JobController@getJobs')->name('user.jobs');
    Route::post('/job/create','JobController@createJob')->name('user.job.create');
    Route::get('/job/update/{jobId}/{status}','JobController@jobStatus')->name('user.job.status.update');

    //Programs
    Route::get('/programs','ProgramController@getPrograms')->name('user.programs');
    Route::post('/programs/create','ProgramController@createProgram')->name('user.program.create');
    Route::get('/program/{id}/{slug}','ProgramController@viewProgram')->name('user.view.program');
    Route::post('/program/update','ProgramController@updateProgram')->name('user.program.update');
    Route::post('/program/cover','ProgramController@uploadCover')->name('user.program.cover');
    Route::post('program/upload/banner','ProgramController@bannerUpload')->name('user.program.banner');

    //Government
    Route::get('/governments','GovernmentController@index')->name('user.governments');
    Route::get('/government/new','GovernmentController@create')->name('user.government.register');
    Route::post('/governments/create','GovernmentController@store')->name('user.government.create');
    Route::get('/government/{id}/{slug}','GovernmentController@show')->name('user.view.government');
    Route::post('/government/cover','GovernmentController@uploadCover')->name('user.government.cover.upload');
    Route::post('government/upload/banner','GovernmentController@bannerUpload')->name('user.government.banner');
    Route::post('/government/logo','GovernmentController@uploadLogo')->name('user.government.logo.upload');

    //ESOS
    Route::get('/esos','EsoController@index')->name('user.esos');
    Route::post('/esos/create','EsoController@store')->name('user.esos.create');
    Route::get('/eso/{id}/{slug}','EsoController@show')->name('user.view.eso');
    Route::post('/eso/cover','EsoController@uploadCover')->name('user.eso.cover.upload');
    Route::post('eso/upload/banner','EsoController@bannerUpload')->name('user.eso.banner');
    Route::post('/eso/logo','EsoController@uploadLogo')->name('user.eso.logo.upload');
    Route::get('/eso/edit/{id}', 'EsoController@edit')->name('user.edit.eso');
    Route::post('/eso/update', 'EsoController@update')->name('user.update.eso');

    //CORPORATES
    Route::get('/corporates','CorporateController@index')->name('user.corporates');
    Route::post('/corporates/create','CorporateController@store')->name('user.corporates.create');
    Route::get('/corporate/{id}/{slug}','CorporateController@show')->name('user.view.corporate');
    Route::post('/corporate/cover','CorporateController@uploadCover')->name('user.corporate.cover.upload');
    Route::post('/corporate/logo','CorporateController@uploadLogo')->name('user.corporate.logo.upload');
    Route::get('/corporate/edit/{id}', 'CorporateController@edit')->name('user.edit.corporate');
    Route::post('corporate/upload/banner','CorporateController@bannerUpload')->name('user.corporate.banner');
    Route::post('/corporate/update', 'CorporateController@update')->name('user.update.corporate');
    Route::post('corporate/update/sectors', 'CorporateController@editSectors')->name('user.corporate.sectors');

    //MENTORS
    Route::get('/mentors','MentorController@index')->name('user.mentors');
    Route::post('/mentors/create','MentorController@store')->name('user.mentor.create');
    Route::post('/mentors/update','MentorController@update')->name('user.mentor.update');
    Route::get('/mentor/{id}/{slug}','MentorController@show')->name('user.view.mentor');
    Route::post('/eso/cover','EsoController@uploadCover')->name('user.eso.cover.upload');
    Route::post('/eso/logo','EsoController@uploadLogo')->name('user.eso.logo.upload');

      //Events
      Route::get('/events','EventController@getEvents')->name('user.events');
      Route::post('/events/create','EventController@createEvent')->name('user.event.create');
      Route::get('/event/{id}/{slug}','EventController@viewEvent')->name('user.view.event');
      Route::post('/event/update','EventController@updateEvent')->name('user.event.update');
      Route::post('/event/cover','EventController@uploadCover')->name('user.event.cover');
      Route::post('event/upload/banner','EventController@bannerUpload')->name('user.event.banner');
      Route::get('/event/update/{id}/{status}','EventController@updateEventStatus')->name('user.update.event.status');

    //FEEDS
    Route::get('feeds','FeedController@getUserFeeds')->name('user.feeds');
    Route::get('feed/update/{id}/{status}','FeedController@updateFeed')->name('user.feed.update');

    //Investment
    Route::post('startup/fund/initiate','InvestmentController@startupFundraising')->name('user.fund.raising');
    Route::get('startup/fund/close/{id}','InvestmentController@closeStartupFundraising')->name('fund.raising.close');
    Route::post('startup/fund/record','InvestmentController@recordStartupFunding')->name('user.fund.record');
    Route::post('startup/send/message','InvestmentController@recordStartupMessage')->name('user.contact.startup');
    Route::get('investment/delete/{id}','InvestmentController@deleteInvestment')->name('investment.delete');

    //Chats
    Route::get('/initiate/chat/{sender}/{receiver}/{type}','ChatController@initiateChat')->name('initiate.chat');
    Route::get('/chats','ChatController@getUserChats')->name('user.chat');
    Route::get('/chats/{category}/{id}','ChatController@getCategoryChats')->name('user.category.chat');

});

Route::group(['prefix'=>'admin','middleware'=>['auth','admin']], function(){
    Route::get('/dashboard', 'AdminController@dashboard')->name('admin.dashboard');

    //Profile Claims
    Route::post('email/claim',  'AdminController@sendClaimEmail')->name('email.claim');
    Route::get('add/listing/{category}', 'AdminController@addAdminEntity')->name('admin.add.listing');

    //Mentors
    Route::get('/mentors', 'AdminController@getMentors')->name('admin.mentors');
    Route::get('/mentor/{id}', 'AdminController@viewMentor')->name('admin.mentor.view');
    Route::post('/add/mentors','MentorController@store')->name('admin.mentor.create');
    Route::get('/mentor/{id}/{status}', 'AdminController@updateMentorStatus')->name('admin.mentor.update');

    //Startups
    Route::get('/startups', 'AdminController@getStartups')->name('admin.startups');
    Route::get('/startup/{id}', 'AdminController@viewStartup')->name('admin.startup.view');
    Route::get('/startup/{id}/{status}', 'AdminController@updateStartupStatus')->name('admin.startup.update');
    Route::post('add/startup', 'StartupController@createStartup')->name('admin.add.startup');
    Route::post('startup/upload/logo','StartupController@uploadLogo')->name('admin.startup.logo');
    Route::get('startup/feature/{startupId}/{feature}','AdminController@featureStartUo')->name('admin.startup.feature');
    Route::post('startup/upload/banner','StartupController@bannerUpload')->name('admin.startup.banner');

    //Investor
    Route::get('/investors', 'AdminController@getInvestors')->name('admin.investors');
    Route::get('/investor/{id}', 'AdminController@viewInvestor')->name('admin.investor.view');
    Route::get('/investor/{id}/{status}', 'AdminController@updateInvestorStatus')->name('admin.investor.update');
    Route::post('/add/investor', 'InvestorController@store')->name('admin.add.investor');
    Route::post('investor/upload/logo','InvestorController@uploadLogo')->name('admin.investor.logo');

    //CORPORATIONS
    Route::get('/corporates', 'AdminController@getCorporations')->name('admin.corporates');
    Route::get('/corporate/{id}', 'AdminController@viewCorporation')->name('admin.corporate.view');
    Route::get('/corporate/{id}/{status}', 'AdminController@updateCorporationStatus')->name('admin.corporate.update');
    Route::post('/add/corporates','CorporateController@store')->name('admin.corporates.create');
    Route::post('/corporate/logo','CorporateController@uploadLogo')->name('admin.corporate.logo.upload');

    //ESOs
    Route::get('/esos', 'AdminController@getEsos')->name('admin.esos');
    Route::get('/eso/{id}', 'AdminController@viewEso')->name('admin.eso.view');
    Route::get('/eso/{id}/{status}', 'AdminController@updateEsoStatus')->name('admin.eso.update');
    Route::post('/add/eso','EsoController@store')->name('admin.esos.create');
    Route::post('/eso/logo','EsoController@uploadLogo')->name('admin.eso.logo.upload');
    Route::post('eso/upload/banner','EsoController@bannerUpload')->name('admin.eso.banner');

    //Government Agencies
    Route::get('/governments', 'AdminController@getGovernments')->name('admin.governments');
    Route::get('/government/{id}', 'AdminController@viewGovernment')->name('admin.government.view');
    Route::get('/government/{id}/{status}', 'AdminController@updateGovernmentStatus')->name('admin.government.update');
    Route::post('/add/governments','GovernmentController@store')->name('admin.government.create');
    Route::post('/government/logo','GovernmentController@uploadLogo')->name('admin.government.logo.upload');


    //Programs
    Route::get('/programs', 'AdminController@getPrograms')->name('admin.programs');
    Route::get('/program/{id}', 'AdminController@viewProgram')->name('admin.program.view');
    Route::get('/program/{id}/{status}', 'AdminController@updateProgramStatus')->name('admin.program.update');
    Route::post('/programs/create','ProgramController@createProgram')->name('admin.program.create');
    Route::post('program/upload/banner','ProgramController@bannerUpload')->name('admin.program.banner');

    //Registrations
    Route::get('/programs/registrations', 'ProgramRegistrationController@getRegistrations')->name('admin.programs.registrations');
    Route::get('/programs/{program}/registrations', 'ProgramRegistrationController@getProgramRegistrations')->name('admin.program.registrations');
    Route::get('/programs/registration/{id}', 'ProgramRegistrationController@viewRegistration')->name('admin.view.registration');
    Route::get('/programs/registrations/export', 'ProgramRegistrationController@exportRegistry')->name('admin.registrations.export');
    Route::get('/programs/export/registrations/{program}', 'ProgramRegistrationController@exportProgramRegistry')->name('admin.program.registrations.export');


    //Events
    Route::get('/events', 'AdminController@getEvents')->name('admin.events');
    Route::get('/event/{id}', 'AdminController@viewEvent')->name('admin.event.view');
    Route::get('/event/{id}/{status}', 'EventController@updateEventStatus')->name('admin.event.update');
    Route::post('/events/create','EventController@createEvent')->name('admin.event.create');
    Route::post('event/upload/banner','EventController@bannerUpload')->name('admin.event.banner');

    //Jobs
    Route::get('/jobs', 'AdminController@getJobs')->name('admin.jobs');
    Route::post('/jobs/create', 'JobController@createJob')->name('admin.job.create');
    Route::get('/job/update/{jobId}/{status}','JobController@jobStatus')->name('admin.job.status.update');

    //Deals
    Route::get('/deals', 'DealController@index')->name('admin.deals');
    Route::get('/deals/{deal}', 'DealController@show')->name('admin.deal.view');
    Route::post('/deals/create', 'DealController@store')->name('admin.deal.create');
    Route::get('/deals/{id}/{state}', 'DealController@changeState')->name('admin.deal.action');

    //Articles
    Route::get('/articles', 'PostController@getArticles')->name('admin.articles');
    Route::post('/articles/create', 'PostController@store')->name('admin.articles.create');
    Route::get('/articles/{id}/{status}', 'PostController@publishArticle')->name('admin.articles.publish');
    Route::get('/article/{id}/preview', 'PostController@show')->name('admin.articles.preview');

    //Blogs
    Route::get('/blogs', 'BlogController@getAllBlogs')->name('admin.blogs');
    Route::get('/blog/create', 'BlogController@create')->name('admin.blog.create');
    Route::post('/blog/create', 'BlogController@createBlog')->name('admin.blog.post');
    Route::get('/blog/{slug}/preview', 'BlogController@previewBlog')->name('admin.blog.preview');
    Route::get('/blog/{id}/{action}', 'BlogController@actionBlog')->name('admin.blog.action');
    Route::get('/blog/update/{id}/{slug}', 'BlogController@editBlog')->name('admin.blog.edit');
    Route::post('/blog/update', 'BlogController@updateBlog')->name('admin.blog.update');

    //Users
    Route::get('/users', 'AdminController@getUsers')->name('admin.users');
    Route::get('/user/{id}', 'AdminController@userDetails')->name('admin.user.details');

});

